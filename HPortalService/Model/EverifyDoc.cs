﻿using HPortalService.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HPortalService.Model
{
    public class EverifyDoc
    {      
            public int BranchId { get; set; }
            public int EverifyDocumentId { get; set; }
            public string Name { get; set; }
            public string Type { get; set; }
            public int Status { get; set; }
            public int ApplicantId { get; set; }
            public string DocumentPath { get; set; }
            public string Description { get; set; }
            public string CardNo { get; set; }
            public string AliensNo { get; set; }
            public string DocumentNo { get; set; }
            public string IssuingAuthority { get; set; }
            public string ExpirationDate { get; set; }
            public string CitizenId { get; set; }
            public bool CitizenFlag { get; set; }
            public int ApplicantEverifyDocId { get; set; }
            public string DocType { get; set; }
            public int UserId { get; set; }
            public int IdDocType { get; set; }
            public int isExpirationDate { get; set; }
        //public string[] AppEverifyDocIdArr { get; set; }
        public string DocumentPathFront { get; set; }
        public string DocumentPathBack { get; set; }
        public string ImageFront { get; set; }
        public string ImageBack { get; set; }
        public int? ApplicantEverifyDocIdFront { get; set; }
        public int? ApplicantEverifyDocIdBack { get; set; }
            public List<EverifyDoc> GeteVerifyLookUp()
            {
                ApplicantDAL _ApplicationDAL = new ApplicantDAL();
                List<EverifyDoc> lsteVerifyLookUp = SQLHelper.ContructList<EverifyDoc>(_ApplicationDAL.GeteVerifyLookUp());
                return lsteVerifyLookUp;
            }
            public int InsEVerifyDoc(EverifyDoc obj)
            {
                ApplicantDAL _ApplicationDAL = new ApplicantDAL();
                return _ApplicationDAL.InsEVerifyDoc(obj);
            }
            public int fnUpEVerifyDoc(EverifyDoc obj)
            {
                ApplicantDAL _ApplicationDAL = new ApplicantDAL();
                return _ApplicationDAL.UpEVerifyDoc(obj);
            }


            public List<EverifyDoc> SelectDocuemntI9(EverifyDoc obj)
            {
                ApplicantDAL _ApplicationDAL = new ApplicantDAL();
                List<EverifyDoc> lsteVerifyLookUp = SQLHelper.ContructList<EverifyDoc>(_ApplicationDAL.SelectDocuemntI9(obj));
                return lsteVerifyLookUp;
            }
        
    }
}
