﻿#region document
/*********************************************************************************************************************************
 * Copyright (c) 2009 ProtaTECHIndia
 * ---------------------------------
 * File Name                            : Security.cs
 * Description                          : Contains functions to encrypt and decrypt string value
 * Namespace                            :  Logic.Security
 * Dependency                           : 
 * ----------------------------------
 * Development History
 * --------------------------------------------------------------------------------------------------------------------------------
 * Developer                    |   Action          |      Date     |   Description
 * --------------------------------------------------------------------------------------------------------------------------------
 * Gaurav Tandon                |   Created         |   12-Jan-10   |   File originally created
   
 **********************************************************************************************************************************/
#endregion document

#region Namespace
using System;
using System.Data;
 
 
 
 
#endregion


/// <summary>
/// public sealed class Security
/// </summary>
public sealed class Security
{
    /// <summary>
    /// Encrypt string value passed as parameter
    /// </summary>
    /// <param name="value">string value</param>
    /// <returns>string value</returns>
    /// <history> Created by Gaurav Tandon
    /// </history>
    public static string Encrypt(string value)
    {
        byte[] encData_byte = new byte[value.Length];
        encData_byte = System.Text.Encoding.UTF8.GetBytes(value);
        string encodedData = Convert.ToBase64String(encData_byte);
        return encodedData;
    }

    /// <summary>
    /// Decrypt string value passed as parameter
    /// </summary>
    /// <param name="value">string value</param>
    /// <returns>string value</returns>
    /// <history> Created by Gaurav Tandon
    /// </history>
    public static string Decrypt(string value)
    {
        System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();
        System.Text.Decoder utf8Decode = encoder.GetDecoder();

        byte[] todecode_byte = Convert.FromBase64String(value);
        int charCount = utf8Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
        char[] decoded_char = new char[charCount];
        utf8Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
        string result = new String(decoded_char);
        return result;
    }
}
