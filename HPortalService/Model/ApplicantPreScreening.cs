﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using HPortalService.DAL;

namespace HPortalService
{
    public class ApplicantPreScreening
    {
        public int ApplicantPreScreeningId { get; set; }
        public int ApplicantId { get; set; }
        public string SSN { get; set; }
        public string NAME { get; set; }

        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string County { get; set; }
        public string Telephone { get; set; }
        public string StateWorkforceAgency { get; set; }
        public string EmailId { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string StateName { get; set; }
        public string ZipCode { get; set; }
        public string DateOfBirth { get; set; }
        public string EmployerName { get; set; }
        public string EmployerPhoneNo { get; set; }
        public string EmployerAddress { get; set; }
        public string EmployerCity { get; set; }
        public string EmployerState { get; set; }
        public string EmployerZipCode { get; set; }
        public string EIN { get; set; }
        public string SignatureImage { get; set; }


        

        public int InsApplicantPreScreening(ApplicantPreScreening obj)
        {
            ApplicantPreScreeningDAL _ApplicationDAL = new ApplicantPreScreeningDAL();
            return _ApplicationDAL.InsApplicantPreScreening(obj);
        }
        public DataSet GetApplicantPreScreening(int ID)
        {
            ApplicantPreScreeningDAL _ApplicationDAL = new ApplicantPreScreeningDAL();
            return _ApplicationDAL.GetApplicantPreScreening(ID);
        }
    }
}
