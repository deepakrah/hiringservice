﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using HPortalService.DAL;


namespace HPortalService
{
    public class LookupQuestionCategory
    {
        public int QuestionCategoryId { get; set; }
        public string Name { get; set; }
        public Int16 DisplayOrder { get; set; }
        public int Parent { get; set; }
        public string CatId { get; set; }

        public List<LookupQuestionCategory> GetLookupQuestionCategory()
        {
            List<LookupQuestionCategory> objList = new List<LookupQuestionCategory>();
            DataSet ds;
            QuestionDAL _AppInfo = new QuestionDAL();
            ds = _AppInfo.GetLookupQuestionCategory();
            objList = SQLHelper.ContructList<LookupQuestionCategory>(ds);

            return objList;
        }
    }
}
