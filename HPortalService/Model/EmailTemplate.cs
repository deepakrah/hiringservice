﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using System.Xml.Xsl;
using System.Xml;
using System.IO;
using HPortalService.DAL;

namespace HPortalService
{
   public class EmailTemplate
    {
        public int EmailTemplateID { get; set; }
        public string EmailType { get; set; }
        public int BranchId { get; set; }
        public string Template { get; set; }

        public EmailTemplate(String EmailType,int BID)
        {
            this.BranchId = BID;
                this.EmailType = EmailType;
            EmailTemplateDAL objED = new EmailTemplateDAL();
            DataSet ds = objED.Get(this);
            if (ds.Tables[0].Rows.Count > 0)
            {           
                this.EmailTemplateID = Convert.ToInt32(ds.Tables[0].Rows[0]["EmailTemplateID"]);
                this.Template = ds.Tables[0].Rows[0]["Template"].ToString();
            }
        }
        public EmailTemplate()
        { }
       
        public int Save(EmailTemplate obj)
        {
            this.EmailType = EmailType;
            EmailTemplateDAL objED = new EmailTemplateDAL();
            return objED.Save(obj);
        }

       

    }
}
