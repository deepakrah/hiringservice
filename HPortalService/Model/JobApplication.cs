﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using HPortalService.DAL;
using System.Data;


namespace HPortalService
{
    public class JobApplication
    {
        public int ApplicantId { get; set; }
        public int JobApplicationId { get; set; }
        public int StatusID { get; set; }
        public int ModifiedBy { get; set; }
        public Nullable<DateTime> Date { get; set; }
        public String Time { get; set; }
        public String AppliedOn { get; set; }
        public String InterviewDate { get; set; }
        public String OrientationDate { get; set; }

        public int interviewVenueId { get; set; }
        public int InterviewSession { get; set; }
        public int IsOrientation { get; set; }

        public int JobApplictionlog_InsUpd(JobApplication obj)
        {
            JobApplicationDAL _JobApplicationDAL = new JobApplicationDAL();
            return _JobApplicationDAL.jobapplictionlog_ins_upd(obj);
        }
        public int BackToQueue(JobApplication obj)
        {
            JobApplicationDAL _JobApplicationDAL = new JobApplicationDAL();
            return _JobApplicationDAL.BackToQueue(obj);
        }
        /// <summary>
        /// To get all pending application requests
        /// </summary>

        public bool VerifyShiftNo(string JobNo, string ShiftNo, int BranchId)
        {
            JobApplicationDAL _JobApplicationDAL = new JobApplicationDAL();
            return _JobApplicationDAL.VerifyShiftNo(JobNo, ShiftNo, BranchId);

        }


        public DataSet GetApplicantForBatchInterview(int BranchId, Nullable<int> VenueId, string date = null, Nullable<int> JobApplicationNo = null)
        {
            DataSet ds;
            JobApplicationDAL _AppInfo = new JobApplicationDAL();
            ds = _AppInfo.GetApplicantForBatchInterview(BranchId, VenueId, date, JobApplicationNo);

            return ds;
        }


        public List<InterviewVenues> GetAllVenues(int applicantId)
        {
            JobApplicationDAL _JobApplicationDAL = new JobApplicationDAL();
            DataSet ds = _JobApplicationDAL.GetAllVenues(applicantId);
            List<InterviewVenues> lstInterviewVenues = SQLHelper.ContructList<InterviewVenues>(ds);
            return lstInterviewVenues;
        }

        public bool VerifyJobNo(string JobNo, int BranchId)
        {
            JobApplicationDAL _JobApplicationDAL = new JobApplicationDAL();
            return _JobApplicationDAL.VerifyJobNo(JobNo, BranchId);
        }



        public bool ScheduleForInterview(int interviewVenueId, bool schedByAdmin, bool schedByUser, int schedById, int InterviewSession, int isOrientation)
        {
            JobApplicationDAL _JobApplicationDAL = new JobApplicationDAL();
            return _JobApplicationDAL.ScheduleForInterview(this.JobApplicationId, interviewVenueId, schedByAdmin, schedByUser, schedById, InterviewSession, isOrientation);
        }

        public int Interview_MarkHired(int JobApplicationId, string JobNo, string ShiftNumber)
        {
            JobApplicationDAL _JobApplicationDAL = new JobApplicationDAL();
            return _JobApplicationDAL.Interview_MarkHired(JobApplicationId, JobNo, ShiftNumber);
        }


        public int Interview_MarkAttend(int JobApplicationID, int isOrientation, int ScheduledBy)
        {
            JobApplicationDAL _JobApplicationDAL = new JobApplicationDAL();
            return _JobApplicationDAL.Interview_MarkAttend(JobApplicationID, isOrientation, ScheduledBy);
        }

        public DataSet GetJobApplicantByID(int ApplicantID)
        {
            JobApplicationDAL _JobApplicationDAL = new JobApplicationDAL();
            return _JobApplicationDAL.GetJobApplicantByID(ApplicantID);
        }
        public DataSet GetCSVData(int branchId, bool ShowOnlyDups, string from, string to)
        {
            JobApplicationDAL _JobApplicationDAL = new JobApplicationDAL();
            return _JobApplicationDAL.GetCSVData(branchId, ShowOnlyDups, from, to);
        }

        public DataSet GetHiredApplicant(string from, string to, int branchId, bool SearchByHomeBranch = false)
        {
            JobApplicationDAL _JobApplicationDAL = new JobApplicationDAL();
            return _JobApplicationDAL.GetHiredApplicant(branchId, from, to, SearchByHomeBranch);
        }

        public DataSet GetHiredApplicantSummary(string from, string to, string source, int branchId, bool SearchByHomeBranch = false)
        {
            JobApplicationDAL _JobApplicationDAL = new JobApplicationDAL();
            return _JobApplicationDAL.GetHiredApplicantSummary(branchId, from, to, source, SearchByHomeBranch);
        }

        public DataSet GetRegistrationReportSummary(string from, string to, string source, int branchId, bool SearchByHomeBranch = false)
        {
            JobApplicationDAL _JobApplicationDAL = new JobApplicationDAL();
            return _JobApplicationDAL.GetRegistrationReportSummary(branchId, from, to, source, SearchByHomeBranch);
        }


        public void UpdateExportStatus(DataTable dt)
        {
            JobApplicationDAL _JobApplicationDAL = new JobApplicationDAL();
            _JobApplicationDAL.InsertBulk(dt);

        }

        public void RejectApplicant(int JobApplicationID, string reason, string Remrks)
        {
            JobApplicationDAL _JobApplicationDAL = new JobApplicationDAL();
            _JobApplicationDAL.RejectApplicant(JobApplicationID, reason, Remrks);
        }

    }
}