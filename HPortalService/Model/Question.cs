﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using HPortalService.DAL;


namespace HPortalService
{
   public  class Question
    {
       public int AppMasterId { get; set; }
        public int QuesId { get; set; }
        public int IsSubmit { get; set; }
        public int QuesSetId { get; set; }
        public string Description { get; set; }
        public int ApplicantId { get; set; }
        public int QuestionCategoryId { get; set; }
        public int AnswerId { get; set; }
        public string AnswerName { get; set; }
        public bool Answered { get; set; }
        public bool Correct { get; set; }
        public string KnockOutStatement { get; set; }
        public bool IsKnockOutQuestion { get; set; }
        public bool isDependent { get; set; }
        public bool isReAttempt { get; set; }
        public IList<Answer> Answers { get; set; }
        public DataSet GetAnswerSetByQuesId(Question obj)
        {
            QuestionDAL _QuesDAL = new QuestionDAL();
            DataSet ds = _QuesDAL.GetAnswerSetByQuesId(obj);
            return ds;
        }

        public List<Question> GetQuestionByCategory(int QuestionCategoryId, int ApplicantId)
        {
            List<Question> objList = new List<Question>();
            DataSet ds;
            QuestionDAL _AppInfo = new QuestionDAL();
            ds = _AppInfo.GetQuestionByCategory(QuestionCategoryId, ApplicantId);
            objList = SQLHelper.ContructList<Question>(ds);
            foreach (Question objQ in objList)
            {
                List<Answer> objAns = new List<Answer>();
                objQ.QuesId = objQ.QuesId;
                objQ.ApplicantId = ApplicantId;                
                objAns = objQ.GetAnswerSetByApplicantId(objQ);
             Answer objAnswer = objAns.FirstOrDefault(u => u.Answered != 0);
             if (objAnswer != null)
             {
                 objQ.AnswerId = objAnswer.Answered;
             }
                objQ.Answers = objAns;
            }

            return objList;
        
        }

        public List<Answer> GetAnswerSetByApplicantId(Question obj)
        {
            List<Answer> objList = new List<Answer>();
            QuestionDAL _QuesDAL = new QuestionDAL();
            DataSet ds = _QuesDAL.GetAnswerSetByApplicantId(obj);
            objList = SQLHelper.ContructList<Answer>(ds);
            return objList;
        }

        public List<Answer> GetFollowupQuestions(int AnswerId)
        {
            List<Answer> objList = new List<Answer>();
            QuestionDAL _QuesDAL = new QuestionDAL();
            DataSet ds = _QuesDAL.GetFollowupQuestions(AnswerId);
            objList = SQLHelper.ContructList<Answer>(ds);
            return objList;
        }
       

        public int SaveAnswer(Question obj)
        {
            QuestionDAL _QuesDAL = new QuestionDAL();
            return _QuesDAL.InsAnswerByApplicant(obj);
        }

        public Decimal GetCorrectAnswerCount(int QuestionCategoryId, int ApplicantID)
        {
            Decimal CorrectCount = 0;
            QuestionDAL _QuesDAL = new QuestionDAL();
            CorrectCount = _QuesDAL.GetCorrectAnswerCount(QuestionCategoryId, ApplicantID);
            return CorrectCount;
        }

        public Decimal GetMaxPoint()
        {
            Decimal CorrectCount = 0;
            QuestionDAL _QuesDAL = new QuestionDAL();
            CorrectCount = _QuesDAL.GetMaxPoint();
            return CorrectCount;
        }

        public List<FAQuestion> fnGetFAQuestion(int BranchId, int TaskId, int ApplicantID)
        {
            List<FAQuestion> objList = new List<FAQuestion>();
            QuestionDAL _QuesDAL = new QuestionDAL();
            DataSet ds = _QuesDAL.GetFAQuestion(BranchId, TaskId, ApplicantID);
            objList = SQLHelper.ContructList<FAQuestion>(ds);
            return objList;
        }
        public List<FAQuestion> fnGetApplicantFAQ(int TaskId, int ApplicantID, int QuestionNo)
        {
            List<FAQuestion> objList = new List<FAQuestion>();
            QuestionDAL _QuesDAL = new QuestionDAL();
            DataSet ds = _QuesDAL.GetApplicantFAQ(TaskId, ApplicantID, QuestionNo);
            objList = SQLHelper.ContructList<FAQuestion>(ds);
            return objList;
        }
        public List<FAQuestion> fnGetApplicantAllFAQ(int TaskId, int ApplicantID)
        {
            List<FAQuestion> objList = new List<FAQuestion>();
            QuestionDAL _QuesDAL = new QuestionDAL();
            DataSet ds = _QuesDAL.GetApplicantAllFAQ(TaskId, ApplicantID);
            objList = SQLHelper.ContructList<FAQuestion>(ds);
            return objList;
        }
        public DataSet fnGetAllApplicantFAQ(int BranchId, int TaskId, int ApplicantID, int Phase)
        {           
            QuestionDAL _QuesDAL = new QuestionDAL();
            DataSet ds = _QuesDAL.GetAllApplicantFAQ(BranchId, TaskId, ApplicantID, Phase);           
            return ds;
        }
        public int InsApplicantFAQ(FAQuestion obj)
        {
            QuestionDAL _QuesDAL = new QuestionDAL();
            return  _QuesDAL.fnInsApplicantFAQ(obj);
        }
        public int fnUpApplicantFAQ(FAQuestion obj)
        {
            QuestionDAL _QuesDAL = new QuestionDAL();
            return _QuesDAL.fnUpApplicantFAQ(obj);
        }

    }
}
