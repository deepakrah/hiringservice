﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using HPortalService.DAL;

namespace HPortalService
{
   public class IndianaWH4
    {
      
        public int ApplicantIndianaWH4Id { get; set; }
        public int ApplicantId { get; set; }
        public string CountyofResidence { get; set; }
        public string PrincipalEmployment { get; set; }
        public string Exemption { get; set; }
        public string SpouseExemption { get; set; }
        public string DependentExemption { get; set; }
        public string AdditionalExemptions { get; set; }
        public int Numberofboxes { get; set; }
        public string total { get; set; }
        public string QualifyingDependent { get; set; }
        public string AdditionalState { get; set; }
        public string AdditionalCounty { get; set; }
        public string SSN { get; set; }
        public string HomeAddress { get; set; }
          public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }

        public int intIndianaWH4(IndianaWH4 obj)
        {
            IndianaWH4DAL _ApplicationDAL = new IndianaWH4DAL();
            return _ApplicationDAL.IntIndianaWH4(obj);
        }
        public DataSet GetIndianaWH4ByID(int ID)
        {
            IndianaWH4DAL _ApplicationDAL = new IndianaWH4DAL();
            return _ApplicationDAL.GetIndianaWH4ByID(ID);
        }
    }
}
