﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace HPortalService.Model
{
    public class ChangeStatus
    {
        string BGCheckUserIdValue = Startup.BGCheckUserIdValue;
        string BGCheckPwdValue = Startup.BGCheckPwdValue;
        string BGPackageValue = Startup.BGPackageValue;
        string BGCrimePackageValue = Startup.BGCrimePackageValue;

        string HPortalResponseValue = Startup.HPortalResponseValue;
        string TempFolderPath = Startup.TempFolderPath;

        public void ChangeToInterview()
        {

            ShowScheduleForInterview();
            //mpeChangeStatus.Show();
        }

        public void changeToOrientation()
        {
            ShowScheduleForOrientation();
            //mpeChangeStatus.Show();
        }

        public void ChangeToRescind(Applicant obj)
        {
            //mvRescind.ActiveViewIndex = 0;
            //mpeRescind.Show();
        }

        void ShowScheduleForInterview()
        {
            //hfScheduleType.Value = "0";
            BindInterviewDate();

            //grdVenues.DataSource = null;
            //grdVenues.DataBind();
        }
        void BindInterviewDate()
        {
            //int branchId = Convert.ToInt16(hid.Value);
            //// Get hiring Locations values if it is selected. 
            //// Added By Sowmya
            //if (ddlChieldBranch.SelectedValue != "0")
            //{
            //    if (branchId != Convert.ToInt32(ddlChieldBranch.SelectedValue))
            //    {
            //        branchId = Convert.ToInt32(ddlChieldBranch.SelectedValue);
            //    }
            //}
            //int type = Convert.ToInt16(hfScheduleType.Value);
            //InterviewVenues obj = new InterviewVenues();

            //Branch objBranch = new Branch(LoggedInUser.BranchId);
            //int YearFilter = 0;
            //if (ddlYears.SelectedValue != "All")
            //{
            //    YearFilter = Convert.ToInt32(ddlYears.SelectedValue);
            //}
            //DataSet objDs = obj.GetInterviewVenueDate(branchId, type, Convert.ToInt32(hfUserType.Value), null, null, objBranch.VenueSetting.AllowSchedOnUnPublished, YearFilter);
            //ddlVenueDate.DataSource = objDs;
            //ddlVenueDate.DataValueField = "Date";
            //ddlVenueDate.DataTextField = "Date";
            //ddlVenueDate.DataBind();
            //ddlVenueDate.Items.Insert(0, "Select Date");
        }

        public void ShowScheduleForOrientation()
        {
            //hfScheduleType.Value = "1";
            //BindInterviewDate();

            //grdVenues.DataSource = null;
            //grdVenues.DataBind();
        }
        public void ChangeStatusToOffer()
        {
            //bool valid = true;

            //Dictionary<int, string> DictgrdEmp = (Dictionary<int, string>)ViewState["Selected"];

            //DataTable dt = new DataTable();
            //dt.Columns.Add("ApplicantId");
            //dt.Columns.Add("JobApplicationId");
            //dt.Columns.Add("FirstName");
            //dt.Columns.Add("LastName");
            //dt.Columns.Add("DOB");
            //dt.Columns.Add("SSN");
            //dt.Columns.Add("TaxStatus");
            //dt.Columns.Add("Allowance");
            //dt.Columns.Add("Additional");
            //dt.Columns.Add("MiddleName");
            //foreach (var Dict in DictgrdEmp)
            //{
            //    string[] strArry = Dict.Value.Split('|');
            //    int ApplicantID = Convert.ToInt32(strArry[1]);
            //    JobApplication _JobApplication = new JobApplication();
            //    //_JobApplication.JobApplicationId = Convert.ToInt32(hdnJobApplicationId.Value);
            //    _JobApplication.JobApplicationId = Convert.ToInt32(Dict.Key);
            //    _JobApplication.StatusID = Convert.ToInt32(ddlChangeStatus.SelectedValue);

            //    Applicant objApplicant = new Applicant();
            //    DataSet ds = objApplicant.GetApplicantByID(ApplicantID);
            //    // dt.Rows.Add(new object[] { ds.Tables[0].Rows[0]["ApplicantId"], ds.Tables[0].Rows[0]["JobApplicationId"], ds.Tables[0].Rows[0]["FirstName"], ds.Tables[0].Rows[0]["LastName"], ds.Tables[0].Rows[0]["DOB"].ToString() != "" ? Convert.ToDateTime(ds.Tables[0].Rows[0]["DOB"]).ToString("MM/dd/yyyy") : "", ds.Tables[0].Rows[0]["SSN"], "", ds.Tables[0].Rows[0]["Allowance"], ds.Tables[0].Rows[0]["AdditionalAmount"], ds.Tables[0].Rows[0]["MiddleName"] });
            //    dt.Rows.Add(new object[] { ds.Tables[0].Rows[0]["ApplicantId"], ds.Tables[0].Rows[0]["JobApplicationId"], ds.Tables[0].Rows[0]["FirstName"], ds.Tables[0].Rows[0]["LastName"], ds.Tables[0].Rows[0]["WDOB"].ToString() != "" ? Convert.ToDateTime(ds.Tables[0].Rows[0]["WDOB"]).ToString("MM/dd/yyyy") : "", ds.Tables[0].Rows[0]["WSSN"], ds.Tables[0].Rows[0]["FedTaxStatuses"], ds.Tables[0].Rows[0]["NoOfAllowances"], ds.Tables[0].Rows[0]["WAdditionalAmount"], ds.Tables[0].Rows[0]["MiddleName"] });
            //}
            //gvOfferApplicant.DataSource = dt;
            //gvOfferApplicant.DataBind();
            //mpeChangeToOffered.Show();


            //if (Convert.ToBoolean(Session["UserName"].ToString().Split(',')[16]))
            //{
            //    DivJdpSubmit.Visible = true;

            //}
        }


        public void ChangeStatusReject()
        {
            //bool valid = true;

            //Dictionary<int, string> DictgrdEmp = (Dictionary<int, string>)ViewState["Selected"];

            //DataTable dt = new DataTable();

            //dt.Columns.Add("ApplicantId");
            //dt.Columns.Add("JobApplicationId");
            //dt.Columns.Add("FirstName");
            //dt.Columns.Add("LastName");
            //dt.Columns.Add("Reason");
            //dt.Columns.Add("Remarks");

            //foreach (var Dict in DictgrdEmp)
            //{
            //    string[] strArry = Dict.Value.Split('|');
            //    int ApplicantID = Convert.ToInt32(strArry[1]);
            //    JobApplication _JobApplication = new JobApplication();
            //    //_JobApplication.JobApplicationId = Convert.ToInt32(hdnJobApplicationId.Value);
            //    _JobApplication.JobApplicationId = Convert.ToInt32(Dict.Key);
            //    _JobApplication.StatusID = Convert.ToInt32(ddlChangeStatus.SelectedValue);

            //    Applicant objApplicant = new Applicant();
            //    DataSet ds = objApplicant.GetApplicantByID(ApplicantID);
            //    dt.Rows.Add(new object[] { ds.Tables[0].Rows[0]["ApplicantId"], ds.Tables[0].Rows[0]["JobApplicationId"], ds.Tables[0].Rows[0]["FirstName"], ds.Tables[0].Rows[0]["LastName"], 0, "" });

            //}
            //mvRejection.ActiveViewIndex = 0;
            //rptRejectApplicant.DataSource = dt;
            //rptRejectApplicant.DataBind();
            //mpeRejectApplicant.Show();


        }

        //public bool fnChangeStatus(int ApplicantID, int JobApplicationId, int _changeStatus, int prevStatus, int UserId, int ScheduleType = 0, int interviewVenueId = 0, int InterviewSessionId = 0, int BranchId = 0)
        //{
        //    ShowMessage ShowMessage = new ShowMessage();
        //    if (UpdateApplicationStatus(ApplicantID, JobApplicationId, _changeStatus, prevStatus, UserId, ScheduleType, interviewVenueId, InterviewSessionId, BranchId))
        //    {

        //        return true;

        //        //ShowMessage.Show("Applicant status has been changed successfully", ShowMessage.messageType.Success);
        //    }
        //    //ShowMessage("Applicant status has been changed successfully");
        //    //lblMsg.Text = "Applicant status has been changed successfully";
        //    //ViewState["Selected"] = null;
        //    //hdnSelectedApplications.Value = "";
        //    //FillGrid(Convert.ToInt32(ddlStatus.SelectedValue), Convert.ToInt32(hid.Value), Convert.ToInt32(hfPageIndex.Value), Convert.ToInt32(DDGridRowCount.SelectedValue));

        //    return false;
        //    //ShowMessage.Show("Invalid Status", ShowMessage.messageType.Error);
        //}

        public DataTable ChangeStatusToHire(List<Applicant> DictgrdEmp)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("ApplicantId");
            dt.Columns.Add("JobApplicationId");
            dt.Columns.Add("FirstName");
            dt.Columns.Add("LastName");
            dt.Columns.Add("JobNo");
            dt.Columns.Add("ShiftNumber");
            foreach (var item in DictgrdEmp)
            {
                int ApplicantID = item.ApplicantId;
                JobApplication _JobApplication = new JobApplication();
                _JobApplication.JobApplicationId = item.JobApplicationId;
                _JobApplication.StatusID = item.ddlChangeStatus;
                Applicant objApplicant = new Applicant();
                DataSet ds = objApplicant.GetApplicantByID(ApplicantID);
                dt.Rows.Add(new object[] { ds.Tables[0].Rows[0]["ApplicantId"], ds.Tables[0].Rows[0]["JobApplicationId"], ds.Tables[0].Rows[0]["FirstName"], ds.Tables[0].Rows[0]["LastName"], ds.Tables[0].Rows[0]["JobNo"], ds.Tables[0].Rows[0]["ShiftNumber"] });
            }
            return dt;
        }

        public string UpdateApplicationStatus(int ApplicantID, int JobApplicationId, int ChangeStatus, int prevStatus, int UserId, int ScheduleType = 0, int interviewVenueId = 0, int InterviewSession = 0, int BranchId = 0, int sendMail = 1, string subject = null, string emailBody = null, string SSN = null)
        {
            try
            {
                string Msg = "";
                SendEmails sendEmails = new SendEmails();
                JobApplication _JobApplication = new JobApplication();
                _JobApplication.JobApplicationId = JobApplicationId;
                _JobApplication.StatusID = ChangeStatus;
                int _userId = UserId;
                _JobApplication.ModifiedBy = _userId;
                if (_JobApplication.StatusID == 2 || _JobApplication.StatusID == 9)
                {

                    _JobApplication.ScheduleForInterview(interviewVenueId, true, false, _userId, InterviewSession, ScheduleType);
                }
                switch (prevStatus)
                {
                    case 15:
                    case 13:
                        if (_JobApplication.StatusID == 18 || _JobApplication.StatusID == 19)
                            _JobApplication.StatusID = 2;
                        break;
                    case 14:
                    case 16:
                        if (_JobApplication.StatusID == 18 || _JobApplication.StatusID == 19)
                            _JobApplication.StatusID = 9;
                        break;
                }
                if (ChangeStatus == 12 || ChangeStatus == 1 || ChangeStatus == 2 || ChangeStatus == 15 || ChangeStatus == 11)
                {
                    Applicant Obj = new Applicant();
                    Obj.UpdateApplicantBgStatus(ApplicantID, 0);
                }
                //un-comment this on production - Re Enabled By Paul (1-Jul-2016)
                _JobApplication.JobApplictionlog_InsUpd(_JobApplication);
                //Send bg CheckRequest on offer
                if (_JobApplication.StatusID == 3)
                {
                    Applicant Obj = new Applicant();
                    int temp = Obj.fnReHiredEmployeeBySSN(SSN.Replace("-",""), ApplicantID);
                    DataSet BGCheck = Obj.GetBGCheckByApplicantId(ApplicantID);
                    if (BGCheck.Tables.Count > 0)
                    {
                        int BgCheckid = Convert.ToInt32(BGCheck.Tables[0].Rows[0]["BgCheckid"]);
                        if (BgCheckid > 0)
                        {
                            //DateTime now = DateTime.Now;
                            //DateTime BGCheckDate = Convert.ToDateTime(BGCheck.Tables[0].Rows[0]["BGCheckDate"]);
                            //int day = (now - BGCheckDate).Days;
                            //if (day <= 60)
                            //{
                            //    Msg = "There is already BGCheck submitted with BgCheckId- " + BgCheckid.ToString() + " on date " + BGCheckDate.ToString() + " <br />Do you really want to submit again.";
                            //    //lblmdBGcheckMsg.Text = "There is already BGCheck submitted with BgCheckId- " + BgCheckid.ToString() + " on date " + BGCheckDate.ToString() + " <br />Do you really want to submit again.";
                            //    //mdBGcheck.Show();
                            //}
                            //else
                            //{
                            //    SendBGCheckRequest(ApplicantID, UserId, BranchId);
                            //}

                            SendBGCheckRequest(ApplicantID, UserId, BranchId);
                        }
                        else
                        {
                            SendBGCheckRequest(ApplicantID, UserId, BranchId);
                        }
                    }

                }
                if (_JobApplication.StatusID == 11)
                    this.fnInsApplicantTask(ApplicantID, _JobApplication.StatusID);
                if (sendMail == 1)
                {
                    if (_JobApplication.StatusID == 9)
                    {
                        sendEmails.setMailContent(Convert.ToInt32(ApplicantID), EStatus.Schedule_for_OrientationNew.ToString(), subject, emailBody);
                        this.fnInsApplicantTask(ApplicantID, 9);
                        sendEmails.setMailContent(Convert.ToInt32(ApplicantID), EStatus.Manager_Offered.ToString(), null, null);
                    }
                    else
                    {
                        sendEmails.setMailContent(Convert.ToInt32(ApplicantID), ((EStatus)_JobApplication.StatusID).ToString(), subject, emailBody);
                    }

                }
                //string msg = "Status successfully updated to '" + ListData().FirstOrDefault(u => u.StatusListID == _JobApplication.StatusID).Name + "'.";              
                return "true";

            }
            catch (Exception ex)
            {
                //SendMail("support@protatech.com", "PDevadoss@protatech.com", "Hportal Error", "ApplicantID : " + AppID + ", " + ex.Message, "draheja@protatechindia.com", "vprajapati@protatechindia.com");
                //ErrorLogger.Log(ex.Message);

                return ex.Message;
            }
        }

        private bool fnInsApplicantTask(int ApplicantId, int StatusId)
        {
            ApplicantTask obj = new ApplicantTask();
            obj.ApplicantId = ApplicantId;
            obj.StatusId = StatusId;
            int temp = obj.InsApplicantTask(obj);
            if (temp > 0)
            {
                return true;
            }
            return false;
        }

        string GetValue(DataTable tbl, string key)
        {
            tbl.DefaultView.RowFilter = "Key='" + key + "'";
            DataView vw = tbl.DefaultView;
            if (vw != null && vw.Count > 0)
            {
                return Convert.ToString(vw[0]["Value"]);
            }
            else
                return null;
        }

        public void SendBGCheckRequest(int ApplicantID, int UserId, int BranchId)
        {
            Applicant objApp = new Applicant();
            DataSet ds = objApp.GetApplicantByID(ApplicantID);

            User objUser = new User(UserId);
            DataTable tbluser = objUser.GetSettings(SettingSections.SendEmail);
            string recruiterEmail = "";
            if (tbluser != null && tbluser.Rows.Count > 0)
            {
                recruiterEmail = GetValue(tbluser, "CopyEmail");// Convert.ToString(tbluser.Rows[0]["CopyEmail"]);
            }
            else
            {
                Branch objBranch = new Branch(BranchId);
                recruiterEmail = objBranch.EmailId;
            }

            if (ds != null && ds.Tables.Count > 0)
            {
                StringBuilder sbEmployee = new StringBuilder();

                sbEmployee.Append("<?xml version=\"1.0\"?>");
                //string userId = System.Configuration.ConfigurationManager.AppSettings["BGCheckUserId"];
                //string password = System.Configuration.ConfigurationManager.AppSettings["BGCheckPwd"];
                //string BGPackage = System.Configuration.ConfigurationManager.AppSettings["BGPackage"];
                //string BGCrimePackage = System.Configuration.ConfigurationManager.AppSettings["BGCrimePackage"];

                string userId = BGCheckUserIdValue;
                string password = BGCheckPwdValue;
                string BGPackage = BGPackageValue;
                string BGCrimePackage = BGCrimePackageValue;
                sbEmployee.Append("<BackgroundCheck userId=\"" + userId + "\" password=\"" + password + "\">");

                DataTable dtEmployee = ds.Tables[0];

                if (dtEmployee.Rows.Count > 0)
                {
                    DataRow dr = dtEmployee.Rows[0];
                    String EncodedFile = "";
                    string FileName = "";
                    if (dr["IsFelony"].ToString() == "True" || dr["IsMisdemeanor"].ToString() == "True" || dr["IsParole"].ToString() == "True" || dr["IsProbation"].ToString() == "True" || dr["IsPendingTrial"].ToString() == "True")
                    {
                        FileName = dr["FirstName"].ToString() + dr["LastName"].ToString() + ".txt";
                        MemoryStream ms = new MemoryStream();
                        string NewFileName = ApplicantID + "_" + System.DateTime.Now.ToString("MMddyyyyHHmmss") + ".txt";
                        StreamWriter sw = File.CreateText(TempFolderPath + @"\" + NewFileName);
                        if (dr["IsFelony"].ToString() == "True")
                        {
                            sw.WriteLine("Crime Type: Convicted of a felony");
                            sw.WriteLine("Discription: " + dr["ConvictionNote"].ToString());
                            sw.WriteLine("Date: " + dr["ConvictionDate"].ToString());
                        }
                        if (dr["IsMisdemeanor"].ToString() == "True")
                        {
                            sw.WriteLine("Crime Type: Convicted of a misdemeanor");
                            sw.WriteLine("Discription: " + dr["ConvictionNote"].ToString());
                            sw.WriteLine("Date: " + dr["ConvictionDate"].ToString());
                        }
                        if (dr["IsParole"].ToString() == "True")
                        {
                            sw.WriteLine("Crime Type: Currently on parole");
                            sw.WriteLine("Discription: " + dr["ParoleProbationNote"].ToString());
                        }
                        if (dr["IsProbation"].ToString() == "True")
                        {
                            sw.WriteLine("Crime Type: Currently on probation");
                            sw.WriteLine("Discription: " + dr["ParoleProbationNote"].ToString());
                        }
                        if (dr["IsPendingTrial"].ToString() == "True")
                        {
                            sw.WriteLine("Crime Type: State nature of the offense and the currently pending trial date");
                            sw.WriteLine("Discription: " + dr["PendingTrialNote"].ToString());
                        }
                        sw.WriteLine("Crime State: " + dr["CrimeState"].ToString());
                        sw.WriteLine("Crime City: " + dr["CrimeCity"].ToString());
                        sw.WriteLine("Crime County: " + dr["CrimeCounty"].ToString());
                        sw.WriteLine("Crime Zip: " + dr["CrimeZip"].ToString());
                        sw.Flush();
                        sw.Close();
                        Byte[] bytes = File.ReadAllBytes(TempFolderPath + @"\" + NewFileName);
                        EncodedFile = Convert.ToBase64String(bytes);
                    }
                    sbEmployee.Append("<BackgroundSearchPackage  action=\"submit\" type=\"" + (EncodedFile == "" ? BGPackage : BGCrimePackage) + "\">");
                    sbEmployee.Append("<Organization  type=\"x:requesting\">");
                    sbEmployee.Append("<OrganizationName>");
                    sbEmployee.Append(Convert.ToString(ds.Tables[0].Rows[0]["JDPLocationCode"]));
                    sbEmployee.Append("</OrganizationName>");
                    sbEmployee.Append("</Organization>");
                    sbEmployee.Append("<ReferenceId>");
                    sbEmployee.Append("WS-" + ApplicantID);
                    sbEmployee.Append("</ReferenceId>");

                    sbEmployee.Append("<PersonalData><PersonName><GivenName>");
                    sbEmployee.Append(dr["FirstName"].ToString());
                    sbEmployee.Append("</GivenName>");
                    sbEmployee.Append("<FamilyName>");
                    sbEmployee.Append(dr["LastName"].ToString());
                    sbEmployee.Append("</FamilyName>");
                    sbEmployee.Append("<MiddleName>");
                    sbEmployee.Append(dr["MiddleName"].ToString());
                    sbEmployee.Append("</MiddleName>");
                    sbEmployee.Append("</PersonName>");
                    sbEmployee.Append("<DemographicDetail><GovernmentId countryCode=\"US\" issuingAuthority=\"SSN\">");
                    sbEmployee.Append(dr["SSN"].ToString());
                    sbEmployee.Append("</GovernmentId>");
                    sbEmployee.Append("<DateOfBirth>");
                    sbEmployee.Append(dr["DOB"].ToString());
                    sbEmployee.Append("</DateOfBirth></DemographicDetail>");
                    sbEmployee.Append("<PostalAddress>");
                    sbEmployee.Append("<PostalCode>");
                    sbEmployee.Append(dr["ZipCode"].ToString());
                    sbEmployee.Append("</PostalCode>");

                    sbEmployee.Append("<Region>");
                    sbEmployee.Append(dr["Abbreviation"].ToString());
                    sbEmployee.Append("</Region>");

                    sbEmployee.Append("<Municipality>");
                    sbEmployee.Append(dr["City"].ToString());
                    sbEmployee.Append("</Municipality>");

                    sbEmployee.Append("<DeliveryAddress><StreetName>");
                    sbEmployee.Append(dr["Address"].ToString());

                    sbEmployee.Append("</StreetName></DeliveryAddress>");

                    sbEmployee.Append("</PostalAddress>");

                    sbEmployee.Append("<EmailAddress>");
                    sbEmployee.Append(dr["EmailId"].ToString());
                    sbEmployee.Append("</EmailAddress>");
                    sbEmployee.Append("</PersonalData>");



                    sbEmployee.Append("<Screenings useConfigurationDefaults=\"yes\">");
                    sbEmployee.Append("<!-- Optional URL to which to post XML status udpates -->");
                    sbEmployee.Append("<AdditionalItems type=\"x:postback_url\"> <Text>");
                    sbEmployee.Append("https://jdpws.schedulingsite.com/api/products");
                    sbEmployee.Append("</Text></AdditionalItems>");
                    sbEmployee.Append("<!-- Integration Type -->");
                    sbEmployee.Append("<AdditionalItems type=\"x:integration_type\"><Text>");
                    sbEmployee.Append("Woodside");
                    sbEmployee.Append("</Text></AdditionalItems>");

                    sbEmployee.Append("<!-- Order Notes -->");
                    sbEmployee.Append("<AdditionalItems type=\"x:ordernotes\"><Text>");
                    sbEmployee.Append(recruiterEmail);
                    sbEmployee.Append("</Text></AdditionalItems>");

                    sbEmployee.Append("<!-- Descision Model -->");
                    sbEmployee.Append("<AdditionalItems type=\"x:decision_model\"><Text>");
                    sbEmployee.Append("REPORT");
                    sbEmployee.Append("</Text></AdditionalItems>");

                    sbEmployee.Append("<!-- supress xpartial-->");
                    sbEmployee.Append("<AdditionalItems type=\"x:postback_suppress_xpartial\"><Text>");
                    sbEmployee.Append("true");
                    sbEmployee.Append("</Text></AdditionalItems>");

                    sbEmployee.Append("</Screenings>");
                    if (EncodedFile != "")
                    {
                        sbEmployee.Append("<SupportingDocumentation><OriginalFileName>" + FileName + "</OriginalFileName><Name>Self-disclosure</Name>");
                        sbEmployee.Append("<EncodedContent>" + EncodedFile + "</EncodedContent></SupportingDocumentation>");
                    }
                    sbEmployee.Append("</BackgroundSearchPackage></BackgroundCheck>");

                    //        var client = new RestSharp.RestClient("https://www.jdpalatine.net/send/interchange");
                    try
                    {
                        ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;
                        WebClient clnt = new WebClient();

                        clnt.Headers.Add("postman-token:99b5138f-adb3-db43-e514-9279b10755c6");
                        clnt.Headers.Add("cache-control:no-cache");
                        clnt.Headers.Add("authorization:Basic Y3NjX3htbF90ZXN0OmZhUEI1ak4zbjVwQw==");
                        string outPut = clnt.UploadString("https://www.jdpalatine.net/send/interchange", "POST", sbEmployee.ToString());

                        //ErrorLogger.Log(outPut);
                        XmlDocument doc = new XmlDocument();
                        doc.LoadXml(outPut);
                        string fileName = ApplicantID.ToString() + "_" + DateTime.Now.ToFileTime().ToString() + ".xml";
                        string fileToSave = HPortalResponseValue;
                        fileToSave = fileToSave + "\\" + fileName;
                        doc.Save(fileToSave);
                        //  bool outPut = objApplicant.UpdateBGCheckStatus(Convert.ToInt32(strApId), status, "");
                        //return outPut ? "SUCCESS" : "ERROR";
                        //Response.Write(outPut);
                        XmlNodeList listNodes = doc.GetElementsByTagName("OrderId");

                        if (listNodes != null && listNodes.Count > 0)
                        {
                            string orderId = listNodes[0].InnerText;
                            if (!String.IsNullOrEmpty(orderId))
                            {
                                objApp.UpdateBGCheckOrderId(ApplicantID, orderId);
                            }
                        }
                        else
                        {
                            listNodes = doc.GetElementsByTagName("OrderStatus");
                            if (listNodes != null && listNodes.Count > 0)
                            {
                                string status = listNodes[0].InnerText;
                                if (status == "x:error")
                                {
                                    listNodes = doc.GetElementsByTagName("ErrorDescription");
                                    if (listNodes != null && listNodes.Count > 0)
                                    {
                                        string errorMessage = listNodes[0].InnerText;
                                        objApp.UpdateBGCheckStatus(ApplicantID, 4, errorMessage);
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        SendEmails obj = new SendEmails();


                        MailContent objMailContent = new MailContent() { From = "support@protatech.com", toEmailaddress = "PDevadoss@protatech.com", displayName = "support@protatech.com", subject = "Hportal Error: JDP", emailBody = "ApplicantID : " + ApplicantID + ", " + ex.Message, strAttachment = null, EventData = null, CopyTo = { "draheja@protatechindia.com", "vprajapati@protatechindia.com" } };
                        obj.SendEmailInBackgroundThread(objMailContent);
                        //SendMail("support@protatech.com", "PDevadoss@protatech.com", "Hportal Error: JDP", "ApplicantID : " + ApplicantID + ", " + ex.Message, "draheja@protatechindia.com", "vprajapati@protatechindia.com");
                        ErrorLogger.Log(ex.Message);
                        throw ex;
                    }

                }
            }
        }


    }
}
