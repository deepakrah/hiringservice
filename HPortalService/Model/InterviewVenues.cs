﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HPortalService.DAL;
using System.Data;


namespace HPortalService
{
    public class InterviewVenues
    {
        public int InterviewVenueId { get; set; }
        public int VenueId { get; set; }
        public string VenueName { get; set; }
        public string BranchName { get; set; }

        public string RoomNo { get; set; }
        public string ParkingInstructions { get; set; }
        public string InterviewType { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string Duration { get; set; }
        public int Sessions { get; set; }
        public int Interviewers { get; set; }
        public int BranchId { get; set; }
        public int ChildBranchId { get; set; }
        public string Date { get; set; }
        public int JobTitleId { get; set; }
        public DateTime Comments { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int Slot { get; set; }
        public Boolean isPublished { get; set; }
        public string ApplicantInterviewExist { get; set; }
        public int JobId { get; set; }
        public int ShiftRoleId { get; set; }
        public string ShiftName { get; set; }
        public string JobName { get; set; }
        public int NewStatus { get; set; }
        public int ScheduleType { get; set; }
        public int UserType { get; set; }
        public string Year { get; set; }
        public int Status { get; set; }
        public string InterviewSession { get; set; }
        public int UserId { get; set; }
        public int Available { get; set; }

        public int InterviewVenue_Ins(InterviewVenues obj)
        {
            JobApplicationDAL _JobApplicationDAL = new JobApplicationDAL();
            return _JobApplicationDAL.InterviewVenue_Ins(obj);
        }

        public List<InterviewVenues> GetInterviewVenueByBranchId(int BranchId, string Type = null, bool SearchByHomeBranch = false)
        {
            DataSet ds;
            JobApplicationDAL _AppInfo = new JobApplicationDAL();
            ds = _AppInfo.GetInterviewVenueBranchId(BranchId, Type, SearchByHomeBranch);
            List<InterviewVenues> lstApplicant = SQLHelper.ContructList<InterviewVenues>(ds);
            return lstApplicant;
        }

        public DataSet GetInterviewVenueDate(int BranchId, int type = 0, int isAdmin = 1, Nullable<int> VenueId = null, string ZipCode = null, bool getAunpublished = false, int YearFilter = 0)
        {
            DataSet ds;
            JobApplicationDAL _AppInfo = new JobApplicationDAL();
            ds = _AppInfo.GetInterviewVenueDate(BranchId, type, isAdmin, VenueId, ZipCode, getAunpublished, YearFilter);

            return ds;
        }

        public DataSet GetInterviewVenueDateVenue(int BranchId, int type = 0, int isAdmin = 1, Nullable<int> VenueId = null, string ZipCode = null, bool getAunpublished = false, int YearFilter = 0)
        {
            DataSet ds;
            JobApplicationDAL _AppInfo = new JobApplicationDAL();
            ds = _AppInfo.GetInterviewVenueDateVenue(BranchId, type, isAdmin, VenueId, ZipCode, getAunpublished, YearFilter);

            return ds;
        }

        public DataSet EditInterviewVenue(InterviewVenues obj)
        {
            DataSet ds;
            JobApplicationDAL _AppInfo = new JobApplicationDAL();
            ds = _AppInfo.EditInterviewVenue(obj);

            return ds;
        }

        public DataSet GetIApplicantInterview(int BranchId, Nullable<int> VenueId, string date = null, Nullable<int> JobApplicationNo = null, int type = 0, bool includeAttended = true, bool checkedIn = false, bool SearchByHomeBranch = false)
        {
            DataSet ds;
            JobApplicationDAL _AppInfo = new JobApplicationDAL();
            ds = _AppInfo.GetApplicantInterview(BranchId, VenueId, date, JobApplicationNo, type, includeAttended, checkedIn, SearchByHomeBranch);

            return ds;
        }

        public DataSet GetAvailInterviewVenueByDate(string Date, int BranchId, int Orientation = 0, int isAdmin = 0, int VenueId = 0)
        {
            DataSet ds;
            JobApplicationDAL _AppInfo = new JobApplicationDAL();
            ds = _AppInfo.GetAvailInterviewVenueByDate(Date, BranchId, Orientation, isAdmin, VenueId);

            return ds;
        }

        //public DataSet GetAvailInterviewVenueByDateVenue(string Date, int BranchId, int Orientation = 0, int isAdmin = 0, int VenueId = 0)
        //{
        //    DataSet ds;
        //    JobApplicationDAL _AppInfo = new JobApplicationDAL();
        //    ds = _AppInfo.GetAvailInterviewVenueByDateVenue(Date, BranchId, Orientation, isAdmin, VenueId);

        //    return ds;
        //}

        public DataSet fnGetJobByDate(DateTime Date, int BranchId)
        {
            DataSet ds;
            JobApplicationDAL _AppInfo = new JobApplicationDAL();
            ds = _AppInfo.GetJobByDate(Date, BranchId);
            return ds;
        }
        public DataSet fnGetShiftRole(int JobId, int BranchId)
        {
            DataSet ds;
            JobApplicationDAL _AppInfo = new JobApplicationDAL();
            ds = _AppInfo.GetShiftRole(JobId, BranchId);
            return ds;
        }

        public DataSet GetApplicantBySessionId(int InterviewSessionId)
        {
            DataSet ds;
            JobApplicationDAL _AppInfo = new JobApplicationDAL();
            ds = _AppInfo.GetApplicantBySessionId(InterviewSessionId);

            return ds;
        }
        public int Mark_Publish(InterviewVenues obj)
        {
            JobApplicationDAL _JobAppDAL = new JobApplicationDAL();
            return _JobAppDAL.Mark_Publish(obj);
        }

        public int DeleteInterviewVenueByInterviewVenueId(int InterviewVenueId)
        {
            JobApplicationDAL _JobAppDAL = new JobApplicationDAL();
            return _JobAppDAL.DeleteInterviewVenueByInterviewVenueId(InterviewVenueId);
        }

        public DataSet GetApplicantNotAttendedInt(int BranchID, int VenueID, string VenueDate)
        {
            JobApplicationDAL _JobAppDAL = new JobApplicationDAL();
            return _JobAppDAL.GetApplicantNotAttendedInt(BranchID, VenueID, VenueDate);
        }
        public DataSet GetVenueSummary(int branchId, string StrartDate = null, string Enddate = null)
        {
            JobApplicationDAL _JobAppDAL = new JobApplicationDAL();
            return _JobAppDAL.GetVenueSummary(branchId, StrartDate, Enddate);
        }

    }
}
