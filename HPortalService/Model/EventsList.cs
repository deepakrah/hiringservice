﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HPortalService
{
    public class EventsList
    {
        public int id { get; set; }
        public int VenueId { get; set; }
        public string title { get; set; }
        public string Type { get; set; }
        public string backgroundColor { get; set; }
        public string start { get; set; }
        public string end { get; set; }
        public string url { get; set; }
        public int Slot { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string Sessions { get; set; }
        public string Interviewer { get; set; }
        public string Duration { get; set; }
        public string Room { get; set; }
        public string Parking { get; set; }
        public string ApplicantInterviewExist { get; set; }
        public bool allDay { get; set; }
        public bool isPublished { get; set; }
        public string BranchName { get; set; }
        public int BranchId { get; set; }
        public string color { get; set; }
        public string bgColor { get; set; }
        public int ShiftRoleId { get; set; }
        public int JobId { get; set; }
        public string ShiftName { get; set; }
        public string JobName { get; set; }

        public string ToUnixTimespan(DateTime date, string time)
        {
            if (time != null && time != "")
            {
                string str = date.ToShortDateString() + " " + time.Substring(0, 5) + " " + time.Substring(time.Length - 2);
                DateTime objDate = Convert.ToDateTime(str);

                TimeSpan tspan = objDate.Subtract(
                    new DateTime(1970, 1, 1, 0, 0, 0));

                //  return (long)Math.Truncate(tspan.TotalSeconds);
                return objDate.ToString("yyyy-MM-dd HH:mm:ss");
            }
            else
                return "";
        }
        public string ToUnixDate(DateTime date)
        {
            if (date != null)
            {
                string str = date.ToShortDateString();
                DateTime objDate = Convert.ToDateTime(str);

                TimeSpan tspan = objDate.Subtract(
                    new DateTime(1970, 1, 1, 0, 0, 0));

                //  return (long)Math.Truncate(tspan.TotalSeconds);
                return objDate.ToString("MM/dd/yyyy");
            }
            else
                return "";
        }
    }
}
