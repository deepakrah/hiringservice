﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HPortalService
{

    public class CanvasImage
    {
        public int ApplicantId { get; set; }
        public string imageData { get; set; }
        public string ImageName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int UserId { get; set; }
        public string SignatureImage { get; set; }
        public bool FileExistsFlag { get; set; }
        
    }
}
