﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Xml.Xsl;
using System.Text;
using System.Xml;
using System.IO;
using System.Configuration;
using Newtonsoft.Json;
using System.Xml;
using System.Text.RegularExpressions;
using HPortalService;
/// <summary>
/// Summary description for MailerUtility
/// </summary>
public class MailerUtility
{
    public MailerUtility()
    {

    }

    public static string GetMailBody(string XsltUrl, string xmldata)
    {
        try
        {
            XslCompiledTransform xDoc = new XslCompiledTransform();
            xDoc.Load(XsltUrl);
            StringBuilder resultString = new StringBuilder();
            XmlTextWriter xmlWriter = new XmlTextWriter(new StringWriter(resultString));
            XmlTextReader xmlReader = new XmlTextReader(new StringReader(xmldata));
            xDoc.Transform(xmlReader, xmlWriter);
            string result = resultString.ToString();
            result = result.Replace("xmlns:asp=\"remove\"", "");
            return result;
        }
        catch (Exception ex)
        {
            return "";
        }
    }

    public static string GetMailBody(EmailParameters objEP)
    {
        try
        {
            EmailTemplate objET = new EmailTemplate(objEP.XMLFilePath, objEP.BranchId);
            string template = objET.Template;
            template = template.Replace("[Name]", objEP.UserName != null ? objEP.UserName : "");
            template = template.Replace("[Branch]", objEP.Branch != null ? objEP.Branch : "");
            template = template.Replace("[VenueName]", objEP.VenueName != null ? objEP.VenueName : "");
            template = template.Replace("[InterviewDate]", objEP.InterviewDate != null ? objEP.InterviewDate : "");
            template = template.Replace("[InterviewTime]", objEP.InterviewTime != null ? objEP.InterviewTime : "");
            template = template.Replace("[VenueAddress]", objEP.Address != null ? objEP.Address : "");
            template = template.Replace("[RoomNo]", objEP.RoomNo != null ? objEP.RoomNo : "");
            template = template.Replace("[StartTime]", objEP.StartTime != null ? objEP.StartTime : "");
            template = template.Replace("[EndTime]", objEP.EndTime != null ? objEP.EndTime : "");
            template = template.Replace("[ParkingInstructions]", objEP.ParkingInstructions != null ? objEP.ParkingInstructions : "");
            template = template.Replace("[ApplicationNumber]", objEP.ApplicationNumber != null ? objEP.ApplicationNumber : "");
            template = template.Replace("[verificationCode]", objEP.Password != null ? objEP.Password : "");
            template = template.Replace("[Email]", objEP.Email != null ? objEP.Email : "");
            template = template.Replace("[Address]", objEP.Address != null ? objEP.Address : "");
            template = template.Replace("[Password]", objEP.Password != null ? objEP.Password : "");
            template = template.Replace("[OfferLetterLink]", objEP.OfferLetterLink != null ? objEP.OfferLetterLink : "");
            template = template.Replace("[WOTCLink]", objEP.WOTCLink != null ? objEP.WOTCLink : "");
            template = template.Replace("[OnlineInterview]", objEP.OnlineInterview != null ? objEP.OnlineInterview : "");
            template = template.Replace("[JobPosition]", objEP.JobPosition != null ? objEP.JobPosition : "");
            return template;
        }
        catch (Exception ex)
        {
            return "";
        }
    }


//    /// <summary>
//    /// 
//    /// </summary>
//    /// <typeparam name="T"></typeparam>
//    /// <param name="type"></param>
//    /// <returns></returns>
//    public static string ConvertoJSON<T>(List<T> type)
//    {
//        System.Web.Script.Serialization.JavaScriptSerializer oSerializer =
//new System.Web.Script.Serialization.JavaScriptSerializer();
//        return oSerializer.Serialize(type);

//    }

//    /// <summary>
//    /// 
//    /// </summary>
//    /// <typeparam name="T"></typeparam>
//    /// <param name="type"></param>
//    /// <returns></returns>
//    public static string ConvertoJSONClass<T>(T type)
//    {
//        System.Web.Script.Serialization.JavaScriptSerializer oSerializer =
//new System.Web.Script.Serialization.JavaScriptSerializer();
//        return oSerializer.Serialize(type);
//    }

//    /// <summary>
//    /// 
//    /// </summary>
//    /// <typeparam name="T"></typeparam>
//    /// <param name="type"></param>
//    /// <returns></returns>
//    public static string ConvertToXML<T>(List<T> type)
//    {

//        System.Web.Script.Serialization.JavaScriptSerializer oSerializer =
//        new System.Web.Script.Serialization.JavaScriptSerializer();
//        string sJSON = oSerializer.Serialize(type);

//        //XmlDocument doc = new XmlDocument();
//        XmlDocument xdoc = JsonConvert.DeserializeXmlNode("{\"root\":" + sJSON + "}", "roots");
//        return xdoc.OuterXml;
//    }

//    /// <summary>
//    /// 
//    /// </summary>
//    /// <typeparam name="T"></typeparam>
//    /// <param name="type"></param>
//    /// <returns></returns>
//    public static string ConvertToXML<T>(T type)
//    {

//        System.Web.Script.Serialization.JavaScriptSerializer oSerializer =
//     new System.Web.Script.Serialization.JavaScriptSerializer();
//        string sJSON = oSerializer.Serialize(type);

//        //XmlDocument doc = new XmlDocument();
//        XmlDocument xdoc = JsonConvert.DeserializeXmlNode("{\"root\":" + sJSON + "}", "roots");
//        return xdoc.OuterXml;
//    }
}
