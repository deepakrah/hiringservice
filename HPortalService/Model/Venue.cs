﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

using HPortalService.DAL;

namespace HPortalService
{
    public class Venue
    {
        public int VenueId { get; set; }
        public int CityId { get; set; }
        public bool SeatPermanent { get; set; }
        public bool SeatPortable { get; set; }
        public bool SeatExt { get; set; }
        public int BranchID { get; set; }
        public int StatusID { get; set; }
        public string Name { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string SignInArea { get; set; }
        public string TravelInstructions { get; set; }
        public string Notes { get; set; }
        public string ParkingInstructions { get; set; }
        public string WebAddress { get; set; }
        public string Description { get; set; }

        public List<Venue> GetVenueByBranchId(int BranchID)
        {
            DataSet ds;
            VenueDAL _VenueInfo = new VenueDAL();
            ds = _VenueInfo.GetVenueByBranchId(BranchID);
            List<Venue> VenueList = SQLHelper.ContructList<Venue>(ds);
            return VenueList;
        }

        public DataSet  GetVenueMapByVenueId(int VenueId)
        {
            DataSet ds;
            VenueDAL _VenueInfo = new VenueDAL();
            ds = _VenueInfo.GetVenueMapByVenueId(VenueId);          
            return ds;
        }
        public DataSet GetInterviewVenuesById(int InterviewVenueId)
        {
            DataSet ds;
            VenueDAL _VenueInfo = new VenueDAL();
            ds = _VenueInfo.GetInterviewVenuesById(InterviewVenueId);
            return ds;
        }

    }
}
