﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HPortalService.DAL;
using System.Data;
namespace HPortalService
{
    public class User
    {
        int _UserId = 0;
        public int UserId { get { return _UserId; } }
        public User(int userId)
        {
            _UserId = userId;
        }


        public DataTable GetSettings(SettingSections section)
        {
            UserDAL objUserDAL = new UserDAL();
            return objUserDAL.GetSettings(Convert.ToString(section), UserId);

        }
        public DataSet fnGetESignature()
        {
            UserDAL objUserDAL = new UserDAL();
            return objUserDAL.GetESignature(UserId);
        }
        
    }
}
