﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using HPortalService.DAL;


namespace HPortalService
{
    public class ApplicantDisclosure
    {
        public int JobApplicationId { get; set; }
        public string Ethnicity { get; set; }
        public string Gender { get; set; }
        public string VeteranStatus { get; set; }
        public String AppliedOn { get; set; }
     
        public int SaveApplicantDisclosure(ApplicantDisclosure obj)
        {
            ApplicantDisclosureDAL _AppDisclosure = new ApplicantDisclosureDAL();
            return _AppDisclosure.UpdateApplicantDisclosure(obj);
        }

        public List<ApplicantDisclosure> GetDisclosureByJobApplicantId(int JobApplicantId)
        {
            DataSet ds;
            ApplicantDisclosureDAL _AppDisclosure = new ApplicantDisclosureDAL();
            ds = _AppDisclosure.GetDisclosureByJobApplicantId(JobApplicantId);
            List<ApplicantDisclosure> lstApplicant = SQLHelper.ContructList<ApplicantDisclosure>(ds);
            return lstApplicant;
        }
    }
}



