﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

using HPortalService.DAL;

namespace HPortalService
{
   public class ApplicantCertification
    {
        public int CertificationID { get; set; }
        public int JobApplicationId { get; set; }
        public Boolean SecurityLicence { get; set; }
        public string LicenseType { get; set; }
        public string LicenseNumber { get; set; }
        public string Agency { get; set; }
        public string IssueMonth { get; set; }
        public string IssueYear { get; set; }
        public string ExpirationMonth { get; set; }
        public string ExpirationYear { get; set; }
        public string Description { get; set; }


        public int SaveCertification(ApplicantCertification obj)
        {
            ApplicantCertificationDAL _AppGeneralInfo = new ApplicantCertificationDAL();
            return _AppGeneralInfo.SaveCertificate(obj);
        }

        public int UpdateCertification(ApplicantCertification obj)
        {
            ApplicantCertificationDAL _AppCertification = new ApplicantCertificationDAL();
            return _AppCertification.UpdateCertificate(obj);
        }



        public List<ApplicantCertification> GetCertificationByJobApplicantId(int JobApplicantId)
        {
            DataSet ds;
            ApplicantCertificationDAL _AppInfo = new ApplicantCertificationDAL();
            ds = _AppInfo.GetCertificateByJobApplicantId(JobApplicantId);
            List<ApplicantCertification> lstApplicant = SQLHelper.ContructList<ApplicantCertification>(ds);
            //foreach (ApplicantCertification obj in lstApplicant)
            //{
            //    obj.IssueMonth = obj.IssueMonth != null ? Convert.ToDateTime(obj.IssueMonth).ToString("MMMM yyyy") : null;
            //    obj.ExpirationMonth = obj.ExpirationMonth != null ? Convert.ToDateTime(obj.ExpirationMonth).ToString("MMMM yyyy") : null;
            //}
            return lstApplicant;
        }

        public int DeleteApplicantCertification()
        {
            ApplicantCertificationDAL _AppReference = new ApplicantCertificationDAL();
            return _AppReference.DeleteApplicantCertificationById(this.CertificationID);
        }

    }
}
