﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using HPortalService.DAL;

namespace HPortalService
{
    public class Branch
    {
        public int BranchId { get; set; }
        public string BranchName { get; set; }
        public int QuesId { get; set; }
        public int QuesSetId { get; set; }
        public string Description { get; set; }
        public int ApplicantId { get; set; }
        public string ZipCode { get; set; }
        public string EmailId { get; set; }
        public bool IsBranchTask { get; set; }
        public int TotalBranchTask { get; set; }
        public string Coordinate { get; set; }
        public bool EnableOnline { get; set; }
        public string OnlineInterview { get; set; }



        public DataSet GetQuestionByBranchId(Branch obj)
        {
            BranchDAL _BranchDAL = new BranchDAL();
            DataSet ds = _BranchDAL.GetQuestionByBranchId(obj);
            return ds;
        }

        private VenueSettings vwSettings = new VenueSettings();
        public VenueSettings VenueSetting { get { return vwSettings; } }
        private InterViewSettings intSettings = new InterViewSettings();
        public InterViewSettings InterViewSetting { get { return intSettings; } }
        private HiringSettings _hiringSettings = new HiringSettings();
        public HiringSettings HiringSetting { get { return _hiringSettings; } }

        public int TaskId { get; set; }
        public int BranchTaskId { get; set; }

        public int OrderNo { get; set; }

        public int TaskLevel { get; set; }
        public bool IsExist { get; set; }
        public int Phase { get; set; }

        public Branch()
        {

        }
        public Branch(int branchId)
        {
            this.BranchId = branchId;
            GetBranchInfo();
            Getsettings();
        }
        public static DataSet GetBranchCoordinatate()
        {
            BranchDAL _BranchDAL = new BranchDAL();
            DataSet ds = _BranchDAL.GetBranchCoordinatate();
            return ds;
        }
        void Getsettings()
        {
            if (BranchId > 0)
            {
                BranchSetting bSettings = new BranchSetting();
                DataSet ds = bSettings.GetBranchSetting(BranchId);
                if (ds != null && ds.Tables.Count > 0)
                {
                    ds.Tables[0].DefaultView.RowFilter = "Section=2 AND Key='MaxQuestion'";
                    DataView vw = ds.Tables[0].DefaultView;
                    if (vw != null && vw.Count > 0)
                    {
                        intSettings = new InterViewSettings { MaxQuestions = Convert.ToInt32(vw[0]["Value"]) };
                    }
                    ds.Tables[0].DefaultView.RowFilter = "Section=3 AND Key='AllowSchedWithoutPublish'";
                    vw = ds.Tables[0].DefaultView;
                    if (vw != null && vw.Count > 0)
                    {
                        vwSettings = new VenueSettings { AllowSchedOnUnPublished = Convert.ToInt32(vw[0]["Value"]) == 1 ? true : false };
                    }
                    ds.Tables[0].DefaultView.RowFilter = "Section=4 AND Key='NotHiring'";
                    vw = ds.Tables[0].DefaultView;
                    if (vw != null && vw.Count > 0)
                    {
                        _hiringSettings.NotHiring = vw[0]["Value"] != null ? Convert.ToBoolean(vw[0]["Value"]) : false;
                    }
                    ds.Tables[0].DefaultView.RowFilter = "Section=4 AND Key='HiringMonth'";
                    vw = ds.Tables[0].DefaultView;
                    if (vw != null && vw.Count > 0)
                    {
                        _hiringSettings.HiringMonth = vw[0]["Value"].ToString();
                    }
                }
            }
        }

        public DataSet GetBranchByUserId(int UserId)
        {
            BranchDAL _BranchDAL = new BranchDAL();
            DataSet ds = _BranchDAL.GetBranchByUserId(UserId);
            return ds;
        }
        public DataSet GetAuditBranchByUserId(int UserId)
        {
            BranchDAL _BranchDAL = new BranchDAL();
            DataSet ds = _BranchDAL.GetAuditBranchByUserId(UserId);
            return ds;
        }
        public DataSet GetWorkingYear(int branchId)
        {
            BranchDAL _BranchDAL = new BranchDAL();
            DataSet ds = _BranchDAL.GetWorkingYear(branchId);
            return ds;
        }
        public string GetEmailId(int BranchId)
        {
            BranchDAL _BranchDAL = new BranchDAL();
            return _BranchDAL.GetEmailId(BranchId);
        }

        public DataTable GetBranchDetails(int BranchId)
        {
            BranchDAL _BranchDAL = new BranchDAL();
            return _BranchDAL.GetBranchDetails(BranchId);
        }
        public DataSet GetInterviewVenues()
        {
            BranchDAL _BranchDAL = new BranchDAL();
            DataSet ds = _BranchDAL.GetInterviewVenues(this.BranchId);
            return ds;
        }

        public int AddCode(int sourceCategory, string source, int ceatedBy)
        {
            BranchDAL _BranchDAL = new BranchDAL();
            return _BranchDAL.AddCode(sourceCategory, this.BranchId, source, ceatedBy);
        }

        public DataSet GetAllSouceCodes()
        {
            BranchDAL _BranchDAL = new BranchDAL();
            DataSet ds = _BranchDAL.GetAllSouceCodes(this.BranchId);
            return ds;
        }

        public DataSet GetSourceCode(string branchsourceCode)
        {
            BranchDAL _BranchDAL = new BranchDAL();
            DataSet ds = _BranchDAL.GetSourceCode(branchsourceCode);
            return ds;
        }

        public DataSet GetBranchSourceByBranchId(string branchId)
        {
            BranchDAL _BranchDAL = new BranchDAL();
            DataSet ds = _BranchDAL.GetBranchSourceByBranchId(branchId);
            return ds;
        }
        public DataSet GetAllBranch()
        {
            BranchDAL _BranchDAL = new BranchDAL();
            DataSet ds = _BranchDAL.GetAllBranch();
            return ds;
        }
        void GetBranchInfo()
        {
            BranchDAL _BranchDAL = new BranchDAL();
            DataSet ds = _BranchDAL.GetBranchInfo(this.BranchId);
            if (ds != null && ds.Tables != null)
            {
                if (ds.Tables.Count > 0)
                {
                    EmailId = Convert.ToString(ds.Tables[0].Rows[0]["emailId"]);
                    BranchName = Convert.ToString(ds.Tables[0].Rows[0]["BranchName"]);
                }
            }
        }

        public static DataSet GetSourceCategory(int branchId)
        {
            BranchDAL _BranchDAL = new BranchDAL();
            DataSet ds = _BranchDAL.GetSourceCategory(branchId);
            return ds;
        }



        public static DataSet GetAllBranches()
        {
            BranchDAL _BranchDAL = new BranchDAL();
            DataSet ds = _BranchDAL.GetAllBranches();
            return ds;
        }

        public static void AddLookupSouceCategory(string Name, string GroupName)
        {
            BranchDAL _BranchDAL = new BranchDAL();
            _BranchDAL.AddLookupSouceCategory(Name, GroupName);
        }

        public static DataSet GetSourceCategoryGroup()
        {
            BranchDAL _BranchDAL = new BranchDAL();
            DataSet ds = _BranchDAL.GetSourceCategoryGroup();
            return ds;
        }
        public static void AddSouceCategory(int branchId, int sourceCategory, Boolean ShowOnWeb)
        {
            BranchDAL _BranchDAL = new BranchDAL();
            _BranchDAL.AddSouceCategory(branchId, sourceCategory, ShowOnWeb);

        }
        public static DataSet GetAllSourceCategory()
        {
            BranchDAL _BranchDAL = new BranchDAL();
            return _BranchDAL.GetAllSourceCategory();

        }

        public int UpdateEmail(string emalId)
        {
            BranchDAL _BranchDAL = new BranchDAL();
            return _BranchDAL.UpdateEmail(this.BranchId, emalId);
        }

        public int UpdateBranch(string emalId, string Address1, string CityName, string state, string Country, string Zip,string primaryPhone, string HiringManager, string Designation, string PayRate)
        {
            BranchDAL _BranchDAL = new BranchDAL();
            return _BranchDAL.UpdateBranch(this.BranchId, emalId, Address1, CityName, state, Country, Zip,primaryPhone,HiringManager, Designation, PayRate);
        }


        public int SaveBranchTask(Branch obj)
        {
            BranchDAL _BranchDAL = new BranchDAL();
            return _BranchDAL.SaveBranchTask(obj);

        }

        public int SaveBranchInterview(Branch obj)
        {
            BranchDAL _BranchDAL = new BranchDAL();
            return _BranchDAL.SaveBranchInterview(obj);
        }
        public DataSet GetBranchInterview(int BranchId)
        {
            BranchDAL _BranchDAL = new BranchDAL();
            return _BranchDAL.GetBranchInterview(BranchId);
        }
        public DataSet GetMangeBranchTask(Branch obj)
        {
            BranchDAL _BranchDAL = new BranchDAL();
            return _BranchDAL.GetMangeBranchTask(obj);

        }
        public DataSet GetBranchTask(Branch obj)
        {
            BranchDAL _BranchDAL = new BranchDAL();
            return _BranchDAL.GetBranchTask(obj);

        }
        public DataSet fnGetBranchAllTask(Branch obj)
        {
            BranchDAL _BranchDAL = new BranchDAL();
            return _BranchDAL.GetBranchAllTask(obj);
        }
        public DataSet GetBranchIsTask(int BranchId)
        {
            BranchDAL _BranchDAL = new BranchDAL();
            return _BranchDAL.GetBranchIsTask(BranchId);
        }

        public DataSet fnGetBranch(int ID)
        {
            BranchDAL _ApplicationDAL = new BranchDAL();
            return _ApplicationDAL.GetBranch(ID);
        }

        public DataSet GetJobPositionByBranchId(int BranchId, int JobPositionId)
        {
            BranchDAL _BranchDAL = new BranchDAL();
            return _BranchDAL.GetJobPositionByBranchId(BranchId, JobPositionId);
        }

        public int UpdateJobPosition(int ApplicantId, string JobPositionId)
        {
            BranchDAL _BranchDAL = new BranchDAL();
            return _BranchDAL.UpJobPosition(ApplicantId, JobPositionId);
        }
    }

    public class BranchSource
    {
        public int SourceCategoryId { get; set; }
        public string Name { get; set; }
        public string GroupName { get; set; }
        public int DisplayOrder { get; set; }
        public int BranchSourceCategoryId { get; set; }
        public bool ShowOnWeb { get; set; }
        public int BranchId { get; set; }
        public int UserId { get; set; }
        public string Source { get; set; }
    }
}
