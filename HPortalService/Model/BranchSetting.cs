﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

using HPortalService.DAL;
namespace HPortalService
{
    public class BranchSetting
    {
        public string Key { get; set; }
        public string Value { get; set; }
        public int BranchId { get; set; }
        public string Section { get; set; }
        public string MaxInterview { get; set; }
        public bool ShowVenuesToApplicant { get; set; }
        public bool OpenHiring { get; set; }
        public string HiringMonth { get; set; }
        public string Email { get; set; }
        public string Address1 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string Zip { get; set; }
        public string HiringManager { get; set; }
        public string Designation { get; set; }
        public string primaryPhone { get; set; }
        public string PayRate { get; set; }

        public int UpdateBranchSetting()
        {
            BranchSettingDAL obj = new BranchSettingDAL();
            return obj.UpdateBranchSetting(this);
        }

        public void GetDefaultEmail(int branchId)
        {

        }

        public void UpdateDefaultEmail(int branchId, string email)
        {

        }


        public DataSet GetBranchSetting(int BranchId)
        {
            BranchSettingDAL obj = new BranchSettingDAL();
            return obj.GetBranchSetting(BranchId);
        }
        public BranchSetting BindSettings(int BranchId)
        {
            BranchSetting objSetting = new BranchSetting();
            Branch objBranch = new Branch(BranchId);
            objSetting.MaxInterview = objBranch.InterViewSetting.MaxQuestions.ToString();
            objSetting.ShowVenuesToApplicant = objBranch.VenueSetting.AllowSchedOnUnPublished;
            objSetting.OpenHiring = objBranch.HiringSetting.NotHiring;
            objSetting.HiringMonth = objBranch.HiringSetting.HiringMonth;
            DataTable dtBranch = objBranch.GetBranchDetails(BranchId);
            if (dtBranch.Rows.Count > 0)
            {
                objSetting.Email = dtBranch.Rows[0]["emailid"].ToString();
                objSetting.Address1 = dtBranch.Rows[0]["Address1"].ToString();
                objSetting.City = dtBranch.Rows[0]["CityName"].ToString();
                objSetting.State = dtBranch.Rows[0]["State"].ToString();
                objSetting.Country = dtBranch.Rows[0]["Country"].ToString();
                objSetting.Zip = dtBranch.Rows[0]["Zip"].ToString();
                objSetting.primaryPhone = dtBranch.Rows[0]["Phone1"].ToString();
                objSetting.HiringManager = dtBranch.Rows[0]["HiringManager"].ToString();
                objSetting.Designation = dtBranch.Rows[0]["Designation"].ToString();
                objSetting.PayRate = dtBranch.Rows[0]["PayRate"].ToString();
            }

            return objSetting;
        }

        public int SaveJobPosition(string JobPositionName, decimal PayRate, int BranchId, int JobPositionId, string JobType, string Description, string PayrateDescription)
        {
            BranchSettingDAL _BranchDAL = new BranchSettingDAL();
            return _BranchDAL.SaveJobPosition(JobPositionName, PayRate, BranchId, JobPositionId, JobType, Description, PayrateDescription);
        }

        public int UpdateJobPosition(int JobPositionId, int Status)
        {
            BranchSettingDAL _BranchDAL = new BranchSettingDAL();
            return _BranchDAL.UpdateJobPosition(JobPositionId, Status);
        }

        public DataSet GetAllPositionByBranchId(int BranchId)
        {
            BranchSettingDAL _BranchDAL = new BranchSettingDAL();
            DataSet ds = _BranchDAL.GetAllPositionByBranchId(BranchId);
            return ds;
        }
    }
}
