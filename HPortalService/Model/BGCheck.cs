﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

using HPortalService.DAL;
namespace HPortalService
{
    public class BGCheck
    {
        public string BGCheckId { get; set; }
        public string ApplicantId { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string MaidenName { get; set; }
        public string YearUsed { get; set; }
        public string YearUsedTo { get; set; }
        public string SSN { get; set; }
        public string DLNumber { get; set; }
        public string State { get; set; }
        public string DOB { get; set; }
        public string IsFelony { get; set; }
        public string IsMisdemeanor { get; set; }
        public string ConvictionNote { get; set; }
        public string ConvictionDate { get; set; }
        
        public string IsParole { get; set; }
        public string IsProbation { get; set; }
        public string ParoleProbationNote { get; set; }
        public string IsPendingTrial { get; set; }
        public string PendingTrialNote { get; set; }

        // BGCheck Address
        public string BGCheckAddressId { get; set; }
        public string PresentStreetAddress { get; set; }
       // public string PresentCityStateZip { get; set; }

        public string PCity { get; set; }
        public string PState { get; set; }
        public string PZip { get; set; }
      

        public string count { get; set; }

        public string CountAddress { get; set; }




       // public string BGCheckAddressId { get; set; }
        public string PriorStreetAddress { get; set; }
       // public string PriorCityStateZip { get; set; }
        public string PriorCity { get; set; }
        public string PriorState { get; set; }
        public string PriorZip { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }

        public string PFromDate { get; set; }
        public string PToDate { get; set; }

        public string BGCheckAddressId1 { get; set; }
        public string PriorStreetAddress1 { get; set; }
       // public string PriorCityStateZip1 { get; set; }
        public string PriorCity1 { get; set; }
        public string PriorState1 { get; set; }
        public string PriorZip1 { get; set; }
        public string FromDate1 { get; set; }
        public string ToDate1 { get; set; }

        public string BGCheckAddressId2 { get; set; }
        public string PriorStreetAddress2 { get; set; }
       // public string PriorCityStateZip2 { get; set; }
        public string PriorCity2 { get; set; }
        public string PriorState2 { get; set; }
        public string PriorZip2 { get; set; }
        public string FromDate2 { get; set; }
        public string ToDate2 { get; set; }

        public string BGCheckAddressId3 { get; set; }
        public string PriorStreetAddress3 { get; set; }
       // public string PriorCityStateZip3 { get; set; }
        public string PriorCity3 { get; set; }
        public string PriorState3 { get; set; }
        public string PriorZip3 { get; set; }
        public string FromDate3 { get; set; }
        public string ToDate3 { get; set; }

        public string BGCheckAddressId4 { get; set; }
        public string PriorStreetAddress4 { get; set; }
       // public string PriorCityStateZip4 { get; set; }
        public string PriorCity4 { get; set; }
        public string PriorState4{ get; set; }
        public string PriorZip4 { get; set; }

        public string FromDate4 { get; set; }
        public string ToDate4 { get; set; }

        public string BGCheckAddressId5 { get; set; }
        public string PriorStreetAddress5 { get; set; }
       // public string PriorCityStateZip5 { get; set; }
        public string PriorCity5 { get; set; }
        public string PriorState5 { get; set; }
        public string PriorZip5 { get; set; }

        public string FromDate5 { get; set; }
        public string ToDate5 { get; set; }

        public string BGCheckAddressId6 { get; set; }
        public string PriorStreetAddress6 { get; set; }
       // public string PriorCityStateZip6 { get; set; }
        public string PriorCity6 { get; set; }
        public string PriorState6 { get; set; }
        public string PriorZip6 { get; set; }
        public string FromDate6 { get; set; }
        public string ToDate6 { get; set; }

        public string BGCheckAddressId7 { get; set; }
        public string PriorStreetAddress7 { get; set; }
       // public string PriorCityStateZip7 { get; set; }
        public string PriorCity7 { get; set; }
        public string PriorState7 { get; set; }
        public string PriorZip7 { get; set; }
        public string FromDate7 { get; set; }
        public string ToDate7 { get; set; }
        public bool isBGchkFreeCopy { get; set; }
        public string CrimeState { get; set; }
        public string CrimeCity { get; set; }
        public string CrimeCounty { get; set; }
        public string CrimeZip { get; set; }

        public int SaveApplicantBGCheck()
        {
            BGCheckDAL obj = new BGCheckDAL();
            return obj.SaveApplicantBGCheck(this);
        }

        public List<BGCheck> GetApplicantBGCheckById(int ApplicantId)
        {
            BGCheckDAL obj = new BGCheckDAL();
            // return obj.GetApplicantBGCheckById(ApplicantId);
            List<BGCheck> lstBGCheck = SQLHelper.ContructList<BGCheck>(obj.GetApplicantBGCheckById(ApplicantId));
            return lstBGCheck;
        }

        public DataSet GetApplicantBGCheckAddressById(int ApplicantId)
        {
            BGCheckDAL obj = new BGCheckDAL();
            return obj.GetApplicantBGCheckAddressById(ApplicantId);
        }

        public int DeleteBgChecksAddressById(int BGCheckAddressId)
        {
            BGCheckDAL obj = new BGCheckDAL();
            return obj.DeleteBgChecksAddressById(BGCheckAddressId);
        }
    

        public DataSet GetBGCheckAddressById(int ApplicantId)
        {
            BGCheckDAL obj = new BGCheckDAL();
            return obj.GetBGCheckAddressById(ApplicantId);
        }
    }
}
