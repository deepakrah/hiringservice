﻿

namespace HPortalService
{
    public class FAQuestion
    {
        public int BranchId { get; set; }
        public int FAQId { get; set; }
        public string Question { get; set; }
        public string Answer { get; set; }
        public int TaskId { get; set; }
        public int ApplicantFAQId { get; set; }
        public int ApplicantId { get; set; }
        public int AnswerGivenBy { get; set; }
        public int IAgreed { get; set; }
        public int QuestionNo { get; set; }
        public string QuestionTittle { get; set; }
        public string TaskName { get; set; }
    }
}
