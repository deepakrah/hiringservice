﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using HPortalService.DAL;
namespace HPortalService
{
   public class ApplicantWage
    {
        public int ApplicantWageId { get; set; }
        public int ApplicantId { get; set; }
        public string Name { get; set; }
        public bool SoleProprietor { get; set; }
        public bool Corporation { get; set; }
        public bool LimitedLiabilityCompany { get; set; }
        public bool GeneralPartnership { get; set; }
        public string MainOfficeAddress { get; set; }
        public string MailingAddress { get; set; }
        public string Phone { get; set; }
        public string BName { get; set; }
        public string Agreement { get; set; }
        public string Allowances { get; set; }
        public string InsuranceCarrierName { get; set; }
        public string InsuranceCarrierAddress { get; set; }
        public string InsuranceCarrierPhone { get; set; }
        public string PolicyNo { get; set; }

        public int intApplicantWage(ApplicantWage obj)
        {
            ApplicantWageDAL _ApplicationDAL = new ApplicantWageDAL();
            return _ApplicationDAL.intApplicantWage(obj);
        }
        public DataSet GetApplicantWageByID(int ID)
        {
            ApplicantWageDAL _ApplicationDAL = new ApplicantWageDAL();
            return _ApplicationDAL.GetApplicantWageByID(ID);
        }
    }
}
