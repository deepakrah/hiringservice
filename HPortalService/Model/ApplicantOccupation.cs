﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using HPortalService.DAL;


namespace HPortalService
{
    public class ApplicantOccupation
    {
        public int JobApplicationId { get; set; }
        public string Employer { get; set; }
        public string PossitionTitle { get; set; }
        public string City { get; set; }
        public int StateId { get; set; }
        public string StartMonth { get; set; }
        public int StartYear { get; set; }
        public string EndMonth { get; set; }
        public int EndYear { get; set; }
        public string SupervisorName { get; set; }
        public string SupervisorPhone { get; set; }
        public string EndingPay { get; set; }
        public Boolean ContactReference { get; set; }
        public string JobDuties { get; set; }
        public string ReasonLeaving { get; set; }
        public int OccupationId { get; set; }
        public string StateName { get; set; }

        public int SaveApplicantOccupation(ApplicantOccupation obj)
        {
            ApplicantOccupationDAL _AppOccupation = new ApplicantOccupationDAL();
            return _AppOccupation.SaveApplicanOccupation(obj);
        }

        public int UpdateApplicantOccupation(ApplicantOccupation obj)
        {
            ApplicantOccupationDAL _AppOccupation = new ApplicantOccupationDAL();
            return _AppOccupation.UpdApplicantOccupation(obj);
        }


        public List<ApplicantOccupation> GetOccupationByJobApplicantId(int JobApplicantId)
        {
            DataSet ds;
            ApplicantOccupationDAL _AppInfo = new ApplicantOccupationDAL();
            ds = _AppInfo.GetOccupationByJobApplicantId(JobApplicantId);
            List<ApplicantOccupation> lstApplicant = SQLHelper.ContructList<ApplicantOccupation>(ds);
            foreach (ApplicantOccupation obj in lstApplicant)
            {
                obj.StartMonth = (obj.StartMonth != null && obj.StartMonth != "0") ? Convert.ToDateTime(DateConvertintoString(obj.StartMonth)).ToString("MM/dd/yyyy") : null;
                obj.EndMonth = (obj.EndMonth != null && obj.EndMonth != "0") ? Convert.ToDateTime(DateConvertintoString(obj.EndMonth)).ToString("MM/dd/yyyy") : null;
            }
            return lstApplicant;
        }
        public int DeleteApplicantOccupation()
        {
            ApplicantOccupationDAL _AppOccupation = new ApplicantOccupationDAL();
            return _AppOccupation.DeleteApplicanOccupationById(this.OccupationId);
        }

        public string DateConvertintoString(string date)
        {
            string d = "01";
            string m = date.Split(' ')[0];
            string y = date.Split(' ')[1];

            return Convert.ToInt32((Months)Enum.Parse(typeof(Months), m)) + "/" + d + "/" + y;
        }
    }


    public enum Months
    {
        January = 1,
        February = 2,
        March = 3,
        April = 4,
        May = 5,
        June = 6,
        July = 7,
        August = 8,
        September = 9,
        October=10,
        November = 11,
        December = 12
    }
}
