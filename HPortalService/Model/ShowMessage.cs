﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HPortalService.Model
{
    public class ShowMessage
    {
        public string Message { get; set; }
        public string Type { get; set; }
        public enum messageType
        {
            Success=1,           
            Warning=2,
            Error = 3,
        }
        public ShowMessage Show(string message, messageType type)
        {
            ShowMessage obj = new ShowMessage()
            {
                Message = message,
                Type = type.ToString()
            };
            return obj;
        }
    }
}
