﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HPortalService.Model
{
    public class JobPosition
    {
        public int ApplicantId { get; set; }
        public int JobApplicationId { get; set; }
        public int ddlChangeStatus { get; set; }
        public int Status { get; set; }
        public int UserId { get; set; }
        public int BranchId { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string JobPositionId { get; set; }
        public decimal PayRate { get; set; }
        public string JobPositionName { get; set; }
        public string JobType { get; set; }
        public string Description { get; set; }
        public string PayrateDescription { get; set; }
        public int OfferType { get; set; }
        public string OfferNotes { get; set; }
    }
}
