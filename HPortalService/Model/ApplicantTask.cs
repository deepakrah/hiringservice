﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

using HPortalService.DAL;

namespace HPortalService
{
    public class ApplicantTask
    {
        public int ApplicantId { get; set; }
        public int JobApplicationId { get; set; }
        public int TaskId { get; set; }
        public string TaskName { get; set; }
        public string TaskUrl { get; set; }
        public int StatusId { get; set; }
        public string StatusName { get; set; }
        public int ApplicantTaskId { get; set; }
        public string Download { get; set; }
        public int OrderNo { get; set; }
        public int IsOptional { get; set; }
        public int VerifiedBy { get; set; }
        public int Type { get; set; }
        public string Question { get; set; }
        public string Answer { get; set; }
        public int IAgreed { get; set; }
        public int Phase { get; set; }
        public int BGStatus { get; set; }
        public int ResultId { get; set; }
        public string VerifiedDate { get; set; }
        public string CreatedDate { get; set; }
        public string DocumentPath { get; set; }
        public int UserId { get; set; }
        public int BranchId { get; set; }
        public string CertifyBy { get; set; }
        public string ReverifyBy { get; set; }
        public string OrientationDate { get; set; }
        public string OrientationTime { get; set; }
        public string VenueName { get; set; }
        public string Parking { get; set; }
        public string SignatureImage { get; set; }
        public string Dublicate { get; set; }
        public DataSet GetApplicantTask()
        {
            ApplicantTaskDAL objDAL = new ApplicantTaskDAL();
            return objDAL.GetApplicantTask(this);
        }
        public DataSet getApplicantSigninAddress()
        {
            ApplicantTaskDAL objDAL = new ApplicantTaskDAL();
            return objDAL.getApplicantSigninAddress(this);
        }
        public int InsApplicantTask(ApplicantTask obj)
        {
            ApplicantTaskDAL objDAL = new ApplicantTaskDAL();
            return objDAL.InsApplicantTask(obj);
        }
        public int GetApplicantCheckInStatus(ApplicantTask obj)
        {
            ApplicantTaskDAL objDAL = new ApplicantTaskDAL();
            return objDAL.GetApplicantCheckInStatus(obj);
        }

        public int fnUpApplicantResultId()
        {
            ApplicantTaskDAL objDAL = new ApplicantTaskDAL();
            return objDAL.UpApplicantResultId(this);
        }

        public DataSet GetEVerifyHistory()
        {
            ApplicantTaskDAL objDAL = new ApplicantTaskDAL();
            return objDAL.GetEVerifyHistory(this);
        }

        public DataSet GetApplicantTaskDocs()
        {
            ApplicantTaskDAL objDAL = new ApplicantTaskDAL();
            return objDAL.GetApplicantTaskDocs(this);
        }
        public DataSet GetApplicantAllTaskDocs()
        {
            ApplicantTaskDAL objDAL = new ApplicantTaskDAL();
            return objDAL.GetApplicantAllTaskDocs(this);
        }
        public DataSet GetAuditApplicantAllTaskForDoc()
        {
            ApplicantTaskDAL objDAL = new ApplicantTaskDAL();
            return objDAL.GetAuditApplicantAllTaskForDoc(this);
        }
        public DataSet fnGetApplicantTaskForDocDownload()
        {
            ApplicantTaskDAL objDAL = new ApplicantTaskDAL();
            return objDAL.GetApplicantTaskForDocDownload(this);
        }

        public DataSet ApplicantTaskUpdate()
        {
            ApplicantTaskDAL objDAL = new ApplicantTaskDAL();
            return objDAL.ApplicantTaskUpdate(this);
        }

        public int ApplicantUpdate()
        {
            ApplicantTaskDAL objDAL = new ApplicantTaskDAL();
            return objDAL.ApplicantUpdate(this);
        }
        public DataSet ApplicantTaskStatusUpdate()
        {
            ApplicantTaskDAL objDAL = new ApplicantTaskDAL();
            return objDAL.ApplicantTaskStatusUpdate(this);
        }
        public void SendBGCheckRequest(int ApplicantID)
        {
            //Applicant objApp = new Applicant();
            //DataSet ds = objApp.GetApplicantByID(ApplicantID);

            //webOnlineTestService.BAL.User objUser = new webOnlineTestService.BAL.User(LoggedInUser.UserId);
            //DataTable tbluser = objUser.GetSettings(SettingSections.SendEmail);
            //string recruiterEmail = "";
            //if (tbluser != null && tbluser.Rows.Count > 0)
            //{
            //    recruiterEmail = GetValue(tbluser, "CopyEmail");// Convert.ToString(tbluser.Rows[0]["CopyEmail"]);
            //}
            //else
            //{
            //    Branch objBranch = new Branch(LoggedInUser.BranchId);
            //    recruiterEmail = objBranch.EmailId;
            //}

            //if (ds != null && ds.Tables.Count > 0)
            //{
            //    StringBuilder sbEmployee = new StringBuilder();

            //    sbEmployee.Append("<?xml version=\"1.0\"?>");
            //    //sbEmployee.Append("<BackgroundCheck userId=\"csc_xml_test\" password=\"faPB5jN3n5pC\">");
            //    string userId = System.Configuration.ConfigurationManager.AppSettings["BGCheckUserId"];
            //    string password = System.Configuration.ConfigurationManager.AppSettings["BGCheckPwd"];
            //    string BGPackage = System.Configuration.ConfigurationManager.AppSettings["BGPackage"];
            //    string BGCrimePackage = System.Configuration.ConfigurationManager.AppSettings["BGCrimePackage"];
            //    sbEmployee.Append("<BackgroundCheck userId=\"" + userId + "\" password=\"" + password + "\">");




            //    DataTable dtEmployee = ds.Tables[0];

            //    if (dtEmployee.Rows.Count > 0)
            //    {
            //        DataRow dr = dtEmployee.Rows[0];
            //        String EncodedFile = "";
            //        string FileName = "";
            //        if (dr["IsFelony"].ToString() == "True" || dr["IsMisdemeanor"].ToString() == "True" || dr["IsParole"].ToString() == "True" || dr["IsProbation"].ToString() == "True" || dr["IsPendingTrial"].ToString() == "True")
            //        {
            //            FileName = dr["FirstName"].ToString() + dr["LastName"].ToString() + ".txt";
            //            MemoryStream ms = new MemoryStream();
            //            string NewFileName = ApplicantID + "_" + System.DateTime.Now.ToString("MMddyyyyHHmmss") + ".txt";
            //            StreamWriter sw = File.CreateText(Server.MapPath("Temp") + @"\" + NewFileName);
            //            if (dr["IsFelony"].ToString() == "True")
            //            {
            //                sw.WriteLine("Crime Type: Convicted of a felony");
            //                sw.WriteLine("Discription: " + dr["ConvictionNote"].ToString());
            //                sw.WriteLine("Date: " + dr["ConvictionDate"].ToString());
            //            }
            //            if (dr["IsMisdemeanor"].ToString() == "True")
            //            {
            //                sw.WriteLine("Crime Type: Convicted of a misdemeanor");
            //                sw.WriteLine("Discription: " + dr["ConvictionNote"].ToString());
            //                sw.WriteLine("Date: " + dr["ConvictionDate"].ToString());
            //            }
            //            if (dr["IsParole"].ToString() == "True")
            //            {
            //                sw.WriteLine("Crime Type: Currently on parole");
            //                sw.WriteLine("Discription: " + dr["ParoleProbationNote"].ToString());
            //            }
            //            if (dr["IsProbation"].ToString() == "True")
            //            {
            //                sw.WriteLine("Crime Type: Currently on probation");
            //                sw.WriteLine("Discription: " + dr["ParoleProbationNote"].ToString());
            //            }
            //            if (dr["IsPendingTrial"].ToString() == "True")
            //            {
            //                sw.WriteLine("Crime Type: State nature of the offense and the currently pending trial date");
            //                sw.WriteLine("Discription: " + dr["PendingTrialNote"].ToString());
            //            }
            //            sw.WriteLine("Crime State: " + dr["CrimeState"].ToString());
            //            sw.WriteLine("Crime City: " + dr["CrimeCity"].ToString());
            //            sw.WriteLine("Crime County: " + dr["CrimeCounty"].ToString());
            //            sw.WriteLine("Crime Zip: " + dr["CrimeZip"].ToString());
            //            sw.Flush();
            //            sw.Close();
            //            Byte[] bytes = File.ReadAllBytes(Server.MapPath("Temp") + @"\" + NewFileName);
            //            EncodedFile = Convert.ToBase64String(bytes);
            //        }
            //        sbEmployee.Append("<BackgroundSearchPackage  action=\"submit\" type=\"" + (EncodedFile == "" ? BGPackage : BGCrimePackage) + "\">");
            //        sbEmployee.Append("<Organization  type=\"x:requesting\">");
            //        sbEmployee.Append("<OrganizationName>");
            //        sbEmployee.Append(Convert.ToString(ds.Tables[0].Rows[0]["JDPLocationCode"]));
            //        sbEmployee.Append("</OrganizationName>");
            //        sbEmployee.Append("</Organization>");
            //        sbEmployee.Append("<ReferenceId>");
            //        sbEmployee.Append("HP-" + ApplicantID);
            //        sbEmployee.Append("</ReferenceId>");

            //        sbEmployee.Append("<PersonalData><PersonName><GivenName>");
            //        sbEmployee.Append(dr["FirstName"].ToString());
            //        sbEmployee.Append("</GivenName>");
            //        sbEmployee.Append("<FamilyName>");
            //        sbEmployee.Append(dr["LastName"].ToString());
            //        sbEmployee.Append("</FamilyName>");
            //        sbEmployee.Append("<MiddleName>");
            //        sbEmployee.Append(dr["MiddleName"].ToString());
            //        sbEmployee.Append("</MiddleName>");
            //        sbEmployee.Append("</PersonName>");
            //        sbEmployee.Append("<DemographicDetail><GovernmentId countryCode=\"US\" issuingAuthority=\"SSN\">");
            //        sbEmployee.Append(dr["SSN"].ToString());
            //        sbEmployee.Append("</GovernmentId>");
            //        sbEmployee.Append("<DateOfBirth>");
            //        sbEmployee.Append(dr["DOB"].ToString());
            //        sbEmployee.Append("</DateOfBirth></DemographicDetail>");
            //        sbEmployee.Append("<PostalAddress>");
            //        sbEmployee.Append("<PostalCode>");
            //        sbEmployee.Append(dr["ZipCode"].ToString());
            //        sbEmployee.Append("</PostalCode>");

            //        sbEmployee.Append("<Region>");
            //        sbEmployee.Append(dr["Abbreviation"].ToString());
            //        sbEmployee.Append("</Region>");

            //        sbEmployee.Append("<Municipality>");
            //        sbEmployee.Append(dr["City"].ToString());
            //        sbEmployee.Append("</Municipality>");

            //        sbEmployee.Append("<DeliveryAddress><StreetName>");
            //        sbEmployee.Append(dr["Address"].ToString());

            //        sbEmployee.Append("</StreetName></DeliveryAddress>");

            //        sbEmployee.Append("</PostalAddress>");

            //        sbEmployee.Append("<EmailAddress>");
            //        sbEmployee.Append(dr["EmailId"].ToString());
            //        sbEmployee.Append("</EmailAddress>");
            //        sbEmployee.Append("</PersonalData>");



            //        sbEmployee.Append("<Screenings useConfigurationDefaults=\"yes\">");
            //        sbEmployee.Append("<!-- Optional URL to which to post XML status udpates -->");
            //        sbEmployee.Append("<AdditionalItems type=\"x:postback_url\"> <Text>");
            //        // sbEmployee.Append("http://wish.protatechindia.com/WebAPIService/api/products");
            //        sbEmployee.Append("http://jdp.schedulingsite.com/api/products");
            //        sbEmployee.Append("</Text></AdditionalItems>");
            //        sbEmployee.Append("<!-- Integration Type -->");
            //        sbEmployee.Append("<AdditionalItems type=\"x:integration_type\"><Text>");
            //        sbEmployee.Append("Woodside");
            //        sbEmployee.Append("</Text></AdditionalItems>");

            //        sbEmployee.Append("<!-- Order Notes -->");
            //        sbEmployee.Append("<AdditionalItems type=\"x:ordernotes\"><Text>");
            //        sbEmployee.Append(recruiterEmail);
            //        sbEmployee.Append("</Text></AdditionalItems>");

            //        sbEmployee.Append("<!-- Descision Model -->");
            //        sbEmployee.Append("<AdditionalItems type=\"x:decision_model\"><Text>");
            //        sbEmployee.Append("REPORT");
            //        sbEmployee.Append("</Text></AdditionalItems>");

            //        sbEmployee.Append("<!-- supress xpartial-->");
            //        sbEmployee.Append("<AdditionalItems type=\"x:postback_suppress_xpartial\"><Text>");
            //        sbEmployee.Append("true");
            //        sbEmployee.Append("</Text></AdditionalItems>");

            //        sbEmployee.Append("</Screenings>");
            //        if (EncodedFile != "")
            //        {
            //            sbEmployee.Append("<SupportingDocumentation><OriginalFileName>" + FileName + "</OriginalFileName><Name>Self-disclosure</Name>");
            //            sbEmployee.Append("<EncodedContent>" + EncodedFile + "</EncodedContent></SupportingDocumentation>");
            //        }
            //        sbEmployee.Append("</BackgroundSearchPackage></BackgroundCheck>");

            //        //        var client = new RestSharp.RestClient("https://www.jdpalatine.net/send/interchange");
            //        try
            //        {
            //            ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;
            //            WebClient clnt = new WebClient();

            //            clnt.Headers.Add("postman-token:99b5138f-adb3-db43-e514-9279b10755c6");
            //            clnt.Headers.Add("cache-control:no-cache");
            //            clnt.Headers.Add("authorization:Basic Y3NjX3htbF90ZXN0OmZhUEI1ak4zbjVwQw==");
            //            string outPut = clnt.UploadString("https://www.jdpalatine.net/send/interchange", "POST", sbEmployee.ToString());

            //            //ErrorLogger.Log(outPut);
            //            XmlDocument doc = new XmlDocument();
            //            doc.LoadXml(outPut);
            //            string fileName = ApplicantID.ToString() + "_" + DateTime.Now.ToFileTime().ToString() + ".xml";
            //            string fileToSave = System.Configuration.ConfigurationManager.AppSettings["HPortalResponse"];
            //            fileToSave = fileToSave + "\\" + fileName;
            //            doc.Save(fileToSave);
            //            //  bool outPut = objApplicant.UpdateBGCheckStatus(Convert.ToInt32(strApId), status, "");
            //            //return outPut ? "SUCCESS" : "ERROR";
            //            //Response.Write(outPut);
            //            XmlNodeList listNodes = doc.GetElementsByTagName("OrderId");

            //            if (listNodes != null && listNodes.Count > 0)
            //            {
            //                string orderId = listNodes[0].InnerText;
            //                if (!String.IsNullOrEmpty(orderId))
            //                {
            //                    objApp.UpdateBGCheckOrderId(ApplicantID, orderId);
            //                }
            //            }
            //            else
            //            {
            //                listNodes = doc.GetElementsByTagName("OrderStatus");
            //                if (listNodes != null && listNodes.Count > 0)
            //                {
            //                    string status = listNodes[0].InnerText;
            //                    if (status == "x:error")
            //                    {
            //                        listNodes = doc.GetElementsByTagName("ErrorDescription");
            //                        if (listNodes != null && listNodes.Count > 0)
            //                        {
            //                            string errorMessage = listNodes[0].InnerText;
            //                            objApp.UpdateBGCheckStatus(ApplicantID, 4, errorMessage);
            //                        }
            //                    }
            //                }
            //            }
            //        }
            //        catch (Exception ex)
            //        {
            //            SendMail("support@protatech.com", "PDevadoss@protatech.com", "Hportal Error: JDP", "ApplicantID : " + ApplicantID + ", " + ex.Message, "draheja@protatechindia.com", "vprajapati@protatechindia.com");
            //            ErrorLogger.Log(ex.Message);
            //            throw ex;
            //        }

            //    }
            //}
        }


    }
}