﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

using HPortalService.DAL;
namespace HPortalService
{
    public class ApplicantRenewal
    {

        public string ApplicantRenewalid { get; set; }
        public int ApplicantId { get; set; }
        public string CompanyName { get; set; }
        public string LicenseeFileNo { get; set; }
        public string TradeName { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string zipcode { get; set; }
        public string DayTimePhone { get; set; }
        public string FAX { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }

        public string Suffix { get; set; }
        public string EmployeeRegCardNo { get; set; }
        public string HomeAddress { get; set; }
        public string Phone { get; set; }
        public string DOB { get; set; }
        public string SSN { get; set; }

        public string EmpCity { get; set; }
        public string EmpState { get; set; }
        public string EmpZipCode { get; set; }
        public string EmpCountry { get; set; }

        public string ScarsMarks { get; set; }
        public string Height { get; set; }
        public string Weight { get; set; }



        public string HairColor { get; set; }
        public string EyeColor { get; set; }
        public string FormerFirstName { get; set; }
        public string FormerMiddleName { get; set; }
        public string FormerLastName { get; set; }

        public string IsVeteran { get; set; }
        public string IsAvailability { get; set; }
        public string IsConvicted { get; set; }
        public string IsCertify { get; set; }



        public int IntEmpRegRenewal(ApplicantRenewal obj)
        {
            ApplicantRenewalDAL _ApplicationDAL = new ApplicantRenewalDAL();
            return _ApplicationDAL.IntEmpRegRenewal(obj);
        }
        public DataSet GetApplicantRenewalByID(int ID)
        {
            ApplicantRenewalDAL _ApplicationDAL = new ApplicantRenewalDAL();
            return _ApplicationDAL.GettApplicantRenewalByID(ID);
        }
    }
}
