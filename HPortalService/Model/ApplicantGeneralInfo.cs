﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

using HPortalService.DAL;

namespace HPortalService
{
    public class ApplicantGeneralInfo
    {
        public int JobApplicationId { get; set; }
        public Boolean IsAdult { get; set; }
        public Boolean SubmitVerification { get; set; }
        public Boolean HiredByCSC { get; set; }
        public string Location { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public Boolean RelativeCSC { get; set; }
        public string RelativeName { get; set; }
        public Boolean ForeignLanguage { get; set; }
        public string LanguageName { get; set; }
        public string AvailableDate { get; set; }
        public Boolean WorkedOnNightWeekend { get; set; }
        public Boolean RellableTransport { get; set; }
        public string PrevFirstName { get; set; }
        public string PrevMiddleName { get; set; }
        public string PrevLastName { get; set; }
        public string Suffix { get; set; }
        public Boolean ArmedForce { get; set; }
        public string Rank { get; set; }
        public string Duties { get; set; }
        public Nullable<int> EmployeeID { get; set; }
        public bool ReEmployee { get; set; }
        public int RehireSourceId { get; set; }
        public int JobPositionId { get; set; }
        public int SaveApplicantGeneralInfo(ApplicantGeneralInfo obj)
        {
            ApplicantGeneralInfoDAL _AppGeneralInfo = new ApplicantGeneralInfoDAL();
            return _AppGeneralInfo.UpdateApplicantGeneralInfo(obj);
        }

        public DataSet SearchApplicantSupport(String Fname, String Lname, String Mobile, String EmailID, String SSN)
        {
            ApplicantGeneralInfoDAL _AppGeneralInfo = new ApplicantGeneralInfoDAL();
            return _AppGeneralInfo.SearchApplicantSupport(Fname,Lname,Mobile,EmailID,SSN);
        }

        public List<ApplicantGeneralInfo> GetGeneralInfoByJobApplicantId(int JobApplicantId)
        {
            DataSet ds;
            ApplicantGeneralInfoDAL _AppInfo = new ApplicantGeneralInfoDAL();
            ds = _AppInfo.GetGeneralInfoByJobApplicantId(JobApplicantId);
            List<ApplicantGeneralInfo> lstApplicant = SQLHelper.ContructList<ApplicantGeneralInfo>(ds);
            foreach (ApplicantGeneralInfo obj in lstApplicant)
            {
                obj.StartDate =obj.StartDate!=null?Convert.ToDateTime(obj.StartDate).ToString("MM/dd/yyyy") :null;
                obj.EndDate =obj.EndDate!=null? Convert.ToDateTime(obj.EndDate).ToString("MM/dd/yyyy") :null;
                obj.AvailableDate = obj.AvailableDate != null ? Convert.ToDateTime(obj.AvailableDate).ToString("MM/dd/yyyy") : null;
            }
            return lstApplicant;
        }
    }
}
