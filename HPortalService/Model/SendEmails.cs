﻿using System;
using System.Configuration;
using System.Data;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Collections.Generic;
using System.Collections;
using System.IO;
using System.Threading;
using HPortalService;
using Microsoft.Extensions.Configuration;
using HPortalService.Model;
using System.Drawing;

/// <summary>
/// Summary description for SendEmails
/// </summary>
public class SendEmails
{
    public enum EStatus
    {

        All = 0,
        Applied = 1,
        Schedule_for_Interview = 2,
        Offered = 3,
        Scehdule_for_NAO = 4,
        Hired = 5,
        Reject = 6,
        JobApplication = 7,
        Registration = 8,
        Schedule_for_Orientation = 9,
        Offer_Rejected = 10,
        Manager_Approval = 11,
        Applying = 12,
        NoShowInterview = 13,
        NoShowOrientation = 14,
        Attended_Interview = 15,
        Attended_Orientation = 16,
        Readdress = 17,
        UnmarkNoShow = 18,
        UnmarkAttended = 19,
        Board = 20,
        PasswordReset = 21,
        Schedule_for_OnlineInterview = 22,
        Manager_Offered = 23,
        Schedule_for_OrientationNew = 24,
        Manager_Answer = 25,
        OnlineInterview_Complete_Later = 26,
        NewApplicant = 28
    }
    //public static readonly logger = "";//LogManager.GetLogger(typeof(SendEmails));
    private static IConfiguration configuration;

    public void EmailSetting(IConfiguration iConfig)
    {
        configuration = iConfig;
    }

    public int PlayerId { get; set; }
    public SendEmails()
    {

    }

    static string UsesmtpSSL = Startup.UsesmtpSSL;
    static string enableMail = Startup.enableMail;
    static string mailServer = Startup.mailServer;
    static string userId = Startup.userId;
    static string password = Startup.password;
    static string authenticate = Startup.authenticate;
    static string AdminEmailID = Startup.AdminEmailID;
    static string fromEmailID = Startup.fromEmailID;
    static string DomainName = Startup.DomainName;
    static string AllowSendMails = Startup.AllowSendMails;
    static string UserName = Startup.UserName;

    public void setMailContent(int ApplicantId, string Type, string subject = null, string emailBody = null)
    {
        var sendOnType = (EStatus)Enum.Parse(typeof(EStatus), Type);

        SendEmailUserInfo objuserInfo = GetUserInfo(ApplicantId, sendOnType);

        switch (sendOnType)
        {
            //case EStatus.Applied:
            //    {
            //        EmailParameters emailParameters = new EmailParameters()
            //        {
            //            UserName = objuserInfo.FullName,

            //            XMLFilePath = "newApplication.xsl",
            //            Subject = "Thank you for submitting your application"
            //        };

            //        SendEmail(emailParameters, objuserInfo.Email);
            //    }
            //    break;

            case EStatus.Hired:
                {
                    EmailParameters emailParameters = new EmailParameters()
                    {
                        UserName = objuserInfo.FullName,
                        BranchEmail = objuserInfo.BranchEmail,
                        Branch = objuserInfo.Branch,
                        XMLFilePath = "5",
                        BranchId = objuserInfo.BranchId,
                        WOTCLink = "https://ims.employmenttax.com/wotc/?id=3992",
                        Subject = "Welcome to Team Woodside!"
                    };

                    SendEmail(emailParameters, objuserInfo.Email);
                }
                break;

            case EStatus.Reject:
                {
                    EmailParameters emailParameters = new EmailParameters()
                    {
                        UserName = objuserInfo.FullName,
                        EmailBody = emailBody,
                        BranchEmail = objuserInfo.BranchEmail,
                        Branch = objuserInfo.Branch,
                        XMLFilePath = "6",
                        BranchId = objuserInfo.BranchId,
                        Subject = !String.IsNullOrEmpty(subject) ? subject : "We appreciate the interest shown by you and the time you have spent in application process"
                    };

                    SendEmail(emailParameters, objuserInfo.Email);
                }
                break;

            case EStatus.Manager_Approval:
                {
                    EmailParameters emailParameters = new EmailParameters()
                    {
                        UserName = objuserInfo.FullName,
                        EmailBody = emailBody,
                        BranchEmail = objuserInfo.BranchEmail,
                        Branch = objuserInfo.Branch,
                        XMLFilePath = "11",
                        BranchId = objuserInfo.BranchId,
                        OfferLetterLink = Startup.WebSiteURL + "/OfferLetter/" + objuserInfo.BranchId + "/" + ApplicantId,
                        Subject = !String.IsNullOrEmpty(subject) ? subject : "Woodside Offer Letter"
                    };

                    Branch Obj = new Branch();

                    DataTable dt = Obj.GetBranchIsTask(objuserInfo.BranchId).Tables[0];
                    if (dt.Rows.Count > 0)
                    {
                        if (Convert.ToBoolean(dt.Rows[0]["IsBranchTask"].ToString()) && Convert.ToInt32(dt.Rows[0]["TotalBranchTask"].ToString()) > 0)
                        {
                            SendEmail(emailParameters, objuserInfo.Email);
                        }
                    }
                }
                break;
            case EStatus.Manager_Offered:
                {
                    EmailParameters emailParameters = new EmailParameters()
                    {
                        UserName = objuserInfo.FullName,
                        EmailBody = emailBody,
                        BranchEmail = objuserInfo.BranchEmail,
                        Branch = objuserInfo.Branch,
                        XMLFilePath = "12",
                        BranchId = objuserInfo.BranchId,
                        OfferLetterLink = Startup.WebSiteURL + "/OfferLetter/" + objuserInfo.BranchId + "/" + ApplicantId,
                        Subject = !String.IsNullOrEmpty(subject) ? subject : "Additional task"
                    };

                    Branch Obj = new Branch();
                    DataTable dt = Obj.GetBranchIsTask(objuserInfo.BranchId).Tables[0];
                    if (dt.Rows.Count > 0)
                    {
                        if (Convert.ToBoolean(dt.Rows[0]["IsBranchTask"].ToString()) && Convert.ToInt32(dt.Rows[0]["TotalBranchTask"].ToString()) > 0)
                        {
                            SendEmail(emailParameters, objuserInfo.Email);
                        }
                    }
                }
                break;
            case EStatus.Scehdule_for_NAO:
                {
                    EmailParameters emailParameters = new EmailParameters()
                    {
                        UserName = objuserInfo.FullName,
                        BranchEmail = objuserInfo.BranchEmail,
                        Branch = objuserInfo.Branch,
                        XMLFilePath = "4",
                        BranchId = objuserInfo.BranchId,
                        Subject = "Congratulations you have been approved for the NAO (New Application Orientation)"
                    };

                    SendEmail(emailParameters, objuserInfo.Email);
                }
                break;

            case EStatus.JobApplication:
                {
                    EmailParameters emailParameters = new EmailParameters()
                    {
                        ApplicationNumber = objuserInfo.ApplicationNumber,
                        FirstName = objuserInfo.FirstName,
                        Email = objuserInfo.Email,
                        LastName = objuserInfo.LastName,
                        Address = objuserInfo.Address,
                        UserName = objuserInfo.FullName,
                        BranchEmail = objuserInfo.BranchEmail,
                        Branch = objuserInfo.Branch,
                        BranchId = objuserInfo.BranchId,
                        XMLFilePath = "7",
                        Subject = "You have received a job application for the position of event staff"
                    };

                    SendEmail(emailParameters, (AdminEmailID));
                }
                break;
            case EStatus.Manager_Answer:
                {
                    EmailParameters emailParameters = new EmailParameters()
                    {
                        ApplicationNumber = objuserInfo.ApplicationNumber,
                        FirstName = objuserInfo.FirstName,
                        Email = objuserInfo.Email,
                        LastName = objuserInfo.LastName,
                        Address = objuserInfo.Address,
                        UserName = objuserInfo.FullName,
                        BranchEmail = objuserInfo.BranchEmail,
                        Branch = objuserInfo.Branch,
                        BranchId = objuserInfo.BranchId,
                        XMLFilePath = "25",
                        Subject = "Task Answer"
                    };
                    SendEmail(emailParameters, objuserInfo.Email);
                }
                break;
            case EStatus.Offered:
                {
                    /* Commented by Yogesh On 02/12/2015
                     EmailParameters emailParameters = new EmailParameters()
                     {
                         UserName = objuserInfo.FullName,
                         ApplicationNumber = DomainName + "SearchImage.ashx?JAPPNum=" + objuserInfo.ApplicationNumber,
                         XMLFilePath = "applicationOffered.xsl",
                         Subject = " Woodside Properties is offering you to work with Us."
                     };
                     PDFDocs objPdf = new PDFDocs();
                     objPdf.CreateOffer(HttpContext.Current.Server.MapPath("~/PDF/Offers/" + ApplicantId + ".pdf"), HttpContext.Current.Server.MapPath("~/"), ApplicantId);
                     String Attachment = HttpContext.Current.Server.MapPath("~/PDF/Offers/" + ApplicantId + ".pdf");
                     System.Net.Mime.ContentType contentType = new System.Net.Mime.ContentType();
                     contentType.MediaType = System.Net.Mime.MediaTypeNames.Application.Octet;
                     contentType.Name = "Offer.pdf";
                     List<Attachment> lstAttachment = new List<System.Net.Mail.Attachment>();
                     Attachment attachment = new Attachment(Attachment, contentType);
                     lstAttachment.Add(attachment);
                     SendEmail(emailParameters, objuserInfo.Email, lstAttachment);
                     * */
                }
                break;
            case EStatus.Registration:
                {
                    EmailParameters emailParameters = new EmailParameters()
                    {
                        UserName = objuserInfo.FullName,
                        Password = objuserInfo.Password,
                        XMLFilePath = "8",
                        BranchEmail = objuserInfo.BranchEmail,
                        Branch = objuserInfo.Branch,
                        BranchId = objuserInfo.BranchId,
                        Subject = "Application Received"
                    };

                    SendEmail(emailParameters, objuserInfo.Email);
                }
                break;
            case EStatus.NewApplicant:
                {
                    EmailParameters emailParameters = new EmailParameters()
                    {
                        UserName = objuserInfo.FullName,
                        JobPosition = objuserInfo.JobPosition,
                        XMLFilePath = "28",
                        BranchEmail = objuserInfo.BranchEmail,
                        Branch = objuserInfo.Branch,
                        BranchId = objuserInfo.BranchId,
                        Subject = "New Applicant"
                    };
                    //manager mail Id
                    objuserInfo.Email = "akinser@woodsidedells.com";
                    SendEmail(emailParameters, objuserInfo.Email);
                }
                break;                
            case EStatus.Schedule_for_Orientation:
                {
                    EmailParameters emailParameters = new EmailParameters()
                    {
                        UserName = objuserInfo.FullName,
                        ApplicationNumber = GenereateBarCode(objuserInfo.ApplicationNumber),
                        VenueName = objuserInfo.VenueName,
                        InterviewDate = objuserInfo.InterviewDate,
                        InterviewTime = objuserInfo.InterviewTime,
                        RoomNo = objuserInfo.RoomNo,
                        ParkingInstructions = objuserInfo.ParkingInstructions,
                        InterviewType = objuserInfo.InterviewType,
                        StartTime = objuserInfo.StartTime,
                        EndTime = objuserInfo.EndTime,
                        Duration = objuserInfo.Duration,
                        Sessions = objuserInfo.Sessions,
                        BranchEmail = objuserInfo.BranchEmail,
                        Branch = objuserInfo.Branch,
                        BranchId = objuserInfo.BranchId,
                        Subject = "Woodside Application Orientation Scheduled",
                        XMLFilePath = "9"
                    };
                    //Appointment objNewApp = new Appointment();
                    //String stb = objNewApp.MakeHourEvent("CSC Application Orientation Scheduled", objuserInfo.VenueAddress, objuserInfo.InterviewDate, objuserInfo.StartTime, objuserInfo.EndTime);
                    //List<Attachment> lstAttachment = new List<System.Net.Mail.Attachment>();
                    //System.IO.MemoryStream ms = new System.IO.MemoryStream(Encoding.ASCII.GetBytes(stb));
                    //System.Net.Mime.ContentType typeCalendar = new System.Net.Mime.ContentType("text/calendar");
                    //System.Net.Mail.Attachment attach1 = new System.Net.Mail.Attachment(ms, typeCalendar);
                    //attach1.ContentDisposition.FileName = "interview.ics";
                    //lstAttachment.Add(attach1);


                    AlternateView fileName = null;
                    Venue obj = new Venue();
                    try
                    {
                        DataSet ds = obj.GetVenueMapByVenueId(Convert.ToInt32(objuserInfo.VenueId.ToString()));

                        //if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                        //{
                        //    lstAttachment = new List<System.Net.Mail.Attachment>();
                        //    if (ds.Tables[0].Rows[0]["DrivingDirectionFile"] != "" && ds.Tables[0].Rows[0]["DrivingDirectionFile"] != null)
                        //    {
                        //        var uri = WishDirectoryPath + "DrivingDir/" + ds.Tables[0].Rows[0]["DrivingDirectionFile"];
                        //        System.Drawing.Image img = DownloadImageFromUrl(uri);
                        //        var stream = new MemoryStream();
                        //        img.Save(stream, System.Drawing.Imaging.ImageFormat.Jpeg);
                        //        stream.Position = 0;
                        //        Attachment attach = new Attachment(stream, "DrivingDirection.jpg", "image/jpg");
                        //        lstAttachment.Add(attach);
                        //    }
                        //    if (ds.Tables[0].Rows[0]["Map"] != "" && ds.Tables[0].Rows[0]["Map"] != null)
                        //    {
                        //        var uri = WishDirectoryPath + "map/" + ds.Tables[0].Rows[0]["Map"];
                        //        System.Drawing.Image img = DownloadImageFromUrl(uri);
                        //        var stream = new MemoryStream();
                        //        img.Save(stream, System.Drawing.Imaging.ImageFormat.Jpeg);
                        //        stream.Position = 0;
                        //        Attachment attach = new Attachment(stream, "map.jpg", "image/jpg");
                        //        lstAttachment.Add(attach);
                        //    }
                        //}
                    }
                    catch { }

                    //SendEmail(emailParameters, objuserInfo.Email, lstAttachment, fileName);
                }
                break;
            case EStatus.Schedule_for_OrientationNew:
                {
                    EmailParameters emailParameters = new EmailParameters()
                    {
                        UserName = objuserInfo.FullName,
                        ApplicationNumber = GenereateBarCode(objuserInfo.ApplicationNumber),
                        VenueName = objuserInfo.VenueName,
                        InterviewDate = objuserInfo.InterviewDate,
                        InterviewTime = objuserInfo.InterviewTime,
                        RoomNo = objuserInfo.RoomNo,
                        ParkingInstructions = objuserInfo.ParkingInstructions,
                        InterviewType = objuserInfo.InterviewType,
                        StartTime = objuserInfo.StartTime,
                        EndTime = objuserInfo.EndTime,
                        Duration = objuserInfo.Duration,
                        Sessions = objuserInfo.Sessions,
                        BranchEmail = objuserInfo.BranchEmail,
                        Branch = objuserInfo.Branch,
                        BranchId = objuserInfo.BranchId,
                        Subject = "Woodside Orientation Details and Paperwork",
                        XMLFilePath = "24",
                        OfferLetterLink = Startup.WebSiteURL + "/OfferLetter/" + objuserInfo.BranchId + "/" + ApplicantId,
                    };
                    //Appointment objNewApp = new Appointment();
                    //String stb = objNewApp.MakeHourEvent("Woodside Application Orientation Scheduled", objuserInfo.VenueAddress, objuserInfo.InterviewDate, objuserInfo.StartTime, objuserInfo.EndTime);
                    //List<Attachment> lstAttachment = new List<System.Net.Mail.Attachment>();
                    //System.IO.MemoryStream ms = new System.IO.MemoryStream(Encoding.ASCII.GetBytes(stb));
                    //System.Net.Mime.ContentType typeCalendar = new System.Net.Mime.ContentType("text/calendar");
                    //System.Net.Mail.Attachment attach1 = new System.Net.Mail.Attachment(ms, typeCalendar);
                    //attach1.ContentDisposition.FileName = "interview.ics";
                    //lstAttachment.Add(attach1);
                    //AlternateView fileName = null;
                    //Venue obj = new Venue();
                    try
                    {
                        //DataSet ds = obj.GetVenueMapByVenueId(Convert.ToInt32(objuserInfo.VenueId.ToString()));
                        //if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                        //{
                        //    //lstAttachment = new List<System.Net.Mail.Attachment>();
                        //    //if (ds.Tables[0].Rows[0]["DrivingDirectionFile"] != "" && ds.Tables[0].Rows[0]["DrivingDirectionFile"] != null)
                        //    //{
                        //    //    var uri = WishDirectoryPath + "DrivingDir/" + ds.Tables[0].Rows[0]["DrivingDirectionFile"];
                        //    //    System.Drawing.Image img = DownloadImageFromUrl(uri);
                        //    //    var stream = new MemoryStream();
                        //    //    img.Save(stream, System.Drawing.Imaging.ImageFormat.Jpeg);
                        //    //    stream.Position = 0;
                        //    //    Attachment attach = new Attachment(stream, "DrivingDirection.jpg", "image/jpg");
                        //    //    lstAttachment.Add(attach);
                        //    //}
                        //    //if (ds.Tables[0].Rows[0]["Map"] != "" && ds.Tables[0].Rows[0]["Map"] != null)
                        //    //{
                        //    //    var uri = WishDirectoryPath + "map/" + ds.Tables[0].Rows[0]["Map"];
                        //    //    System.Drawing.Image img = DownloadImageFromUrl(uri);
                        //    //    var stream = new MemoryStream();
                        //    //    img.Save(stream, System.Drawing.Imaging.ImageFormat.Jpeg);
                        //    //    stream.Position = 0;
                        //    //    Attachment attach = new Attachment(stream, "map.jpg", "image/jpg");
                        //    //    lstAttachment.Add(attach);
                        //    //}
                        //}
                    }
                    catch { }
                    SendEmail(emailParameters, objuserInfo.Email);
                }
                break;
            case EStatus.PasswordReset:
                {
                    EmailParameters emailParameters = new EmailParameters()
                    {
                        UserName = objuserInfo.FullName,
                        Password = objuserInfo.Password,
                        Subject = "Password reset successfully.",
                        BranchEmail = objuserInfo.BranchEmail,
                        Branch = objuserInfo.Branch,
                        XMLFilePath = "21",
                        BranchId = objuserInfo.BranchId,
                    };
                    SendEmail(emailParameters, objuserInfo.Email);
                }
                break;
            case EStatus.Readdress:
                {
                    EmailParameters emailParameters = new EmailParameters()
                    {
                        UserName = objuserInfo.FullName,
                        Password = objuserInfo.Password,
                        EmailBody = emailBody,
                        BranchEmail = objuserInfo.BranchEmail,
                        Branch = objuserInfo.Branch,
                        BranchId = objuserInfo.BranchId,
                        Subject = !String.IsNullOrEmpty(subject) ? subject : "Complete your Job Application with Woodside",
                        XMLFilePath = "17"
                    };
                    SendEmail(emailParameters, objuserInfo.Email);
                }
                break;
            case EStatus.Schedule_for_Interview:
                {


                    EmailParameters emailParameters = new EmailParameters()
                    {
                        UserName = objuserInfo.FullName,
                        ApplicationNumber = GenereateBarCode(objuserInfo.ApplicationNumber),
                        VenueName = objuserInfo.VenueName,
                        InterviewDate = objuserInfo.InterviewDate,
                        InterviewTime = objuserInfo.InterviewTime,
                        RoomNo = objuserInfo.RoomNo,
                        ParkingInstructions = objuserInfo.ParkingInstructions,
                        InterviewType = objuserInfo.InterviewType,
                        StartTime = objuserInfo.StartTime,
                        EndTime = objuserInfo.EndTime,
                        Duration = objuserInfo.Duration,
                        Sessions = objuserInfo.Sessions,
                        BranchEmail = objuserInfo.BranchEmail,
                        BranchId = objuserInfo.BranchId,
                        Branch = objuserInfo.Branch,
                        Subject = "Woodside Interview Details"

                    };



                    emailParameters.XMLFilePath = objuserInfo.InterviewType;

                    Appointment objNewApp = new Appointment();
                    String stb = objNewApp.MakeHourEvent("Woodside Application Interview Scheduled", objuserInfo.VenueAddress, objuserInfo.InterviewDate, objuserInfo.StartTime, objuserInfo.EndTime);
                    List<Attachment> lstAttachment = new List<System.Net.Mail.Attachment>();
                    System.IO.MemoryStream ms = new System.IO.MemoryStream(Encoding.ASCII.GetBytes(stb));
                    System.Net.Mime.ContentType typeCalendar = new System.Net.Mime.ContentType("text/calendar");
                    System.Net.Mail.Attachment attach1 = new System.Net.Mail.Attachment(ms, typeCalendar);
                    attach1.ContentDisposition.FileName = "interview.ics";
                    lstAttachment.Add(attach1);





                    AlternateView fileName = null;
                    Venue obj = new Venue();


                    try
                    {
                        DataSet ds = obj.GetVenueMapByVenueId(Convert.ToInt32(objuserInfo.VenueId.ToString()));

                        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                        {
                            lstAttachment = new List<System.Net.Mail.Attachment>();
                            if (ds.Tables[0].Rows[0]["DrivingDirectionFile"].ToString() != "" && ds.Tables[0].Rows[0]["DrivingDirectionFile"] != null)
                            {
                                //var uri = WishDirectoryPath + "DrivingDir/" + ds.Tables[0].Rows[0]["DrivingDirectionFile"];
                                //System.Drawing.Image img = DownloadImageFromUrl(uri);
                                //var stream = new MemoryStream();
                                //img.Save(stream, System.Drawing.Imaging.ImageFormat.Jpeg);
                                //stream.Position = 0;
                                //Attachment attach = new Attachment(stream, "DrivingDirection.jpg", "image/jpg");
                                //lstAttachment.Add(attach);
                            }
                            if (ds.Tables[0].Rows[0]["Map"].ToString() != "" && ds.Tables[0].Rows[0]["Map"] != null)
                            {
                                //var uri = WishDirectoryPath + "map/" + ds.Tables[0].Rows[0]["Map"];
                                //System.Drawing.Image img = DownloadImageFromUrl(uri);
                                //var stream = new MemoryStream();
                                //img.Save(stream, System.Drawing.Imaging.ImageFormat.Jpeg);
                                //stream.Position = 0;
                                //Attachment attach = new Attachment(stream, "map.jpg", "image/jpg");
                                //lstAttachment.Add(attach);
                            }
                        }
                    }
                    catch { }
                    SendEmail(emailParameters, objuserInfo.Email, lstAttachment, fileName);
                }
                break;
            case EStatus.Schedule_for_OnlineInterview:
                {
                    EmailParameters emailParameters = new EmailParameters()
                    {
                        UserName = objuserInfo.FullName,
                        ApplicationNumber = GenereateBarCode(objuserInfo.ApplicationNumber),
                        InterviewDate = objuserInfo.InterviewDate,
                        InterviewTime = objuserInfo.InterviewTime,
                        StartTime = objuserInfo.StartTime,
                        EndTime = objuserInfo.EndTime,
                        Duration = objuserInfo.Duration,
                        BranchEmail = objuserInfo.BranchEmail,
                        BranchId = objuserInfo.BranchId,
                        Branch = objuserInfo.Branch,
                        Subject = "Woodside Application Online Interview Scheduled"
                    };
                    emailParameters.XMLFilePath = objuserInfo.InterviewType;
                    Appointment objNewApp = new Appointment();
                    String stb = objNewApp.MakeHourEvent("Woodside Application Online Interview Scheduled", objuserInfo.VenueAddress, objuserInfo.InterviewDate, objuserInfo.StartTime, objuserInfo.EndTime);
                    List<Attachment> lstAttachment = new List<System.Net.Mail.Attachment>();
                    System.IO.MemoryStream ms = new System.IO.MemoryStream(Encoding.ASCII.GetBytes(stb));
                    System.Net.Mime.ContentType typeCalendar = new System.Net.Mime.ContentType("text/calendar");
                    System.Net.Mail.Attachment attach1 = new System.Net.Mail.Attachment(ms, typeCalendar);
                    attach1.ContentDisposition.FileName = "interview.ics";
                    lstAttachment.Add(attach1);
                    AlternateView fileName = null;
                    SendEmail(emailParameters, objuserInfo.Email, lstAttachment, fileName);
                }
                break;
            case EStatus.OnlineInterview_Complete_Later:
                {
                    EmailParameters emailParameters = new EmailParameters()
                    {
                        UserName = objuserInfo.FullName,
                        EmailBody = emailBody,
                        BranchEmail = objuserInfo.BranchEmail,
                        Branch = objuserInfo.Branch,
                        BranchId = objuserInfo.BranchId,
                        OnlineInterview = objuserInfo.OnlineInterview,
                        Subject = !String.IsNullOrEmpty(subject) ? subject : "Online Interview",
                        XMLFilePath = "26"
                    };
                    SendEmail(emailParameters, objuserInfo.Email);
                }
                break;
        }

    }

    public static string GenereateBarCode(string txtCode)
    {
        byte[] byteImage = null;
        string barCode = txtCode;
        //System.Web.UI.WebControls.Image imgBarCode = new System.Web.UI.WebControls.Image();
        using (Bitmap bitMap = new Bitmap(barCode.Length * 40, 80))
        {
            using (Graphics graphics = Graphics.FromImage(bitMap))
            {
                Font oFont = new Font("IDAutomationHC39M", 16);
                PointF point = new PointF(2f, 2f);
                SolidBrush blackBrush = new SolidBrush(Color.Black);
                SolidBrush whiteBrush = new SolidBrush(Color.White);
                graphics.FillRectangle(whiteBrush, 0, 0, bitMap.Width, bitMap.Height);
                graphics.DrawString("*" + barCode + "*", oFont, blackBrush, point);
            }
            using (MemoryStream ms = new MemoryStream())
            {
                //string path = HttpContext.Current.Server.MapPath("Images/");
                // bitMap.Save(path+"/barcode.png", System.Drawing.Imaging.ImageFormat.Png);
                bitMap.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                byteImage = ms.ToArray();

            }
            return "data:image/png;base64," + Convert.ToBase64String(byteImage);
        }
    }

    //public System.Drawing.Image DownloadImageFromUrl(string imageUrl)
    //{
    //    System.Drawing.Image image = null;

    //    try
    //    {
    //        System.Net.HttpWebRequest webRequest = (System.Net.HttpWebRequest)System.Net.HttpWebRequest.Create(imageUrl);
    //        webRequest.AllowWriteStreamBuffering = true;
    //        webRequest.Timeout = 30000;
    //        System.Net.WebResponse webResponse = webRequest.GetResponse();
    //        System.IO.Stream stream = webResponse.GetResponseStream();
    //        image = System.Drawing.Image.FromStream(stream);

    //        webResponse.Close();
    //    }
    //    catch (Exception ex)
    //    {
    //        return null;
    //    }

    //    return image;
    //}

    public SendEmailUserInfo GetUserInfo(int ApplicantId, EStatus obj)
    {
        SendEmailUserInfo objuserInfo = new SendEmailUserInfo();
        if (obj != EStatus.Registration)
        {
            Applicant userInfo = new Applicant();
            DataSet ds = userInfo.GetApplicantByID(ApplicantId);
            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                objuserInfo.ApplicationNumber = ds.Tables[0].Rows[0]["JobApplicationId"].ToString();
                objuserInfo.Password = ds.Tables[0].Rows[0]["pass"].ToString();
                objuserInfo.FullName = ds.Tables[0].Rows[0]["FirstName"] + " " + Convert.ToString(ds.Tables[0].Rows[0]["MiddleName"]) + " " + ds.Tables[0].Rows[0]["LastName"];
                objuserInfo.Email = ds.Tables[0].Rows[0]["EmailId"].ToString();
                objuserInfo.LogginedUserFullName = ds.Tables[0].Rows[0]["FirstName"] + " " + ds.Tables[0].Rows[0]["MiddleName"] + " " + ds.Tables[0].Rows[0]["LastName"];
                objuserInfo.LogginedUserEmail = ds.Tables[0].Rows[0]["EmailId"].ToString();
                objuserInfo.FirstName = ds.Tables[0].Rows[0]["FirstName"].ToString();
                objuserInfo.LastName = ds.Tables[0].Rows[0]["LastName"].ToString();
                objuserInfo.Address = ds.Tables[0].Rows[0]["Address"].ToString();
                objuserInfo.VenueName = ds.Tables[0].Rows[0]["VenueName"].ToString();
                objuserInfo.VenueAddress = ds.Tables[0].Rows[0]["VenueAddress"].ToString();
                objuserInfo.VenueId = ds.Tables[0].Rows[0]["VenueId"].ToString();
                objuserInfo.StartTime = ds.Tables[0].Rows[0]["StartTime"].ToString();
                objuserInfo.EndTime = ds.Tables[0].Rows[0]["EndTime"].ToString();
                objuserInfo.RoomNo = ds.Tables[0].Rows[0]["RoomNo"].ToString();
                objuserInfo.ParkingInstructions = ds.Tables[0].Rows[0]["ParkingInstructions"].ToString();
                objuserInfo.InterviewType = ds.Tables[0].Rows[0]["InterviewType"].ToString();
                objuserInfo.Branch = ds.Tables[0].Rows[0]["BranchName"].ToString();
                objuserInfo.BranchEmail = ds.Tables[0].Rows[0]["BranchEmail"].ToString();
                objuserInfo.BranchId = Convert.ToInt32(ds.Tables[0].Rows[0]["BranchId"]);
                objuserInfo.OnlineInterview = ds.Tables[0].Rows[0]["OnlineInterview"].ToString();
                objuserInfo.JobPosition = ds.Tables[0].Rows[0]["JobPositionName"].ToString();                
                var date = ds.Tables[0].Rows[0]["Date"];
                if (!String.IsNullOrEmpty(Convert.ToString(date)))
                {
                    objuserInfo.InterviewDate = Convert.ToDateTime(date).ToString("MM/dd/yyyy");
                    objuserInfo.StartTime = GetTime(ds.Tables[0].Rows[0]["StartTime"].ToString());
                    if (objuserInfo.InterviewType == "Block")
                    {
                        objuserInfo.EndTime = GetTime(ds.Tables[0].Rows[0]["EndTime"].ToString());

                    }
                    else
                        objuserInfo.EndTime = GetTime(ds.Tables[0].Rows[0]["StartTime"].ToString());
                }

            }
        }
        else
        {
            Applicant userInfo = new Applicant();
            //DataSet ds = userInfo.GetApplicantByID(ApplicantId);
            DataSet ds = userInfo.GetApplicantPassword(Convert.ToInt32(ApplicantId));
            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                objuserInfo.FullName = ds.Tables[0].Rows[0]["FullName"].ToString();
                objuserInfo.Email = ds.Tables[0].Rows[0]["EmailId"].ToString();
                objuserInfo.Password = ds.Tables[0].Rows[0]["Password"].ToString();
                objuserInfo.BranchId = Convert.ToInt32(ds.Tables[0].Rows[0]["BranchId"].ToString());
                objuserInfo.Branch = ds.Tables[0].Rows[0]["BranchName"].ToString();
            }
        }
        return objuserInfo;
    }

    public string GetTime(String time)
    {
        string str = Convert.ToDateTime(DateTime.Now).ToShortDateString() + " " + time.Substring(0, 5) + " " + time.Substring(time.Length - 2);
        DateTime objDate = Convert.ToDateTime(str);
        return objDate.ToString("HH:mm");
    }


    public void SendEmail(EmailParameters emailParameters, String toMailAddress, List<Attachment> strAttachment = null, AlternateView EventData = null)
    {


        //string xmlData = emailParameters.GetXML();
        // string strBody = !String.IsNullOrEmpty(emailParameters.EmailBody) ? emailParameters.EmailBody : MailerUtility.GetMailBody(HttpContext.Current.Server.MapPath("~") + "\\xslt\\" + emailParameters.XMLFilePath, xmlData);
        string strBody = !String.IsNullOrEmpty(emailParameters.EmailBody) ? emailParameters.EmailBody : MailerUtility.GetMailBody(emailParameters);
        //****************Calling the Send Mail Function *******************************
        MailContent objMailContent = new MailContent() { From = "HR@Woodsideranch.com", toEmailaddress = toMailAddress, displayName = "Woodside Recruiting Staff", subject = emailParameters.Subject, emailBody = strBody, strAttachment = strAttachment, EventData = EventData, BranchId = emailParameters.BranchId };

        SendEmailInBackgroundThread(objMailContent);
    }

    public void SendEmailInBackgroundThread(MailContent objMailContent)
    {
        //Thread bgThread = new Thread(new ParameterizedThreadStart(SendAttachment));
        //bgThread.IsBackground = true;
        //bgThread.Start(objMailContent);
        SendAttachment(objMailContent, UserName);
    }


    public static void SendAttachment(Object objMail, string UserName)
    {
        MailContent objMC = (MailContent)objMail;
        if (objMC.toEmailaddress.StartsWith("admin"))
        {
            // objMC.toEmailaddress = "solomenn@mailinator.com";
        }

        SmtpClient smtpClient = new SmtpClient();
        MailMessage mailMessage = new MailMessage();

        // mailMessage.Headers.Add("Disposition-Notification-To", "ykumar@protatechindia.com");

        bool flag = false;
        bool UseSMTPSSL = false;
        if (!string.IsNullOrEmpty(authenticate))
        {
            flag = Convert.ToBoolean(authenticate);
        }
        if (!string.IsNullOrEmpty(UsesmtpSSL))
        {
            UseSMTPSSL = Convert.ToBoolean(UsesmtpSSL);
        }

        string host = mailServer;

        string address = fromEmailID;


        //if (UserName != null)
        if (objMC.BranchId > 0)
        {

            Branch obj = new Branch();
            string email = obj.GetEmailId(objMC.BranchId);
            if (email != "")
            {
                address = email;
            }
        }

        if (!String.IsNullOrEmpty(objMC.From))
        {
            address = objMC.From;
        }

        MailAddress from = new MailAddress(address, objMC.displayName);

        if (objMC.CopyTo.Count > 0)
        {
            foreach (string copyTo in objMC.CopyTo)
            {
                if (!string.IsNullOrEmpty(copyTo))
                {
                    mailMessage.CC.Add(new MailAddress(copyTo));
                }
            }
        }


        try
        {

            smtpClient.EnableSsl = UseSMTPSSL;
            smtpClient.Host = host;
            mailMessage.From = from;
            mailMessage.To.Add(objMC.toEmailaddress);
            mailMessage.Subject = objMC.subject;
            mailMessage.IsBodyHtml = true;

            if (objMC.EventData != null)
            {
                mailMessage.AlternateViews.Add(objMC.EventData);

                System.Net.Mime.ContentType typeHTML = new System.Net.Mime.ContentType("text/html");
                AlternateView viewHTML = AlternateView.CreateAlternateViewFromString(objMC.emailBody, typeHTML);
                viewHTML.TransferEncoding = System.Net.Mime.TransferEncoding.SevenBit;
                mailMessage.AlternateViews.Add(viewHTML);
            }
            else
            {
                mailMessage.Body = objMC.emailBody;
            }
            if (objMC.strAttachment != null)
            {
                foreach (Attachment a in objMC.strAttachment)
                {

                    mailMessage.Attachments.Add(a);

                }
            }
            if (flag)
            {
                NetworkCredential credentials = new NetworkCredential(userId, password);
                smtpClient.UseDefaultCredentials = false;
                smtpClient.Credentials = credentials;
            }
            else
            {
                smtpClient.UseDefaultCredentials = true;
            }

            string sendMail = AllowSendMails;


            if (string.IsNullOrEmpty(sendMail) || (sendMail.ToUpper() != "NO"))
            {
                if (!string.IsNullOrEmpty(objMC.emailBody))
                {
                    smtpClient.Send(mailMessage);
                    smtpClient.Dispose();
                }

            }


            // update Mail log status of IsDelivered
            //MailBoxManager.UpdateDeliveryStatus(MailerLogID, true, "Successfully Send.");

        }
        catch (Exception ex)
        {
            ErrorLogger.Log(ex.Message);
            ErrorLogger.Log(ex.StackTrace);
            //update Mail log status of IsDelivered = False and Logtext = ex.mesage
            //logger.ErrorFormat(ex.Message);
            throw ex;
        }
    }

    public void SendBulEmail(Object objMail, string UserName, int BranchId = 0)
    {

        MailContent objMC = (MailContent)objMail;
        if (objMC.toEmailaddress.StartsWith("admin"))
        {
            // objMC.toEmailaddress = "solomenn@mailinator.com";
        }

        SmtpClient smtpClient = new SmtpClient();
        MailMessage mailMessage = new MailMessage();

        // mailMessage.Headers.Add("Disposition-Notification-To", "ykumar@protatechindia.com");

        bool flag = false;
        bool UseSMTPSSL = false;
        if (!string.IsNullOrEmpty(authenticate))
        {
            flag = Convert.ToBoolean(authenticate);
        }
        if (!string.IsNullOrEmpty(UsesmtpSSL))
        {
            UseSMTPSSL = Convert.ToBoolean(UsesmtpSSL);
        }

        string host = mailServer;
        string address = fromEmailID;


        if (UserName != null)
        {


            Branch obj = new Branch();
            string email = obj.GetEmailId(BranchId);
            if (email != "")
            {
                address = email;
            }
        }

        if (!String.IsNullOrEmpty(objMC.From))
        {
            address = objMC.From;
        }

        MailAddress from = new MailAddress(address, objMC.displayName);

        if (objMC.CopyTo.Count > 0)
        {
            foreach (string copyTo in objMC.CopyTo)
            {
                if (!string.IsNullOrEmpty(copyTo))
                {
                    mailMessage.CC.Add(new MailAddress(copyTo));
                }
            }
        }


        try
        {
            //ErrorLogger.Log(objMC.toEmailaddress);
            //  objMC.toEmailaddress = "ykumar@protaTECHIndia.com";
            smtpClient.EnableSsl = false;
            smtpClient.Host = host;
            smtpClient.Port = 25;
            mailMessage.From = from;
            mailMessage.To.Add(objMC.toEmailaddress);
            mailMessage.Subject = objMC.subject;
            mailMessage.IsBodyHtml = true;

            if (objMC.EventData != null)
            {
                mailMessage.AlternateViews.Add(objMC.EventData);

                System.Net.Mime.ContentType typeHTML = new System.Net.Mime.ContentType("text/html");
                AlternateView viewHTML = AlternateView.CreateAlternateViewFromString(objMC.emailBody, typeHTML);
                viewHTML.TransferEncoding = System.Net.Mime.TransferEncoding.SevenBit;
                mailMessage.AlternateViews.Add(viewHTML);
            }
            else
            {
                mailMessage.Body = objMC.emailBody;
            }
            if (objMC.strAttachment != null)
            {
                foreach (Attachment a in objMC.strAttachment)
                {

                    mailMessage.Attachments.Add(a);

                }
            }
            if (flag)
            {
                NetworkCredential credentials = new NetworkCredential(UserName, password);
                smtpClient.UseDefaultCredentials = false;
                smtpClient.Credentials = credentials;
            }
            else
            {
                smtpClient.UseDefaultCredentials = true;
            }

            string sendMail = Convert.ToString(AllowSendMails);


            if (!string.IsNullOrEmpty(sendMail) && (sendMail.ToUpper() != "NO"))
            {
                if (!string.IsNullOrEmpty(objMC.emailBody))
                {
                    smtpClient.Send(mailMessage);
                    smtpClient.Dispose();
                }

            }


            // update Mail log status of IsDelivered
            //MailBoxManager.UpdateDeliveryStatus(MailerLogID, true, "Successfully Send.");

        }
        catch (Exception ex)
        {
            ErrorLogger.Log(ex.Message);
            ErrorLogger.Log(ex.StackTrace);
            // update Mail log status of IsDelivered =False and Logtext = ex.mesage
            // logger.ErrorFormat(ex.Message);
            throw ex;
        }
    }

}


public class MailContent
{
    public string toEmailaddress { get; set; }
    public string displayName { get; set; }
    public string subject { get; set; }
    public string emailBody { get; set; }
    public List<Attachment> strAttachment { get; set; }
    public AlternateView EventData { get; set; }
    public string From { get; set; }
    List<String> _copyList = new List<string>();
    public List<String> CopyTo { get { return _copyList; } }
    public int BranchId { get; set; }
}

public class SendEmailUserInfo
{

    public string FullName { get; set; }
    public string LogginedUserFullName { get; set; }
    public string Email { get; set; }
    public string LogginedUserEmail { get; set; }
    public string UserType { get; set; }
    public int ApplicantId { get; set; }
    public string ApplicationNumber { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string Address { get; set; }
    public string InterviewDate { get; set; }
    public string InterviewTime { get; set; }
    public string Password { get; set; }
    public string VenueName { get; set; }
    public string VenueAddress { get; set; }
    public string VenueId { get; set; }
    public string RoomNo { get; set; }
    public string ParkingInstructions { get; set; }
    public string InterviewType { get; set; }
    public string StartTime { get; set; }
    public string EndTime { get; set; }
    public string Duration { get; set; }
    public int Sessions { get; set; }
    public string Branch { get; set; }
    public string BranchEmail { get; set; }
    public int BranchId { get; set; }
    public string Interviewlink { get; set; }
    public string OnlineInterview { get; set; }
    public string Comment { get; set; }
    public string JobPosition { get; set; }
}


