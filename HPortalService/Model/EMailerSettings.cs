﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

 
    public class EMailerSettings
    {
        public int MailerSettingId { get; set; }
        public int ScenerioId { get; set; }
        public int ReceiverTypeId { get; set; }
        public int MemberShipTypeId { get; set; }
        public string XSLTFilePath { get; set; }
        public string Subject { get; set; }
        public string MailFrom { get; set; }
        public string MailerEmailId { get; set; }
        public string MailServer { get; set; }
        public string Password { get; set; }
        public string UserId { get; set; }
        public string StaffToMailID { get; set; }
    }
 
