﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using HPortalService.DAL;


namespace HPortalService
{
    public class ApplicantDegree
    {
        public int JobApplicationId { get; set; }
        public string HighestEducation { get; set; }
        public string EducationSchool { get; set; }
        public string EducationStateId { get; set; }
        public string StateName { get; set; }
        public string HighSchoolName { get; set; }
        public string GradDate { get; set; }
        public string Location { get; set; }
        public string GradMonth { get; set; }
        public string GradeYear { get; set; }
        public string OtherState { get; set; }
        public bool DegreeCompleted { get; set; }
        public string AcademicCompletionDate { get; set; }
        public string AcademicCertificate { get; set; }
        public string FileData { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
        
        public int SaveApplicantDegree(ApplicantDegree obj)
        {
            ApplicantDegreeDAL _AppDegree = new ApplicantDegreeDAL();
            return _AppDegree.UpdateApplicantDegree(obj);
        }

        public List<ApplicantDegree> GetDegreeByJobApplicantId(int JobApplicantId)
        {
            DataSet ds;
            ApplicantDegreeDAL _AppInfo = new ApplicantDegreeDAL();
            ds = _AppInfo.GetDegreeByJobApplicantId(JobApplicantId);
            List<ApplicantDegree> lstApplicant = SQLHelper.ContructList<ApplicantDegree>(ds);
            //foreach (ApplicantDegree obj in lstApplicant)
            //{
            //    obj.GradMonth = obj.GradMonth != null ? Convert.ToDateTime(obj.GradMonth).ToString("MMMM YYYY") : null;
            //}
            return lstApplicant;
        }
    }


}
