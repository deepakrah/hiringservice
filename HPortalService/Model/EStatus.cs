﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HPortalService
{
    public enum EStatus
    {

        All = 0,
        Applied = 1,
        Schedule_for_Interview = 2,
        Offered = 3,
        Scehdule_for_NAO = 4,
        Hired = 5,
        Reject = 6,
        JobApplication=7,
        Registration=8,
        Schedule_for_Orientation=9,
        Offer_Rejected=10,
        Manager_Approval=11,
        Applying = 12,
        NoShowInterview=13,
        NoShowOrientation=14,
        Attended_Interview=15,
        Attended_Orientation=16,
        Readdress=17,
        UnmarkNoShow=18,
        UnmarkAttended=19,
        Board=20,
        PasswordReset=21,
        Schedule_for_OnlineInterview = 22,
        Manager_Offered=23,
        Schedule_for_OrientationNew = 24,
        Manager_Answer = 25,
        OnlineInterview_Complete_Later=26
    }
}
