﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using HPortalService.DAL;

namespace HPortalService
{
    public class Validate
    {
        //Validate

        public int ApplicantId { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string SSN { get; set; }
        //public DateTime? DateOfBirth { get; set; }
        [DefaultValue(null)]
        public DateTime? DateOfBirth { get; set; }
        public int Finish { get; set; }
        public int Type { get; set; }
        public Validate()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public int InsApplicantValidate(Validate Validate)
        {
            ValidateDAL obj = new ValidateDAL();
            return obj.fnInsApplicantValidate(this);
        }
        public List<Validate> GetApplicantValidate(int ApplicantId)
        {
            ValidateDAL obj = new ValidateDAL();
            // return obj.GetApplicantBGCheckById(ApplicantId);
            List<Validate> lstBGCheck = SQLHelper.ContructList<Validate>(obj.fnGetApplicantValidate(ApplicantId));
            return lstBGCheck;
        }

        public List<Validate> GetApplicantValidatebySSN(string SSN)
        {
            ValidateDAL obj = new ValidateDAL();
            // return obj.GetApplicantBGCheckById(ApplicantId);
            List<Validate> lstBGCheck = SQLHelper.ContructList<Validate>(obj.GetApplicantValidatebySSN(SSN));
            return lstBGCheck;
        }
        
    }
}
