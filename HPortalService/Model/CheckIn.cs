﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HPortalService
{
    public class CheckIn
    {
        public int ApplicantId { get; set; }
        public string JobApplicationId { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string EmailID { get; set; }
        public string CheckedIn { get; set; }
        public int UserId { get; set; }
        public int Type { get; set; }
        public string CheckInDate { get; set; }
        public bool isAttend { get; set; }
        public string PrimaryPhone { get; set; }
        public string SecondaryPhone { get; set; }
        public int AppTask { get; set; }
        public string ApplicantEverifyDoc { get; set; }
        public int FormI9Verification { get; set; }
        public string AppTaskExist { get; set; }
        public string AppTaskExist2 { get; set; }
        public string TaskPendingStatus { get; set; }
        public string TaskPendingStatus2 { get; set; }
        public string TaskCompletedStatus1 { get; set; }
        public string TaskCompletedStatus2 { get; set; }
        public string TaskCompletedStatus { get; set; }


        public int BranchId { get; set; }
        public int ChildBranchId { get; set; }
        public int VenueId { get; set; }
        public string VenueName { get; set; }
        public string roomNo { get; set; }
        public string startTime { get; set; }
        public string status { get; set; }
        public string total { get; set; }
        public string notcheckedInCount { get; set; }
        public string checkedInCount { get; set; }

        public int Completed { get; set; }
        public bool fnInsApplicantTask(int ApplicantId, int StatusId, int type = 0)
        {
            ApplicantTask obj = new ApplicantTask();
            obj.ApplicantId = ApplicantId;
            obj.StatusId = StatusId;
            obj.Type = type;
            int temp = obj.InsApplicantTask(obj);
            if (temp > 0)
            {
                return true;
            }
            return false;
        }
    }


}
