﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using HPortalService.DAL;

namespace HPortalService
{
    public class ApplicantAtlantaAllownaceCertificateG4
    {
        public int ApplicantAtlantaAllownaceCertificateId { get; set; }
        public int ApplicantId { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public int? MaritalStatus { get; set; }
        public int? MarriedFilingJointbothSpouses { get; set; }
        public int? MarriedFilingJointoneSpouse { get; set; }
        public int? MarriedFilingSeparate { get; set; }
        public int? HeadofHousehold { get; set; }
        public string DependentAllowances { get; set; }
        public string AddtionalAllowances { get; set; }
        public string AddtionalWithholding { get; set; }
        public bool? StandardDeductionYourselfAge { get; set; }
        public bool? StandardDeductionYourselfBlind { get; set; }
        public bool? StandardDeductionSpouseAge { get; set; }
        public bool? StandardDeductionSpouseBlind { get; set; }
        public int? Boxeschecked { get; set; }
        public string Amount { get; set; }
        public string ItemizedDeductions { get; set; }
        public string GeorgiaStandardDeduction { get; set; }
        public string SubtractLineB { get; set; }
        public string AllowableDeductions { get; set; }
        public string AddAmounts { get; set; }
        public string EstimateofTaxable { get; set; }
        public string SubtractLineF { get; set; }
        public string DividetheAmount { get; set; }
        public string LetterUsed { get; set; }
        public string TotalAllowances { get; set; }
        public bool? IsIncomeTaxLiability { get; set; }
        public string Residence { get; set; }
        public string SpouseResidence { get; set; }
        public bool? IsResidenceExempt { get; set; }
        public DateTime CreatedDate { get; set; }
        public string SSN { get; set; }
        public string EIN { get; set; }
        public string EmployerName { get; set; }
        public string EmployerBusinessAddress { get; set; }
        public string EmployerCity { get; set; }
        public string EmployerState { get; set; }
        public string EmployerZIPCode { get; set; }


        public int intApplicantAtlantaAllownaceCertificateG4(ApplicantAtlantaAllownaceCertificateG4 obj)
        {
            ApplicantAtlantaAllownaceCertificateG4DAL _ApplicationDAL = new ApplicantAtlantaAllownaceCertificateG4DAL();
            return _ApplicationDAL.intApplicantAtlantaAllownaceCertificateG4(obj);
        }
        public DataSet GetApplicantAtlantaAllownaceCertificateG4ByID(int ID)
        {
            ApplicantAtlantaAllownaceCertificateG4DAL _ApplicationDAL = new ApplicantAtlantaAllownaceCertificateG4DAL();
            return _ApplicationDAL.GetApplicantAtlantaAllownaceCertificateG4ByID(ID);
        }
    }
}
