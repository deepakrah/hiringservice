﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using HPortalService.DAL;

namespace HPortalService
{
   public class CallLog
    {  public int CallLogId { get; set; }
       public String CallAction { get; set; }
       public int UpdatedBy { get; set; }
       public String UpdatedDate { get; set; }
       public String Remarks { get; set; }
       public int ApplicantId { get; set; }

       public void SaveCallLog()
       {
           CallLogDAL obj = new CallLogDAL();
           obj.SaveLog(this);
       }

       public DataSet GetLog(int ApplicantId)
       {
           CallLogDAL obj = new CallLogDAL();
           return obj.GetLog(ApplicantId);
       }
    }
}
