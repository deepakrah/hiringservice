﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HPortalService
{
    public enum SettingSections
    {
        SendEmail = 1,
        Interview = 2,
        Venue = 3,
        Hiring=4
    }
}
