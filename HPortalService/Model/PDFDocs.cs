﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Text;
using System.Text.RegularExpressions;
using HPortalService;
using HPortalService.Model;
using HPortalService.DAL;
using iTextSharp.text.html.simpleparser;
/// <summary>
/// Summary description for PDFDocs
/// </summary>
public class PDFDocs
{
    public PDFDocs()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public void ConvertMailContentToPDF(string FilePath, string PDFPATh)
    {
        string text = System.IO.File.ReadAllText(FilePath);
        string html = Regex.Replace(text, @"(<img\/?[^>]+>)", @"", RegexOptions.IgnoreCase);
        html = Regex.Replace(html, @"(<style>\/?[^>]+</style>)", @"", RegexOptions.IgnoreCase);

        Document document = new Document();

        PdfWriter.GetInstance(document, new FileStream(PDFPATh, FileMode.Create));

        document.Open();
        StyleSheet styles = new StyleSheet();
        styles.LoadTagStyle("body", "size", "10px");
        styles.LoadTagStyle("th", "size", "10px");
        styles.LoadTagStyle("td", "size", "10px");
        styles.LoadTagStyle("body", "face", "Verdana");
        styles.LoadTagStyle("td", "face", "Verdana");
        styles.LoadTagStyle("th", "face", "Verdana");
        styles.LoadTagStyle("body", "face", "Verdana");
        styles.LoadTagStyle("p", "border-bottom-color", "Black");
        styles.LoadTagStyle("p", "border-bottom-width", "1px");
        styles.LoadTagStyle("p", "border-bottom-style", "solid");

        HTMLWorker hw = new HTMLWorker(document);
        hw.SetStyleSheet(styles);
        hw.Parse(new StringReader(html));
        document.Close();

    }

    public void SaveMailContentToText(string FilePath, string str, EStatus status)
    {
        StringBuilder strMail = new StringBuilder();
        switch (status)
        {
            case EStatus.Schedule_for_Interview:

                strMail.Append("<div style='height:5px;border:1px solid black'>.........................................................................................................</div>");
                break;

        }
        strMail.Append(str);
        System.IO.File.AppendAllText(FilePath, strMail.ToString());




    }

    public void GenerateSelectedApplicantReport(string FilePath, string Path, Dictionary<int, string> DictgrdEmp)
    {
        Document doc = new Document();

        try
        {

            //Create a PDF file in specific path

            PdfWriter writer = PdfWriter.GetInstance(doc, new FileStream(FilePath, FileMode.Create));
            PageEventHelper pageEventHelper = new PageEventHelper();
            writer.PageEvent = pageEventHelper;

            //Open the PDF document

            doc.Open();


            //Add Content to PDF
            foreach (var Dict in DictgrdEmp)
            {
                string[] strArry = Dict.Value.Split('|');
                int ApplicantID = Convert.ToInt32(strArry[1]);
                int jobApplication = Convert.ToInt32(Dict.Key);
                //Create document


                Add_Content_To_PDF(doc, Path, ApplicantID, jobApplication);
                doc.NewPage();

            }


            // Closing the document

            doc.Close();

        }
        catch (Exception ex)
        {
            doc.Close();
        }


    }

    public void CreateDocument(string FilePath, string Path, int JobApplicantID, int ApplicantId)
    {

        //Create document

        Document doc = new Document();
        //Create PDF Table


        try
        {

            //Create a PDF file in specific path

            PdfWriter writer = PdfWriter.GetInstance(doc, new FileStream(FilePath, FileMode.Create));
            PageEventHelper pageEventHelper = new PageEventHelper();
            writer.PageEvent = pageEventHelper;

            //Open the PDF document

            doc.Open();



            //Add Content to PDF

            Add_Content_To_PDF(doc, Path, JobApplicantID, ApplicantId);



            // Closing the document

            doc.Close();

        }
        catch (Exception ex)
        {
            doc.Close();
            ErrorLogger.Log(ex.Message);
            ErrorLogger.Log(ex.StackTrace);
        }

    }

    public void CreateOffer(string FilePath, string Path, int ApplicantId)
    {

        //Create document

        Document doc = new Document();
        //Create PDF Table


        try
        {

            Applicant objApplicant = new Applicant();
            DataSet ds = objApplicant.GetApplicantByID(ApplicantId);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {

                //Create a PDF file in specific path

                PdfWriter writer = PdfWriter.GetInstance(doc, new FileStream(FilePath, FileMode.Create));
                PageEventHelper pageEventHelper = new PageEventHelper();
                writer.PageEvent = pageEventHelper;

                //Open the PDF document

                doc.Open();



                //Add Content to PDF


                float[] headers = { 10, 50 };  //Header Widths
                Font verdana = FontFactory.GetFont("Verdana", 18, Font.NORMAL, BaseColor.BLACK);

                //Add Title to the PDF file at the top
                PdfPTable HeaderTable = new PdfPTable(2);
                HeaderTable.SetWidths(headers);
                HeaderTable.WidthPercentage = 100;

                Paragraph p = new Paragraph(new Phrase("CONDITIONAL JOB OFFER", FontFactory.GetFont("Verdana", 12, Font.UNDERLINE, BaseColor.BLACK)));
                HeaderTable.AddCell(new PdfPCell(p) { Colspan = 2, Border = 0, PaddingTop = 10, PaddingBottom = 60, HorizontalAlignment = Element.ALIGN_CENTER });

                // Closing the document
                HeaderTable.AddCell(new PdfPCell(new Phrase("Date:", FontFactory.GetFont("Verdana", 10, Font.NORMAL, BaseColor.BLACK))) { Border = 0, PaddingTop = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
                HeaderTable.AddCell(new PdfPCell(new Phrase(DateTime.Now.ToShortDateString(), FontFactory.GetFont("Verdana", 10, Font.NORMAL, BaseColor.BLACK))) { Border = 0, PaddingTop = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });

                HeaderTable.AddCell(new PdfPCell(new Phrase("Employee Name:", FontFactory.GetFont("Verdana", 10, Font.NORMAL, BaseColor.BLACK))) { Border = 0, PaddingTop = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
                HeaderTable.AddCell(new PdfPCell(new Phrase(ds.Tables[0].Rows[0]["FirstName"] + " " + ds.Tables[0].Rows[0]["LastName"], FontFactory.GetFont("Verdana", 10, Font.NORMAL, BaseColor.BLACK))) { Border = 0, PaddingTop = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });

                HeaderTable.AddCell(new PdfPCell(new Phrase("Address:", FontFactory.GetFont("Verdana", 10, Font.NORMAL, BaseColor.BLACK))) { Border = 0, PaddingTop = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
                HeaderTable.AddCell(new PdfPCell(new Phrase(ds.Tables[0].Rows[0]["Address"].ToString(), FontFactory.GetFont("Verdana", 10, Font.NORMAL, BaseColor.BLACK))) { Border = 0, PaddingTop = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });



                HeaderTable.AddCell(new PdfPCell(new Phrase("Dear " + ds.Tables[0].Rows[0]["FirstName"] + ",", FontFactory.GetFont("Verdana", 10, Font.NORMAL, BaseColor.BLACK))) { Colspan = 2, Border = 0, PaddingTop = 25, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });

                HeaderTable.AddCell(new PdfPCell(new Phrase("In response to your application and subsequent interview, we are pleased to offer you employment in our organization on the following terms and conditions:", FontFactory.GetFont("Verdana", 10, Font.NORMAL, BaseColor.BLACK))) { Colspan = 2, Border = 0, PaddingTop = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });


                HeaderTable.AddCell(new PdfPCell(new Phrase("Regards,\n\n\nHR Team", FontFactory.GetFont("Verdana", 10, Font.NORMAL, BaseColor.BLACK))) { Colspan = 2, Border = 0, PaddingTop = 65, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });


                doc.Add(HeaderTable);

                doc.Close();

            }
        }
        catch (Exception ex)
        {
            doc.Close();
            ErrorLogger.Log(ex.Message);
            ErrorLogger.Log(ex.StackTrace);
        }

    }


    private void Add_Content_To_PDF(Document Doc, String Path, int ApplicantId, int JobApplicantID)
    {

        float[] headers = { 10, 40, 10 };  //Header Widths
        Font verdana = FontFactory.GetFont("Verdana", 18, Font.NORMAL, BaseColor.BLACK);

        //Add Title to the PDF file at the top
        PdfPTable HeaderTable = new PdfPTable(3);
        HeaderTable.SetWidths(headers);
        HeaderTable.WidthPercentage = 100;
        Image gif = Image.GetInstance(Path + "images/logo-h-lg.png");
        gif.WidthPercentage = 35;
        PdfPCell cellWithImage = new PdfPCell() { Border = 0, PaddingBottom = 20, HorizontalAlignment = Element.ALIGN_CENTER };
        cellWithImage.AddElement(gif);
        HeaderTable.AddCell(cellWithImage);
        HeaderTable.AddCell(new PdfPCell(new Phrase("Woodside Properties\n" + "Employment Application", verdana)) { Border = 0, PaddingTop = 10, PaddingBottom = 10, HorizontalAlignment = Element.ALIGN_CENTER });
        HeaderTable.AddCell(new PdfPCell(new Phrase("")) { Border = 0 });
        HeaderTable.AddCell(new PdfPCell(new Phrase("Application#: " + JobApplicantID + "\n", FontFactory.GetFont("Verdana", 10, Font.NORMAL, BaseColor.BLACK))) { Colspan = 3, Border = 0, PaddingBottom = 4, BorderWidthBottom = 0.5f, HorizontalAlignment = Element.ALIGN_RIGHT });


        Doc.Add(HeaderTable);


        AddDetailwithOutBlock(Doc, ApplicantId);


        //Paragraph p = new Paragraph(new Phrase("Assessment Questions", FontFactory.GetFont("Verdana", 12, Font.NORMAL, BaseColor.GRAY)));
        //p.SpacingBefore = 1.0f;
        //p.SpacingAfter = 5.1f;
        //Doc.Add(p);

        AddAssesment(Doc, ApplicantId);
        //AddGeneralAssesment(Doc, ApplicantId);
        AddGeneralInfo(Doc, JobApplicantID);


        AddDegreeAndOccupation(Doc, JobApplicantID);
        AddCertificateAndReference(Doc, JobApplicantID);
        AddDisclosure(Doc, JobApplicantID, ApplicantId);
    }


    public void AddDetailwithOutBlock(Document doc, int ApplicantID)
    {
        Applicant objApplicant = new Applicant();
        DataSet ds = objApplicant.GetApplicantByID(ApplicantID);
        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
        {
            DataRow dr = ds.Tables[0].Rows[0];
            float[] contentHeader = { 22, 22, 22 };
            PdfPTable HeaderTable = new PdfPTable(3);
            HeaderTable.SetWidths(contentHeader);
            HeaderTable.WidthPercentage = 100;

            HeaderTable.AddCell(new PdfPCell(new Phrase("First Name", FontFactory.GetFont("Verdana", 11, Font.BOLD, BaseColor.BLACK))) { Border = 0, PaddingTop = 8, HorizontalAlignment = Element.ALIGN_LEFT });
            HeaderTable.AddCell(new PdfPCell(new Phrase("Middle Name", FontFactory.GetFont("Verdana", 11, Font.BOLD, BaseColor.BLACK))) { Border = 0, PaddingTop = 8, HorizontalAlignment = Element.ALIGN_LEFT });
            HeaderTable.AddCell(new PdfPCell(new Phrase("Last Name", FontFactory.GetFont("Verdana", 11, Font.BOLD, BaseColor.BLACK))) { Border = 0, PaddingTop = 8, HorizontalAlignment = Element.ALIGN_LEFT });

            HeaderTable.AddCell(new PdfPCell(new Phrase(dr["FirstName"].ToString(), FontFactory.GetFont("Verdana", 11, Font.NORMAL, BaseColor.BLACK))) { Border = 0, PaddingTop = 3, HorizontalAlignment = Element.ALIGN_LEFT });
            HeaderTable.AddCell(new PdfPCell(new Phrase(dr["MiddleName"].ToString(), FontFactory.GetFont("Verdana", 11, Font.NORMAL, BaseColor.BLACK))) { Border = 0, PaddingTop = 3, HorizontalAlignment = Element.ALIGN_LEFT });
            HeaderTable.AddCell(new PdfPCell(new Phrase(dr["LastName"].ToString(), FontFactory.GetFont("Verdana", 11, Font.NORMAL, BaseColor.BLACK))) { Border = 0, PaddingTop = 3, HorizontalAlignment = Element.ALIGN_LEFT });


            HeaderTable.AddCell(new PdfPCell(new Phrase("Address", FontFactory.GetFont("Verdana", 11, Font.BOLD, BaseColor.BLACK))) { Colspan = 2, Border = 0, PaddingTop = 3, HorizontalAlignment = Element.ALIGN_LEFT });
            HeaderTable.AddCell(new PdfPCell(new Phrase("Zip Code", FontFactory.GetFont("Verdana", 11, Font.BOLD, BaseColor.BLACK))) { Border = 0, PaddingTop = 3, HorizontalAlignment = Element.ALIGN_LEFT });


            HeaderTable.AddCell(new PdfPCell(new Phrase(dr["Address"].ToString(), FontFactory.GetFont("Verdana", 11, Font.NORMAL, BaseColor.BLACK))) { Colspan = 2, Border = 0, PaddingTop = 3, HorizontalAlignment = Element.ALIGN_LEFT });
            HeaderTable.AddCell(new PdfPCell(new Phrase(dr["ZipCode"].ToString(), FontFactory.GetFont("Verdana", 11, Font.NORMAL, BaseColor.BLACK))) { Border = 0, PaddingTop = 3, HorizontalAlignment = Element.ALIGN_LEFT });

            HeaderTable.AddCell(new PdfPCell(new Phrase("City", FontFactory.GetFont("Verdana", 11, Font.BOLD, BaseColor.BLACK))) { Border = 0, PaddingTop = 3, HorizontalAlignment = Element.ALIGN_LEFT });

            HeaderTable.AddCell(new PdfPCell(new Phrase("State", FontFactory.GetFont("Verdana", 11, Font.BOLD, BaseColor.BLACK))) { Border = 0, PaddingTop = 3, HorizontalAlignment = Element.ALIGN_LEFT });
            HeaderTable.AddCell(new PdfPCell(new Phrase("Country", FontFactory.GetFont("Verdana", 11, Font.BOLD, BaseColor.BLACK))) { Border = 0, PaddingTop = 3, HorizontalAlignment = Element.ALIGN_LEFT });

            HeaderTable.AddCell(new PdfPCell(new Phrase(dr["City"].ToString(), FontFactory.GetFont("Verdana", 11, Font.NORMAL, BaseColor.BLACK))) { Border = 0, PaddingTop = 3, HorizontalAlignment = Element.ALIGN_LEFT });

            HeaderTable.AddCell(new PdfPCell(new Phrase(dr["StateName"].ToString(), FontFactory.GetFont("Verdana", 11, Font.NORMAL, BaseColor.BLACK))) { Border = 0, PaddingTop = 3, HorizontalAlignment = Element.ALIGN_LEFT });
            HeaderTable.AddCell(new PdfPCell(new Phrase(dr["CountryName"].ToString(), FontFactory.GetFont("Verdana", 11, Font.NORMAL, BaseColor.BLACK))) { Border = 0, PaddingTop = 3, HorizontalAlignment = Element.ALIGN_LEFT });

            PdfPTable emailTable = new PdfPTable(4);
            //   emailTable.SetWidths(contentHeader);
            emailTable.WidthPercentage = 100;


            emailTable.AddCell(new PdfPCell(new Phrase("Email ", FontFactory.GetFont("Verdana", 11, Font.BOLD, BaseColor.BLACK))) { Border = 0, PaddingTop = 3, HorizontalAlignment = Element.ALIGN_LEFT });
            emailTable.AddCell(new PdfPCell(new Phrase(dr["EmailId"].ToString(), FontFactory.GetFont("Verdana", 11, Font.NORMAL, BaseColor.BLACK))) { Border = 0, PaddingTop = 3, PaddingLeft = -85, HorizontalAlignment = Element.ALIGN_LEFT });
            //---Phone

            emailTable.AddCell(new PdfPCell(new Phrase("Mob ", FontFactory.GetFont("Verdana", 11, Font.BOLD, BaseColor.BLACK))) { Border = 0, PaddingTop = 3, PaddingLeft = 90, HorizontalAlignment = Element.ALIGN_LEFT });
            emailTable.AddCell(new PdfPCell(new Phrase(dr["PrimaryPhone"].ToString(), FontFactory.GetFont("Verdana", 11, Font.NORMAL, BaseColor.BLACK))) { Border = 0, PaddingTop = 3, HorizontalAlignment = Element.ALIGN_LEFT });

            // Job Position
            PdfPTable JobPositionTable = new PdfPTable(2);
            JobPositionTable.WidthPercentage = 100;

            JobPositionTable.AddCell(new PdfPCell(new Phrase("Job Position ", FontFactory.GetFont("Verdana", 11, Font.BOLD, BaseColor.BLACK))) { Border = 0, PaddingTop = 3, HorizontalAlignment = Element.ALIGN_LEFT });
            JobPositionTable.AddCell(new PdfPCell(new Phrase(dr["JobPositionName"].ToString(), FontFactory.GetFont("Verdana", 11, Font.NORMAL, BaseColor.BLACK))) { Border = 0, PaddingTop = 3, PaddingLeft = -190, HorizontalAlignment = Element.ALIGN_LEFT });

            // Reffered By
            PdfPTable RefferedByTable = new PdfPTable(2);
            RefferedByTable.WidthPercentage = 100;
            string RefferedBy = "";
            if (dr["RefferedBy"].ToString() == "Organization")
            {
                RefferedBy = dr["OrganizationName"].ToString() + " (Organization)";
            }
            else if (dr["RefferedBy"].ToString() == "CSCEmployee")
            {
                RefferedBy = dr["EmployeeFirstName"].ToString() + " " + dr["EmployeeLastName"].ToString() + " (Woodside Employee)";
            }
            else if (dr["RefferedBy"].ToString() == "Other")
            {
                RefferedBy = "Other";
            }
            RefferedByTable.AddCell(new PdfPCell(new Phrase("Referred By ", FontFactory.GetFont("Verdana", 11, Font.BOLD, BaseColor.BLACK))) { Border = 0, PaddingTop = 3, HorizontalAlignment = Element.ALIGN_LEFT });
            RefferedByTable.AddCell(new PdfPCell(new Phrase(RefferedBy, FontFactory.GetFont("Verdana", 11, Font.NORMAL, BaseColor.BLACK))) { Border = 0, PaddingTop = 3, PaddingLeft = -190, HorizontalAlignment = Element.ALIGN_LEFT });

            //   HeaderTable.AddCell(new PdfPCell(emailTable));

            doc.Add(HeaderTable);
            doc.Add(emailTable);
            doc.Add(JobPositionTable);
            doc.Add(RefferedByTable);
        }
    }


    public void AddDetailwithBlock(Document doc, int applicantID)
    {
        float[] contentHeader = { 30, 70 };
        PdfPTable HeaderTable = new PdfPTable(2);
        HeaderTable.SetWidths(contentHeader);
        HeaderTable.WidthPercentage = 100;
        HeaderTable.AddCell(new PdfPCell(new Phrase("\n\n")) { Colspan = 2, Border = 0, PaddingTop = 10, HorizontalAlignment = Element.ALIGN_LEFT });
        HeaderTable.AddCell(new PdfPCell(new Phrase("First Name", FontFactory.GetFont("Verdana", 11, Font.BOLD, BaseColor.BLACK))) { Border = 0, PaddingTop = 10, HorizontalAlignment = Element.ALIGN_LEFT });
        HeaderTable.AddCell(new PdfPCell(CreateTable(25, "Nitin")) { Border = 0, PaddingTop = 3, HorizontalAlignment = Element.ALIGN_CENTER });

        HeaderTable.AddCell(new PdfPCell(new Phrase("Last Name", FontFactory.GetFont("Verdana", 11, Font.BOLD, BaseColor.BLACK))) { Border = 0, PaddingTop = 10, HorizontalAlignment = Element.ALIGN_LEFT });
        HeaderTable.AddCell(new PdfPCell(CreateTable(25, "Agrawal")) { Border = 0, PaddingTop = 3, HorizontalAlignment = Element.ALIGN_CENTER });

        HeaderTable.AddCell(new PdfPCell(new Phrase("Email", FontFactory.GetFont("Verdana", 11, Font.BOLD, BaseColor.BLACK))) { Border = 0, PaddingTop = 10, HorizontalAlignment = Element.ALIGN_LEFT });
        HeaderTable.AddCell(new PdfPCell(CreateTable(25, "nitin_5906@yahoo.com")) { Border = 0, PaddingTop = 3, HorizontalAlignment = Element.ALIGN_CENTER });


        HeaderTable.AddCell(new PdfPCell(new Phrase("Address", FontFactory.GetFont("Verdana", 11, Font.BOLD, BaseColor.BLACK))) { Border = 0, PaddingTop = 10, HorizontalAlignment = Element.ALIGN_LEFT });
        HeaderTable.AddCell(new PdfPCell(CreateTable(25, "B-13/23a")) { Border = 0, PaddingTop = 3, HorizontalAlignment = Element.ALIGN_CENTER });

        HeaderTable.AddCell(new PdfPCell(new Phrase("ZipCode", FontFactory.GetFont("Verdana", 11, Font.BOLD, BaseColor.BLACK))) { Border = 0, PaddingTop = 10, HorizontalAlignment = Element.ALIGN_LEFT });
        HeaderTable.AddCell(new PdfPCell(CreateTable(25, "110074")) { Border = 0, PaddingTop = 3, HorizontalAlignment = Element.ALIGN_CENTER });

        HeaderTable.AddCell(new PdfPCell(new Phrase("How did you hear about us?", FontFactory.GetFont("Verdana", 11, Font.BOLD, BaseColor.BLACK))) { Border = 0, PaddingTop = 10, HorizontalAlignment = Element.ALIGN_LEFT });
        HeaderTable.AddCell(new PdfPCell(CreateTable(25, "Social Neworking Website")) { Border = 0, PaddingTop = 3, HorizontalAlignment = Element.ALIGN_CENTER });

        HeaderTable.AddCell(new PdfPCell(new Phrase("Refered By", FontFactory.GetFont("Verdana", 11, Font.BOLD, BaseColor.BLACK))) { Border = 0, PaddingTop = 10, HorizontalAlignment = Element.ALIGN_LEFT });
        HeaderTable.AddCell(new PdfPCell(CreateTable(25, "Nitin")) { Border = 0, PaddingTop = 3, HorizontalAlignment = Element.ALIGN_CENTER });
        doc.Add(HeaderTable);
    }




    public void AddAssesment(Document Doc, int ApplicantID)
    {
        Applicant objApplicant = new Applicant();
        DataSet ds = objApplicant.GetApplicantAnswer(ApplicantID);
        List<Question> lstApplicant = SQLHelper.ContructList<Question>(ds);

        List<Question> lstPre = lstApplicant.Where(u => u.QuestionCategoryId == 1).Select(u => u).ToList();
        if (lstPre.Count > 0)
        {

            Paragraph p = new Paragraph(new Phrase("Pre-requisite Questions", FontFactory.GetFont("Verdana", 12, Font.NORMAL, BaseColor.GRAY)));
            p.SpacingBefore = 1.0f;
            p.SpacingAfter = 5.1f;
            Doc.Add(p);
            float[] assesmentHeader = { 90, 10 };
            PdfPTable HeaderTable = new PdfPTable(2);
            HeaderTable.SetWidths(assesmentHeader);
            HeaderTable.WidthPercentage = 100;
            foreach (Question que in lstPre)
            {
                string desc = que.Description;
                Phrase objP = new Phrase();
                if (que.isReAttempt)
                {
                    objP.Add(new Paragraph("*", FontFactory.GetFont("Verdana", 11, Font.NORMAL, BaseColor.RED)));
                }
                objP.Add(new Paragraph(desc, FontFactory.GetFont("Verdana", 11, Font.NORMAL, BaseColor.BLACK)));

                HeaderTable.AddCell(new PdfPCell(objP) { BorderColor = BaseColor.GRAY, Padding = 3, HorizontalAlignment = Element.ALIGN_LEFT });

                HeaderTable.AddCell(new PdfPCell(new Phrase(que.AnswerName, FontFactory.GetFont("Verdana", 11, Font.NORMAL, BaseColor.BLACK))) { BorderColor = BaseColor.GRAY, Padding = 3, HorizontalAlignment = Element.ALIGN_CENTER });
                if (que.QuesId == 14 && que.AnswerId == 27)
                {
                    HeaderTable.AddCell(new PdfPCell(GetConviction(ApplicantID)) { Colspan = 2, Border = 0, PaddingTop = 3, HorizontalAlignment = Element.ALIGN_CENTER });
                }
            }
            Doc.Add(HeaderTable);
        }


        lstPre = lstApplicant.Where(u => u.QuestionCategoryId == 2).Select(u => u).ToList();
        if (lstPre.Count > 0)
        {

            Paragraph p = new Paragraph(new Phrase("Assessment Questions", FontFactory.GetFont("Verdana", 12, Font.NORMAL, BaseColor.GRAY)));
            p.SpacingBefore = 1.0f;
            p.SpacingAfter = 5.1f;
            Doc.Add(p);
            float[] assesmentHeader = { 90, 10 };
            PdfPTable HeaderTable = new PdfPTable(2);
            HeaderTable.SetWidths(assesmentHeader);
            HeaderTable.WidthPercentage = 100;
            foreach (Question que in lstPre)
            {
                HeaderTable.AddCell(new PdfPCell(new Phrase(que.Description, FontFactory.GetFont("Verdana", 11, Font.NORMAL, BaseColor.BLACK))) { BorderColor = BaseColor.GRAY, Padding = 3, HorizontalAlignment = Element.ALIGN_LEFT });
                HeaderTable.AddCell(new PdfPCell(new Phrase(que.AnswerName, FontFactory.GetFont("Verdana", 11, Font.NORMAL, BaseColor.BLACK))) { BorderColor = BaseColor.GRAY, Padding = 3, HorizontalAlignment = Element.ALIGN_CENTER });

            }
            Doc.Add(HeaderTable);
        }


    }

    public void AddGeneralAssesment(Document Doc, int ApplicantID)
    {
        Question objQuestion = new Question();
        objQuestion.ApplicantId = ApplicantID;

        LookupQuestionCategory objLookQuesCat = new LookupQuestionCategory();

        List<LookupQuestionCategory> lLookupQuestionByCat = objLookQuesCat.GetLookupQuestionCategory();
        List<LookupQuestionCategory> parentCat = lLookupQuestionByCat.Where(u => u.Parent == 0).Select(u => u).ToList();

        foreach (LookupQuestionCategory objParent in parentCat)
        {
            Paragraph p = new Paragraph(new Phrase(objParent.Name, FontFactory.GetFont("Verdana", 12, Font.NORMAL, BaseColor.GRAY)));
            p.SpacingBefore = 1.0f;
            p.SpacingAfter = 2.1f;
            Doc.Add(p);

            List<LookupQuestionCategory> ChilCat = lLookupQuestionByCat.Where(u => u.Parent == objParent.QuestionCategoryId).Select(u => u).ToList();
            Decimal TotalPoints = 0;
            foreach (LookupQuestionCategory objChilD in ChilCat)
            {
                Paragraph pChilD = new Paragraph(new Phrase(objChilD.Name, FontFactory.GetFont("Verdana", 12, Font.NORMAL, BaseColor.GRAY)));
                p.SpacingBefore = 0.0f;
                //p.SpacingAfter = 5.1f;



                float[] assesmentHeader = { 90, 20 };
                PdfPTable HeaderTable = new PdfPTable(2);
                HeaderTable.SetWidths(assesmentHeader);
                HeaderTable.WidthPercentage = 100;



                Decimal CorrectCount = objQuestion.GetCorrectAnswerCount(objChilD.QuestionCategoryId, objQuestion.ApplicantId);
                TotalPoints += CorrectCount;



                Paragraph pCount = new Paragraph(new Phrase("Points: " + CorrectCount + "    ", FontFactory.GetFont("Verdana", 12, Font.NORMAL, BaseColor.GRAY)));
                pCount.Alignment = Element.ALIGN_RIGHT;
                //pCount.SpacingBefore = 1.0f;
                pCount.SpacingAfter = 5.1f;

                HeaderTable.AddCell(new PdfPCell(pChilD) { Border = 0, PaddingBottom = 5, PaddingTop = 10, HorizontalAlignment = Element.ALIGN_LEFT });

                HeaderTable.AddCell(new PdfPCell(pCount) { Border = 0, PaddingBottom = 5, PaddingTop = 10, HorizontalAlignment = Element.ALIGN_CENTER });

                Doc.Add(HeaderTable);

                float[] assesmentHeader1 = { 90, 10 };
                HeaderTable = new PdfPTable(2);
                HeaderTable.SetWidths(assesmentHeader1);
                HeaderTable.WidthPercentage = 100;


                List<Question> lstQuestions = objQuestion.GetQuestionByCategory(objChilD.QuestionCategoryId, objQuestion.ApplicantId);
                foreach (Question Ques in lstQuestions)
                {
                    Answer objAns = Ques.Answers.FirstOrDefault(u => u.Answered != 0);
                    if (objAns != null)
                    {
                        HeaderTable.AddCell(new PdfPCell(new Phrase(Ques.Description, FontFactory.GetFont("Verdana", 11, Font.NORMAL, BaseColor.BLACK))) { BorderColor = BaseColor.GRAY, Padding = 3, HorizontalAlignment = Element.ALIGN_LEFT });
                        HeaderTable.AddCell(new PdfPCell(new Phrase(objAns != null ? objAns.AnswerName : "-", FontFactory.GetFont("Verdana", 11, Font.NORMAL, BaseColor.BLACK))) { BorderColor = BaseColor.GRAY, Padding = 3, HorizontalAlignment = Element.ALIGN_CENTER });
                    }
                }
                Doc.Add(HeaderTable);
            }

            Decimal maxPoint = objQuestion.GetMaxPoint();
            Paragraph pTotCount = new Paragraph(new Phrase(" Total possible points/Total points received = " + maxPoint + "/" + TotalPoints + "\n", FontFactory.GetFont("Verdana", 12, Font.NORMAL, BaseColor.GRAY)));
            pTotCount.Alignment = Element.ALIGN_RIGHT;
            pTotCount.SpacingBefore = 1.0f;
            pTotCount.SpacingAfter = 0.1f;
            Doc.Add(pTotCount);

            decimal Percent = TotalPoints / maxPoint * 100;
            Percent = Math.Round(Percent, 2);
            pTotCount = new Paragraph(new Phrase("Compatibility Score = " + Percent + "%\n", FontFactory.GetFont("Verdana", 12, Font.NORMAL, BaseColor.GRAY)));
            pTotCount.Alignment = Element.ALIGN_RIGHT;
            pTotCount.SpacingBefore = 0.0f;
            pTotCount.SpacingAfter = 0.1f;
            Doc.Add(pTotCount);
        }

    }

    public string NumberToWords(int number)
    {
        //if (number == 0)
        //    return "Zero";

        //if (number < 0)
        //    return "minus " + NumberToWords(Math.Abs(number));

        //string words = "";

        //if (number > 0)
        //{
        //    if (words != "")
        //        words += "and ";

        //    var unitsMap = new[] { "Zero", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen" };
        //    var tensMap = new[] { "Zero", "Ten", "Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety" };

        //    if (number < 20)
        //        words += unitsMap[number];
        //    else
        //    {
        //        words += tensMap[number / 10];
        //        if ((number % 10) > 0)
        //            words += "-" + unitsMap[number % 10];
        //    }
        //}

        return number.ToString();
    }

    public void AddGeneralInfo(Document doc, int JobApplicationID)
    {
        ApplicantGeneralInfo obj = new ApplicantGeneralInfo();
        obj = obj.GetGeneralInfoByJobApplicantId(JobApplicationID).FirstOrDefault();
        if (obj != null)
        {
            Paragraph p = new Paragraph(new Phrase("General Information", FontFactory.GetFont("Verdana", 12, Font.NORMAL, BaseColor.GRAY)));
            p.SpacingBefore = 0.0f;
            p.SpacingAfter = 5.1f;
            doc.Add(p);

            float[] contentHeader = { 80, 20 };
            PdfPTable HeaderTable = new PdfPTable(2);
            HeaderTable.SetWidths(contentHeader);
            HeaderTable.WidthPercentage = 100;

            //HeaderTable.AddCell(new PdfPCell(new Phrase("Are you at least 18 years of age? ", FontFactory.GetFont("Verdana", 11, Font.BOLD, BaseColor.BLACK))) {  PaddingTop = 5,PaddingLeft =5,PaddingBottom =5, HorizontalAlignment = Element.ALIGN_LEFT });
            //HeaderTable.AddCell(new PdfPCell(new Phrase(obj.IsAdult ? "Yes" : "No", FontFactory.GetFont("Verdana", 11, Font.NORMAL, BaseColor.BLACK))) {   PaddingTop = 5, PaddingLeft = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT});

            //HeaderTable.AddCell(new PdfPCell(new Phrase("Can you, after employment, submit verification of your legal right to work in the United States? ", FontFactory.GetFont("Verdana", 11, Font.BOLD, BaseColor.BLACK))) {  PaddingTop = 5,PaddingLeft =5,PaddingBottom =5, HorizontalAlignment = Element.ALIGN_LEFT });
            //HeaderTable.AddCell(new PdfPCell(new Phrase(obj.SubmitVerification ? "Yes" : "No", FontFactory.GetFont("Verdana", 11, Font.NORMAL, BaseColor.BLACK))) {   PaddingTop = 5, PaddingLeft = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT});


            HeaderTable.AddCell(new PdfPCell(new Phrase("Have you ever been hired by Woodside? ", FontFactory.GetFont("Verdana", 11, Font.BOLD, BaseColor.BLACK))) { PaddingTop = 5, PaddingLeft = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
            HeaderTable.AddCell(new PdfPCell(new Phrase(obj.HiredByCSC ? "Yes" : "No", FontFactory.GetFont("Verdana", 11, Font.NORMAL, BaseColor.BLACK))) { PaddingTop = 5, PaddingLeft = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });


            if (obj.HiredByCSC)
            {
                HeaderTable.AddCell(new PdfPCell(new Phrase("If yes, location?", FontFactory.GetFont("Verdana", 11, Font.BOLD, BaseColor.BLACK))) { PaddingTop = 5, PaddingLeft = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
                HeaderTable.AddCell(new PdfPCell(new Phrase(obj.Location != null ? obj.Location : "", FontFactory.GetFont("Verdana", 11, Font.NORMAL, BaseColor.BLACK))) { PaddingTop = 5, PaddingLeft = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
                HeaderTable.AddCell(new PdfPCell(new Phrase("Employment Start Date(W/Woodside) ", FontFactory.GetFont("Verdana", 11, Font.BOLD, BaseColor.BLACK))) { PaddingTop = 5, PaddingLeft = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
                HeaderTable.AddCell(new PdfPCell(new Phrase(obj.StartDate != null ? obj.StartDate : "", FontFactory.GetFont("Verdana", 11, Font.NORMAL, BaseColor.BLACK))) { PaddingTop = 5, PaddingLeft = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
                HeaderTable.AddCell(new PdfPCell(new Phrase("Employment End Date(W/Woodside)  ", FontFactory.GetFont("Verdana", 11, Font.BOLD, BaseColor.BLACK))) { PaddingTop = 5, PaddingLeft = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
                HeaderTable.AddCell(new PdfPCell(new Phrase(obj.EndDate != null ? obj.EndDate : "", FontFactory.GetFont("Verdana", 11, Font.NORMAL, BaseColor.BLACK))) { PaddingTop = 5, PaddingLeft = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
            }


            HeaderTable.AddCell(new PdfPCell(new Phrase("Have you ever served in the US Armed Force?", FontFactory.GetFont("Verdana", 11, Font.BOLD, BaseColor.BLACK))) { PaddingTop = 5, PaddingLeft = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
            HeaderTable.AddCell(new PdfPCell(new Phrase(obj.ArmedForce ? "Yes" : "No", FontFactory.GetFont("Verdana", 11, Font.NORMAL, BaseColor.BLACK))) { PaddingTop = 5, PaddingLeft = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
            if (obj.ArmedForce)
            {
                HeaderTable.AddCell(new PdfPCell(new Phrase("If yes, Rank Held at Discharge", FontFactory.GetFont("Verdana", 11, Font.BOLD, BaseColor.BLACK))) { PaddingTop = 5, PaddingLeft = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
                HeaderTable.AddCell(new PdfPCell(new Phrase(obj.Rank, FontFactory.GetFont("Verdana", 11, Font.NORMAL, BaseColor.BLACK))) { PaddingTop = 5, PaddingLeft = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });

                HeaderTable.AddCell(new PdfPCell(new Phrase("If yes, Relevant duties at discharge", FontFactory.GetFont("Verdana", 11, Font.BOLD, BaseColor.BLACK))) { PaddingTop = 5, PaddingLeft = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
                HeaderTable.AddCell(new PdfPCell(new Phrase(obj.Duties, FontFactory.GetFont("Verdana", 11, Font.NORMAL, BaseColor.BLACK))) { PaddingTop = 5, PaddingLeft = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
            }
            // HeaderTable.AddCell(new PdfPCell(new Phrase("If yes,give their name(s)", FontFactory.GetFont("Verdana", 11, Font.BOLD, BaseColor.BLACK))) { PaddingTop = 5, PaddingLeft = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
            //HeaderTable.AddCell(new PdfPCell(new Phrase(obj.RelativeName != null ? obj.RelativeName : "", FontFactory.GetFont("Verdana", 11, Font.NORMAL, BaseColor.BLACK))) { PaddingTop = 5, PaddingLeft = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });



            HeaderTable.AddCell(new PdfPCell(new Phrase("Are you fluent In any foreign languages?", FontFactory.GetFont("Verdana", 11, Font.BOLD, BaseColor.BLACK))) { PaddingTop = 5, PaddingLeft = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
            HeaderTable.AddCell(new PdfPCell(new Phrase(obj.ForeignLanguage ? "Yes" : "No", FontFactory.GetFont("Verdana", 11, Font.NORMAL, BaseColor.BLACK))) { PaddingTop = 5, PaddingLeft = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
            if (obj.ForeignLanguage)
            {

                HeaderTable.AddCell(new PdfPCell(new Phrase("If yes, what languages? ", FontFactory.GetFont("Verdana", 11, Font.BOLD, BaseColor.BLACK))) { PaddingTop = 5, PaddingLeft = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
                HeaderTable.AddCell(new PdfPCell(new Phrase(obj.LanguageName != null ? obj.LanguageName : "", FontFactory.GetFont("Verdana", 11, Font.NORMAL, BaseColor.BLACK))) { PaddingTop = 5, PaddingLeft = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
            }
            HeaderTable.AddCell(new PdfPCell(new Phrase("If hired, what would be the first date you are available to start?", FontFactory.GetFont("Verdana", 11, Font.BOLD, BaseColor.BLACK))) { PaddingTop = 5, PaddingLeft = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
            HeaderTable.AddCell(new PdfPCell(new Phrase(obj.AvailableDate != null ? obj.AvailableDate : "", FontFactory.GetFont("Verdana", 11, Font.NORMAL, BaseColor.BLACK))) { PaddingTop = 5, PaddingLeft = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });

            //HeaderTable.AddCell(new PdfPCell(new Phrase("Are you able to work on nights & weekends ? ", FontFactory.GetFont("Verdana", 11, Font.BOLD, BaseColor.BLACK))) {  PaddingTop = 5,PaddingLeft =5,PaddingBottom =5, HorizontalAlignment = Element.ALIGN_LEFT });
            //HeaderTable.AddCell(new PdfPCell(new Phrase(obj.WorkedOnNightWeekend ? "Yes" : "No", FontFactory.GetFont("Verdana", 11, Font.NORMAL, BaseColor.BLACK))) {   PaddingTop = 5, PaddingLeft = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });

            //HeaderTable.AddCell(new PdfPCell(new Phrase("Do you have rellable transpotation?  ", FontFactory.GetFont("Verdana", 11, Font.BOLD, BaseColor.BLACK))) {  PaddingTop = 5,PaddingLeft =5,PaddingBottom =5, HorizontalAlignment = Element.ALIGN_LEFT });
            //HeaderTable.AddCell(new PdfPCell(new Phrase(obj.RellableTransport ? "Yes" : "No", FontFactory.GetFont("Verdana", 11, Font.NORMAL, BaseColor.BLACK))) {   PaddingTop = 5, PaddingLeft = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });

            doc.Add(HeaderTable);



            p = new Paragraph(new Phrase(" Other Name Used", FontFactory.GetFont("Verdana", 12, Font.NORMAL, BaseColor.GRAY)));
            p.SpacingBefore = 1.0f;
            p.SpacingAfter = 5.1f;
            doc.Add(p);

            float[] otherNameHeader = { 25, 25, 25, 25 };
            HeaderTable = new PdfPTable(4);
            HeaderTable.SetWidths(otherNameHeader);
            HeaderTable.WidthPercentage = 100;

            HeaderTable.AddCell(new PdfPCell(new Phrase("Suffix ", FontFactory.GetFont("Verdana", 11, Font.BOLD, BaseColor.BLACK))) { Border = 1, BorderWidthLeft = 0.5f, BorderWidthBottom = 0.5f, PaddingTop = 5, PaddingLeft = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
            HeaderTable.AddCell(new PdfPCell(new Phrase("Prev First Name ", FontFactory.GetFont("Verdana", 11, Font.BOLD, BaseColor.BLACK))) { Border = 1, BorderWidthLeft = 0.5f, BorderWidthBottom = 0.5f, PaddingTop = 5, PaddingLeft = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
            HeaderTable.AddCell(new PdfPCell(new Phrase("Prev Middle Name ", FontFactory.GetFont("Verdana", 11, Font.BOLD, BaseColor.BLACK))) { Border = 1, BorderWidthLeft = 0.5f, BorderWidthBottom = 0.5f, PaddingTop = 5, PaddingLeft = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
            HeaderTable.AddCell(new PdfPCell(new Phrase("Prev Last Name ", FontFactory.GetFont("Verdana", 11, Font.BOLD, BaseColor.BLACK))) { Border = 1, BorderWidthLeft = 0.5f, BorderWidthRight = 0.5f, BorderWidthBottom = 0.5f, PaddingTop = 5, PaddingLeft = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });

            HeaderTable.AddCell(new PdfPCell(new Phrase(obj.Suffix != null ? obj.Suffix : "--", FontFactory.GetFont("Verdana", 11, Font.NORMAL, BaseColor.BLACK))) { Border = 1, BorderWidthLeft = 0.5f, BorderWidthBottom = 0.5f, PaddingTop = 5, PaddingLeft = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
            HeaderTable.AddCell(new PdfPCell(new Phrase(obj.PrevFirstName != null ? obj.PrevFirstName : "--", FontFactory.GetFont("Verdana", 11, Font.NORMAL, BaseColor.BLACK))) { Border = 1, BorderWidthLeft = 0.5f, BorderWidthBottom = 0.5f, PaddingTop = 5, PaddingLeft = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
            HeaderTable.AddCell(new PdfPCell(new Phrase(obj.PrevMiddleName != null ? obj.PrevMiddleName : "--", FontFactory.GetFont("Verdana", 11, Font.NORMAL, BaseColor.BLACK))) { Border = 1, BorderWidthLeft = 0.5f, BorderWidthBottom = 0.5f, PaddingTop = 5, PaddingLeft = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
            HeaderTable.AddCell(new PdfPCell(new Phrase(obj.PrevLastName != null ? obj.PrevLastName : "--", FontFactory.GetFont("Verdana", 11, Font.NORMAL, BaseColor.BLACK))) { Border = 1, BorderWidthRight = 0.5f, BorderWidthLeft = 0.5f, BorderWidthBottom = 0.5f, PaddingTop = 5, PaddingLeft = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
            doc.Add(HeaderTable);
        }
    }


    public void AddDegreeAndOccupation(Document doc, int JobApplicationID)
    {
        ApplicantDegree objDegree = new ApplicantDegree();
        objDegree = objDegree.GetDegreeByJobApplicantId(JobApplicationID).FirstOrDefault();
        if (objDegree != null)
        {
            Paragraph p = new Paragraph(new Phrase("Highest Degree Obtained or Currently Enrolled", FontFactory.GetFont("Verdana", 12, Font.NORMAL, BaseColor.GRAY)));
            p.SpacingBefore = 1.0f;
            p.SpacingAfter = 5.1f;
            doc.Add(p);

            float[] otherNameHeader = { 25, 25, 25 };
            PdfPTable HeaderTable = new PdfPTable(3);
            HeaderTable.SetWidths(otherNameHeader);
            HeaderTable.WidthPercentage = 100;

            HeaderTable.AddCell(new PdfPCell(new Phrase("High school name", FontFactory.GetFont("Verdana", 11, Font.BOLD, BaseColor.BLACK))) { PaddingTop = 5, PaddingLeft = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
            HeaderTable.AddCell(new PdfPCell(new Phrase("Graduation Date", FontFactory.GetFont("Verdana", 11, Font.BOLD, BaseColor.BLACK))) { PaddingTop = 5, PaddingLeft = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
            HeaderTable.AddCell(new PdfPCell(new Phrase("Location ", FontFactory.GetFont("Verdana", 11, Font.BOLD, BaseColor.BLACK))) { PaddingTop = 5, PaddingLeft = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });

            HeaderTable.AddCell(new PdfPCell(new Phrase(objDegree.HighSchoolName != null ? objDegree.HighSchoolName : "--", FontFactory.GetFont("Verdana", 11, Font.NORMAL, BaseColor.BLACK))) { PaddingTop = 5, PaddingLeft = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
            HeaderTable.AddCell(new PdfPCell(new Phrase(objDegree.GradMonth, FontFactory.GetFont("Verdana", 11, Font.NORMAL, BaseColor.BLACK))) { PaddingTop = 5, PaddingLeft = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
            HeaderTable.AddCell(new PdfPCell(new Phrase(objDegree.Location != null ? objDegree.Location : "---", FontFactory.GetFont("Verdana", 11, Font.NORMAL, BaseColor.BLACK))) { PaddingTop = 5, PaddingLeft = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });

            HeaderTable.AddCell(new PdfPCell(new Phrase("Highest Level of Education", FontFactory.GetFont("Verdana", 11, Font.BOLD, BaseColor.BLACK))) { PaddingTop = 5, PaddingLeft = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
            HeaderTable.AddCell(new PdfPCell(new Phrase("School of Highest Level of Education?", FontFactory.GetFont("Verdana", 11, Font.BOLD, BaseColor.BLACK))) { PaddingTop = 5, PaddingLeft = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
            HeaderTable.AddCell(new PdfPCell(new Phrase("State ", FontFactory.GetFont("Verdana", 11, Font.BOLD, BaseColor.BLACK))) { PaddingTop = 5, PaddingLeft = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });


            HeaderTable.AddCell(new PdfPCell(new Phrase(objDegree.HighestEducation != null ? objDegree.HighestEducation : "--", FontFactory.GetFont("Verdana", 11, Font.NORMAL, BaseColor.BLACK))) { PaddingTop = 5, PaddingLeft = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
            HeaderTable.AddCell(new PdfPCell(new Phrase(objDegree.EducationSchool != null ? objDegree.EducationSchool : "--", FontFactory.GetFont("Verdana", 11, Font.NORMAL, BaseColor.BLACK))) { PaddingTop = 5, PaddingLeft = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
            HeaderTable.AddCell(new PdfPCell(new Phrase(objDegree.StateName != null ? objDegree.StateName : "---", FontFactory.GetFont("Verdana", 11, Font.NORMAL, BaseColor.BLACK))) { PaddingTop = 5, PaddingLeft = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
            doc.Add(HeaderTable);





            ApplicantOccupation objOcc = new ApplicantOccupation();
            List<ApplicantOccupation> objList = objOcc.GetOccupationByJobApplicantId(JobApplicationID);
            if (objList.Count > 0)
            {
                p = new Paragraph(new Phrase("Current Employer or Occupation", FontFactory.GetFont("Verdana", 12, Font.NORMAL, BaseColor.GRAY)));
                p.SpacingBefore = 1.0f;
                p.SpacingAfter = 5.1f;
                doc.Add(p);
                p = new Paragraph(new Phrase("For example, an occupation can include full-time students, and/or self-employment. If you are a student and/or self-employed please enter this into the Employer field below. If you are unemployed and do not have a current or recent employer please enter unemployed in the Employer field below.\n", FontFactory.GetFont("Verdana", 8, Font.NORMAL, BaseColor.GRAY)));
                doc.Add(p);
                
                foreach (ApplicantOccupation obj in objList)
                {
                    float[] EmployerNameHeader = { 25, 25, 25, 25 };
                    HeaderTable = new PdfPTable(4);
                    HeaderTable.SetWidths(EmployerNameHeader);
                    HeaderTable.WidthPercentage = 100;
                    p = new Paragraph(new Phrase(""));
                    p.SpacingAfter = 2.1f;
                    doc.Add(p);
                    HeaderTable.AddCell(new PdfPCell(new Phrase("Employer", FontFactory.GetFont("Verdana", 11, Font.BOLD, BaseColor.BLACK))) { PaddingTop = 5, PaddingLeft = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
                    HeaderTable.AddCell(new PdfPCell(new Phrase("Position/End Title ", FontFactory.GetFont("Verdana", 11, Font.BOLD, BaseColor.BLACK))) { PaddingTop = 5, PaddingLeft = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
                    HeaderTable.AddCell(new PdfPCell(new Phrase("City ", FontFactory.GetFont("Verdana", 11, Font.BOLD, BaseColor.BLACK))) { PaddingTop = 5, PaddingLeft = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
                    HeaderTable.AddCell(new PdfPCell(new Phrase("State  ", FontFactory.GetFont("Verdana", 11, Font.BOLD, BaseColor.BLACK))) { BorderWidthBottom = 0.5f, PaddingTop = 5, PaddingLeft = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });

                    HeaderTable.AddCell(new PdfPCell(new Phrase(obj.Employer, FontFactory.GetFont("Verdana", 11, Font.NORMAL, BaseColor.BLACK))) { PaddingTop = 5, PaddingLeft = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
                    HeaderTable.AddCell(new PdfPCell(new Phrase(obj.PossitionTitle, FontFactory.GetFont("Verdana", 11, Font.NORMAL, BaseColor.BLACK))) { PaddingTop = 5, PaddingLeft = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
                    HeaderTable.AddCell(new PdfPCell(new Phrase(obj.City, FontFactory.GetFont("Verdana", 11, Font.NORMAL, BaseColor.BLACK))) { PaddingTop = 5, PaddingLeft = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
                    HeaderTable.AddCell(new PdfPCell(new Phrase(obj.StateName, FontFactory.GetFont("Verdana", 11, Font.NORMAL, BaseColor.BLACK))) { BorderWidthBottom = 0.5f, PaddingTop = 5, PaddingLeft = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });


                    HeaderTable.AddCell(new PdfPCell(new Phrase("Start Date ", FontFactory.GetFont("Verdana", 11, Font.BOLD, BaseColor.BLACK))) { PaddingTop = 5, PaddingLeft = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
                    HeaderTable.AddCell(new PdfPCell(new Phrase("End Date ", FontFactory.GetFont("Verdana", 11, Font.BOLD, BaseColor.BLACK))) { PaddingTop = 5, PaddingLeft = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
                    HeaderTable.AddCell(new PdfPCell(new Phrase("Supervisor Name  ", FontFactory.GetFont("Verdana", 11, Font.BOLD, BaseColor.BLACK))) { PaddingTop = 5, PaddingLeft = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
                    HeaderTable.AddCell(new PdfPCell(new Phrase("Supervisor Phone   ", FontFactory.GetFont("Verdana", 11, Font.BOLD, BaseColor.BLACK))) { PaddingTop = 5, PaddingLeft = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });

                    HeaderTable.AddCell(new PdfPCell(new Phrase(obj.StartMonth, FontFactory.GetFont("Verdana", 11, Font.NORMAL, BaseColor.BLACK))) { PaddingTop = 5, PaddingLeft = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
                    HeaderTable.AddCell(new PdfPCell(new Phrase(obj.EndMonth, FontFactory.GetFont("Verdana", 11, Font.NORMAL, BaseColor.BLACK))) { PaddingTop = 5, PaddingLeft = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
                    HeaderTable.AddCell(new PdfPCell(new Phrase(obj.SupervisorName, FontFactory.GetFont("Verdana", 11, Font.NORMAL, BaseColor.BLACK))) { PaddingTop = 5, PaddingLeft = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
                    HeaderTable.AddCell(new PdfPCell(new Phrase(obj.SupervisorPhone, FontFactory.GetFont("Verdana", 11, Font.NORMAL, BaseColor.BLACK))) { BorderWidthBottom = 0.5f, PaddingTop = 5, PaddingLeft = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });



                    HeaderTable.AddCell(new PdfPCell(new Phrase("Ending Pay ", FontFactory.GetFont("Verdana", 11, Font.BOLD, BaseColor.BLACK))) { PaddingTop = 5, PaddingLeft = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
                    HeaderTable.AddCell(new PdfPCell(new Phrase("May we contact for reference ", FontFactory.GetFont("Verdana", 11, Font.BOLD, BaseColor.BLACK))) { Colspan = 2, PaddingTop = 5, PaddingLeft = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
                    HeaderTable.AddCell(new PdfPCell(new Phrase("Job Duties  ", FontFactory.GetFont("Verdana", 11, Font.BOLD, BaseColor.BLACK))) { PaddingTop = 5, PaddingLeft = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });


                    HeaderTable.AddCell(new PdfPCell(new Phrase(obj.EndingPay, FontFactory.GetFont("Verdana", 11, Font.NORMAL, BaseColor.BLACK))) { PaddingTop = 5, PaddingLeft = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
                    HeaderTable.AddCell(new PdfPCell(new Phrase(obj.ContactReference ? "Yes" : "No", FontFactory.GetFont("Verdana", 11, Font.NORMAL, BaseColor.BLACK))) { Colspan = 2, PaddingTop = 5, PaddingLeft = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
                    HeaderTable.AddCell(new PdfPCell(new Phrase(obj.JobDuties, FontFactory.GetFont("Verdana", 11, Font.NORMAL, BaseColor.BLACK))) { PaddingTop = 5, PaddingLeft = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });

                    HeaderTable.AddCell(new PdfPCell(new Phrase("Reason for Leaving:" + obj.ReasonLeaving, FontFactory.GetFont("Verdana", 11, Font.NORMAL, BaseColor.BLACK))) { Colspan = 4, BorderWidthBottom = 0.5f, PaddingTop = 5, PaddingLeft = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });

                    doc.Add(HeaderTable);

                }
            }


        }
    }


    public void AddCertificateAndReference(Document doc, int JobApplicationID)
    {
        ApplicantCertification objDegree = new ApplicantCertification();
        List<ApplicantCertification> objList = objDegree.GetCertificationByJobApplicantId(JobApplicationID);
        if (objList.Count > 0)
        {
            Paragraph p = new Paragraph(new Phrase("Certification/Licenses", FontFactory.GetFont("Verdana", 12, Font.NORMAL, BaseColor.GRAY)));
            p.SpacingBefore = 1.0f;
            p.SpacingAfter = 5.1f;
            doc.Add(p);
            foreach (ApplicantCertification obj in objList)
            {
                p = new Paragraph(new Phrase(""));
                p.SpacingAfter = 2.1f;
                doc.Add(p);
                float[] EmployerNameHeader = { 25, 25, 25 };
                PdfPTable HeaderTable = new PdfPTable(3);
                HeaderTable.SetWidths(EmployerNameHeader);
                HeaderTable.WidthPercentage = 100;

                HeaderTable.AddCell(new PdfPCell(new Phrase("Cert/License Type", FontFactory.GetFont("Verdana", 11, Font.BOLD, BaseColor.BLACK))) { PaddingTop = 5, PaddingLeft = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
                HeaderTable.AddCell(new PdfPCell(new Phrase("Cert/License Number ", FontFactory.GetFont("Verdana", 11, Font.BOLD, BaseColor.BLACK))) { PaddingTop = 5, PaddingLeft = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
                HeaderTable.AddCell(new PdfPCell(new Phrase("Cert/License Agency ", FontFactory.GetFont("Verdana", 11, Font.BOLD, BaseColor.BLACK))) { PaddingTop = 5, PaddingLeft = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });

                HeaderTable.AddCell(new PdfPCell(new Phrase(obj.LicenseType, FontFactory.GetFont("Verdana", 11, Font.NORMAL, BaseColor.BLACK))) { PaddingTop = 5, PaddingLeft = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
                HeaderTable.AddCell(new PdfPCell(new Phrase(obj.LicenseNumber, FontFactory.GetFont("Verdana", 11, Font.NORMAL, BaseColor.BLACK))) { PaddingTop = 5, PaddingLeft = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
                HeaderTable.AddCell(new PdfPCell(new Phrase(obj.Agency, FontFactory.GetFont("Verdana", 11, Font.NORMAL, BaseColor.BLACK))) { PaddingTop = 5, PaddingLeft = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });


                HeaderTable.AddCell(new PdfPCell(new Phrase("Cert/License Description", FontFactory.GetFont("Verdana", 11, Font.BOLD, BaseColor.BLACK))) { PaddingTop = 5, PaddingLeft = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
                HeaderTable.AddCell(new PdfPCell(new Phrase("Issue Date ", FontFactory.GetFont("Verdana", 11, Font.BOLD, BaseColor.BLACK))) { PaddingTop = 5, PaddingLeft = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
                HeaderTable.AddCell(new PdfPCell(new Phrase("Expiry Date ", FontFactory.GetFont("Verdana", 11, Font.BOLD, BaseColor.BLACK))) { PaddingTop = 5, PaddingLeft = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });

                HeaderTable.AddCell(new PdfPCell(new Phrase(obj.Description, FontFactory.GetFont("Verdana", 11, Font.NORMAL, BaseColor.BLACK))) { PaddingTop = 5, PaddingLeft = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });

                HeaderTable.AddCell(new PdfPCell(new Phrase(obj.IssueMonth, FontFactory.GetFont("Verdana", 11, Font.NORMAL, BaseColor.BLACK))) { PaddingTop = 5, PaddingLeft = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
                HeaderTable.AddCell(new PdfPCell(new Phrase(obj.ExpirationMonth, FontFactory.GetFont("Verdana", 11, Font.NORMAL, BaseColor.BLACK))) { PaddingTop = 5, PaddingLeft = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });

                doc.Add(HeaderTable);

            }
        }

        ApplicantReference objReferences = new ApplicantReference();
        List<ApplicantReference> objRefList = objReferences.GetReferenceByJobApplicantId(JobApplicationID);
        if (objRefList.Count > 0)
        {
            Paragraph p = new Paragraph(new Phrase("References", FontFactory.GetFont("Verdana", 12, Font.NORMAL, BaseColor.GRAY)));
            p.SpacingBefore = 1.0f;
            p.SpacingAfter = 3.1f;
            doc.Add(p);
            foreach (ApplicantReference obj in objRefList)
            {
                p = new Paragraph(new Phrase(""));
                p.SpacingAfter = 2.1f;
                doc.Add(p);
                float[] EmployerNameHeader = { 25, 25, 25, 25 };
                PdfPTable HeaderTable = new PdfPTable(4);
                HeaderTable.SetWidths(EmployerNameHeader);
                HeaderTable.WidthPercentage = 100;

                HeaderTable.AddCell(new PdfPCell(new Phrase("Name", FontFactory.GetFont("Verdana", 11, Font.BOLD, BaseColor.BLACK))) { PaddingTop = 5, PaddingLeft = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
                HeaderTable.AddCell(new PdfPCell(new Phrase("Relationship ", FontFactory.GetFont("Verdana", 11, Font.BOLD, BaseColor.BLACK))) { PaddingTop = 5, PaddingLeft = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
                HeaderTable.AddCell(new PdfPCell(new Phrase("Primary Phone Number ", FontFactory.GetFont("Verdana", 11, Font.BOLD, BaseColor.BLACK))) { PaddingTop = 5, PaddingLeft = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
                HeaderTable.AddCell(new PdfPCell(new Phrase("Alternate Phone Number", FontFactory.GetFont("Verdana", 11, Font.BOLD, BaseColor.BLACK))) { PaddingTop = 5, PaddingLeft = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });


                HeaderTable.AddCell(new PdfPCell(new Phrase(obj.Name, FontFactory.GetFont("Verdana", 11, Font.NORMAL, BaseColor.BLACK))) { PaddingTop = 5, PaddingLeft = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
                HeaderTable.AddCell(new PdfPCell(new Phrase(obj.RelationShip, FontFactory.GetFont("Verdana", 11, Font.NORMAL, BaseColor.BLACK))) { PaddingTop = 5, PaddingLeft = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
                HeaderTable.AddCell(new PdfPCell(new Phrase(obj.PrimaryPhone, FontFactory.GetFont("Verdana", 11, Font.NORMAL, BaseColor.BLACK))) { PaddingTop = 5, PaddingLeft = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });

                HeaderTable.AddCell(new PdfPCell(new Phrase(obj.AlternatePhone, FontFactory.GetFont("Verdana", 11, Font.NORMAL, BaseColor.BLACK))) { PaddingTop = 5, PaddingLeft = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });


                doc.Add(HeaderTable);

            }

        }

    }


    public void AddDisclosure(Document doc, int JobApplication, int applicantID)
    {
        ApplicantDisclosure obj = new ApplicantDisclosure();
        obj = obj.GetDisclosureByJobApplicantId(JobApplication).FirstOrDefault();
        if (obj != null)
        {
            Paragraph p = new Paragraph(new Phrase("EEO Voluntary Self Disclosure", FontFactory.GetFont("Verdana", 12, Font.NORMAL, BaseColor.GRAY)));
            p.SpacingBefore = 1.0f;
            p.SpacingAfter = 5.1f;
            doc.Add(p);
            p = new Paragraph(new Phrase("Woodside-USA is an equal opportunity employer. In recognition of its responsibility to its staff and the community it serves. Woodside-USA affirms its policy to assure fair and equal treatment in its employment process policies for all persons. We consider applicants for all positions without regard to race, color, religion, sex, age, national origin, disabled or veteran status, or other legally protected status. To help us track our organizational success, we ask your assistance in filling out this voluntary self-identification form. In addition to our internal tracking. Woodside-USA must meet government record-keeping and reporting requirements. Completion of this form is voluntary and will not affect your consideration for employment with Woodside-USA.\n\n", FontFactory.GetFont("Verdana", 8, Font.NORMAL, BaseColor.GRAY)));
            doc.Add(p);
            
            float[] EmployerNameHeader = { 25 };
            PdfPTable HeaderTable = new PdfPTable(1);
            HeaderTable.SetWidths(EmployerNameHeader);
            HeaderTable.WidthPercentage = 100;

            HeaderTable.AddCell(new PdfPCell(new Phrase("Ethnicity/Race", FontFactory.GetFont("Verdana", 11, Font.BOLD, BaseColor.BLACK))) { Border = 0, PaddingTop = 5, PaddingLeft = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
            HeaderTable.AddCell(new PdfPCell(new Phrase(obj.Ethnicity, FontFactory.GetFont("Verdana", 11, Font.NORMAL, BaseColor.BLACK))) { Border = 0, PaddingTop = 5, PaddingLeft = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
            HeaderTable.AddCell(new PdfPCell(new Phrase("Gender: ", FontFactory.GetFont("Verdana", 11, Font.BOLD, BaseColor.BLACK))) { Border = 0, PaddingTop = 5, PaddingLeft = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
            HeaderTable.AddCell(new PdfPCell(new Phrase(obj.Gender, FontFactory.GetFont("Verdana", 11, Font.NORMAL, BaseColor.BLACK))) { Border = 0, PaddingTop = 5, PaddingLeft = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });

            HeaderTable.AddCell(new PdfPCell(new Phrase("Veteran Status", FontFactory.GetFont("Verdana", 11, Font.BOLD, BaseColor.BLACK))) { Border = 0, PaddingTop = 5, PaddingLeft = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
            HeaderTable.AddCell(new PdfPCell(new Phrase(obj.VeteranStatus, FontFactory.GetFont("Verdana", 11, Font.NORMAL, BaseColor.BLACK))) { Border = 0, PaddingTop = 5, PaddingLeft = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });


            HeaderTable.AddCell(new PdfPCell(new Phrase("", FontFactory.GetFont("Verdana", 11, Font.NORMAL, BaseColor.BLACK))) { Border = 1, PaddingTop = 25, PaddingLeft = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
            doc.Add(HeaderTable);


            p = new Paragraph(new Phrase("Agree\n", FontFactory.GetFont("Verdana", 12, Font.NORMAL, BaseColor.GRAY)));
            doc.Add(p);
            p = new Paragraph(new Phrase("\n\nI hereby certify that all statement made in this application are true and correct to the best of my knowledge and belief. I authorize past employers, schools, persons and organizations having relevant information or knowledge to release to Woodside Properties for its use in deciding whether or not to offer me employment and specifically waive any required written notification. I hereby release employers, schools, persons and organizations from all liability in responding to inquiries in connection with my application. Upon written request by me, within a reasonable period of time, Woodside Properties will make available to me the nature and scope of all records of every type that are obtained.\n\n\n\n", FontFactory.GetFont("Verdana", 8, Font.NORMAL, BaseColor.GRAY)));
            doc.Add(p);

            Applicant objApplicant = new Applicant();
            DataSet ds = objApplicant.GetApplicantByID(applicantID);

            PdfPTable signatureTble = new PdfPTable(2);


            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                signatureTble.SetWidths(new float[] { 50, 50 });
                signatureTble.WidthPercentage = 100;
                signatureTble.AddCell(new PdfPCell(new Phrase(ds.Tables[0].Rows[0]["FirstName"] + " " + ds.Tables[0].Rows[0]["LastName"], FontFactory.GetFont("Verdana", 12, Font.BOLD, BaseColor.GRAY))) { Border = 0, PaddingTop = 5, PaddingLeft = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
                signatureTble.AddCell(new PdfPCell(new Phrase(obj.AppliedOn + " ", FontFactory.GetFont("Verdana", 12, Font.BOLD, BaseColor.GRAY))) { Border = 0, PaddingTop = 5, PaddingLeft = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
                doc.Add(signatureTble);
                //p = new Paragraph(new Phrase(ds.Tables[0].Rows[0]["FirstName"] + " " + ds.Tables[0].Rows[0]["LastName"], FontFactory.GetFont("Verdana", 12, Font.BOLD, BaseColor.GRAY)));
                //doc.Add(p);
                //p = new Paragraph(new Phrase(ds.Tables[0].Rows[0]["AppliedOn"] + " ", FontFactory.GetFont("Verdana", 12, Font.BOLD, BaseColor.GRAY)));
                //doc.Add(p);
            }
        }
    }

    private void AddCellToHeader(PdfPTable tableLayout, string cellText)
    {

        tableLayout.AddCell(new PdfPCell(new Phrase(cellText, new Font(Font.FontFamily.HELVETICA, 8, 1, BaseColor.WHITE))) { HorizontalAlignment = Element.ALIGN_CENTER, Padding = 5, BackgroundColor = new BaseColor(0, 51, 102) });

    }


    private PdfPTable GetConviction(int ApplicantID)
    {
        PdfPTable pdf = new PdfPTable(3);

        pdf.AddCell(new PdfPCell(new Phrase("Title", FontFactory.GetFont("Verdana", 8, Font.NORMAL, BaseColor.BLACK))) { HorizontalAlignment = Element.ALIGN_CENTER, Border = 1, BorderWidthLeft = 0.5f, BorderWidthTop = 0.5f, BorderWidthBottom = 0.5f, BorderWidthRight = 0.5f, FixedHeight = 15 });
        pdf.AddCell(new PdfPCell(new Phrase("Conviction Type", FontFactory.GetFont("Verdana", 8, Font.NORMAL, BaseColor.BLACK))) { HorizontalAlignment = Element.ALIGN_CENTER, Border = 1, BorderWidthLeft = 0.5f, BorderWidthTop = 0.5f, BorderWidthBottom = 0.5f, BorderWidthRight = 0.5f, FixedHeight = 15 });
        pdf.AddCell(new PdfPCell(new Phrase("Conviction Year", FontFactory.GetFont("Verdana", 8, Font.NORMAL, BaseColor.BLACK))) { HorizontalAlignment = Element.ALIGN_CENTER, Border = 1, BorderWidthLeft = 0.5f, BorderWidthTop = 0.5f, BorderWidthBottom = 0.5f, BorderWidthRight = 0.5f, FixedHeight = 15 });

        Conviction objCOnv = new Conviction() { ApplicantId = ApplicantID };
        List<Conviction> lstConv = objCOnv.GetConvictionByID();
        foreach (Conviction conv in lstConv)
        {
            pdf.AddCell(new PdfPCell(new Phrase(conv.name, FontFactory.GetFont("Verdana", 8, Font.NORMAL, BaseColor.BLACK))) { HorizontalAlignment = Element.ALIGN_CENTER, Border = 1, BorderWidthLeft = 0.5f, BorderWidthTop = 0.5f, BorderWidthBottom = 0.5f, BorderWidthRight = 0.5f, FixedHeight = 15 });
            pdf.AddCell(new PdfPCell(new Phrase(conv.Type, FontFactory.GetFont("Verdana", 8, Font.NORMAL, BaseColor.BLACK))) { HorizontalAlignment = Element.ALIGN_CENTER, Border = 1, BorderWidthLeft = 0.5f, BorderWidthTop = 0.5f, BorderWidthBottom = 0.5f, BorderWidthRight = 0.5f, FixedHeight = 15 });
            pdf.AddCell(new PdfPCell(new Phrase(conv.Year.ToString(), FontFactory.GetFont("Verdana", 8, Font.NORMAL, BaseColor.BLACK))) { HorizontalAlignment = Element.ALIGN_CENTER, Border = 1, BorderWidthLeft = 0.5f, BorderWidthTop = 0.5f, BorderWidthBottom = 0.5f, BorderWidthRight = 0.5f, FixedHeight = 15 });


        }


        return pdf;
    }


    private PdfPTable CreateTable(int column, string data)
    {
        PdfPTable pdf = new PdfPTable(column);
        char[] strArray = data.ToCharArray();
        int length = strArray.Length;
        for (int i = 0; i < column; i++)
        {
            if (length > i)
                pdf.AddCell(new PdfPCell(new Phrase(strArray[i].ToString().ToUpper(), FontFactory.GetFont("Verdana", 8, Font.NORMAL, BaseColor.BLACK))) { HorizontalAlignment = Element.ALIGN_CENTER, Border = 1, BorderWidthLeft = 0.5f, BorderWidthTop = 0.5f, BorderWidthBottom = 0.5f, BorderWidthRight = 0.5f, FixedHeight = 15 });
            else
                pdf.AddCell(new PdfPCell() { HorizontalAlignment = Element.ALIGN_CENTER, Border = 1, BorderWidthLeft = 0.5f, BorderWidthTop = 0.5f, BorderWidthBottom = 0.5f, BorderWidthRight = 0.5f, FixedHeight = 15 });

        }


        return pdf;
    }


    // Method to add single cell to the body

    private void AddCellToBody(PdfPTable tableLayout, string cellText)
    {

        tableLayout.AddCell(new PdfPCell(new Phrase(cellText, new Font(Font.FontFamily.HELVETICA, 8, 1, BaseColor.BLACK))) { HorizontalAlignment = Element.ALIGN_CENTER, Padding = 5, BackgroundColor = BaseColor.WHITE });

    }


}

public class PageEventHelper : PdfPageEventHelper
{

    public override void OnEndPage(PdfWriter writer, Document document)
    {
        base.OnEndPage(writer, document);

        var content = writer.DirectContent;
        var pageBorderRect = new Rectangle(document.PageSize);

        pageBorderRect.Left += document.LeftMargin - 10;
        pageBorderRect.Right -= document.RightMargin - 10;
        pageBorderRect.Top -= document.TopMargin - 10;
        pageBorderRect.Bottom += document.BottomMargin - 10;

        content.SetColorStroke(BaseColor.BLACK);
        content.Rectangle(pageBorderRect.Left, pageBorderRect.Bottom, pageBorderRect.Width, pageBorderRect.Height);
        content.Stroke();
    }
}
