﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

using HPortalService.DAL;

namespace HPortalService
{
    public class ApplicantCompDesign
    {
        public int ApplicantWageId { get; set; }
        public int ApplicantId { get; set; }
        public string Physician { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }

        public int insApplicantCompDesign(ApplicantCompDesign obj)
        {
            WorkerCompDesignDAL _ApplicationDAL = new WorkerCompDesignDAL();
            return _ApplicationDAL.insApplicantCompDesign(obj);
        }
        public DataSet GetApplicantCompDesignByID(int ID)
        {
            WorkerCompDesignDAL _ApplicationDAL = new WorkerCompDesignDAL();
            return _ApplicationDAL.GetApplicantCompDesignByID(ID);
        }
    }
}