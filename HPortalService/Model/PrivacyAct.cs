﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

using HPortalService.DAL;

namespace HPortalService
{
    public class PrivacyAct
    {
        public int ApplicantId { get; set; }
        public int ApplicantPrivacyActId { get; set; }
        public string Name { get; set; }
        public string ApplicantInfo { get; set; }
        public string SSN { get; set; }
        public string DateOfBirth { get; set; }
        public string Gender { get; set; }
        public string HomeAddress { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public string PrimaryTelephone { get; set; }
        public string PrimaryEmail { get; set; }
        public string AlternateTelephone { get; set; }
        public string AlternateEMail { get; set; }
        public string Allergies { get; set; }
        public string Medications { get; set; }
        public string PrimaryContactName { get; set; }
        public string Relationship { get; set; }
        public string PrimaryContactTelephone { get; set; }
        public string PrimaryContactEmail { get; set; }
        public string ContactAlternateTelephone { get; set; }
        public string ContactAlternateEMail { get; set; }
        public string AlternateContactName1 { get; set; }
        public string AlternateRelationship1 { get; set; }
        public string AlternatePTelephone1 { get; set; }
        public string AlternatePEmailAddress1 { get; set; }
        public string AlternateATelephone1 { get; set; }
        public string AlternateAEmailAddress1 { get; set; }
        public string AlternateContactName2 { get; set; }
        public string AlternateRelationship2 { get; set; }
        public string AlternatePTelephone2 { get; set; }
        public string AlternatePEmailAddress2 { get; set; }
        public string AlternateATelephone2 { get; set; }
        public string AlternateAEmailAddress2 { get; set; }       


        public enum RehireSource
        {
            OfferedPopupAdmin = 1,
            EmailVerifyApplicant = 2,
            ReturningEmployeeApplicant = 3,
            HiredByCSCApplicant = 4

        }

        public List<PrivacyAct> fnApplicantPrivacyAct(int ApplicantId)
        {
            DataSet ds;
            PrivacyActDAL _ApplicationDAL = new PrivacyActDAL();
            ds = _ApplicationDAL.fnGetApplicantPrivacyAct(ApplicantId);
            List<PrivacyAct> lstApplicant = SQLHelper.ContructList<PrivacyAct>(ds);
            foreach (PrivacyAct objApp in lstApplicant)
            {
                objApp.DateOfBirth = objApp.DateOfBirth != null ? Convert.ToDateTime(objApp.DateOfBirth).ToString("MM/dd/yyyy") : null;
            }
            return lstApplicant;
        }
       public class LookUpRelationship
        {
            public int EmergencyRelationshipTypesId { get; set; }
            public string Name { get; set; }
        }
       public List<LookUpRelationship> fnGetRelationShip(int ApplicantId)
        {
            DataSet ds;
            PrivacyActDAL _ApplicationDAL = new PrivacyActDAL();
            ds = _ApplicationDAL.fnGetRelationShip(ApplicantId);
            List<LookUpRelationship> lstApplicant = SQLHelper.ContructList<LookUpRelationship>(ds);
            return lstApplicant;
        }
    }

}