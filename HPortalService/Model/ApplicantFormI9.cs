﻿using HPortalService.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace HPortalService.Model
{
    public class ApplicantFormI9
    {


        public int ApplicantFormI9Id { get; set; }
        public int ApplicantId { get; set; }
        public string FirstName { get; set; }
        public string MiddleInitial { get; set; }
        public string LastName { get; set; }
        public string OtherLastName { get; set; }
        public string Address { get; set; }
        public string AptNumber { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string DateOfBirth { get; set; }
        public string SSN { get; set; }
        public string Email { get; set; }
        public string Telephone { get; set; }
        public string AttestPerjury { get; set; }
        public string RegistrationNumber { get; set; }
        public string RegistrationNumberType { get; set; }
        public string ExpirationDate { get; set; }
        public string FormI94AddminssionNumber { get; set; }
        public string ForeignPassportNumber { get; set; }
        public string IssuanceCountry { get; set; }
        public string EmployeeSignature { get; set; }
        public string EmployeeSignatureDate { get; set; }
        public string Preparer { get; set; }
        public string PreparerNumber { get; set; }
        public string SignaturePreparer { get; set; }
        public string PreparerSignatureDate { get; set; }
        public string PFirstName { get; set; }
        public string PLastName { get; set; }
        public string PAddress { get; set; }
        public string PCity { get; set; }
        public string PState { get; set; }
        public string PZipCode { get; set; }
        public string NoOfAllowances { get; set; }

        public string BranchName { get; set; }
        public string BranchAddress1 { get; set; }
        public string BranchAddress2 { get; set; }


        public string BranchCityName { get; set; }
        public string BranchState { get; set; }
        public string BranchCountry { get; set; }
        public string BranchZip { get; set; }
        public string BranchPhone1 { get; set; }
        public string BranchPhone2 { get; set; }
        public string BranchFax { get; set; }
        public string BranchEmail { get; set; }
        public string CompnayTaxId { get; set; }
        public string CompnayName { get; set; }
        public string FirstDayEmployment { get; set; }
        public int VerifiedBy { get; set; }
        public string FirstNameEmployer { get; set; }
        public string LastNameEmployer { get; set; }
        public string Type { get; set; }
        public int UserId { get; set; }
        public string DocumentNo { get; set; }
        public string ListADocumentNumber { get; set; }
        public string ListBDocumentNumber { get; set; }
        public string ListCDocumentNumber { get; set; }  
        public string DocumentType { get; set; }
        public string EmailId { get; set; }
        public string DOB { get; set; }
        public string HireDate { get; set; }
        public string CitizenshipStatus { get; set; }
        public string ListBDocumentType { get; set; }
        public string ListBIssuingAuthority { get; set; }
        public string ListBExpirationDate { get; set; }
        public string AlliancePartnerCompanyID { get; set; }
        public string AlienI94Number { get; set; }
        public string AlienOrI94 { get; set; }
        public string DocExpDate { get; set; }
        public string PassportIssuingAuthority { get; set; }
        public string ListCDocumentType { get; set; }
        public string ListCIssuingAuthority { get; set; }
        public string ListCExpirationDate { get; set; }
        public string url { get; set; }
        public int SaveApplicantFormI9(ApplicantFormI9 obj)
        {
            ApplicantDAL _ApplicationDAL = new ApplicantDAL();
            return _ApplicationDAL.SaveApplicantFormI9(obj);
        }
        public int fnUpdateApplicantFormI9(int ApplicantId, int UserId, DateTime FirstDayEmployment, string fileName, int Type)
        {
            ApplicantDAL _ApplicationDAL = new ApplicantDAL();
            return _ApplicationDAL.UpdateApplicantFormI9(ApplicantId, UserId, FirstDayEmployment, fileName, Type);
        }
        public DataSet fnGetApplicantFormI9(int ApplicantId)
        {
            ApplicantDAL _ApplicationDAL = new ApplicantDAL();
            return _ApplicationDAL.GetApplicantFormI9(ApplicantId);
        }
        public List<ApplicantFormI9> GetApplicantFormI9(ApplicantFormI9 obj)
        {
            // 
            ApplicantDAL _ApplicationDAL = new ApplicantDAL();
            List<ApplicantFormI9> lstApplicantFormI9 = SQLHelper.ContructList<ApplicantFormI9>(_ApplicationDAL.GetApplicantFormI9(obj));
            return lstApplicantFormI9;
        }
        public List<ApplicantFormI9> GetW4Form(ApplicantFormI9 obj)
        {
            // 
            ApplicantDAL _ApplicationDAL = new ApplicantDAL();
            List<ApplicantFormI9> lstW4Form = SQLHelper.ContructList<ApplicantFormI9>(_ApplicationDAL.GetW4Form(obj));
            return lstW4Form;

        }

        public DataSet GetApplicantFormI9Details(ApplicantFormI9 obj)
        {
            ApplicantDAL _ApplicationDAL = new ApplicantDAL();
            return _ApplicationDAL.GetApplicantFormI9(obj);
        }

        public List<ApplicantFormI9> fnGetApplicantEVerifyList(int ApplicantId)
        {
            ApplicantDAL _ApplicationDAL = new ApplicantDAL();
            List<ApplicantFormI9> lstApplicantFormI9 = SQLHelper.ContructList<ApplicantFormI9>(_ApplicationDAL.fnGetApplicantEVerifyList(ApplicantId));
            return lstApplicantFormI9;
        }
        static string AlliancePartnerID = Startup.AlliancePartnerID;
        static string AlliancePartnerLogin = Startup.AlliancePartnerLogin;
        static string AlliancePartnerPassword = Startup.AlliancePartnerPassword;
        public string fnGeneratUrl(DataTable dt,string firstDayEmployment)
        {
            string url = "";
            string DocExpDate = "";
            if (dt.Rows[0]["CitizenshipStatus"].ToString() == "3")
            {
                DocExpDate = (!string.IsNullOrEmpty(Convert.ToString(dt.Rows[0]["DocExpDate"])) ? Convert.ToDateTime(dt.Rows[0]["DocExpDate"]).ToString("MM/dd/yyyy") : "");
            }
            else
            {
                DocExpDate = (!string.IsNullOrEmpty(Convert.ToString(dt.Rows[0]["ListBExpirationDate"])) ? Convert.ToDateTime(dt.Rows[0]["ListBExpirationDate"]).ToString("MM/dd/yyyy") : "");
            }
            if (dt.Rows[0]["DocumentType"].ToString() == "List A")
            {
                url = "https://www.formi9.com/formi9verify/formI9loginservice.aspx?" +
                             "AlliancePartnerID=" +AlliancePartnerID+
                             "&AlliancePartnerLogin=" + AlliancePartnerLogin +
                             "&AlliancePartnerPassword=" +AlliancePartnerPassword +
                             "&AlliancePartnerCompanyID=" + dt.Rows[0]["AlliancePartnerCompanyID"] + "&ReturnProcessURL=https://jdpws.schedulingsite.com/api/Everify&B2BUserName=&Target=EVPRequest" +
                             "&RequestID=" + dt.Rows[0]["ApplicantId"] +
                             "&LastName=" + HttpUtility.UrlEncode(dt.Rows[0]["LastName"].ToString()) +
                             "&FirstName=" + HttpUtility.UrlEncode(dt.Rows[0]["FirstName"].ToString()) +
                             "&MiddleInitial=" + HttpUtility.UrlEncode(dt.Rows[0]["MiddleInitial"].ToString()) +
                             "&MaidenName=&SSN=" + Convert.ToString(dt.Rows[0]["SSN"]) +
                              "&DOB=" + (!string.IsNullOrEmpty(Convert.ToString(dt.Rows[0]["DOB"])) ? Convert.ToDateTime(dt.Rows[0]["DOB"]).ToString("MM/dd/yyyy") : "") +
                             "&Address=" + Convert.ToString(dt.Rows[0]["Address"]) +
                              "&HireDate=" + (!string.IsNullOrEmpty(Convert.ToString(dt.Rows[0]["HireDate"])) ? Convert.ToDateTime(dt.Rows[0]["HireDate"]).ToString("MM/dd/yyyy") : Convert.ToDateTime(firstDayEmployment).ToString("MM/dd/yyyy")) +
                             "&CitizenshipStatus=" + Convert.ToString(dt.Rows[0]["CitizenshipStatus"]) +
                             "&DocumentType=" + Convert.ToString(dt.Rows[0]["ListBDocumentType"]) +
                             "&AlienI94Number=" + Convert.ToString(dt.Rows[0]["AlienI94Number"]) +
                             "&AlienDocType=" + Convert.ToString(dt.Rows[0]["AlienOrI94"]) +
                             "&Passport=" + Convert.ToString(dt.Rows[0]["ListBDocumentNumber"]) +
                             "&DocumentNumber=" + Convert.ToString(dt.Rows[0]["ListBDocumentNumber"]) +
                              "&DocExpDate=" + DocExpDate +
                              "&PassportIssuingAuthority=" + Convert.ToString(dt.Rows[0]["PassportIssuingAuthority"]) +
                              "&EmailAddress=" + Convert.ToString(dt.Rows[0]["EmailId"]);
            }
            else
            {
                url = "https://www.formi9.com/formi9verify/formI9loginservice.aspx?" +
                            "AlliancePartnerID=" +AlliancePartnerID +
                            "&AlliancePartnerLogin=" + AlliancePartnerLogin +
                            "&AlliancePartnerPassword=" + AlliancePartnerPassword +
                            "&AlliancePartnerCompanyID=" + dt.Rows[0]["AlliancePartnerCompanyID"] + "&ReturnProcessURL=https://jdpws.schedulingsite.com/api/Everify&B2BUserName=&Target=EVPRequest" +
                            "&RequestID=" + dt.Rows[0]["ApplicantId"] +
                            "&LastName=" + HttpUtility.UrlEncode(dt.Rows[0]["LastName"].ToString()) +
                            "&FirstName=" + HttpUtility.UrlEncode(dt.Rows[0]["FirstName"].ToString()) +
                            "&MiddleInitial=" + HttpUtility.UrlEncode(dt.Rows[0]["MiddleInitial"].ToString()) +
                            "&MaidenName=&SSN=" + Convert.ToString(dt.Rows[0]["SSN"]) +
                            "&DOB=" + (!string.IsNullOrEmpty(Convert.ToString(dt.Rows[0]["DOB"])) ? Convert.ToDateTime(dt.Rows[0]["DOB"]).ToString("MM/dd/yyyy") : "") +
                            "&Address=" + dt.Rows[0]["Address"] +
                            "&HireDate=" + (!string.IsNullOrEmpty(Convert.ToString(dt.Rows[0]["HireDate"])) ? Convert.ToDateTime(dt.Rows[0]["HireDate"]).ToString("MM/dd/yyyy") : Convert.ToDateTime(firstDayEmployment).ToString("MM/dd/yyyy")) +
                            "&CitizenshipStatus=" + Convert.ToString(dt.Rows[0]["CitizenshipStatus"]) +
                            "&DocumentType=" + Convert.ToString(dt.Rows[0]["DocumentType"]) +
                            "&AlienI94Number=" + Convert.ToString(dt.Rows[0]["AlienI94Number"]) +
                            "&AlienDocType=" + Convert.ToString(dt.Rows[0]["AlienOrI94"]) +
                            "&Passport=" + Convert.ToString(dt.Rows[0]["ForeignPassportNumber"]) +
                            "&DocExpDate=" + DocExpDate +
                            "&PassportIssuingAuthority=" + Convert.ToString(dt.Rows[0]["PassportIssuingAuthority"]) +
                            "&ListBDocumentType=" + Convert.ToString(dt.Rows[0]["ListBDocumentType"]) +
                            "&ListBDocumentNumber=" + Convert.ToString(dt.Rows[0]["ListBDocumentNumber"]) +
                            "&ListBIssuingAuthority=" + Convert.ToString(dt.Rows[0]["ListBIssuingAuthority"]) +
                            "&ListBExpirationDate=" + (!string.IsNullOrEmpty(Convert.ToString(dt.Rows[0]["ListBExpirationDate"])) ? Convert.ToDateTime(dt.Rows[0]["ListBExpirationDate"]).ToString("MM/dd/yyyy") : "") +
                            "&ListCDocumentType=" + Convert.ToString(dt.Rows[0]["ListCDocumentType"]) +
                            "&ListCDocumentNumber=" + Convert.ToString(dt.Rows[0]["ListCDocumentNumber"]) +
                            "&ListCIssuingAuthority=" + Convert.ToString(dt.Rows[0]["ListCIssuingAuthority"]) +
                            "&ListCExpirationDate=" + (!string.IsNullOrEmpty(Convert.ToString(dt.Rows[0]["ListCExpirationDate"])) ? Convert.ToDateTime(dt.Rows[0]["ListCExpirationDate"]).ToString("MM/dd/yyyy") : "") +
                            "&EmailAddress=" + Convert.ToString(dt.Rows[0]["EmailId"]);
            }
            return url;
        }
    }
}
