﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using HPortalService.DAL;

namespace HPortalService
{
    public class ApplicantNewYorkW4
    {
        public int ApplicantNewYorkW4Id { get; set; }
        public int ApplicantId { get; set; }
        public string FedTaxStatus { get; set; }
        public bool IsResidentNewYork { get; set; }
        public bool IsResidentYonkers { get; set; }
        public string NumberofAllowances { get; set; }
        public string NumberofAllowancesforNewYork { get; set; }
        public string StateAmount { get; set; }
        public string CityAmount { get; set; }
        public string Numberofboxes { get; set; }
        public string YonkersAmount { get; set; }
        public bool IsClaimedExemption { get; set; }
        public bool IsRehire { get; set; }
        public string FirstDate { get; set; }
        public bool IsDependent { get; set; }
        public string QualifiesDate { get; set; }
        public string SSN { get; set; }
        public string HomeAddress { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string ApartmentNumber { get; set; }
        public string EmployerName { get; set; }
        public string BranchAddress { get; set; }
        public string EIN { get; set; }
        public string NumberofDependents  { get; set; }
		public string CollegeTuitionCr  { get; set; }
		public string StateHouseholdCr  { get; set; }
		public string PropertyTaxCr  { get; set; }
		public string DependentCareCr  { get; set; }
		public string EarnedIncomeCr  { get; set; }
		public string StateChildCr  { get; set; }
		public string SchoolTaxCr  { get; set; }
		public string OtherCr  { get; set; }
		public string HeadofHouseHoldStatus  { get; set; }
		public string FederalAdjustments  { get; set; }
		public string DivideEstimate  { get; set; }
		public string StateTaxReturn  { get; set; }
		public string TaxpayersSpousesWorking  { get; set; }
		public string FederalItemizedDeductions  { get; set; }
		public string EstimatedStateLocalForeignTax  { get; set; }
		public string Subtractline20FromLine19  { get; set; }
		public string EstimatedCollegeTuitionDeduction  { get; set; }
		public string AddLines21and22  { get; set; }
		public string FederalFilingStatus  { get; set; }
		public string SubtractLine24fromLine23  { get; set; }
		public string DivideLine25  { get; set; }
		public string AmountFromLine6Above  { get; set; }
		public string AddLines15through17above  { get; set; }
		public string AddLines27and28  { get; set; }

        public int intIndianaWH4(ApplicantNewYorkW4 obj)
        {
            ApplicantNewYorkW4DAL _ApplicationDAL = new ApplicantNewYorkW4DAL();
            return _ApplicationDAL.IntApplicantNewYorkW4(obj);
        }
        public DataSet GetIndianaWH4ByID(int ID)
        {
            ApplicantNewYorkW4DAL _ApplicationDAL = new ApplicantNewYorkW4DAL();
            return _ApplicationDAL.GetApplicantNewYorkW4ByID(ID);
        }
    }
}
