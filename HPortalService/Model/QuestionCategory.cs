﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

using HPortalService.DAL;

namespace HPortalService
{
   public class QuestionCategory
    {
        public int QuestionCategoryId { get; set; }
        public string Name { get; set; }
        public int DisplayOrder { get; set; }
        public Nullable<int> Parent { get; set; }

        public List<QuestionCategory> GetQuestionCategory()
        {
            List<QuestionCategory> objList = new List<QuestionCategory>();
            DataSet ds;
            QuestionCategoryDAL _AppInfo = new QuestionCategoryDAL();
            ds = _AppInfo.GetQuestionCategories();
            objList = SQLHelper.ContructList<QuestionCategory>(ds);
            return objList;

        }
    }


}
