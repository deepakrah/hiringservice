﻿using HPortalService.DAL;
using System.Collections.Generic;

namespace HPortalService.Model
{
    public class IssuingAuthority
    {

        public int IssuingAuthorityId { get; set; }
        public string Name { get; set; }
        public int EverifyDocumentId { get; set; }

        public List<IssuingAuthority> SelectIssuingAuth(IssuingAuthority obj)
        {
            ApplicantDAL _ApplicationDAL = new ApplicantDAL();
            List<IssuingAuthority> lsteVerifyLookUp = SQLHelper.ContructList<IssuingAuthority>(_ApplicationDAL.SelectIssuingAuth(obj));
            return lsteVerifyLookUp;
        }

    }
}
