using System;
using System.IO;
using System.Text;
/// <summary>
/// Summary description for Appointment
/// </summary>
public class Appointment
{
    private StreamWriter writer = null;
    public Appointment()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public string GetFormatedDate(string date, string time)
    {
        DateTime d = Convert.ToDateTime(date + " " + time).ToUniversalTime();
        string YY = d.Year.ToString();
        string MM = d.Month.ToString("D2");
        string DD = d.Day.ToString("D2");
        string hh = d.Hour.ToString("D2");
        string mm = d.Minute.ToString("D2");
        string ss = d.Second.ToString("D2");

        return String.Format("{0}{1}{2}T{3}{4}{5}Z", YY, MM, DD, hh, mm, ss);

    }
    public string GetFormattedTime(string time)
    {
        string[] times = time.Split(':');
        string HH = string.Empty;
        string MM = string.Empty;
        if (Convert.ToInt32(times[0]) < 10) HH = "0" + times[0];
        else HH = times[0];
        if (Convert.ToInt32(times[1]) < 10) MM = "0" + times[0];
        else MM = times[1];
        return HH + MM + "00Z";

    }
    public string MakeDayEvent(string subject, string location, string startDate, DateTime endDate)
    {
        string filePath = string.Empty;
        string path = "";//HttpContext.Current.Server.MapPath(@"\iCal\");
        filePath = path + subject + ".ics";
        writer = new StreamWriter(filePath);
        writer.WriteLine("BEGIN:VCALENDAR");
        writer.WriteLine("VERSION:2.0");
        writer.WriteLine("PRODID:-//hacksw/handcal//NONSGML v1.0//EN");
        writer.WriteLine("BEGIN:VEVENT");


        // string startDay = "VALUE=DATE:" + GetFormatedDate(startDate);
        // string endDay = "VALUE=DATE:" + GetFormatedDate(endDate);

        //  writer.WriteLine("DTSTART;" + startDay);
        //  writer.WriteLine("DTEND;" + endDay);
        writer.WriteLine("SUMMARY:" + subject);
        writer.WriteLine("LOCATION:" + location);
        writer.WriteLine("END:VEVENT");
        writer.WriteLine("END:VCALENDAR");
        writer.Close();

        return filePath;
    }


    public string MakeHourEvent(string subject, string location, string date, string startTime, string endTime)
    {
        //Create an instance of mail message
     

        StringBuilder stb = new StringBuilder();
        stb.Append("BEGIN:VCALENDAR\r\n");
        stb.Append("VERSION:2.0\r\n");
        stb.Append("PRODID:-//Woodside v1.0//EN\r\n");

        stb.Append("BEGIN:VEVENT\r\n");

        string startDateTime = GetFormatedDate(date, startTime);
        string endDateTime = GetFormatedDate(date, endTime);

        stb.Append("DTSTART:" + startDateTime + "\r\n");
        stb.Append("DTEND:" + endDateTime + "\r\n");
        stb.Append("ORGANIZER;CN=Woodside Properties:mailto:nagrawal@protatechindia.com" + "\r\n");
        stb.Append("DESCRIPTION:" + subject + "\r\n");
        stb.Append("SUMMARY:" + subject + "\r\n");
        stb.Append("LOCATION:" + location + "\r\n");
        stb.Append("SEQUENCE:0" + "\r\n");
        stb.Append("BEGIN:VALARM" + "\r\n");
        stb.Append("TRIGGER:-PT24H" + "\r\n");
        stb.Append("ACTION:DISPLAY" + "\r\n");
        stb.Append("DESCRIPTION:Reminder" + "\r\n");
        stb.Append("END:VALARM" + "\r\n");
        stb.Append("BEGIN:VALARM" + "\r\n");
        stb.Append("TRIGGER:-PT2H" + "\r\n");
        stb.Append("REPEAT:1" + "\r\n");
        stb.Append("DURATION:PT15M" + "\r\n");
        stb.Append("ACTION:DISPLAY" + "\r\n");
        stb.Append("DESCRIPTION:Reminder" + "\r\n");
        stb.Append("END:VALARM" + "\r\n");
        stb.Append("END:VEVENT" + "\r\n");
        stb.Append("END:VCALENDAR" + "\r\n");
      
      

        //Adress the message       
        return stb.ToString();
    }
    public string MakeOnlineInterview(string subject, string location, string date, string startTime, string endTime)
    {
        StringBuilder stb = new StringBuilder();
        stb.Append("BEGIN:VCALENDAR\r\n");
        stb.Append("VERSION:2.0\r\n");
        stb.Append("PRODID:-//Woodside v1.0//EN\r\n");
        stb.Append("BEGIN:VEVENT\r\n");
        stb.Append("ORGANIZER;CN=Woodside Properties:mailto:nagrawal@protatechindia.com" + "\r\n");
        stb.Append("DESCRIPTION:" + subject + "\r\n");
        stb.Append("SUMMARY:" + subject + "\r\n");
        stb.Append("LOCATION:" + location + "\r\n");
        return stb.ToString();
    }
}