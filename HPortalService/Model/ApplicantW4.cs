﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using HPortalService.DAL;
using HPortalService;
using HPortalService.Model;

namespace HPortalService.Model
{
    public class ApplicantW4
    {

        public int ApplicantId { get; set; }
        public int JobApplicationId { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string EmailID { get; set; }
        public string PicturePath { get; set; }
        public string ZipCode { get; set; }
        public string AppliedOn { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public int StateId { get; set; }
        public int CountryId { get; set; }
        public string SSN { get; set; }
        public String FedTaxStatus { get; set; }
        public string SignatureImage { get; set; }
        public int NoOfAllowances { get; set; }
        public Boolean NameDiffer { get; set; }
        public int W4FormId { get; set; }
        public string WFirstName { get; set; }
        public string WMiddleName { get; set; }
        public string WLastName { get; set; }
        public String WDOB { get; set; }
        public string WSSN { get; set; }
        public string WCity { get; set; }
        public int WStateId { get; set; }
        public int WCountryId { get; set; }
        public string WZipCode { get; set; }
        public decimal WAdditionalAmount { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Exempt { get; set; }
        public string EIN { get; set; }
        public string BranchAddress { get; set; }
        public int AppTaskId { get; set; }
        public int UserId { get; set; }

        public bool IsMultipleJob { get; set; }
        public int ClaimDepUnderAge { get; set; }
        public int ClaimDepOther { get; set; }
        public int TotalClaim { get; set; }
        public int OtherIncome { get; set; }
        public int Deductions { get; set; }
        public int ExtraWithholding { get; set; }
        public int TwoJobsAmount { get; set; }
        public int ThreeJobsAmountA { get; set; }
        public int ThreeJobsAmountB { get; set; }
        public int ThreeJobsAmountC { get; set; }
        public int PayPeriod { get; set; }
        public int DivideAnnualAmount { get; set; }
        public int DeductionsWorksheet1 { get; set; }
        public int DeductionsWorksheet2 { get; set; }
        public int DeductionsWorksheet3 { get; set; }
        public int DeductionsWorksheet4 { get; set; }
        public int DeductionsWorksheet5 { get; set; }
        public string IsGenerate { get; set; }

        public int UpdateFormW4()
        {
            ApplicantW4DAL _ApplicantW4DAL = new ApplicantW4DAL();
            return _ApplicantW4DAL.UpdateFormW4(this);
        }


        public DataSet GetW4Form(ApplicantW4 obj)
        {
            ApplicantW4DAL _ApplicantW4DAL = new ApplicantW4DAL();
            return _ApplicantW4DAL.GetW4Form(obj);
        }
    }

}
