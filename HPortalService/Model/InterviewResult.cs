﻿using HPortalService.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HPortalService
{
    public class InterviewResult
    {
        public int TakenBy { get; set; }
        public int ApplicantId { get; set; }
        public int JobApplicationId { get; set; }
        public string Result { get; set; }
        public string Remarks { get; set; }
        public int StatusID { get; set; }
        public string Reason { get; set; }

        public int SaveResult()
        {
            InterviewResultDAL objDal = new InterviewResultDAL();
            return objDal.SaveResult(ApplicantId, JobApplicationId, TakenBy, Result, Remarks);
        }
        public bool GetResult()
        {
            InterviewResultDAL objDal = new InterviewResultDAL();
            return objDal.GetResult(JobApplicationId);
        }
    }
}
