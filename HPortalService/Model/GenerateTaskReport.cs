﻿
using HPortalService.DAL;
using HPortalService.Model;
using Microsoft.Extensions.Configuration;
//using Microsoft.Reporting.WebForms;
using System.Data;

namespace HPortalService
{
    public class GenerateTaskReport
    {
           
        //string ServerPath = Startup.ServerPath;
        public string GenerateReport(ApplicantTask objTask)
        {
            //ApplicantTask objTask = new ApplicantTask();           
            string RName = "";
            switch (objTask.TaskId.ToString())
            {
                case "2":
                    {
                        RName = "EmployeeW4";//Form W-4
                        break;
                    }
                case "4":
                    {
                        RName = "HRPMRForm";//Paycheck Mailing Release Form
                        break;
                    }
                case "5":
                    {
                        RName = "HRMAACForm";//Mutual Agreement to Arbitrate Claims
                        break;
                    }
                case "6":
                    {
                        RName = "HRREHEAWSForm";//Receipt of Employee Handbook and Employment-At-Will Statement
                        break;
                    }
                case "7":
                    {
                        RName = "HRSTForm";
                        break;
                    }
                case "8":
                    {
                        RName = "HRPYFEHCForm";
                        break;
                    }
                case "9":
                    {
                        RName = "HRUEPForm";
                        break;
                    }
                case "11":
                    {
                        RName = "FormI9";
                        break;
                    }
                case "14":
                    {
                        RName = "BGCHECKFORM";
                        break;
                    }
                case "17":
                    {
                        RName = "OfferLetterFORM";
                        break;
                    }
                case "18":
                    {
                        RName = "FormI9Admin1";
                        break;
                    }

                case "19":
                    {
                        RName = "FormI9Admin";
                        break;
                    }
                case "20":
                    {
                        RName = "PoliciesAckForm";
                        break;
                    }
                case "21":
                    {
                        RName = "Zurich";
                        break;
                    }
                case "22":
                    {
                        RName = "HREPUALAForm";
                        break;
                    }
                case "23":
                    {
                        RName = "PrivacyActForm";
                        break;
                    }
                case "24":
                    {
                        RName = "EmpHandBook";
                        break;
                    }
                case "25":
                    {
                        RName = "AckJobResponsibility";
                        break;
                    }
                case "26":
                    {
                        RName = "MealRestPolicyAck";
                        break;
                    }
                case "27":
                    {
                        RName = "MealRestPolicyWaiver";
                        break;
                    }
                case "28":
                    {
                        RName = "WageTheftSignoff";
                        break;
                    }
                case "29":
                    {
                        RName = "WorkerCompDesign";
                        break;
                    }
                case "30":
                    {
                        RName = "ApplicantWithholdingsCertificate";
                        break;
                    }
                case "31":
                    {
                        RName = "ApplicantExempCertificate";
                        break;
                    }
                case "32":
                    {
                        RName = "EmpRegRenewal";
                        break;
                    }
                case "33":
                    {
                        RName = "AtlantaSGEWithholdingAllownaceCertificate";
                        break;
                    }
                case "34":
                    {
                        RName = "FormCTW4EmpWithholdingCertificate";
                        break;
                    }
                case "35":
                    {
                        RName = "IndianaFormWH4";
                        break;
                    }
                case "36":
                    {
                        RName = "NewYorkWithholdingAllowanceCertificate";
                        break;
                    }
                case "37":
                    {
                        RName = "NewYorkCorrectionLawArticle23A";
                        break;
                    }
                case "38":
                    {
                        RName = "NewYorkCorrectionLawArticle23AAckForm";
                        break;
                    }
                case "39":
                    {
                        RName = "EmpArizonaWithholdingElection";
                        break;
                    }
                case "40":
                    {
                        RName = "ApplicantResidencyForm";
                        break;
                    }
            }

            if (RName != "")
            {
               // Setup the report viewer object and get the array of bytes
               // ReportViewer viewer = new ReportViewer();
                string imagePath;
                //byte[] bytes;
                //string fileName = "";

                //viewer.ProcessingMode = ProcessingMode.Local;
                //viewer.LocalReport.ReportPath = ServerPath + @"\Report\" + RName + ".rdlc";

                //viewer.LocalReport.EnableExternalImages = true;

                //imagePath = ServerPath + @"\ApplicantForm\" + objTask.ApplicantId + "/" + ((objTask.ApplicantId > 0) ? objTask.SignatureImage : "").ToString();
                //ReportParameter parameter = new ReportParameter("Path", imagePath);
                //viewer.LocalReport.SetParameters(parameter);
                //viewer.LocalReport.Refresh();
            }
                //Applicant objApp = new Applicant();
                //DataSet dsApplicant = new DataSet();

                //if (RName == "HRUEPForm")
                //{
                //    ApplicantUniform obj = new ApplicantUniform();
                //    obj.ApplicantId = ApplicantId;
                //    dsApplicant = obj.GetAppUniform(obj);
                //}
                //else if (RName == "FormI9" || RName == "FormI9Admin1" || RName == "FormI9Admin")
                //{
                //    ApplicantFormI9 obj = new ApplicantFormI9();
                //    obj.ApplicantId = ApplicantId;
                //    dsApplicant = obj.GetApplicantFormI9Details(obj);
                //}
                //else if (RName == "BGCHECKFORM")
                //{
                //    //BGCheck obj = new BGCheck();

                //    //webOnlineTestService.DAL.BGCheckDAL obj1 = new webOnlineTestService.DAL.BGCheckDAL();
                //    // return obj.GetApplicantBGCheckById(ApplicantId);

                //    SQL objSql = new SQL();
                //    objSql.AddParameter("@ApplicantId", DbType.Int32, ParameterDirection.Input, 0, ApplicantId);
                //    dsApplicant = objSql.ExecuteDataSet("p_GetApplicantBGCheckById1");
                //}
                //else if (RName == "Zurich")
                //{
                //    //if (context.Request.QueryString["generate"] == null)
                //    //{
                //    //    webOnlineTestService.DAL.BGCheckDAL obj1 = new webOnlineTestService.DAL.BGCheckDAL();
                //    //    SQL objSql = new SQL();
                //    //    objSql.AddParameter("@ApplicantId", DbType.Int32, ParameterDirection.Input, 0, ApplicantId);
                //    //    objSql.AddParameter("@Date", DbType.DateTime, ParameterDirection.Input, 0, DateTime.Now);
                //    //    objSql.AddParameter("@PrintedName", DbType.String, ParameterDirection.Input, 0, ZurichPrintedName);
                //    //    objSql.AddParameter("@HomeAddress", DbType.String, ParameterDirection.Input, 0, ZurichAddress);
                //    //    objSql.AddParameter("@City", DbType.String, ParameterDirection.Input, 0, ZurichCity);
                //    //    objSql.AddParameter("@STATE", DbType.String, ParameterDirection.Input, 0, ZurichStateName);
                //    //    objSql.AddParameter("@Zip", DbType.String, ParameterDirection.Input, 0, ZurichZipCode);
                //    //    objSql.AddParameter("@AppTaskId", DbType.Int32, ParameterDirection.Input, 0, AppTaskId);
                //    //    objSql.ExecuteDataSet("p_ApplicantZurich_Ins");
                //    //}

                //    ////dsApplicant = objApp.GetApplicantByID(ApplicantId);

                //    //SQL objSql1 = new SQL();
                //    //objSql1.AddParameter("@ApplicantId", DbType.Int32, ParameterDirection.Input, 0, ApplicantId);
                //    //dsApplicant = objSql1.ExecuteDataSet("p_GetApplicantZurichDetails");

                //    //if (dsApplicant.Tables[0].Rows.Count > 0)
                //    //{
                //    //    ZurichPrintedName = dsApplicant.Tables[0].Rows[0]["ZurichPrintedName"].ToString();
                //    //    PrintDate = dsApplicant.Tables[0].Rows[0]["ZurichDate"].ToString();
                //    //    ZurichAddress = dsApplicant.Tables[0].Rows[0]["ZurichAddress"].ToString();
                //    //    ZurichCity = dsApplicant.Tables[0].Rows[0]["ZurichCity"].ToString();
                //    //    ZurichStateName = dsApplicant.Tables[0].Rows[0]["ZurichStateName"].ToString();
                //    //    ZurichZipCode = dsApplicant.Tables[0].Rows[0]["ZurichZipCode"].ToString();
                //    //    ZurichEmployerName = "Woodside Properties";
                //    //}

                //}
                //else if (RName == "PrivacyActForm")
                //{

                //    //PrivacyAct objPrivet = new PrivacyAct();
                //    //objPrivet.ApplicantId = ApplicantId;
                //    //objPrivet.DateOfBirth = ApplicantPrivacyActId;
                //    //objPrivet.Name = Name;
                //    //objPrivet.ApplicantInfo = ApplicantInfo;
                //    //objPrivet.SSN = SSN;
                //    //objPrivet.DateOfBirth = DateOfBirth;
                //    //objPrivet.Gender = Gender;
                //    //objPrivet.HomeAddress = HomeAddress;
                //    //objPrivet.City = City;
                //    //objPrivet.State = State;
                //    //objPrivet.PostalCode = PostalCode;
                //    //objPrivet.PrimaryTelephone = PrimaryTelephone;
                //    //objPrivet.PrimaryEmail = PrimaryEmail;
                //    //objPrivet.AlternateTelephone = AlternateTelephone;
                //    //objPrivet.AlternateEMail = AlternateEMail;
                //    //objPrivet.Allergies = Allergies;
                //    //objPrivet.Medications = Medications;
                //    //objPrivet.PrimaryContactName = PrimaryContactName;
                //    //objPrivet.Relationship = Relationship;
                //    //objPrivet.PrimaryContactTelephone = PrimaryContactTelephone;
                //    //objPrivet.PrimaryContactEmail = PrimaryContactEmail;
                //    //objPrivet.ContactAlternateTelephone = ContactAlternateTelephone;
                //    //objPrivet.ContactAlternateEMail = ContactAlternateEMail;
                //    //objPrivet.AlternateContactName1 = AlternateContactName1;
                //    //objPrivet.AlternateRelationship1 = AlternateRelationship1;
                //    //objPrivet.AlternatePTelephone1 = AlternatePTelephone1;
                //    //objPrivet.AlternatePEmailAddress1 = AlternatePEmailAddress1;
                //    //objPrivet.AlternateATelephone1 = AlternateATelephone1;
                //    //objPrivet.AlternateAEmailAddress1 = AlternateAEmailAddress1;
                //    //objPrivet.AlternateContactName2 = AlternateContactName2;
                //    //objPrivet.AlternateRelationship2 = AlternateRelationship2;
                //    //objPrivet.AlternatePTelephone2 = AlternatePTelephone2;
                //    //objPrivet.AlternatePEmailAddress2 = AlternatePEmailAddress2;
                //    //objPrivet.AlternateATelephone2 = AlternateATelephone2;
                //    //objPrivet.AlternateAEmailAddress2 = AlternateAEmailAddress2;
                //    //int temp = obj1.fnInsApplicantValidate(objPrivet);

                //    //SQL objSql = new SQL();
                //    //objSql.AddParameter("@ApplicantId", DbType.Int32, ParameterDirection.Input, 0, ApplicantId);
                //    //dsApplicant = objSql.ExecuteDataSet("p_GetApplicantPrivacyActReport");
                //}
                //else if (RName == "WageTheftSignoff")
                //{
                //    ApplicantWage AppWage = new ApplicantWage();
                //    dsApplicant = AppWage.GetApplicantWageByID(ApplicantId);
                //}
                //else if (RName == "WorkerCompDesign")
                //{
                //    ApplicantCompDesign AppWage = new ApplicantCompDesign();
                //    dsApplicant = AppWage.GetApplicantCompDesignByID(ApplicantId);
                //}
                //else if (RName == "ApplicantWithholdingsCertificate")
                //{
                //    ApplicantExempCertificate AppWage = new ApplicantExempCertificate();
                //    dsApplicant = AppWage.GetApplicantWithholdingCertificateByID(ApplicantId);
                //}
                //else if (RName == "ApplicantExempCertificate")
                //{
                //    ApplicantExempCertificate AppWage = new ApplicantExempCertificate();
                //    dsApplicant = AppWage.GetApplicantExempCertificateByID(ApplicantId);
                //}
                //else if (RName == "EmpRegRenewal")
                //{
                //    ApplicantRenewal obj = new ApplicantRenewal();
                //    dsApplicant = obj.GetApplicantRenewalByID(ApplicantId);
                //}
                //else if (RName == "AtlantaSGEWithholdingAllownaceCertificate")
                //{
                //    ApplicantAtlantaAllownaceCertificateG4 obj = new ApplicantAtlantaAllownaceCertificateG4();
                //    dsApplicant = obj.GetApplicantAtlantaAllownaceCertificateG4ByID(ApplicantId);
                //}
                //else if (RName == "FormCTW4EmpWithholdingCertificate")
                //{
                //    ApplicantCTW4EmpWithholdingCertificate obj = new ApplicantCTW4EmpWithholdingCertificate();
                //    dsApplicant = obj.GetApplicantCTW4EmpWithholdingCertificateByID(ApplicantId);
                //}
                //else if (RName == "IndianaFormWH4")
                //{
                //    IndianaWH4 obj = new IndianaWH4();
                //    dsApplicant = obj.GetIndianaWH4ByID(ApplicantId);
                //}
                //else if (RName == "NewYorkWithholdingAllowanceCertificate")
                //{
                //    ApplicantNewYorkW4 obj = new ApplicantNewYorkW4();
                //    dsApplicant = obj.GetIndianaWH4ByID(ApplicantId);
                //}
                //else if (RName == "EmpArizonaWithholdingElection")
                //{
                //    AppArizonaWithholdingElection obj = new AppArizonaWithholdingElection();
                //    dsApplicant = obj.GetAppArizonaWithholdingElectionByID(ApplicantId);
                //}
                //else if (RName == "ApplicantResidencyForm")
                //{
                //    ApplicantResidencyForm obj = new ApplicantResidencyForm();
                //    dsApplicant = obj.GetApplicantResidencyForm(ApplicantId);
                //}
                //else
                //{
                //    dsApplicant = objApp.GetApplicantByID(ApplicantId);
                //}
                //// Create Report DataSource
                ////ReportDataSource rds = new ReportDataSource();           
                ////rds.Name = "DataSet1";
                ////rds.Value = dsApplicant.Tables[0];

                ////dataSource.Add(rds);
                //// Variables
                ////Warning[] warnings;
                ////string[] streamIds;
                ////string mimeType = string.Empty;
                ////string encoding = string.Empty;
                ////string extension = string.Empty;

                //if (RName != "")
                //{
                //    // Setup the report viewer object and get the array of bytes
                //    ReportViewer viewer = new ReportViewer();
                //    //string imagePath;
                //    //byte[] bytes;
                //    //string fileName = "";

                //    //viewer.ProcessingMode = ProcessingMode.Local;
                //    //viewer.LocalReport.ReportPath = context.Server.MapPath("") + @"\Report\" + RName + ".rdlc";

                //    //viewer.LocalReport.EnableExternalImages = true;

                //    //imagePath = new Uri(context.Server.MapPath("") + @"\ApplicantForm\" + ApplicantId + "/" + ((dsApplicant.Tables[0].Rows.Count > 0) ? dsApplicant.Tables[0].Rows[0]["SignatureImage"].ToString() : "")).AbsoluteUri;
                //    //ReportParameter parameter = new ReportParameter("Path", imagePath);
                //    //viewer.LocalReport.SetParameters(parameter);
                //    //viewer.LocalReport.Refresh();
                //    //if (RName == "WageTheftSignoff")
                //    //{
                //    //    //imagePath = new Uri(context.Server.MapPath("") + @"\EmployerSignature\" + UserId + "/" + UserId.ToString() + ".png").AbsoluteUri;
                //    //    imagePath = new Uri(context.Server.MapPath("") + @"\EmployerSignature\JimSignature.jpg").AbsoluteUri;
                //    //    ReportParameter parameterAdminUser = new ReportParameter("EmployerSignature", imagePath);
                //    //    viewer.LocalReport.SetParameters(parameterAdminUser);
                //    //    viewer.LocalReport.Refresh();
                //    //    ReportParameter p_EmployerName = new ReportParameter("EmployerName", (UserName == "undefined" || UserName == "null" || UserName == null) ? " " : UserName);
                //    //    viewer.LocalReport.SetParameters(p_EmployerName);
                //    //    viewer.LocalReport.Refresh();
                //    //    ReportParameter p_EmployerTitle = new ReportParameter("EmployerTitle", (EmployerTitle == "undefined" || EmployerTitle == "null" || EmployerTitle == null) ? " " : EmployerTitle);
                //    //    viewer.LocalReport.SetParameters(p_EmployerTitle);
                //    //    viewer.LocalReport.Refresh();
                //    //}

                //    //if (RName == "FormI9Admin1" || RName == "FormI9Admin")
                //    //{
                //    //    imagePath = new Uri(context.Server.MapPath("") + @"\UserSignature\" + UserId + "/" + UserId.ToString() + ".png").AbsoluteUri;
                //    //    ReportParameter parameterAdmin = new ReportParameter("AdminPath", imagePath);
                //    //    viewer.LocalReport.SetParameters(parameterAdmin);
                //    //    viewer.LocalReport.Refresh();
                //    //}

                //    //ReportParameter parameter1 = new ReportParameter("TaskStatus", string.IsNullOrEmpty(Status) ? "2" : Status);
                //    //viewer.LocalReport.SetParameters(parameter1);
                //    //viewer.LocalReport.Refresh();
                //    //if (RName == "HRMAACForm")
                //    //{
                //    //    //imagePath = new Uri(context.Server.MapPath("") + @"\EmployerSignature\" + UserId + "/" + UserId.ToString() + ".png").AbsoluteUri;
                //    //    imagePath = new Uri(context.Server.MapPath("") + @"\EmployerSignature\JimSignature.jpg").AbsoluteUri;
                //    //    ReportParameter parameterAdminUser = new ReportParameter("EmployerSignature", imagePath);
                //    //    viewer.LocalReport.SetParameters(parameterAdminUser);
                //    //    viewer.LocalReport.Refresh();
                //    //    ReportParameter p_EmployerName = new ReportParameter("EmployerName", (UserName == "undefined" || UserName == "null" || UserName == null) ? " " : UserName);
                //    //    viewer.LocalReport.SetParameters(p_EmployerName);
                //    //    viewer.LocalReport.Refresh();
                //    //    ReportParameter p_EmployerTitle = new ReportParameter("EmployerTitle", (EmployerTitle == "undefined" || EmployerTitle == "null" || EmployerTitle == null) ? " " : EmployerTitle);
                //    //    viewer.LocalReport.SetParameters(p_EmployerTitle);
                //    //    viewer.LocalReport.Refresh();
                //    //}

                //    //if (RName == "OfferLetterFORM")
                //    //{
                //    //    ReportParameter p_OrientationDate = new ReportParameter("OrientationDate", (objTask.OrientationDate == "undefined" || objTask.OrientationDate == "null" || objTask.OrientationDate == null) ? " " : objTask.OrientationDate);
                //    //    viewer.LocalReport.SetParameters(p_OrientationDate);
                //    //    viewer.LocalReport.Refresh();

                //    //    ReportParameter p_OrientationTime = new ReportParameter("OrientationTime", (objTask.OrientationTime == "undefined" || objTask.OrientationTime == "null" || objTask.OrientationTime == null) ? " " : objTask.OrientationTime);
                //    //    viewer.LocalReport.SetParameters(p_OrientationTime);
                //    //    viewer.LocalReport.Refresh();

                //    //    ReportParameter p_VenueName = new ReportParameter("Location", (objTask.VenueName == "undefined" || objTask.VenueName == "null" || objTask.VenueName == null) ? " " : objTask.VenueName);
                //    //    viewer.LocalReport.SetParameters(p_VenueName);
                //    //    viewer.LocalReport.Refresh();

                //    //    ReportParameter p_Parking = new ReportParameter("Parking", (objTask.Parking == "undefined" || objTask.Parking == "null" || objTask.Parking == null) ? " " : objTask.Parking);
                //    //    viewer.LocalReport.SetParameters(p_Parking);
                //    //    viewer.LocalReport.Refresh();
                //    //}
                //    //if (RName == "Zurich")
                //    //{
                //    //    ReportParameter p_ZurichPrintedName = new ReportParameter("PrintedName", (ZurichPrintedName == "undefined" || ZurichPrintedName == "null" || ZurichPrintedName == null) ? " " : ZurichPrintedName);
                //    //    viewer.LocalReport.SetParameters(p_ZurichPrintedName);
                //    //    viewer.LocalReport.Refresh();
                //    //    ReportParameter p_PrintDate = new ReportParameter("PrintDate", (PrintDate == "undefined" || PrintDate == "null" || PrintDate == null) ? " " : PrintDate);
                //    //    viewer.LocalReport.SetParameters(p_PrintDate);
                //    //    viewer.LocalReport.Refresh();
                //    //    ReportParameter p_ZurichAddress = new ReportParameter("ZurichAddress", (ZurichAddress == "undefined" || ZurichAddress == "null" || ZurichAddress == null) ? " " : ZurichAddress);
                //    //    viewer.LocalReport.SetParameters(p_ZurichAddress);
                //    //    viewer.LocalReport.Refresh();
                //    //    ReportParameter p_ZurichCity = new ReportParameter("ZurichCity", (ZurichCity == "undefined" || ZurichCity == "null" || ZurichCity == null) ? " " : ZurichCity);
                //    //    viewer.LocalReport.SetParameters(p_ZurichCity);
                //    //    viewer.LocalReport.Refresh();
                //    //    ReportParameter p_ZurichStateName = new ReportParameter("ZurichStateName", (ZurichStateName == "undefined" || ZurichStateName == "null" || ZurichStateName == null) ? " " : ZurichStateName);
                //    //    viewer.LocalReport.SetParameters(p_ZurichStateName);
                //    //    viewer.LocalReport.Refresh();
                //    //    ReportParameter p_ZurichZipCode = new ReportParameter("ZurichZipCode", (ZurichZipCode == "undefined" || ZurichZipCode == "null" || ZurichZipCode == null) ? " " : ZurichZipCode);
                //    //    viewer.LocalReport.SetParameters(p_ZurichZipCode);
                //    //    viewer.LocalReport.Refresh();
                //    //    ReportParameter p_ZurichEmployerName = new ReportParameter("ZurichEmployerName", (ZurichEmployerName == "undefined" || ZurichEmployerName == "null" || ZurichEmployerName == null) ? " " : ZurichEmployerName);
                //    //    viewer.LocalReport.SetParameters(p_ZurichEmployerName);
                //    //    viewer.LocalReport.Refresh();
                //    //}
                //    ////if (RName == "EmpRegRenewal")
                //    ////{
                //    ////    //imagePath = new Uri(context.Server.MapPath("") + @"\EmployerSignature\" + UserId + "/" + UserId.ToString() + ".png").AbsoluteUri;
                //    ////    imagePath = new Uri(context.Server.MapPath("") + @"\EmployerSignature\JimSignature.jpg").AbsoluteUri;
                //    ////    ReportParameter parameterAdminUser = new ReportParameter("EmployerSignature", imagePath);
                //    ////    viewer.LocalReport.SetParameters(parameterAdminUser);
                //    ////    viewer.LocalReport.Refresh();
                //    ////    ReportParameter p_EmployerName = new ReportParameter("EmployerName", (UserName == "undefined" || UserName == "null" || UserName == null) ? " " : UserName);
                //    ////    viewer.LocalReport.SetParameters(p_EmployerName);
                //    ////    viewer.LocalReport.Refresh();
                //    ////}
                //    //viewer.LocalReport.SubreportProcessing += new SubreportProcessingEventHandler(SetSubDataSource);
                //    //viewer.LocalReport.DataSources.Add(rds); // Add datasource here




                //    //bytes = viewer.LocalReport.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);

                //    ////Byte[] bytes = viewer.LocalReport.Render("PDF");

                //    //if (dsApplicant.Tables[0].Rows.Count > 0)
                //    //    fileName = RName + "_" + dsApplicant.Tables[0].Rows[0]["FirstName"].ToString().Replace("'", "") + "_" + ApplicantId + "_" + DateTime.Now.ToString("MM-dd-yyyy-hh-mm-ss");


                //    //if (!string.IsNullOrEmpty(AppTaskId) && string.IsNullOrEmpty(print))
                //    //{

                //    //    //Now that you have all the bytes representing the PDF report, buffer it and send it to the client.

                //    //    string path = context.Server.MapPath("") + (@"\ApplicantForm\" + ApplicantId);
                //    //    if (!Directory.Exists(path))
                //    //    {
                //    //        DirectoryInfo di = Directory.CreateDirectory(path);

                //    //    }
                //    //    using (FileStream stream = new FileStream((path + "/" + fileName + ".pdf"), FileMode.Create))
                //    //    {
                //    //        stream.Write(bytes, 0, bytes.Length);
                //    //    }

                //    //    if (context.Request.QueryString["generate"] != null)
                //    //    {
                //    //        SQL objSql = new SQL();
                //    //        objSql.AddParameter("@ApplicantId", DbType.Int32, ParameterDirection.Input, 0, ApplicantId);
                //    //        objSql.AddParameter("@DocumentPath", DbType.String, ParameterDirection.Input, 0, (fileName.Trim() + "." + extension));
                //    //        objSql.AddParameter("@ApplicantTaskId ", DbType.Int32, ParameterDirection.Input, 0, Convert.ToInt32(AppTaskId));
                //    //        objSql.AddParameter("@CreatedBy", DbType.Int32, ParameterDirection.Input, 0, Convert.ToInt32(UserId));
                //    //        objSql.ExecuteNonQuery("p_ApplicantDocumentGenerate_Ins");
                //    //    }
                //    //    else
                //    //    {
                //    //        Applicant obj = new Applicant();
                //    //        obj.SaveApplicantDocument(ApplicantId, (fileName.Trim() + "." + extension), Convert.ToInt32(AppTaskId), Convert.ToInt32(UserId));
                //    //    }
                //    //    context.Response.Buffer = false;
                //    //    //context.Response.Clear();
                //    //    //context.Response.ContentType = mimeType;
                //    //    //context.Response.AddHeader("content-disposition", "attachment; filename=" + RName + "." + extension);
                //    //    //context.Response.BinaryWrite(bytes); // create the file
                //    //    //context.Response.Flush(); // sen
                //    //    context.Response.End();
                //    //}
                //    //else
                //    //{

                //    //    if (!string.IsNullOrEmpty(print))
                //    //    {

                //    //        ApplicantFormI9 obj = new ApplicantFormI9();
                //    //        obj.ApplicantId = ApplicantId;
                //    //        int temp = obj.fnUpdateApplicantFormI9(ApplicantId, Convert.ToInt32(UserId), DateTime.Now, "", 2);
                //    //    }
                //    //    context.Response.Buffer = true;
                //    //    context.Response.Clear();
                //    //    context.Response.ContentType = mimeType;
                //    //    if (ReportName != "11" && ReportName != "18" && ReportName != "19")
                //    //        context.Response.AddHeader("content-disposition", "attachment; filename=" + RName + "." + extension);
                //    //    context.Response.BinaryWrite(bytes); // create the file
                //    //    context.Response.Flush(); // sen
                //    //}
                //}

                ////if (ReportName == "3")
                ////{
                ////    string path = context.Server.MapPath("") + @"\eVerifyDoc\" + BranchId + @"\" + ApplicantId + @"\";//Location for inside Test Folder  
                ////    string[] Filenames = Directory.GetFiles(path);

                ////    using (ZipFile zip = new ZipFile())
                ////    {
                ////        zip.AddFiles(Filenames, "eVerifyDoc");//Zip file inside filename  
                ////        zip.Save(context.Server.MapPath("") + (@"\ApplicantForm\" + ApplicantId) + @"\E-Verify.zip");//location and name for creating zip file  
                ////    }

                ////    using (ZipFile zip = new ZipFile())
                ////    {
                ////        zip.AddFiles(Filenames, "eVerifyDoc");//Zip file inside filename  
                ////                                              //zip.Save(@"D:\Projectzip.zip");//location and name for creating zip file  
                ////        context.Response.Clear();
                ////        context.Response.BufferOutput = false;
                ////        string zipName = String.Format("{0}.zip", "E-Verify");
                ////        context.Response.ContentType = "application/zip";
                ////        context.Response.AddHeader("content-disposition", "attachment; filename=" + zipName);
                ////        zip.Save(context.Response.OutputStream);
                ////        context.Response.End();
                ////    }
                ////}
                return "test";
        }
        //public string SetSubDataSource()
        //{
        //    DataTable dtbl = new DataTable("DataTable1");
        //    BGCheck obj = new BGCheck();
        //    dtbl = obj.GetApplicantBGCheckAddressById(ApplicantId).Tables[0];
        //    ReportDataSource rds = new ReportDataSource();
        //    rds.Name = "DataSet1";
        //    rds.Value = dtbl;
        //    //e.DataSources.Add(rds);
        //    return "test";
        //}       
    }
}
