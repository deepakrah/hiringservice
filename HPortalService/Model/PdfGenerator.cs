﻿using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using HPortalService.DAL;
using System.Data;
using System.Text;
using iTextSharp.text;

namespace HPortalService.Model
{
    public class PdfGenerator
    {

        private void ListFieldNames(string ReportTemplatePath)
        {
            string pdfTemplate = ReportTemplatePath;

            // create a new PDF reader based on the PDF template document
            PdfReader pdfReader = new PdfReader(pdfTemplate);

            StringBuilder sb = new StringBuilder();
            foreach (var item in pdfReader.AcroFields.Fields)
            {
                sb.Append(item.Key.ToString() + "\n");
            }
            Console.WriteLine(sb.ToString());
        }

        public void FillW4Form(int ApplicantId, int AppTaskId, int UserId, string IsGenerate)
        {
            string path = @"P:\\WSRApplicantForm\\" + ApplicantId;

            string pdfTemplate = Startup.ReportTemplate + "W42020.pdf";
            //ListFieldNames(pdfTemplate);
            DataTable dt = new DataTable();
            ReportDAL obj = new ReportDAL();
            dt = obj.GetApplicantByID_rpt(ApplicantId, AppTaskId).Tables[0];
            string NewFileName = "";
            string newFile = "";
            if (dt.Rows.Count > 0)
            {

                NewFileName = "W42020_" + dt.Rows[0]["FirstName"].ToString().Replace("'", "") + "_" + ApplicantId + "_" + DateTime.Now.ToString("MM-dd-yyyy-hh-mm-ss") + ".pdf";
                newFile = path + "\\" + NewFileName;
            }


            if (!Directory.Exists(path))
            {
                DirectoryInfo di = Directory.CreateDirectory(path);

            }
            PdfReader pdfReader = new PdfReader(pdfTemplate);
            PdfStamper pdfStamper = new PdfStamper(pdfReader, new FileStream(
                        newFile, FileMode.Create));

            AcroFields pdfFormFields = pdfStamper.AcroFields;

            // set form pdfFormFields

            //Step 1
            pdfFormFields.SetField("topmostSubform[0].Page1[0].Step1a[0].f1_01[0]", dt.Rows[0]["WFirstName"].ToString() + " " + dt.Rows[0]["WMiddleName"].ToString()); // First Name
            pdfFormFields.SetField("topmostSubform[0].Page1[0].Step1a[0].f1_02[0]", dt.Rows[0]["WLastName"].ToString()); // Last Name
            pdfFormFields.SetField("topmostSubform[0].Page1[0].Step1a[0].f1_03[0]", dt.Rows[0]["Address1"].ToString()); // Address
            pdfFormFields.SetField("topmostSubform[0].Page1[0].Step1a[0].f1_04[0]", dt.Rows[0]["Address2"].ToString()); // City, State and Zip
            pdfFormFields.SetField("topmostSubform[0].Page1[0].f1_05[0]", dt.Rows[0]["WSSN"].ToString()); // SSN

            if (dt.Rows[0]["FedTaxStatuses"].ToString() == "1")
            {
                pdfFormFields.SetField("topmostSubform[0].Page1[0].c1_1[0]", "1", true); // Single
            }
            else if (dt.Rows[0]["FedTaxStatuses"].ToString() == "2")
            {
                pdfFormFields.SetField("topmostSubform[0].Page1[0].c1_1[1]", "2", true); // Married
            }
            else if (dt.Rows[0]["FedTaxStatuses"].ToString() == "3")
            {
                pdfFormFields.SetField("topmostSubform[0].Page1[0].c1_1[2]", "3", true); // Head of household
            }

            //Step 2
            pdfFormFields.SetField("topmostSubform[0].Page1[0].Step2c[0].c1_2[0]", dt.Rows[0]["IsMultipleJob"].ToString() == "True" ? "1" : "", true); // withheld

            //Step 3
            pdfFormFields.SetField("topmostSubform[0].Page1[0].Step3_ReadOrder[0].f1_06[0]", dt.Rows[0]["ClaimDepUnderAge"].ToString()); // 
            pdfFormFields.SetField("topmostSubform[0].Page1[0].Step3_ReadOrder[0].f1_07[0]", dt.Rows[0]["ClaimDepOther"].ToString());
            pdfFormFields.SetField("topmostSubform[0].Page1[0].f1_08[0]", dt.Rows[0]["TotalClaim"].ToString());

            //Step 4
            pdfFormFields.SetField("topmostSubform[0].Page1[0].f1_09[0]", dt.Rows[0]["OtherIncome"].ToString());
            pdfFormFields.SetField("topmostSubform[0].Page1[0].f1_10[0]", dt.Rows[0]["Deductions"].ToString());
            pdfFormFields.SetField("topmostSubform[0].Page1[0].f1_11[0]", dt.Rows[0]["ExtraWithholding"].ToString());

            //Step 5
            pdfFormFields.SetField("topmostSubform[0].Page1[0].f1_13[0]", dt.Rows[0]["BranchAddress"].ToString());
            pdfFormFields.SetField("topmostSubform[0].Page1[0].f1_14[0]", "");
            pdfFormFields.SetField("topmostSubform[0].Page1[0].f1_15[0]", dt.Rows[0]["EIN"].ToString());

            //Page 3
            pdfFormFields.SetField("topmostSubform[0].Page3[0].f3_01[0]", dt.Rows[0]["TwoJobsAmount"].ToString());
            pdfFormFields.SetField("topmostSubform[0].Page3[0].f3_02[0]", dt.Rows[0]["ThreeJobsAmountA"].ToString());
            pdfFormFields.SetField("topmostSubform[0].Page3[0].f3_03[0]", dt.Rows[0]["ThreeJobsAmountB"].ToString());
            pdfFormFields.SetField("topmostSubform[0].Page3[0].f3_04[0]", dt.Rows[0]["ThreeJobsAmountC"].ToString());
            pdfFormFields.SetField("topmostSubform[0].Page3[0].f3_05[0]", dt.Rows[0]["PayPeriod"].ToString());
            pdfFormFields.SetField("topmostSubform[0].Page3[0].f3_06[0]", dt.Rows[0]["DivideAnnualAmount"].ToString());
            pdfFormFields.SetField("topmostSubform[0].Page3[0].f3_07[0]", dt.Rows[0]["DeductionsWorksheet1"].ToString());
            pdfFormFields.SetField("topmostSubform[0].Page3[0].f3_08[0]", dt.Rows[0]["DeductionsWorksheet2"].ToString());
            pdfFormFields.SetField("topmostSubform[0].Page3[0].f3_09[0]", dt.Rows[0]["DeductionsWorksheet3"].ToString());
            pdfFormFields.SetField("topmostSubform[0].Page3[0].f3_10[0]", dt.Rows[0]["DeductionsWorksheet4"].ToString());
            pdfFormFields.SetField("topmostSubform[0].Page3[0].f3_11[0]", dt.Rows[0]["DeductionsWorksheet5"].ToString());


            // report by reading values from completed PDF
            //string sTmp = "W-4 Completed for " + pdfFormFields.GetField("f1_09(0)") + " " +
            //pdfFormFields.GetField("f1_10(0)");
            //MessageBox.Show(sTmp, "Finished");


            var pdfContentByte = pdfStamper.GetOverContent(1); // page number

            if (File.Exists(path + "\\" + dt.Rows[0]["SignatureImage"].ToString()))
            {

                // Add Image
                iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(path + "\\" + dt.Rows[0]["SignatureImage"].ToString());
                image.ScaleToFit(70f, 30f);
                image.SetAbsolutePosition(120, 138);
                pdfContentByte.AddImage(image);
            }

            // Add Textbox
            BaseFont bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
            pdfContentByte.SaveState();
            pdfContentByte.BeginText();
            pdfContentByte.MoveText(480, 140);
            pdfContentByte.SetFontAndSize(bf, 10);
            pdfContentByte.ShowText(Convert.ToDateTime(dt.Rows[0]["CreatedDate"].ToString()).ToString("MM/dd/yyyy"));

            pdfContentByte.EndText();

            pdfStamper.FormFlattening = true;
            // close the pdf
            pdfStamper.Close();

            if (!string.IsNullOrEmpty(IsGenerate))
            {
                SQL objSql = new SQL();
                objSql.AddParameter("@ApplicantId", DbType.Int32, ParameterDirection.Input, 0, ApplicantId);
                objSql.AddParameter("@DocumentPath", DbType.String, ParameterDirection.Input, 0, NewFileName);
                objSql.AddParameter("@ApplicantTaskId ", DbType.Int32, ParameterDirection.Input, 0, Convert.ToInt32(AppTaskId));
                objSql.AddParameter("@CreatedBy", DbType.Int32, ParameterDirection.Input, 0, Convert.ToInt32(UserId));
                objSql.ExecuteNonQuery("p_ApplicantDocumentGenerate_Ins");
            }
            else
            {
                Applicant objApplicant = new Applicant();
                objApplicant.SaveApplicantDocument(ApplicantId, NewFileName, Convert.ToInt32(AppTaskId), Convert.ToInt32(UserId));
            }
        }

        public void FillW4Form2021(int ApplicantId, int AppTaskId, int UserId, string IsGenerate)
        {
            string path = @"P:\\WSRApplicantForm\\" + ApplicantId;

            string pdfTemplate = Startup.ReportTemplate + "W42021.pdf";
            //ListFieldNames(pdfTemplate);
            DataTable dt = new DataTable();
            ReportDAL obj = new ReportDAL();
            dt = obj.GetApplicantByID_rpt(ApplicantId, AppTaskId).Tables[0];
            string NewFileName = "";
            string newFile = "";
            if (dt.Rows.Count > 0)
            {

                NewFileName = "W42021_" + dt.Rows[0]["FirstName"].ToString().Replace("'", "") + "_" + ApplicantId + "_" + DateTime.Now.ToString("MM-dd-yyyy-hh-mm-ss") + ".pdf";
                newFile = path + "\\" + NewFileName;
            }


            if (!Directory.Exists(path))
            {
                DirectoryInfo di = Directory.CreateDirectory(path);

            }
            PdfReader pdfReader = new PdfReader(pdfTemplate);
            PdfStamper pdfStamper = new PdfStamper(pdfReader, new FileStream(
                        newFile, FileMode.Create));

            AcroFields pdfFormFields = pdfStamper.AcroFields;

            // set form pdfFormFields

            //Step 1
            pdfFormFields.SetField("topmostSubform[0].Page1[0].Step1a[0].f1_01[0]", dt.Rows[0]["WFirstName"].ToString() + " " + dt.Rows[0]["WMiddleName"].ToString()); // First Name
            pdfFormFields.SetField("topmostSubform[0].Page1[0].Step1a[0].f1_02[0]", dt.Rows[0]["WLastName"].ToString()); // Last Name
            pdfFormFields.SetField("topmostSubform[0].Page1[0].Step1a[0].f1_03[0]", dt.Rows[0]["Address1"].ToString()); // Address
            pdfFormFields.SetField("topmostSubform[0].Page1[0].Step1a[0].f1_04[0]", dt.Rows[0]["Address2"].ToString()); // City, State and Zip
            pdfFormFields.SetField("topmostSubform[0].Page1[0].f1_05[0]", dt.Rows[0]["WSSN"].ToString()); // SSN

            if (dt.Rows[0]["FedTaxStatuses"].ToString() == "1")
            {
                pdfFormFields.SetField("topmostSubform[0].Page1[0].c1_1[0]", "1", true); // Single
            }
            else if (dt.Rows[0]["FedTaxStatuses"].ToString() == "2")
            {
                pdfFormFields.SetField("topmostSubform[0].Page1[0].c1_1[1]", "2", true); // Married
            }
            else if (dt.Rows[0]["FedTaxStatuses"].ToString() == "3")
            {
                pdfFormFields.SetField("topmostSubform[0].Page1[0].c1_1[2]", "3", true); // Head of household
            }

            //Step 2
            pdfFormFields.SetField("topmostSubform[0].Page1[0].Step2c[0].c1_2[0]", dt.Rows[0]["IsMultipleJob"].ToString() == "True" ? "1" : "", true); // withheld

            //Step 3
            pdfFormFields.SetField("topmostSubform[0].Page1[0].Step3_ReadOrder[0].f1_06[0]", dt.Rows[0]["ClaimDepUnderAge"].ToString()); // 
            pdfFormFields.SetField("topmostSubform[0].Page1[0].Step3_ReadOrder[0].f1_07[0]", dt.Rows[0]["ClaimDepOther"].ToString());
            pdfFormFields.SetField("topmostSubform[0].Page1[0].f1_08[0]", dt.Rows[0]["TotalClaim"].ToString());

            //Step 4
            pdfFormFields.SetField("topmostSubform[0].Page1[0].f1_09[0]", dt.Rows[0]["OtherIncome"].ToString());
            pdfFormFields.SetField("topmostSubform[0].Page1[0].f1_10[0]", dt.Rows[0]["Deductions"].ToString());
            pdfFormFields.SetField("topmostSubform[0].Page1[0].f1_11[0]", dt.Rows[0]["ExtraWithholding"].ToString());

            //Step 5
            pdfFormFields.SetField("topmostSubform[0].Page1[0].f1_13[0]", dt.Rows[0]["BranchAddress"].ToString());
            pdfFormFields.SetField("topmostSubform[0].Page1[0].f1_14[0]", "");
            pdfFormFields.SetField("topmostSubform[0].Page1[0].f1_15[0]", dt.Rows[0]["EIN"].ToString());

            //Page 3
            pdfFormFields.SetField("topmostSubform[0].Page3[0].f3_01[0]", dt.Rows[0]["TwoJobsAmount"].ToString());
            pdfFormFields.SetField("topmostSubform[0].Page3[0].f3_02[0]", dt.Rows[0]["ThreeJobsAmountA"].ToString());
            pdfFormFields.SetField("topmostSubform[0].Page3[0].f3_03[0]", dt.Rows[0]["ThreeJobsAmountB"].ToString());
            pdfFormFields.SetField("topmostSubform[0].Page3[0].f3_04[0]", dt.Rows[0]["ThreeJobsAmountC"].ToString());
            pdfFormFields.SetField("topmostSubform[0].Page3[0].f3_05[0]", dt.Rows[0]["PayPeriod"].ToString());
            pdfFormFields.SetField("topmostSubform[0].Page3[0].f3_06[0]", dt.Rows[0]["DivideAnnualAmount"].ToString());
            pdfFormFields.SetField("topmostSubform[0].Page3[0].f3_07[0]", dt.Rows[0]["DeductionsWorksheet1"].ToString());
            pdfFormFields.SetField("topmostSubform[0].Page3[0].f3_08[0]", dt.Rows[0]["DeductionsWorksheet2"].ToString());
            pdfFormFields.SetField("topmostSubform[0].Page3[0].f3_09[0]", dt.Rows[0]["DeductionsWorksheet3"].ToString());
            pdfFormFields.SetField("topmostSubform[0].Page3[0].f3_10[0]", dt.Rows[0]["DeductionsWorksheet4"].ToString());
            pdfFormFields.SetField("topmostSubform[0].Page3[0].f3_11[0]", dt.Rows[0]["DeductionsWorksheet5"].ToString());


            // report by reading values from completed PDF
            //string sTmp = "W-4 Completed for " + pdfFormFields.GetField("f1_09(0)") + " " +
            //pdfFormFields.GetField("f1_10(0)");
            //MessageBox.Show(sTmp, "Finished");


            var pdfContentByte = pdfStamper.GetOverContent(1); // page number

            if (File.Exists(path + "\\" + dt.Rows[0]["SignatureImage"].ToString()))
            {

                // Add Image
                iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(path + "\\" + dt.Rows[0]["SignatureImage"].ToString());
                image.ScaleToFit(70f, 30f);
                image.SetAbsolutePosition(120, 138);
                pdfContentByte.AddImage(image);
            }

            // Add Textbox
            BaseFont bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
            pdfContentByte.SaveState();
            pdfContentByte.BeginText();
            pdfContentByte.MoveText(480, 140);
            pdfContentByte.SetFontAndSize(bf, 10);
            pdfContentByte.ShowText(Convert.ToDateTime(dt.Rows[0]["CreatedDate"].ToString()).ToString("MM/dd/yyyy"));

            pdfContentByte.EndText();

            pdfStamper.FormFlattening = true;
            // close the pdf
            pdfStamper.Close();

            if (!string.IsNullOrEmpty(IsGenerate))
            {
                SQL objSql = new SQL();
                objSql.AddParameter("@ApplicantId", DbType.Int32, ParameterDirection.Input, 0, ApplicantId);
                objSql.AddParameter("@DocumentPath", DbType.String, ParameterDirection.Input, 0, NewFileName);
                objSql.AddParameter("@ApplicantTaskId ", DbType.Int32, ParameterDirection.Input, 0, Convert.ToInt32(AppTaskId));
                objSql.AddParameter("@CreatedBy", DbType.Int32, ParameterDirection.Input, 0, Convert.ToInt32(UserId));
                objSql.ExecuteNonQuery("p_ApplicantDocumentGenerate_Ins");
            }
            else
            {
                Applicant objApplicant = new Applicant();
                objApplicant.SaveApplicantDocument(ApplicantId, NewFileName, Convert.ToInt32(AppTaskId), Convert.ToInt32(UserId));
            }
        }

        public void FillAerosolTransmissibleWaiverForm(int ApplicantId, int AppTaskId, int UserId)
        {
            try
            {
                string path = @"P:\\\WSRApplicantForm\\" + ApplicantId;
                //string pdfTemplate = Startup.ReportTemplate + "2020AerosolTransmissibleWaiver.pdf";

                string pdfTemplate = @"D:\\inetpub\\wwwHportalWSRService\\wwwroot\\ReportTemplate\\2020AerosolTransmissibleWaiver.pdf";

                DataTable dt = new DataTable();
                ReportDAL obj = new ReportDAL();
                dt = obj.GetApplicantByID_rpt(ApplicantId, AppTaskId).Tables[0];
                string NewFileName = "";
                string newFile = "";
                if (dt.Rows.Count > 0)
                {
                    NewFileName = "2020AerosolTransmissibleWaiver" + dt.Rows[0]["FirstName"].ToString().Replace("'", "") + "_" + ApplicantId + "_" + DateTime.Now.ToString("MM-dd-yyyy-hh-mm-ss") + ".pdf";
                    newFile = path + "\\" + NewFileName;
                }
                if (!Directory.Exists(path))
                {
                    DirectoryInfo di = Directory.CreateDirectory(path);
                }
                PdfReader pdfReader = new PdfReader(pdfTemplate);
                PdfStamper pdfStamper = new PdfStamper(pdfReader, new FileStream(
                            newFile, FileMode.Create));
                AcroFields pdfFormFields = pdfStamper.AcroFields;
                var pdfContentByte = pdfStamper.GetOverContent(1); // page number
                if (File.Exists(path + "\\" + dt.Rows[0]["SignatureImage"].ToString()))
                {
                    iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(path + "\\" + dt.Rows[0]["SignatureImage"].ToString());
                    image.ScaleToFit(70f, 30f);
                    image.SetAbsolutePosition(270, 250);
                    pdfContentByte.AddImage(image);
                }
                BaseFont bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                pdfContentByte.SaveState();
                pdfContentByte.BeginText();
                pdfContentByte.MoveText(455, 250);
                pdfContentByte.SetFontAndSize(bf, 10);
                pdfContentByte.ShowText(Convert.ToDateTime(dt.Rows[0]["CreatedDate"].ToString()).ToString("MM/dd/yyyy"));
                pdfContentByte.EndText();

                //fill name on top
                pdfContentByte.BeginText();
                pdfContentByte.MoveText(95, 610);
                pdfContentByte.SetFontAndSize(bf, 10);
                pdfContentByte.ShowText(dt.Rows[0]["FirstName"].ToString() + " " + dt.Rows[0]["LastName"].ToString());
                pdfContentByte.EndText();

                pdfContentByte.BeginText();
                pdfContentByte.MoveText(80, 250);
                pdfContentByte.SetFontAndSize(bf, 10);
                pdfContentByte.ShowText(dt.Rows[0]["FirstName"].ToString() + " " + dt.Rows[0]["LastName"].ToString());
                pdfContentByte.EndText();

                pdfStamper.FormFlattening = true;
                pdfStamper.Close();
                Applicant objApplicant = new Applicant();
                objApplicant.SaveApplicantDocument(ApplicantId, NewFileName, Convert.ToInt32(AppTaskId), Convert.ToInt32(UserId));
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}