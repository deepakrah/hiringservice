﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using HPortalService.DAL;

namespace HPortalService
{
    public class ApplicantResidencyForm
    {
        public int ApplicantResidencyFormId { get; set; }
        public int ApplicantId { get; set; }
        public string SSN { get; set; }
        public string NAME { get; set; }
        public string StreetAddress { get; set; }
        public string SecondAddress { get; set; }
        public string City { get; set; }
        public string STATE { get; set; }
        public string ZipCode { get; set; }
        public string Phone { get; set; }
        public string Municipality { get; set; }
        public string County { get; set; }
        public string PsdCode { get; set; }
        public string TotalResident { get; set; }
        public string EmailId { get; set; }
        
        public int InsApplicantResidencyForm(ApplicantResidencyForm obj)
        {
            ApplicantResidencyFormDAL _ApplicationDAL = new ApplicantResidencyFormDAL();
            return _ApplicationDAL.InsApplicantResidencyForm(obj);
        }
        public DataSet GetApplicantResidencyForm(int ID)
        {
            ApplicantResidencyFormDAL _ApplicationDAL = new ApplicantResidencyFormDAL();
            return _ApplicationDAL.GetApplicantResidencyForm(ID);
        }
    }
}
