﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HPortalService
{
    public class SourceCode
    {
        public string BranchSourceId { get; set; }
        public string Source { get; set; }
        public int BranchId { get; set; }
        public string CreatedOn { get; set; }
        public string SourceCategory { get; set; }
        public string Name { get; set; }
        public string GroupName { get; set; }
        public Boolean ShowOnWeb { get; set; }
    }
}
