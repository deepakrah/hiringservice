﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HPortalService
{
   public class Answer
    {
        public int AnswerId { get; set; }
        public int QuesId { get; set; }
        public string AnswerName { get; set; }
        public bool isTrue { get; set; }
        public int NextQuestion { get; set; }
        public int Answered { get; set; }
      
    }
}
