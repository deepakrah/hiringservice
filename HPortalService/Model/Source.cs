﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

using HPortalService.DAL;

namespace HPortalService
{
    public class Source
    {
        public Nullable<int> SourceId { get; set; }
        public string SourceName { get; set; }
        public bool promptForValue { get; set; }
        public Nullable<int> BranchId { get; set; }
        public decimal Amount { get; set; }
        public Nullable<int> Month { get; set; }
        public Nullable<int> Year { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public string DateFrom { get; set; }
        public string DateTo { get; set; }
        public int Method { get; set; }      
        public string NoOfMonths { get; set; }
        public string BranchName { get; set; }
        public int Apps { get; set; }
        public string STATUS { get; set; }
        public string HowDidYouHear { get; set; }
        public int CurrentStatusYear { get; set; }

        public int Save()
        {
            SourceDAL obj = new SourceDAL();
            return obj.Save(this);
        }

        public DataSet Get()
        {
            SourceDAL obj = new SourceDAL();
            return obj.Get(this);
        }


        public int Disable()
        {
            SourceDAL obj = new SourceDAL();
            return obj.Disable(this);
        }

        public int InsertExpense()
        {
            SourceDAL obj = new SourceDAL();
            return obj.InsertExpense(this);
        }

        public DataSet GetExpense(int? branchid, string from, string to)
        {
            SourceDAL obj = new SourceDAL();
            return obj.GetExpense(branchid, from, to);
        
        }
        public DataSet fnGetSourceReport(int Year)
        {
            SourceDAL obj = new SourceDAL();
            return obj.GetSourceReport(Year);
        }

    }
}
