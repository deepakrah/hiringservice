﻿using HPortalService.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace HPortalService.Model
{
    public class ApplicantUniform
    {
        public int ApplicantUniformId { get; set; }
        public string Height { get; set; }
        public string HeightFeet { get; set; }
        public string HeightInche { get; set; }
        public string Weight { get; set; }
        public string JacketSize { get; set; }
        public string BlazerSize { get; set; }
        public string ShoeSize { get; set; }
        public string PantSize { get; set; }
        public string GloveSize { get; set; }
        public string PoloShirtSize { get; set; }
        public string ButtonDownShirtSize { get; set; }
        public int ApplicantId { get; set; }

        public int SaveApplicantUniform(ApplicantUniform obj)
        {
            ApplicantDAL _ApplicationDAL = new ApplicantDAL();
            return _ApplicationDAL.SaveApplicantUniform(obj);
        }
        public List<ApplicantUniform> GetApplicantUniform(ApplicantUniform obj)
        {
            ApplicantDAL _ApplicationDAL = new ApplicantDAL();
            List<ApplicantUniform> lstApplicantUniform = SQLHelper.ContructList<ApplicantUniform>(_ApplicationDAL.GetApplicantUniform(obj));
            return lstApplicantUniform;

        }
        public DataSet GetAppUniform(ApplicantUniform obj)
        {
            ApplicantDAL _ApplicationDAL = new ApplicantDAL();
            return (_ApplicationDAL.GetApplicantUniform(obj));
        }

    }

}
