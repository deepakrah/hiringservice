﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

using HPortalService.DAL;

namespace HPortalService
{
 public class ApplicantAddress
    {
        public int JobApplicationId { get; set; }
        public Boolean isMailingDiff { get; set; }
        public string AddressType { get; set; }
        public string Address { get; set; }
        public string Unit { get; set; }
        public string City { get; set; }
        public int ZipCode { get; set; }
        public int StateId { get; set; }
        public int CountryId { get; set; }
        public string StateName { get; set; }
        public string CountryName { get; set; }

        public int SaveApplicantAddress(ApplicantAddress obj)
        {
            ApplicantAddressDAL _AppAddress = new ApplicantAddressDAL();
            return _AppAddress.UpdateApplicantAddress(obj);
        }

        public List<ApplicantAddress> GetApplicantAddress(int JobApplicantID)
        {
           DataSet ds;
            ApplicantAddressDAL _AppAddress = new ApplicantAddressDAL();
            ds = _AppAddress.GetApplicantAddress(JobApplicantID);
            List<ApplicantAddress> lstApplicant = SQLHelper.ContructList<ApplicantAddress>(ds);
            return lstApplicant;
        }

    }
}
