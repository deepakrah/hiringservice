﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using HPortalService.DAL;



namespace HPortalService
{ 
    public class Lookup
    {
        public int StateId { get; set; }
        public string Name { get; set; }
        public int CountryId { get; set; }
        public string CountryName { get; set; }
        public String Abbreviation { get; set; }

        public DataSet GetCountry()
        {
            LookupDAL _LookupDAL = new LookupDAL();
            DataSet ds = _LookupDAL.GetAllCountry();
            return ds;
        }

        public DataSet GetStateByCountryId(Lookup obj)
        {
            LookupDAL _LookupDAL = new LookupDAL();
            DataSet ds = _LookupDAL.GetStateByCountryId(obj);
            return ds;
        }

        public List<Country> GetCountriesAndStates(){
            List<Country> objCountries = new List<Country>();
            LookupDAL _LookupDAL = new LookupDAL();
            DataSet ds = _LookupDAL.GetAllCountry();
           objCountries= SQLHelper.ContructList<Country>(ds);
           foreach (Country objC in objCountries)
           {
               Lookup obj = new Lookup();
               obj.CountryId = objC.CountryId;
                ds = _LookupDAL.GetStateByCountryId(obj);
                List<State> objStates = SQLHelper.ContructList<State>(ds);
                objC.States = objStates;
           }
           return objCountries;
        }

    }


    public class Country {

        public int CountryId { get; set; }
        public String Name { get; set; }
        public String Abbreviation { get; set; }
        public int IsActive { get; set; }
        public IList<State> States { get; set; }  
    }


    public class State {
        public int StateId { get; set; }
        public String Name { get; set; }
        public String Abbreviation { get; set; }
       
    }
}
