﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HPortalService.DAL;

namespace HPortalService
{
   public class EmailLog
    {
        public int UserId { get; set; }
        public int ApplicantId { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public int EmailLogID { get; set; }

        public void SaveLog()
        {
            EmailLogDAL obj = new EmailLogDAL();
            obj.SaveLog(this);
        }
    }
}
