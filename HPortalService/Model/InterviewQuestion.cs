﻿using HPortalService.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;


namespace HPortalService
{
   public class InterviewQuestion
    {
       public int InterviewQuestionId { get; set; }       
       public string QuesDesc { get; set; }       
       public int BranchId { get; set; }
       public string OptionDesc { get; set; }
       public int InterviewOptionId { get; set; }
        public int ApplicantId { get; set; }
        public Boolean rubics { get; set; }
        public string Remarks { get; set; }
        public string Score { get; set; }

       public int Save()
       {
           InterviewQuestionDAL objInterviewQuestionDAL = new InterviewQuestionDAL();
           return objInterviewQuestionDAL.Save(this);
       }

       public void SaveOption()
       {
           InterviewQuestionDAL objInterviewQuestionDAL = new InterviewQuestionDAL();
            objInterviewQuestionDAL.SaveOption(this);
       
       }

       public DataSet GetQuestions()
       {
           InterviewQuestionDAL objInterviewQuestionDAL = new InterviewQuestionDAL();
           return objInterviewQuestionDAL.GetQuestions(this);
       }

       public int SaveApplicantOption()
       {
           InterviewQuestionDAL objInterviewQuestionDAL = new InterviewQuestionDAL();
           return objInterviewQuestionDAL.SaveApplicantOption(this);
       }

       public DataSet GetOptions()
       {
           InterviewQuestionDAL objInterviewQuestionDAL = new InterviewQuestionDAL();
           return objInterviewQuestionDAL.GetOptions(this);

       }

       public int Delete()
       {
           InterviewQuestionDAL objInterviewQuestionDAL = new InterviewQuestionDAL();
           return objInterviewQuestionDAL.Delete(this);
       }

       public DataSet GetApplicantInterviewAnswers()
       {
           InterviewQuestionDAL objInterviewQuestionDAL = new InterviewQuestionDAL();
           return objInterviewQuestionDAL.GetApplicantInterviewAnswers(this);

       }
    }
}
