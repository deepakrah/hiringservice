﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HPortalService.Model
{
    public class FormWT4
    {
        public int? FormWT4Id { get; set; }
        public int? ApplicantId { get; set; }      
        public string HireDate { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public int? ZipCode { get; set; }
        public int? FedTaxStatus { get; set; }
        public int? ExemptSelf { get; set; }
        public int? ExemptSpouse { get; set; }
        public int? ExemptDependent { get; set; }
        public int? TotalExempt { get; set; }
        public string AdditionalAmount { get; set; }
        public string Exempt { get; set; }
    }
}
