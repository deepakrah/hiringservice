﻿using HPortalService.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data;
using System.Net.Mail;
using System.Net;

using HPortalService.Model;

namespace HPortalService
{
    public class JobFairParticipant
    {
        public int JFApplicantID { get; set; }
        public int JFParticipantID { get; set; }
    }
    public class JobFair
    {
        public int ApplicantId { get; set; }
        public int JFApplicantID { get; set; }
        public string JFParticipantID { get; set; }
        public int JobFairID { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string PrimaryPhone { get; set; }
        public string Comments { get; set; }
        public int SignIn { get; set; }
        public Boolean sendMail { get; set; }
        public DateTime? SignInTime { get; set; }
        public DateTime? SignOutTime { get; set; }
        public bool SignInFlag { get; set; }
        public bool SignOutFlag { get; set; }
        public bool IsEMailSend { get; set; }
        public string JFParticipant { get; set; }
        public string Name { get; set; }
        public bool JFParticipantFlag { get; set; }
        public int PageIndex { get; set; }
        public int PageCount { get; set; }
        public string sortCol { get;set; }
        public string SortOrder { get; set; }

        static string UsesmtpSSL = Startup.UsesmtpSSL;
        static string enableMail = Startup.enableMail;
        static string mailServer = Startup.mailServer;
        static string userId = Startup.userId;
        static string password = Startup.password;
        static string authenticate = Startup.authenticate;
        static string AdminEmailID = Startup.AdminEmailID;
        static string fromEmailID = Startup.fromEmailID;
        static string DomainName = Startup.DomainName;
        static string AllowSendMails = Startup.AllowSendMails;
        static string UserName = Startup.UserName;

        public int Register(JobFair obj)
        {
            JobFairDAL _ApplicationDAL = new JobFairDAL();
            return _ApplicationDAL.Register(obj);
        }
        public int InsJFPApplicants(JobFair obj)
        {
            JobFairDAL _ApplicationDAL = new JobFairDAL();
            return _ApplicationDAL.InsJFPApplicants(obj);
        }
        public int SignInSave(JobFair obj)
        {
            JobFairDAL _ApplicationDAL = new JobFairDAL();
            return _ApplicationDAL.SignInSave(obj);
        }
        public DataSet GetJobFairApplicant(JobFair obj)
        {
           
            JobFairDAL _obj = new JobFairDAL();
            return _obj.GetJobFairApplicant(obj);
        }
        public DataSet GetJobFairParticipant(JobFair obj)
        {
            string password = String.Empty;
            JobFairDAL _obj = new JobFairDAL();
            return _obj.GetJobFairParticipant(obj);

        }
        public int SignOutSave(JobFair obj)
        {
            JobFairDAL _ApplicationDAL = new JobFairDAL();
            return _ApplicationDAL.SignOutSave(obj);
        }
        public void SendEmail(EmailParameters emailParameters, String toMailAddress, List<Attachment> strAttachment = null, AlternateView EventData = null)
        {
            JobFairDAL _obj = new JobFairDAL();
            string strBody = "";
            //EmailTemplate objET = new EmailTemplate();
            //objET.EmailType = "27";
            //objET.BranchId = 0;
            //DataSet ds = _obj.Get(objET);           
            //if(ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            //{
            //    ds.Tables[0].Rows[0]["Template"].ToString();
            // }
            strBody = !String.IsNullOrEmpty(emailParameters.EmailBody) ? emailParameters.EmailBody : MailerUtility.GetMailBody(emailParameters);

            //****************Calling the Send Mail Function *******************************
            MailContent objMailContent = new MailContent() { From = "HR@woodsideranch.com", toEmailaddress = toMailAddress, displayName = "Woodside Recruiting Staff", subject = emailParameters.Subject, emailBody = strBody, strAttachment = strAttachment, EventData = EventData, BranchId = emailParameters.BranchId };

            SendEmailInBackgroundThread(objMailContent);
        }
        public void SendEmailInBackgroundThread(MailContent objMailContent)
        {
            //Thread bgThread = new Thread(new ParameterizedThreadStart(SendAttachment));
            //bgThread.IsBackground = true;
            //bgThread.Start(objMailContent);
            SendAttachment(objMailContent, UserName);
        }
        public static void SendAttachment(Object objMail, string UserName)
        {
            MailContent objMC = (MailContent)objMail;
            if (objMC.toEmailaddress.StartsWith("admin"))
            {
                // objMC.toEmailaddress = "solomenn@mailinator.com";
            }

            SmtpClient smtpClient = new SmtpClient();
            MailMessage mailMessage = new MailMessage();

            // mailMessage.Headers.Add("Disposition-Notification-To", "ykumar@protatechindia.com");

            bool flag = false;
            bool UseSMTPSSL = false;
            if (!string.IsNullOrEmpty(authenticate))
            {
                flag = Convert.ToBoolean(authenticate);
            }
            if (!string.IsNullOrEmpty(UsesmtpSSL))
            {
                UseSMTPSSL = Convert.ToBoolean(UsesmtpSSL);
            }

            string host = mailServer;

            string address = fromEmailID;


            //if (UserName != null)
            if (objMC.BranchId > 0)
            {

                Branch obj = new Branch();
                string email = obj.GetEmailId(objMC.BranchId);
                if (email != "")
                {
                    address = email;
                }
            }

            if (!String.IsNullOrEmpty(objMC.From))
            {
                address = objMC.From;
            }

            MailAddress from = new MailAddress(address, objMC.displayName);

            if (objMC.CopyTo.Count > 0)
            {
                foreach (string copyTo in objMC.CopyTo)
                {
                    if (!string.IsNullOrEmpty(copyTo))
                    {
                        mailMessage.CC.Add(new MailAddress(copyTo));
                    }
                }
            }


            try
            {

                smtpClient.EnableSsl = false;
                smtpClient.Host = host;
                mailMessage.From = from;
                smtpClient.Port = 25;
                mailMessage.To.Add(objMC.toEmailaddress);
                mailMessage.Subject = objMC.subject;
                mailMessage.IsBodyHtml = true;

                if (objMC.EventData != null)
                {
                    mailMessage.AlternateViews.Add(objMC.EventData);

                    System.Net.Mime.ContentType typeHTML = new System.Net.Mime.ContentType("text/html");
                    AlternateView viewHTML = AlternateView.CreateAlternateViewFromString(objMC.emailBody, typeHTML);
                    viewHTML.TransferEncoding = System.Net.Mime.TransferEncoding.SevenBit;
                    mailMessage.AlternateViews.Add(viewHTML);
                }
                else
                {
                    mailMessage.Body = objMC.emailBody;
                }
                if (objMC.strAttachment != null)
                {
                    foreach (Attachment a in objMC.strAttachment)
                    {

                        mailMessage.Attachments.Add(a);

                    }
                }
                if (flag)
                {
                    NetworkCredential credentials = new NetworkCredential(userId, password);
                    smtpClient.UseDefaultCredentials = false;
                    smtpClient.Credentials = credentials;
                }
                else
                {
                    smtpClient.UseDefaultCredentials = true;
                }

                string sendMail = AllowSendMails;


                if (string.IsNullOrEmpty(sendMail) || (sendMail.ToUpper() != "NO"))
                {
                    if (!string.IsNullOrEmpty(objMC.emailBody))
                    {
                        smtpClient.Send(mailMessage);
                        smtpClient.Dispose();
                    }

                }


                // update Mail log status of IsDelivered
                //MailBoxManager.UpdateDeliveryStatus(MailerLogID, true, "Successfully Send.");

            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.Message);
                ErrorLogger.Log(ex.StackTrace);
                //update Mail log status of IsDelivered = False and Logtext = ex.mesage
                //logger.ErrorFormat(ex.Message);
                throw ex;
            }
        }

        public DataSet GetJobFairApplicantById(JobFair obj)
        {
            string password = String.Empty;
            JobFairDAL _obj = new JobFairDAL();
            return _obj.GetJobFairApplicantById(obj);

        }

    }
}
