﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

using HPortalService.DAL;

namespace HPortalService
{
  public class ApplicantReference
    {
        public int ApplicantReferenceID { get; set; }
        public int JobApplicationId { get; set; }
        public string Name { get; set; }
        public string RelationShip { get; set; }
        public string PrimaryPhone { get; set; }
        public string AlternatePhone { get; set; }

        public int SaveReferences()
        {
            ApplicantReferenceDAL _AppGeneralInfo = new ApplicantReferenceDAL();
            return _AppGeneralInfo.SaveReference(this);
        }


        public List<ApplicantReference> GetReferenceByJobApplicantId(int JobApplicantId)
        {
            DataSet ds;
            ApplicantReferenceDAL _AppInfo = new ApplicantReferenceDAL();
            ds = _AppInfo.GetReferenceByJobApplicantId(JobApplicantId);
            List<ApplicantReference> lstApplicant = SQLHelper.ContructList<ApplicantReference>(ds);
            return lstApplicant;
        }

        public int DeleteApplicantReference()
        {
            ApplicantReferenceDAL _AppReference = new ApplicantReferenceDAL();
            return _AppReference.DeleteApplicantReferenceById(this.ApplicantReferenceID);
        }

        public int UpdateReferences()
        {
            ApplicantReferenceDAL _AppReference = new ApplicantReferenceDAL();
            return _AppReference.UpdateReference(this);
        }

      

    }
}
