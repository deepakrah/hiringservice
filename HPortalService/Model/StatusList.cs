﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HPortalService.Model
{
    public class StatusList
    {
        public int StatusListID { get; set; }
        public string Name { get; set; }
        public IList<StatusList> ChildList { get; set; }

      public  List<StatusList> ListData()
        {
            List<StatusList> objList = new List<StatusList>();
            objList.Add(new StatusList()
            {
                StatusListID = 0,
                Name = "All",
                ChildList = new List<StatusList>() {
                  new StatusList() { StatusListID = 12 ,Name="Applying"},
                  new StatusList() { StatusListID = 1 ,Name="Applied"},
                  //new StatusList() { StatusListID = 22,Name="Scheduled for an online-interview" },
                  //new StatusList() { StatusListID = 23,Name="Re-Scheduled for an online-interview" },
                  new StatusList() { StatusListID = 2,Name="Scheduled for an interview" },
                  new StatusList() { StatusListID = 2,Name="Re-Scheduled for an interview" },
                  new StatusList() { StatusListID = 15,Name="Attended-Int" },
                  new StatusList() { StatusListID = 13,Name="No Show Interview" },
                  new StatusList() { StatusListID = 11,Name="Approved" },
                  new StatusList() { StatusListID = 3 ,Name="Offered"},
                  new StatusList() { StatusListID = 9,Name="Schedule for Orientation" },
                  new StatusList() { StatusListID = 16,Name="Attended-Ori" },
                  new StatusList() { StatusListID = 14,Name="No Show Orientation" },
                  new StatusList() { StatusListID = 6 ,Name="Rejected"},
                  new StatusList() { StatusListID = 17 ,Name="Readdress"},
                  new StatusList() { StatusListID = 20 ,Name="Rehire"},
                  new StatusList() { StatusListID = 5 ,Name="Hired"}

            }
            });

            objList.Add(new StatusList()
            {
                StatusListID = 12,
                Name = "Applying",
                ChildList = new List<StatusList>() {
                     new StatusList() { StatusListID = 1 ,Name="Applied"},
                 new StatusList() { StatusListID = 6 ,Name="Rejected"},
                new StatusList() { StatusListID = 17 ,Name="Readdress"}}
            });
            objList.Add(new StatusList()
            {
                StatusListID = 1,
                Name = "Applied",
                ChildList = new List<StatusList>() {
                new StatusList() { StatusListID = 2,Name="Scheduled for an interview" },
               // new StatusList() { StatusListID = 22,Name=" Scheduled for an online-interview" },
                  new StatusList() { StatusListID = 6 ,Name="Rejected"},
                new StatusList() { StatusListID = 17 ,Name="Readdress"}}
            });
            objList.Add(new StatusList()
            {
                StatusListID = 2,
                Name = "Scheduled for an interview",
                ChildList = new List<StatusList>() {
                new StatusList() { StatusListID = 15,Name="Attended-Int" },
                  new StatusList() { StatusListID = 13,Name="No Show Interview" } ,
                  new StatusList() { StatusListID = 2,Name="Re-Scheduled for an interview" },
                  new StatusList() { StatusListID = 6 ,Name="Rejected"},
                new StatusList() { StatusListID = 17 ,Name="Readdress"},
                new StatusList() { StatusListID = 1 ,Name="Applied"}
            }
            });
            //objList.Add(new StatusList()
            //{
            //    StatusListID = 22,
            //    Name = "Scheduled for an online-interview",
            //    ChildList = new List<StatusList>() {
            //        new StatusList() { StatusListID = 15,Name="Attended-Int" },
            //          new StatusList() { StatusListID = 13,Name="No Show Interview" } ,
            //          //new StatusList() { StatusListID = 23,Name="Re-Scheduled for an online-interview" },
            //          new StatusList() { StatusListID = 6 ,Name="Rejected"},
            //        new StatusList() { StatusListID = 17 ,Name="Readdress"},
            //        new StatusList() { StatusListID = 1 ,Name="Applied"}
            //    }
            //});
            objList.Add(new StatusList()
            {
                StatusListID = 15,
                Name = "Attended-Int",
                ChildList = new List<StatusList>() {
                    new StatusList() { StatusListID = 19,Name="Clear Attended" },
                     new StatusList() { StatusListID = 3 ,Name="Offered"},
                new StatusList() { StatusListID = 11,Name="Approved" },
                 new StatusList() { StatusListID = 17 ,Name="Readdress"},
                 new StatusList() { StatusListID = 6 ,Name="Rejected"}}

            });
            objList.Add(new StatusList()
            {
                StatusListID = 13,
                Name = "No Show Interview",
                ChildList = new List<StatusList>() {
                 new StatusList() { StatusListID = 18,Name="Clear NoShow" },
                  new StatusList() { StatusListID = 2,Name="Re-Scheduled for an interview" }, 
                 //new StatusList() { StatusListID = 23,Name="Re-Scheduled for an online-interview" },
                   new StatusList() { StatusListID = 17 ,Name="Readdress"},
                 new StatusList() { StatusListID = 6 ,Name="Rejected"},
            new StatusList() { StatusListID = 1 ,Name="Applied"}
            }
            });
            objList.Add(new StatusList()
            {
                StatusListID = 11,
                Name = "Approved",
                ChildList = new List<StatusList>() {
                new StatusList() { StatusListID = 3 ,Name="Offered"},
                 new StatusList() { StatusListID = 17 ,Name="Readdress"},
                 new StatusList() { StatusListID = 6 ,Name="Rejected"}}
            });

            objList.Add(new StatusList()
            {
                StatusListID = 3,
                Name = "Offered",
                ChildList = new List<StatusList>() {
                new StatusList() { StatusListID = 9,Name="Schedule for Orientation" } ,
                 new StatusList() { StatusListID = 17 ,Name="Readdress"},
                new StatusList() { StatusListID = 6 ,Name="Rejected"},
                new StatusList() { StatusListID = 20 ,Name="Rehire"}
               }
            });

            objList.Add(new StatusList()
            {
                StatusListID = 9,
                Name = "Schedule for Orientation",
                ChildList = new List<StatusList>() {
                new StatusList() { StatusListID = 9,Name="Re-Schedule for Orientation" } ,
                  new StatusList() { StatusListID = 16,Name="Attended-Ori" },
                  new StatusList() { StatusListID = 14,Name="No Show Orientation" },
                   new StatusList() { StatusListID = 17 ,Name="Readdress"},
                   new StatusList() { StatusListID = 6 ,Name="Rejected"}
               }
            });
            objList.Add(new StatusList()
            {
                StatusListID = 16,
                Name = "Attended-Ori",
                ChildList = new List<StatusList>() {
                    new StatusList() { StatusListID = 19,Name="Clear Attended" },
                new StatusList() { StatusListID = 5,Name="Hired" },
                 new StatusList() { StatusListID = 17 ,Name="Readdress"},
                 new StatusList() { StatusListID = 6 ,Name="Rejected"},
            new StatusList() { StatusListID = 20 ,Name="Rehire"}
            }

            });
            objList.Add(new StatusList()
            {
                StatusListID = 14,
                Name = "No Show Orientation",
                ChildList = new List<StatusList>() {
                new StatusList() { StatusListID = 18,Name="Clear NoShow" },
                 new StatusList() { StatusListID = 9,Name="Schedule for Orientation" },
                  new StatusList() { StatusListID = 17 ,Name="Readdress"},
                 new StatusList() { StatusListID = 6 ,Name="Rejected"}}
            });


            objList.Add(new StatusList()
            {
                StatusListID = 5,
                Name = "Hired"

            });
            objList.Add(new StatusList()
            {
                //14- july Kailash 2017
                StatusListID = 17,
                Name = "Readdress",
                ChildList = new List<StatusList>() {
                new StatusList() { StatusListID = 12 ,Name="Applying"},
              new StatusList() { StatusListID = 1,Name="Applied" },
              new StatusList() {StatusListID = 2,    Name = "Scheduled for an interview"} ,
              new StatusList() {StatusListID = 22,    Name = "Scheduled for an  online-interview"},
              new StatusList() { StatusListID = 6 ,Name="Rejected"}

            }
            });
            objList.Add(new StatusList()
            {
                StatusListID = 6,
                Name = "Rejected",
                ChildList = new List<StatusList>() {
                 new StatusList() { StatusListID = 2,Name="Scheduled for an interview" },
                  new StatusList() {StatusListID = 22,    Name = "Scheduled for an  online-interview"} ,
                 new StatusList() { StatusListID = 9,Name="Schedule for Orientation" }}

            });
            objList.Add(new StatusList()
            {
                StatusListID = 20,
                Name = "Rehire",
                ChildList = new List<StatusList>() {
                 new StatusList() { StatusListID = 3,Name="Offered" }}

            });
            return objList;
        }

    }
}
