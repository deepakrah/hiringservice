﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using HPortalService.DAL;

namespace HPortalService
{
    public class ApplicantCTW4EmpWithholdingCertificate
    {
        public int ApplicantCTW4EmpWithholdingCertificateId { get; set; }
        public int ApplicantId { get; set; }
        public string WithholdingCode { get; set; }
        public string AdditionalAmount { get; set; }
        public string ReducedAmount { get; set; }
        public string LegalResidence { get; set; }    
        public bool IsRehired { get; set; }
        public string RehiredDate { get; set; }       
        public string SignatureImage { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string SSN { get; set; }
        public string EIN { get; set; }
        public string EmployerName { get; set; }
        public string EmployerBusinessAddress { get; set; }
        public string EmployerCity { get; set; }
        public string EmployerState { get; set; }
        public string EmployerZIPCode { get; set; }
        public string EmployerContactPerson { get; set; }
        public string ContactPersonPhone { get; set; }
        public string HomeAddress { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
       

        public int intApplicantCTW4EmpWithholdingCertificate(ApplicantCTW4EmpWithholdingCertificate obj)
        {
            ApplicantCTW4EmpWithholdingCertificateDAL _ApplicationDAL = new ApplicantCTW4EmpWithholdingCertificateDAL();
            return _ApplicationDAL.IntApplicantCTW4EmpWithholdingCertificate(obj);
        }
        public DataSet GetApplicantCTW4EmpWithholdingCertificateByID(int ID)
        {
            ApplicantCTW4EmpWithholdingCertificateDAL _ApplicationDAL = new ApplicantCTW4EmpWithholdingCertificateDAL();
            return _ApplicationDAL.GetApplicantCTW4EmpWithholdingCertificateByID(ID);
        }
    }
}
