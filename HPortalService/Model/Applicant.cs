﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using HPortalService.DAL;
using HPortalService;
using HPortalService.Model;

namespace HPortalService
{
    public class Applicant
    {
        public bool chkShowAllYears { get; set; }
        public string SortCoumn { get; set; }
        public int ApplicantId { get; set; }
        public int JobApplicationId { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string EmailID { get; set; }
        public string PicturePath { get; set; }
        public string ZipCode { get; set; }
        public string AppliedOn { get; set; }
        public string AuthenticationType { get; set; }
        public string Password { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public int StateId { get; set; }
        public int CountryId { get; set; }
        public string PrimaryPhone { get; set; }
        public string SecondaryPhone { get; set; }
        public string Unit { get; set; }
        public string StateName { get; set; }
        public string status1 { get; set; }

        public string UserType { get; set; }
        public string UserTypeID { get; set; }
        public string CountryName { get; set; }
        public int UserId { get; set; }
        public string HowDidYouHear { get; set; }
        public string RefferedBy { get; set; }
        public int Status { get; set; }
        public string StatusName { get; set; }
        public string ApplicantStatus { get; set; }
        public int BranchId { get; set; }
        public int ChildBranchId { get; set; }
        public Boolean IsAgree { get; set; }
        public string AppliedOnStart { get; set; }
        public string AppliedOnEnd { get; set; }
        public int AnswerId { get; set; }
        public String DOB { get; set; }
        public string SSN { get; set; }
        public string TaxStatus { get; set; }
        public Nullable<int> NoOfDependency { get; set; }
        public string CurrPage { get; set; }
        public Nullable<Decimal> AdditionalAmount { get; set; }
        public string BranchName { get; set; }
        public string BranchAddress2 { get; set; }
        public string BranchCityName { get; set; }
        public string BranchState { get; set; }
        public string BranchZip { get; set; }
        public string BranchPhoneNo { get; set; }
        public string OrganizationName { get; set; }
        public string CSCIDNumber { get; set; }
        public string EmployeeLastName { get; set; }
        public string EmployeeFirstName { get; set; }
        public string EmployeeIDNumber { get; set; }
        public Boolean isVerified { get; set; }
        public string BranchSourceId { get; set; }
        public string Initials { get; set; }
        public Boolean isCSCReferral { get; set; }
        public Boolean isOrganizationReferral { get; set; }
        public string CSCReferralName { get; set; }
        public string OrganizationReferralName { get; set; }
        public int BGStatus { get; set; }
        public string BGCheckID { get; set; }
        public bool Rehire { get; set; }
        public int RehireSourceId { get; set; }

        public bool ReEmployee { get; set; }
        public string BGCheckMessage { get; set; }
        public Decimal Allowance { get; set; }
        public String FedTaxStatus { get; set; }
        public string SignatureImage { get; set; }
        public int NoOfAllowances { get; set; }
        public Boolean NameDiffer { get; set; }
        public int W4FormId { get; set; }
        public string WFirstName { get; set; }
        public string WMiddleName { get; set; }
        public string WLastName { get; set; }
        public String WDOB { get; set; }
        public string WSSN { get; set; }
        public string WCity { get; set; }
        public int WStateId { get; set; }
        public int WCountryId { get; set; }
        public string WZipCode { get; set; }
        public decimal WAdditionalAmount { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }

        public string VenueName { get; set; }
        public string ParkingInstructions { get; set; }
        public string HiringManager { get; set; }
        public string Designation { get; set; }
        public string PayRate { get; set; }
        public string Hours { get; set; }
        public string Exempt { get; set; }
        public bool EnableOnline { get; set; }
        public string OnlineInterview { get; set; }

        public bool BGFlag { get; set; }
        public int JdpStatus { get; set; }
        public string ZurichDate { get; set; }
        public string ZurichPrintedName { get; set; }
        public string ZurichAddress { get; set; }
        public string ZurichCity { get; set; }
        public string ZurichStateName { get; set; }
        public string ZurichZipCode { get; set; }

        public string EIN { get; set; }
        public string BranchAddress { get; set; }
        public string ModifiedDate { get; set; }
        public string OrientationDate { get; set; }
        public string InterviewDate { get; set; }

        public int NoShowInterview { get; set; }
        public int NoShowOrientation { get; set; }
        public int EmailCount { get; set; }
        public int CallCount { get; set; }
        public string DuplicateStatus { get; set; }
        public string Year { get; set; }
        public bool includeAllStatuses { get; set; }
        public int PageIndex { get; set; }
        public int PageCount { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public bool chkShowDuplicate { get; set; }
        public string ddlDuplicateFilter { get; set; }
        public string sortOrder { get; set; }
        public string sortExpression { get; set; }
        public bool SearchByHomeBranch { get; set; }
        public int TotalCount { get; set; }
        public bool IsBranchTask { get; set; }
        public int TotalBranchTask { get; set; }
        public int QuestCount1 { get; set; }
        public int QuestCount2 { get; set; }
        public int QuestAnsCount1 { get; set; }
        public int IAgreedAns1 { get; set; }
        public int QuestAnsCount2 { get; set; }
        public int IAgreedAns2 { get; set; }
        public int AppPhase1TaskExist { get; set; }
        public int AppPhase2TaskExist { get; set; }
        public int Taskphase1PendingStatus { get; set; }
        public int Taskphase1CompletedStatus { get; set; }
        public int Taskphase2PendingStatus { get; set; }
        public int Taskphase2CompletedStatus { get; set; }
        public int Taskphase1VerifyStatus { get; set; }
        public int Taskphase2VerifyStatus { get; set; }
        public string F9Status { get; set; }
        public int ResultId { get; set; }
        public int CrimeStatus { get; set; }
        public int EVerify { get; set; }
        public int Completed { get; set; }
        public int ddlChangeStatus { get; set; }
        public string ShowMessage { get; set; }
        public string InterviewVenueId { get; set; }
        public string InterviewSession { get; set; }
        public string ddlReason { get; set; }
        public string RejectRemarks { get; set; }
        public int AppTaskId { get; set; }
        public string ParentBranch { get; set; }
        public string subject { get; set; }
        public string emailBody { get; set; }
        public string JobPosition { get; set; }
        public string JobPositionId { get; set; }
        public string JobPositionName { get; set; }
        public string Token { get; set; }
        public string JobType { get; set; }
        public string OfferType { get; set; }
        public string OfferNotes { get; set; }
        //public object SQLHelper { get; private set; }

        /// <summary>
        /// To insert data into Applicant table
        /// </summary>
        /// 

        public enum RehireSource
        {
            OfferedPopupAdmin = 1,
            EmailVerifyApplicant = 2,
            ReturningEmployeeApplicant = 3,
            HiredByCSCApplicant = 4

        }
        public int UpdateApplicantBgStatus(int applicantId, int StatusId)
        {
            ApplicantDAL _ApplicationDAL = new ApplicantDAL();
            return _ApplicationDAL.UpdateApplicantBgStatus(applicantId, StatusId);
        }


        public int ResetApplicant(int applicantId, int Type, int LoggedInUserId)
        {
            ApplicantDAL _ApplicationDAL = new ApplicantDAL();
            return _ApplicationDAL.ResetApplicant(applicantId, Type, LoggedInUserId);
        }
        public int SelectedResetApplicant(int applicantId, int Type, int LoggedInUserId, int ApplicantTaskId)
        {
            ApplicantDAL _ApplicationDAL = new ApplicantDAL();
            return _ApplicationDAL.SelectedResetApplicant(applicantId, Type, LoggedInUserId, ApplicantTaskId);
        }
        public int TestResetApplicantById(string applicantId)
        {
            ApplicantDAL _ApplicationDAL = new ApplicantDAL();
            return _ApplicationDAL.TestResetApplicantById(applicantId);
        }
        public int Save(Applicant obj)
        {
            ApplicantDAL _ApplicationDAL = new ApplicantDAL();
            return _ApplicationDAL.Register(obj);
        }
        public DataSet Login(Applicant obj)
        {
            ApplicantDAL _ApplicationDAL = new ApplicantDAL();
            return _ApplicationDAL.Login(obj);
        }

        public int PushToHireInitial(ApplicantDisclosure obj, JobApplication objJobApplication)
        {
            ApplicantDAL _ApplicationDAL = new ApplicantDAL();
            return _ApplicationDAL.PushToHireInitial(this, obj, objJobApplication);

        }
        public int UpdateSupportBgStatus(int UserID)
        {
            ApplicantDAL _ApplicationDAL = new ApplicantDAL();
            return _ApplicationDAL.UpdateSupportBgStatus(this, UserID);
        }
        public int PushToCue(JobApplication objJobApplication)
        {
            ApplicantDAL _ApplicationDAL = new ApplicantDAL();
            return _ApplicationDAL.PushToCue(this, objJobApplication);
        }

        public int UpdateFOrmW14()
        {
            ApplicantDAL _ApplicationDAL = new ApplicantDAL();
            return _ApplicationDAL.UpdateFOrmW14(this);
        }
        public int UpdateFromWT4(FormWT4 obj)
        {
            ApplicantDAL _ApplicationDAL = new ApplicantDAL();
            return _ApplicationDAL.UpdateFromWT4(obj);
        }
        public void ResetPassword()
        {
            ApplicantDAL _ApplicationDAL = new ApplicantDAL();
            _ApplicationDAL.ResetPassword(this);
        }

        public DataSet LoginApplicant(Applicant obj)
        {
            ApplicantDAL _ApplicationDAL = new ApplicantDAL();
            return _ApplicationDAL.LoginApplicant(obj);
        }


        public DataSet AdminValidateUser(Applicant obj)
        {
            ApplicantDAL _ApplicationDAL = new ApplicantDAL();
            return _ApplicationDAL.AdminValidateUser(obj);
        }

        public DataSet AuditLoginApplicant(Applicant obj)
        {
            ApplicantDAL _ApplicationDAL = new ApplicantDAL();
            return _ApplicationDAL.AuditLoginApplicant(obj);
        }
        public DataSet ReHireLoginApplicant(Applicant obj)
        {
            ApplicantDAL _ApplicationDAL = new ApplicantDAL();
            return _ApplicationDAL.ReHireLoginApplicant(obj);
        }
        public DataSet GetW4Form(Applicant obj)
        {
            ApplicantDAL _ApplicationDAL = new ApplicantDAL();
            return _ApplicationDAL.GetW4Form(obj);
        }

        public string DoVerificationByLink(string VerificationLink)
        {
            ApplicantDAL _ApplicationDAL = new ApplicantDAL();
            return _ApplicationDAL.DoVerificationByLink(VerificationLink);
        }
        public string GetVerificationCode(string EmailID)
        {
            ApplicantDAL _ApplicationDAL = new ApplicantDAL();
            return _ApplicationDAL.GetVerificationCode(EmailID);
        }

        public string VerifyEmail(string EmailID, string code, int VerifyBy, int UserType = 1)
        {
            ApplicantDAL _ApplicationDAL = new ApplicantDAL();
            return _ApplicationDAL.VerifyEmail(EmailID, code, VerifyBy, UserType);
        }

        public DataSet GetDocument(int ID)
        {
            ApplicantDAL _ApplicationDAL = new ApplicantDAL();
            return _ApplicationDAL.GetDocument(ID);
        }

        public int SaveApplicantDocument(int ApplicantId, string DocumentPath, int ApplicantTaskId, int CreatedBy)
        {
            ApplicantDAL _ApplicationDAL = new ApplicantDAL();
            return _ApplicationDAL.SaveApplicantDocument(ApplicantId, DocumentPath, ApplicantTaskId, CreatedBy);
        }


        public DataSet GetApplicantProgress(int ID)
        {
            ApplicantDAL _ApplicationDAL = new ApplicantDAL();
            return _ApplicationDAL.GetApplicantProgress(ID);
        }

        public DataSet GetApplicantByID(int ID)
        {
            ApplicantDAL _ApplicationDAL = new ApplicantDAL();
            return _ApplicationDAL.GetApplicantByID(ID);
        }
        public DataSet GetApplicantForApproveById(int ID)
        {
            ApplicantDAL _ApplicationDAL = new ApplicantDAL();
            return _ApplicationDAL.GetApplicantForApproveById(ID);
        }

        public DataSet GetApplicantForWT4ByID(int ID)
        {
            ApplicantDAL _ApplicationDAL = new ApplicantDAL();
            return _ApplicationDAL.GetApplicantForWT4ByID(ID);
        }
        public DataSet GetPwdByApplicantId(int ID)
        {
            ApplicantDAL _ApplicationDAL = new ApplicantDAL();
            return _ApplicationDAL.GetPwdByApplicantId(ID);
        }

        public DataSet GetApplicantValidateByID(int ID)
        {
            ApplicantDAL _ApplicationDAL = new ApplicantDAL();
            return _ApplicationDAL.GetApplicantValidateByID(ID);
        }



        public DataSet GetApplicantPassword(int ID)
        {
            string password = String.Empty;
            ApplicantDAL _ApplicantDAL = new ApplicantDAL();
            return _ApplicantDAL.GetApplicantPassword(ID);

        }
        public DataSet CheckInInterview(int JobApplicationID, int BranchId, int VenueID)
        {
            string password = String.Empty;
            ApplicantDAL _ApplicantDAL = new ApplicantDAL();
            return _ApplicantDAL.CheckInInterview(JobApplicationID, BranchId, VenueID);

        }

        public void UpdateSignatureImageAdmin(CanvasImage obj)
        {
            ApplicantDAL _ApplicationDAL = new ApplicantDAL();
            _ApplicationDAL.UpdateSignatureImageAdmin(obj);
        }



        public void UpdateSignatureImage(CanvasImage obj)
        {
            ApplicantDAL _ApplicationDAL = new ApplicantDAL();
            _ApplicationDAL.UpdateSignatureImage(obj);
        }

        public DataSet GetApplicantAnswer(int ID)
        {
            ApplicantDAL _ApplicationDAL = new ApplicantDAL();
            return _ApplicationDAL.GetApplicantAnswer(ID);
        }
        /// <summary>
        /// To Update data into Applicant table
        /// </summary>
        public DataSet UpdateProfile(Applicant obj)
        {
            ApplicantDAL _ApplicationDAL = new ApplicantDAL();
            return _ApplicationDAL.UpdateProfile(obj);
        }

        public int UpdateCompleteProfile()
        {
            ApplicantDAL _ApplicationDAL = new ApplicantDAL();
            return _ApplicationDAL.UpdateCompleteProfile(this);
        }

        public int UpdateSupportProfile(int UserID)
        {
            ApplicantDAL _ApplicationDAL = new ApplicantDAL();
            return _ApplicationDAL.UpdateSupportProfile(this, UserID);
        }


        public bool CheckSSN(int ApplicantId, String SSN, bool rehire = false)
        {
            ApplicantDAL _ApplicationDAL = new ApplicantDAL();
            return _ApplicationDAL.CheckSSN(ApplicantId, SSN, rehire);
        }

        public int ChangeProfilePic(Applicant obj)
        {
            ApplicantDAL _ApplicationDAL = new ApplicantDAL();
            return _ApplicationDAL.ChangeProfilePicture(obj);
        }
        /// <summary>
        /// To get all pending application requests
        /// </summary>
        public DataTable GetApplicant_Status(Applicant obj)
        {
            ApplicantDAL _ApplicantDAL = new ApplicantDAL();
            return _ApplicantDAL.GetApplicantByStatus(obj);
        }

        public DataTable fnGetApplicantForTaskDownload(Applicant obj)
        {
            ApplicantDAL _ApplicantDAL = new ApplicantDAL();
            return _ApplicantDAL.GetApplicantForTaskDownload(obj);
        }
        public int CountApplicationsForTaskDownload(Applicant obj)
        {
            ApplicantDAL _ApplicantDAL = new ApplicantDAL();
            return _ApplicantDAL.CountApplicationsForTaskDownload(obj);
        }
        // 28 -july -2017 kailash
        public DataTable GetApplicant_StatusAll(int statusId, int BranchId, int PageIndex, int PageCount, string IsAgree, bool includeAllStatuses, string FirstName = null, string LastName = null, string EmailID = null, string PrimaryPhone = null, string ZipCode = null, string AppliedOnStart = null, string AppliedOnEnd = null, string SortColumn = "", string Dir = "", string Duplicate = null, Boolean showDuplicate = false, string year = null, bool SearchByHomeBranch = false)
        {
            ApplicantDAL _ApplicantDAL = new ApplicantDAL();
            return _ApplicantDAL.GetApplicantByStatusAll(statusId, BranchId, PageIndex, PageCount, includeAllStatuses, IsAgree, FirstName, LastName, EmailID, PrimaryPhone, ZipCode, AppliedOnStart, AppliedOnEnd, SortColumn, Dir, Duplicate, showDuplicate, year, SearchByHomeBranch);
        }

        public DataTable GetApplicant_ForExport(int statusId, int BranchId, string IsAgree, string FirstName = null, string LastName = null, string EmailID = null, string ZipCode = null, string AppliedOnStart = null, string AppliedOnEnd = null, string PrimaryPhone = null, string Duplicate = null, Boolean showDuplicate = false, string year = null, bool includeAllStatuses = false, bool SearchByHomeBranch = false)
        {
            ApplicantDAL _ApplicantDAL = new ApplicantDAL();
            return _ApplicantDAL.GetApplicantForExport(statusId, BranchId, IsAgree, FirstName, LastName, EmailID, ZipCode, AppliedOnStart, AppliedOnEnd, PrimaryPhone, Duplicate, showDuplicate, year, includeAllStatuses, SearchByHomeBranch);
        }

        public int CountApplications(Applicant obj)
        {
            ApplicantDAL _ApplicantDAL = new ApplicantDAL();
            return _ApplicantDAL.CountApplications(obj);
        }

        public DataSet ApplicantStatusByApplicantId(Applicant obj)
        {
            ApplicantDAL _ApplicationDAL = new ApplicantDAL();
            return _ApplicationDAL.GetApplicantStatusByApplicantId(obj);
        }

        public int SaveApplicantAgree()
        {
            ApplicantDAL _AppDisclosure = new ApplicantDAL();
            return _AppDisclosure.UpdateApplicantIsAgree(this.ApplicantId, this.IsAgree, this.Initials);
        }

        public int saveDownloadPdf(int AuditUserId, int ApplicantId, string DocumentTitle)
        {
            ApplicantDAL _AppDisclosure = new ApplicantDAL();
            return _AppDisclosure.saveDownloadPdf(AuditUserId, ApplicantId, DocumentTitle);
        }

        public List<Applicant> GetRejectApplicant(int branchId, Nullable<int> status = null, string StartDate = null, string enddate = null, bool SearchByHomeBranch = false)
        {
            DataSet ds;
            ApplicantDAL _AppAgree = new ApplicantDAL();
            ds = _AppAgree.GetRejectedApplicant(branchId, status, StartDate, enddate, SearchByHomeBranch);
            List<Applicant> lstApplicant = SQLHelper.ContructList<Applicant>(ds);
            return lstApplicant;
        }

        public DataSet GetApplicantAllTask(int BranchId, string FirstName, string LastName, bool SearchByHomeBranch = false, string SSN = null, int StatusId = 0, string year = null)
        {
            DataSet ds;
            ApplicantDAL _AppAgree = new ApplicantDAL();
            ds = _AppAgree.GetApplicantAllTask(BranchId, FirstName, LastName, SearchByHomeBranch, SSN, StatusId, year);
            return ds;
        }
        public DataSet GetAuditApplicantAllTask(int BranchId, string FirstName, string LastName, bool SearchByHomeBranch = false, string SSN = null)
        {
            DataSet ds;
            ApplicantDAL _AppAgree = new ApplicantDAL();
            ds = _AppAgree.GetAuditApplicantAllTask(BranchId, FirstName, LastName, SearchByHomeBranch, SSN);
            return ds;
        }
        public DataSet GetHiredRehiredReport(string Year)
        {
            DataSet ds;
            ApplicantDAL _AppAgree = new ApplicantDAL();
            ds = _AppAgree.GetHiredRehiredReport(Year);
            return ds;
        }
        public List<Applicant> GetIsAgreeByJobApplicantId(int ApplicantId)
        {
            DataSet ds;
            ApplicantDAL _AppAgree = new ApplicantDAL();
            ds = _AppAgree.GetIsAgreeByJApplicantId(ApplicantId);
            List<Applicant> lstApplicant = SQLHelper.ContructList<Applicant>(ds);
            return lstApplicant;
        }
        public DataSet GetBGCheckByApplicantId(int ApplicantId)
        {
            DataSet ds;
            ApplicantDAL _AppAgree = new ApplicantDAL();
            ds = _AppAgree.GetBGCheckByApplicantId(ApplicantId);
            return ds;
        }
        public List<Applicant> UpdateApplicantInfo(Applicant obj)
        {
            DataSet ds;
            ApplicantDAL _ApplicationDAL = new ApplicantDAL();
            ds = _ApplicationDAL.UpdateApplicantInformation(obj);
            List<Applicant> lstApplicant = SQLHelper.ContructList<Applicant>(ds);
            foreach (Applicant objApp in lstApplicant)
            {
                objApp.DOB = objApp.DOB != null ? Convert.ToDateTime(objApp.DOB).ToString("MM/dd/yyyy") : null;
            }
            return lstApplicant;
        }
        public DataSet CheckSSNDuplicate(Applicant obj)
        {
            DataSet ds;
            ApplicantDAL _ApplicationDAL = new ApplicantDAL();
            ds = _ApplicationDAL.CheckSSNDuplicate(obj);
            return ds;
        }
        public int UpdateName(Applicant obj)
        {

            ApplicantDAL _ApplicationDAL = new ApplicantDAL();
            return _ApplicationDAL.UpdateName(obj);

        }
        public int UpdateCurrentPage(int ApplicantId, string CurrPage)
        {
            ApplicantDAL _ApplicationDAL = new ApplicantDAL();
            return _ApplicationDAL.UpdateCurrentPage(ApplicantId, CurrPage);
        }
        public Boolean CheckVerification(int ApplicantId)
        {
            ApplicantDAL _ApplicationDAL = new ApplicantDAL();
            return _ApplicationDAL.CheckVerification(ApplicantId);
        }
        public DataSet GetDuplicates(int branchId)
        {
            ApplicantDAL _ApplicationDAL = new ApplicantDAL();
            return _ApplicationDAL.GetDuplicates(branchId);
        }
        public int MarkPrinted(string appIds)
        {
            ApplicantDAL _ApplicationDAL = new ApplicantDAL();
            return _ApplicationDAL.MarkPrinted(appIds);
        }
        public DataTable GetLookUpBgStatus()
        {
            ApplicantDAL _ApplicationDAL = new ApplicantDAL();
            return _ApplicationDAL.GetLookUpBgStatus();
        }

        public DataTable GetLookUpApplicationStatus()
        {
            ApplicantDAL _ApplicationDAL = new ApplicantDAL();
            return _ApplicationDAL.GetLookUpApplicationStatus();
        }
        public DataSet GetSummary(int branchId, string Year, bool SearchByHomeBranch = false)
        {
            ApplicantDAL _ApplicationDAL = new ApplicantDAL();
            return _ApplicationDAL.GetSummary(branchId, Year, SearchByHomeBranch);
        }
        public DataSet ViewLog(int applicantId)
        {
            ApplicantDAL _ApplicationDAL = new ApplicantDAL();
            return _ApplicationDAL.ViewLog(applicantId);
        }

        public string ResetPassword(string email)
        {
            ApplicantDAL _ApplicationDAL = new ApplicantDAL();
            return _ApplicationDAL.ResetPassword(email);
        }
        public string fnReHiredEmployee(string email)
        {
            ApplicantDAL _ApplicationDAL = new ApplicantDAL();
            return _ApplicationDAL.fnReHiredEmployee(email);
        }
        public bool UpdateBGCheckOrderId(int applicantId, string orderId)
        {
            ApplicantDAL _ApplicationDAL = new ApplicantDAL();
            return _ApplicationDAL.UpdateBGCheckOrderId(applicantId, orderId) == 1;
        }

        public bool UpdateBGCheckStatus(int applicantId, int statusId, string message)
        {
            ApplicantDAL _ApplicationDAL = new ApplicantDAL();
            return _ApplicationDAL.UpdateBGCheckStatus(applicantId, statusId, message) == 1;
        }
        public bool TransferLocation(int applicantId, int destination, int UserId)
        {
            ApplicantDAL _ApplicationDAL = new ApplicantDAL();
            return _ApplicationDAL.TransferLocation(applicantId.ToString(), destination, UserId) > 0;
        }
        public bool TransferLocation(string applicantId, int destination, int UserId)
        {
            ApplicantDAL _ApplicationDAL = new ApplicantDAL();
            return _ApplicationDAL.TransferLocation(applicantId, destination, UserId) > 0;
        }

        public string UpdateWOTC(int ID)
        {
            ApplicantDAL _ApplicationDAL = new ApplicantDAL();
            return _ApplicationDAL.UpdateWOTC(ID);
        }

        public List<Applicant> GetApplicantInterviewVenue(int ApplicantId)
        {
            DataSet ds;
            ApplicantDAL _ApplicationDAL = new ApplicantDAL();
            ds = _ApplicationDAL.GetApplicantInterviewVenue(ApplicantId);
            List<Applicant> lstApplicant = SQLHelper.ContructList<Applicant>(ds);
            foreach (Applicant objApp in lstApplicant)
            {
                objApp.DOB = objApp.DOB != null ? Convert.ToDateTime(objApp.DOB).ToString("MM/dd/yyyy") : null;
            }
            return lstApplicant;
        }
        public List<Applicant> fnGetApplicationDetails(int ApplicantId)
        {
            DataSet ds;
            ApplicantDAL _ApplicationDAL = new ApplicantDAL();
            ds = _ApplicationDAL.fnGetApplicationDetails(ApplicantId);
            List<Applicant> lstApplicant = SQLHelper.ContructList<Applicant>(ds);
            foreach (Applicant objApp in lstApplicant)
            {
                objApp.DOB = objApp.DOB != null ? Convert.ToDateTime(objApp.DOB).ToString("MM/dd/yyyy") : null;
            }
            return lstApplicant;
        }
        public List<Applicant> GetApplicantZurichDetails(int ApplicantId)
        {
            DataSet ds;
            ApplicantDAL _ApplicationDAL = new ApplicantDAL();
            ds = _ApplicationDAL.GetApplicantZurichDetails(ApplicantId);
            List<Applicant> lstApplicant = SQLHelper.ContructList<Applicant>(ds);
            foreach (Applicant objApp in lstApplicant)
            {
                objApp.ZurichDate = objApp.ZurichDate != null ? Convert.ToDateTime(objApp.ZurichDate).ToString("MM/dd/yyyy") : null;
            }
            return lstApplicant;
        }
        public DataSet GetDataforVerification(int ID)
        {
            ApplicantDAL _ApplicationDAL = new ApplicantDAL();
            return _ApplicationDAL.GetDataforVerification(ID);
        }
        public int FormI9Verification_Ins(string queuepk, int ResultId, int RequestId, string ResultErrorCode, string STATUS,
            string CaseNumber, string InvoiceAmount, string B2BStatus, string B2BSubStatus, string ProcessStatus, string InvoicingStatus)
        {
            ApplicantDAL _ApplicationDAL = new ApplicantDAL();
            return _ApplicationDAL.FormI9Verification_Ins(queuepk, ResultId, RequestId, ResultErrorCode, STATUS,
            CaseNumber, InvoiceAmount, B2BStatus, B2BSubStatus, ProcessStatus, InvoicingStatus);
        }
        public DataSet fnGetESignature(int ApplicantId)
        {
            ApplicantDAL objUserDAL = new ApplicantDAL();
            return objUserDAL.GetESignature(ApplicantId);
        }

        public int SaveApplicantZurich(Applicant obj)
        {
            ApplicantDAL _ApplicationDAL = new ApplicantDAL();
            return _ApplicationDAL.SaveApplicantZurich(obj);
        }

        public DataSet fnGetBranch(int ID)
        {
            ApplicantDAL _ApplicationDAL = new ApplicantDAL();
            return _ApplicationDAL.GetBranch(ID);
        }
        public DataTable fnGetConfiguration(String Name)
        {
            ApplicantDAL _ApplicationDAL = new ApplicantDAL();
            return _ApplicationDAL.GetConfiguration(Name);
        }

        public int fnReHiredEmployeeBySSN(string SSN, int ApplicantId)
        {
            ApplicantDAL _ApplicationDAL = new ApplicantDAL();
            return _ApplicationDAL.fnReHiredEmployeeBySSN(SSN, ApplicantId);

        }

        public DataTable GetHiredRehiredReport(int UserId, DateTime StartDate, DateTime EndDate)
        {
            ApplicantDAL _ApplicationDAL = new ApplicantDAL();
            return _ApplicationDAL.GetHiredRehiredReport(UserId, StartDate, EndDate);
        }
        public DataTable GetApplicantRehireDetails(int ApplicantId)
        {
            ApplicantDAL _ApplicationDAL = new ApplicantDAL();
            return _ApplicationDAL.GetApplicantRehireDetails(ApplicantId);
        }
        public DataSet GetApplicantDocumentById(int ApplicantId, int ApplicantTaskId)
        {
            ApplicantDAL _ApplicationDAL = new ApplicantDAL();
            return _ApplicationDAL.GetApplicantDocumentById(ApplicantId, ApplicantTaskId);
        }
    }



    public class ApplicantDashboard
    {

        public int ApplicantId { get; set; }
        public int JobApplicationId { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string PrimaryPhone { get; set; }
        public string SecondaryPhone { get; set; }
        public string ZipCode { get; set; }
        public string ParentBranch { get; set; }
        public string BranchName { get; set; }
        public string LastName { get; set; }
        public string Reason { get; set; }
        public string DuplicateStatus { get; set; }
        public string Remarks { get; set; }

        public int BranchId { get; set; }
        public int Status { get; set; }

        public Boolean IsAgree { get; set; }
        public string EmailID { get; set; }
        public string SSN { get; set; }
        public string Address { get; set; }
        public String DOB { get; set; }
        public string ModifiedDate { get; set; }

        public int Completed { get; set; }
        public string status1 { get; set; }
        public string AppliedOn { get; set; }
        public int EmailCount { get; set; }
        public int CallCount { get; set; }
        public string InterviewDate { get; set; }

        public int NoShowInterview { get; set; }
        public int NoShowOrientation { get; set; }
        public int BGStatus { get; set; }
        public string City { get; set; }
        public string BGCheckID { get; set; }

        public string JDPLocationCode { get; set; }
        public string Rehire { get; set; }
        public string BGCheckMessage { get; set; }
        public string TaskStatus { get; set; }

        public bool IsBranchTask { get; set; }
        public int TotalBranchTask { get; set; }
        public int QuestCount1 { get; set; }
        public int QuestCount2 { get; set; }
        public int QuestAnsCount1 { get; set; }
        public int IAgreedAns1 { get; set; }
        public int QuestAnsCount2 { get; set; }
        public int IAgreedAns2 { get; set; }
        public int AppPhase1TaskExist { get; set; }
        public int AppPhase2TaskExist { get; set; }
        public int Taskphase1PendingStatus { get; set; }
        public int Taskphase1CompletedStatus { get; set; }
        public int Taskphase2PendingStatus { get; set; }
        public int Taskphase2CompletedStatus { get; set; }
        public int Taskphase1VerifyStatus { get; set; }
        public int Taskphase2VerifyStatus { get; set; }
        public string F9Status { get; set; }
        public int ResultId { get; set; }
        public int CrimeStatus { get; set; }
        public int EVerify { get; set; }
        public int TotalCount { get; set; }
        public string jobPositionId { get; set; }
        public string jobPositionName { get; set; }
    }

    //end

}