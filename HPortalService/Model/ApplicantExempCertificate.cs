﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

using HPortalService.DAL;

namespace HPortalService
{
    public class ApplicantExempCertificate
    {
        public int ApplicantId { get; set; }
        public int? NoOfResidence { get; set; }
        public string Address { get; set; }
        public string Zipcode { get; set; }
        public string PublicSchool { get; set; }
        public string SchoolDistNo { get; set; }
        public int? PersonalExemption { get; set; }
        public int? MarriedExemption { get; set; }
        public string DependentsExemption { get; set; }
        public string TotalExemption { get; set; }
        public string AdditionalWithholding { get; set; }
        public string SSN { get; set; }

        public int intApplicantExempCertificate(ApplicantExempCertificate obj)
        {
            ApplicantExempCertificateDAL _ApplicationDAL = new ApplicantExempCertificateDAL();
            return _ApplicationDAL.intApplicantExempCertificate(obj);
        }
        public DataSet GetApplicantExempCertificateByID(int ID)
        {
            ApplicantExempCertificateDAL _ApplicationDAL = new ApplicantExempCertificateDAL();
            return _ApplicationDAL.GetApplicantExempCertificateByID(ID);
        }
        public int intApplicantWithholdingCertificate(ApplicantExempCertificate obj)
        {
            ApplicantExempCertificateDAL _ApplicationDAL = new ApplicantExempCertificateDAL();
            return _ApplicationDAL.intApplicantWithholdingCertificate(obj);
        }
        public DataSet GetApplicantWithholdingCertificateByID(int ID)
        {
            ApplicantExempCertificateDAL _ApplicationDAL = new ApplicantExempCertificateDAL();
            return _ApplicationDAL.GetApplicantWithholdingCertificateByID(ID);
        }
    }
}