﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HPortalService.Model
{
    public class StatusSummary
    {
        public int Status { get; set; }
        public int Total { get; set; }
    }
}
