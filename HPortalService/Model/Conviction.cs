﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

using HPortalService.DAL;
namespace HPortalService
{
   public class Conviction
    {
       public int ConvictionID { get; set; }
       public string name { get; set; }
         public int Year { get; set; }
         public string Type { get; set; }
         public int DisplayOrder { get; set; }
         public int ApplicantId { get; set; }  
       
       
       public List<Conviction> GetConvictionByID()
        {
            ConvictionDAL objDAL=new ConvictionDAL();        
               List<Conviction> lstConvictions = objDAL.GetConvictionByID(this.ApplicantId);
            return lstConvictions;
        }

       public int SaveConviction()
        {
           ConvictionDAL objDAL=new ConvictionDAL(); 
            return objDAL.SaveConviction(this);
        }

        public int DeleteConviction()
        {
            ConvictionDAL objDAL = new ConvictionDAL();
            return objDAL.DeleteConviction(this.ApplicantId);
        }
    }

    

}
