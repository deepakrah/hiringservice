﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

using HPortalService.DAL;

namespace HPortalService
{
   public class AppArizonaWithholdingElection
    {

        public string ArizonaWithholdingElectionId { get; set; }
        public int ApplicantId { get; set; }
        public string SSN { get; set; }
        public string BranchId { get; set; }
        public string HomeAddress { get; set; }
        public string Street { get; set; }
        public string RuralRoute { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZIPCode { get; set; }
        public bool IsTaxableWages { get; set; }
        public string TaxableWagesPer { get; set; }
        public string ExtraAmount { get; set; }
        public bool IsElect { get; set; }
        public string SignatureImage { get; set; }

        public int intAppArizonaWithholdingElection(AppArizonaWithholdingElection obj)
        {
            AppArizonaWithholdingElectionDAL _ApplicationDAL = new AppArizonaWithholdingElectionDAL();
            return _ApplicationDAL.IntAppArizonaWithholdingElection(obj);
        }
        public DataSet GetAppArizonaWithholdingElectionByID(int ID)
        {
            AppArizonaWithholdingElectionDAL _ApplicationDAL = new AppArizonaWithholdingElectionDAL();
            return _ApplicationDAL.GetAppArizonaWithholdingElectionByID(ID);
        }
    }
}
