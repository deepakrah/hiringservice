<?xml version="1.0" encoding="utf-8"?>
<xsl:transform version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

        <style type="text/css">
          body,td,th {
          font-family: Verdana;
          font-size: 12px;
          }
        </style>
      </head>

      <body>
        <p>
          <center>
            <img src="https://hportal2.schedulingsite.com/assets/images/email-header.png" width="900px"/>
          </center>
          <br/>
          <br/>
        </p>
        <p>
          Dear  <xsl:value-of select="UserInfo/UserName" />
        </p>
        <p>
          Congratulations! On behalf of Contemporary Services Corporation, I am pleased to offer you the position of Event Staff 1.
        </p>
        <p>
          <br/>
        </p>
        <p>
          This is a conditional offer of employment contingent on your acceptance of this offer; your successful completion and submission of all hiring paperwork and procedures; your acknowledgement and consent to the company’s arbitration agreement and all other company policies; completion of a satisfactory background screening and verification of your eligibility of employment within the United States; your successful completion of all required training; and your successful completion of a 90-day probationary employment period. Your continued employment with Contemporary Services Corporation is at will and will require both satisfactory job performance and compliance with all existing and future company policies.
          <br/>
        </p>
        <p>
          If you have any questions, please call or e-mail Candace E Eikner 4042238157 or our Human Resources Manager at email11870@csc-usa.com. I look forward to working with you.
          Sincerely,
          <br/>

        </p>
        <p>
          <img  width="100" height="100" src="{UserInfo/ApplicationNumber}" alt="bar" >

          </img>

        </p>
        <p>Thank you</p>

        <p>
          Candace E Eikner<br/>
          Contemporary Services Corporation
        </p>
      </body>
    </html>
  </xsl:template>
</xsl:transform>
