<?xml version="1.0" encoding="utf-8"?>
<xsl:transform version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

        <style type="text/css">
          body,td,th {
          font-family: Verdana;
          font-size: 12px;
          }
        </style>
      </head>

      <body>
        <p>
          <center>
            <img src="https://hportal2.schedulingsite.com/assets/images/email-header.png" width="900px"/>
          </center>
          <br/>
          <br/>
        </p>
        <p>
          Dear  <xsl:value-of select="UserInfo/UserName" />
        </p>
        <p>
          We are pleased to inform you that after reviewing your profile you have been shortlisted for the interview.
          Your interview venue details is given below.
        </p>

        <p>
          Venue: <xsl:value-of select="UserInfo/VenueName" />

        </p>
        <p>
         Interview Date: <xsl:value-of select="UserInfo/InterviewDate" /> Time:  <xsl:value-of select="UserInfo/InterviewTime" />

        </p>
        <p>
          Address: <xsl:value-of select="UserInfo/VenueAddress" />

        </p>
        <p>
          Please carry a printed copy of this email at the venue. 
          </p>
        <p>
          <img  width="100" height="100" src="{UserInfo/ApplicationNumber}" alt="bar" />
        
        </p>
        <p>
        </p>


        <p>Thank you</p>

        <p>
          Candace E Eikner<br/>
          Contemporary Services Corporation
          
        </p>
        <br/>
        Location :   <xsl:value-of select="UserInfo/Branch" />

        <br/>
        <p>
          <xsl:if test="UserInfo/Sender = ''">
            <b>*Please do not respond to this email.Contact the branch you are applying for.</b>
          </xsl:if>

        </p>
      </body>
    </html>
  </xsl:template>
</xsl:transform>
