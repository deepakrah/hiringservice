﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:transform version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

        <style type="text/css">
          body,td,th {
          font-family: Verdana;
          font-size: 12px;
          }
        </style>
      </head>

      <body>
        <p>
          <center>
            <img src="https://hportal2.schedulingsite.com/assets/images/email-header.png" width="900px"/>
          </center>
          <br/>
          <br/>
        </p>
        <p>
          Dear Applicant,
        </p>
        <p>
          Thank you for beginning an application with Contemporary Services Corporation! Please use the verification code below to confirm your email address:

        </p>
        <p style="font-size:14px;font-weight:bold">
          verification code is: <xsl:value-of select="UserInfo/Password" />

        </p>
        <p>Sincerely,</p>
        <br/>
        <p>
          CSC Human Resources<br/>
          Contemporary Services Corporation
          <br/>
          Location :   <xsl:value-of select="UserInfo/Branch" />
        </p>
        <br/>
        <p>
          <b>*Please do not respond to this email. Contact the branch you are applying for.</b>
        </p>
      </body>
    </html>
  </xsl:template>
</xsl:transform>
