﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:transform version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

        <style type="text/css">
          body,td,th {
          font-family: Verdana;
          font-size: 12px;
          }
        </style>
      </head>

      <body>
        <p>
          <center>
            <img src="https://hportal2.schedulingsite.com/assets/images/email-header.png" width="900px"/>
          </center>
          <br/>
          <br/>
        </p>
        <p>
          Dear <xsl:value-of select="UserInfo/UserName" />,
        </p>
        <p>
          We appreciate your interest in working with Contemporary Services Corporation. At this time we are unable to move forward until your application has been completed. Click the link below to make any necessary edits to your application. Upon completion of your application the next step is to schedule an interview date.
        </p>
        <p>
          Your auto generated password is: <xsl:value-of select="UserInfo/Password" />
          <p>
            Please click the link below to proceed to your application:<br/>
            <a href="http://hportal.schedulingsite.com/index.html#/Login">http://hportal.schedulingsite.com/index.html#/SelectBranch</a>
          </p>
        </p>
        <p>
          Your interest in employment with Contemporary Services Corporation is appreciated. Best wishes in your employment search.
        </p>

        <p>Thank you</p>

        <p>
          Human Resources<br/>
          Contemporary Services Corporation

        </p>
        <br/>
        Location:<xsl:value-of select="UserInfo/Branch" />

        <br/>
        <p>
            <b>*Please do not respond to this email. Contact the branch you are applying for.</b>
     

        </p>
      </body>
    </html>
  </xsl:template>
</xsl:transform>
