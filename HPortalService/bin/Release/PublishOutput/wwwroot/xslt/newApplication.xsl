<?xml version="1.0" encoding="utf-8"?>
<xsl:transform version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

        <style type="text/css">
          body,td,th {
          font-family: Verdana;
          font-size: 12px;
          }
        </style>
      </head>

      <body>
        <p>
          <center>
            <img src="https://hportal2.schedulingsite.com/assets/images/email-header.png" width="900px"/>
          </center>
          <br/>
          <br/>
        </p>
        <p>
          Dear <xsl:value-of select="UserInfo/UserName" />,
        </p>
        <p>
      
          Thank you for submitting your application in response to our recruiting efforts for the position of Event staff.
          We anticipate that we will be back in touch once your application is processed.
        </p>
        <p>
          Your interest in employment with Contemporary Services Corporation is appreciated. Best wishes in your employment search.
        </p>

        <p>Thank you</p>

        <p>
          CSC Recruiting Staff
         
        </p>
        <br/>
        Location :   <xsl:value-of select="UserInfo/Branch" />

        <br/>
        <p>
          <xsl:if test="UserInfo/Sender = ''">
            <b>*Please do not respond to this email.Contact the branch you are applying for.</b>
          </xsl:if>

        </p>
      </body>
    </html>
  </xsl:template>
</xsl:transform>
