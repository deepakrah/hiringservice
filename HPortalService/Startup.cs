﻿using HPortalService.Helpers;
using HPortalService.Service;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System.IO;
using Microsoft.Extensions.FileProviders;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using Microsoft.AspNetCore.Mvc.Formatters;
using static HPortalService.Controllers.ProductsController;

namespace HPortalService
{
    public class Startup
    {
        public IConfigurationRoot Configuration
        {
            get;
            set;
        }
        public static string ConnectionString
        {
            get;
            private set;
        }
        public static string ServerPath
        {
            get;
            private set;
        }


        public static string UsesmtpSSL
        {
            get;
            private set;
        }
        public static string EnableSsl
        {
            get;
            private set;
        }
        public static string enableMail
        {
            get;
            private set;
        }
        public static string mailServer
        {
            get;
            private set;
        }
        public static string userId
        {
            get;
            private set;
        }
        public static string password
        {
            get;
            private set;
        }

        public static string authenticate
        {
            get;
            private set;
        }

        public static string AdminEmailID
        {
            get;
            private set;
        }

        public static string fromEmailID
        {
            get;
            private set;
        }

        public static string DomainName
        {
            get;
            private set;
        }

        public static string AllowSendMails
        {
            get;
            private set;
        }

        public static string UserName
        {
            get;
            private set;
        }
        public static string AlliancePartnerID
        {
            get;
            private set;
        }
        public static string AlliancePartnerLogin
        {
            get;
            private set;
        }
        public static string AlliancePartnerPassword
        {
            get;
            private set;
        }
        public static string BGCheckUserIdValue
        {
            get;
            private set;
        }
        public static string BGCheckPwdValue
        {
            get;
            private set;
        }
        public static string BGPackageValue
        {
            get;
            private set;
        }
        public static string BGCrimePackageValue
        {
            get;
            private set;
        }
        public static string HPortalResponseValue
        {
            get;
            private set;
        }
        public static string TempFolderPath
        {
            get;
            private set;
        }
        public static string ReturnBatchPrintPath
        {
            get;
            private set;
        }
        public static string BatchPrintPath
        {
            get;
            private set;
        }
        public static string ReportTemplate
        {
            get;
            private set;
        }

        public static string ReportSavePath
        {
            get;
            private set;
        }
        public static string WebSiteURL
        {
            get;
            private set;
        }

        public Startup(IHostingEnvironment env)
        {
            Configuration = new ConfigurationBuilder().SetBasePath(env.ContentRootPath).AddJsonFile("appSettings.json").Build();
        }
        //public Startup(IConfiguration configuration)
        //{
        //    Configuration = configuration;

        //}

        //public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors();
            services.AddSingleton<IFileProvider>(
                new PhysicalFileProvider(
                    Path.Combine(Directory.GetCurrentDirectory(), "wwwroot")));
            services.AddMvc(config =>
            {
                config.InputFormatters.Insert(0, new XDocumentInputFormatter());
            }).AddXmlDataContractSerializerFormatters()
                .AddXmlSerializerFormatters();


            var appSettingsSection = Configuration.GetSection("AppSettings");
            services.Configure<AppSettings>(appSettingsSection);
            var appSettings = appSettingsSection.Get<AppSettings>();
            var key = Encoding.ASCII.GetBytes(appSettings.Secret);
            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(x =>
            {
                x.RequireHttpsMetadata = false;
                x.SaveToken = true;
                x.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false
                };
            });
            services.AddScoped<IAuthorizeService, AuthorizeService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {

            // Expose the members of the 'Microsoft.AspNetCore.Http' namespace 
            // at the top of the file:
            // using Microsoft.AspNetCore.Http;
            app.UseStatusCodePages(async context =>
            {
                context.HttpContext.Response.ContentType = "text/plain";

                await context.HttpContext.Response.WriteAsync(
                    "Status code page, status code: " +
                    context.HttpContext.Response.StatusCode);
            });



            app.UseStaticFiles(); // For the wwwroot folder

            app.UseStaticFiles(new StaticFileOptions
            {
                FileProvider = new Microsoft.Extensions.FileProviders.PhysicalFileProvider(
                    Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "xslt")),
                RequestPath = "/xslt"
            });

            app.UseDirectoryBrowser(new DirectoryBrowserOptions
            {
                FileProvider = new Microsoft.Extensions.FileProviders.PhysicalFileProvider(
                    Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "xslt")),
                RequestPath = "/xslt"
            });

            //app.UseDirectoryBrowser(new DirectoryBrowserOptions
            //{
            //    FileProvider = new Microsoft.Extensions.FileProviders.PhysicalFileProvider(
            //       Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "ApplicantForm")),
            //    RequestPath = "/ApplicantForm"
            //});
            app.UseFileServer(new FileServerOptions()
            {
                FileProvider = new Microsoft.Extensions.FileProviders.PhysicalFileProvider(
                   @"P:\WSRApplicantForm"),
                RequestPath = "/ApplicantForm",
                EnableDirectoryBrowsing = true
            });
            app.UseDirectoryBrowser(new DirectoryBrowserOptions
            {
                FileProvider = new Microsoft.Extensions.FileProviders.PhysicalFileProvider(
                   Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "imagedoc/ProfileImages")),
                RequestPath = "/imagedoc/ProfileImages"
            });


            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseCors(builder => builder
             .AllowAnyOrigin()
             .AllowAnyMethod()
             .AllowAnyHeader()
             .AllowCredentials()
             );
            app.UseAuthentication();
            //app.UseMvc();
            app.UseMvc(routes =>
            {
                routes.MapRoute("default", "{controller}/{action}/{id?}");
            });
            ConnectionString = Configuration["ConnectionStrings:DefaultConnection"];
            ServerPath = Configuration["EmailSetting:ServerPath"];

            UsesmtpSSL = Configuration["EmailSetting:UsesmtpSSL"];
            EnableSsl = Configuration["EmailSetting:EnableSsl"];
            enableMail = Configuration["EmailSetting:enableMail"];
            mailServer = Configuration["EmailSetting:mailServer"];
            userId = Configuration["EmailSetting:userId"];
            password = Configuration["EmailSetting:password"];
            authenticate = Configuration["EmailSetting:authenticate"];
            AdminEmailID = Configuration["EmailSetting:AdminEmailID"];
            fromEmailID = Configuration["EmailSetting:fromEmailID"];
            DomainName = Configuration["EmailSetting:DomainName"];
            AllowSendMails = Configuration["EmailSetting:AllowSendMails"];
            UserName = Configuration["EmailSetting:UserName"];
            AlliancePartnerID = Configuration["EmailSetting:AlliancePartnerID"];
            AlliancePartnerLogin = Configuration["EmailSetting:AlliancePartnerLogin"];
            AlliancePartnerPassword = Configuration["EmailSetting:AlliancePartnerPassword"];
            BGCheckUserIdValue = Configuration["EmailSetting:BGCheckUserIdValue"];
            BGCheckPwdValue = Configuration["EmailSetting:BGCheckPwdValue"];
            BGPackageValue = Configuration["EmailSetting:BGPackageValue"];
            BGCrimePackageValue = Configuration["EmailSetting:BGCrimePackageValue"];
            HPortalResponseValue = Configuration["EmailSetting:HPortalResponseValue"];
            TempFolderPath = Configuration["EmailSetting:TempFolderPath"];
            ReturnBatchPrintPath = Configuration["EmailSetting:ReturnBatchPrintPath"];
            BatchPrintPath = Configuration["EmailSetting:BatchPrintPath"];
            ReportTemplate = Configuration["EmailSetting:ReportTemplate"];
            ReportSavePath = Configuration["EmailSetting:ReportSavePath"];
            WebSiteURL = Configuration["EmailSetting:WebSiteURL"];
        }
    }
}
