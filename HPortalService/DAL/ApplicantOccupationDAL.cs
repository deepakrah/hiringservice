﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;


namespace HPortalService.DAL
{
    public class ApplicantOccupationDAL
    {
        public int SaveApplicanOccupation(ApplicantOccupation obj)
        {
            SQL objSql = new SQL();
            object objRowAffected;
            objSql.AddParameter("@JobApplicationId", DbType.Int32, ParameterDirection.Input, 0, obj.JobApplicationId);
            objSql.AddParameter("@Employer", DbType.String, ParameterDirection.Input, 0, obj.Employer);
            objSql.AddParameter("@PossitionTitle", DbType.String, ParameterDirection.Input, 0, obj.PossitionTitle);
            objSql.AddParameter("@City", DbType.String, ParameterDirection.Input, 0, obj.City);
            objSql.AddParameter("@StateId", DbType.Int32, ParameterDirection.Input, 0, obj.StateId);
            objSql.AddParameter("@StartMonth", DbType.String, ParameterDirection.Input, 0, obj.StartMonth);
            objSql.AddParameter("@StartYear", DbType.Int32, ParameterDirection.Input, 0, obj.StartYear);
            objSql.AddParameter("@EndMonth", DbType.String, ParameterDirection.Input, 0, obj.EndMonth);
            objSql.AddParameter("@EndYear", DbType.Int32, ParameterDirection.Input, 0, obj.EndYear);
            objSql.AddParameter("@SupervisorName", DbType.String, ParameterDirection.Input, 0, obj.SupervisorName);
            objSql.AddParameter("@SupervisorPhone", DbType.String, ParameterDirection.Input, 0, obj.SupervisorPhone);
            objSql.AddParameter("@EndingPay", DbType.String, ParameterDirection.Input, 0, obj.EndingPay);
            objSql.AddParameter("@ContactReference", DbType.Boolean, ParameterDirection.Input, 0, obj.ContactReference);
            objSql.AddParameter("@JobDuties", DbType.String, ParameterDirection.Input, 0, obj.JobDuties);
            objSql.AddParameter("@ReasonLeaving", DbType.String, ParameterDirection.Input, 0, obj.ReasonLeaving);
            objRowAffected = objSql.ExecuteNonQuery("p_ApplicantOccupation_Ins");
            return Convert.ToInt32(objRowAffected);
        }
        public int UpdApplicantOccupation(ApplicantOccupation obj)
        {
            SQL objSql = new SQL();
            object objRowAffected;
            objSql.AddParameter("@OccupationId", DbType.Int32, ParameterDirection.Input, 0, obj.OccupationId);
            objSql.AddParameter("@JobApplicationId", DbType.Int32, ParameterDirection.Input, 0, obj.JobApplicationId);
            objSql.AddParameter("@Employer", DbType.String, ParameterDirection.Input, 0, obj.Employer);
            objSql.AddParameter("@PossitionTitle", DbType.String, ParameterDirection.Input, 0, obj.PossitionTitle);
            objSql.AddParameter("@City", DbType.String, ParameterDirection.Input, 0, obj.City);
            objSql.AddParameter("@StateId", DbType.Int32, ParameterDirection.Input, 0, obj.StateId);
            objSql.AddParameter("@StartMonth", DbType.String, ParameterDirection.Input, 0, obj.StartMonth);
            objSql.AddParameter("@StartYear", DbType.Int32, ParameterDirection.Input, 0, obj.StartYear);
            objSql.AddParameter("@EndMonth", DbType.String, ParameterDirection.Input, 0, obj.EndMonth);
            objSql.AddParameter("@EndYear", DbType.Int32, ParameterDirection.Input, 0, obj.EndYear);
            objSql.AddParameter("@SupervisorName", DbType.String, ParameterDirection.Input, 0, obj.SupervisorName);
            objSql.AddParameter("@SupervisorPhone", DbType.String, ParameterDirection.Input, 0, obj.SupervisorPhone);
            objSql.AddParameter("@EndingPay", DbType.String, ParameterDirection.Input, 0, obj.EndingPay);
            objSql.AddParameter("@ContactReference", DbType.Boolean, ParameterDirection.Input, 0, obj.ContactReference);
            objSql.AddParameter("@JobDuties", DbType.String, ParameterDirection.Input, 0, obj.JobDuties);
            objSql.AddParameter("@ReasonLeaving", DbType.String, ParameterDirection.Input, 0, obj.ReasonLeaving);
            objRowAffected = objSql.ExecuteNonQuery("p_ApplicantOccupation_Upd");
            return Convert.ToInt32(objRowAffected);
        }



        public DataSet GetOccupationByJobApplicantId(int JobApplicationId)
        {
            SQL objSql = new SQL();
            DataSet ds;
            objSql.AddParameter("@JobApplicationId", DbType.Int32, ParameterDirection.Input, 0, JobApplicationId);
            ds = objSql.ExecuteDataSet("p_ApplicantOccupation_Get");
            return ds;
        }


        public int DeleteApplicanOccupationById(int OccupationId)
        {
            SQL objSql = new SQL();
            object objRowAffected;
            objSql.AddParameter("@OccupationId", DbType.Int32, ParameterDirection.Input, 0, OccupationId);
            objRowAffected = objSql.ExecuteNonQuery("p_ApplicantOccupation_Del");
            return Convert.ToInt32(objRowAffected);
        }
    }
}
