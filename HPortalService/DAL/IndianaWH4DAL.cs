﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;


namespace HPortalService.DAL
{
    class IndianaWH4DAL
    {
        public int IntIndianaWH4(IndianaWH4 obj)
        {

            SQL objSql = new SQL();
            object objContentId;
            int albumcontentId = 0;
            objSql.AddParameter("@ApplicantId", DbType.Int32, ParameterDirection.Input, 0, obj.ApplicantId);          
            objSql.AddParameter("@CountyofResidence", DbType.String, ParameterDirection.Input, 0, obj.CountyofResidence);
            objSql.AddParameter("@PrincipalEmployment", DbType.String, ParameterDirection.Input, 0, obj.PrincipalEmployment);
            if (!string.IsNullOrEmpty(obj.Exemption))
            objSql.AddParameter("@Exemption", DbType.Int32, ParameterDirection.Input, 0, obj.Exemption);
            if (!string.IsNullOrEmpty(obj.SpouseExemption))
            objSql.AddParameter("@SpouseExemption", DbType.Int32, ParameterDirection.Input, 0, obj.SpouseExemption);
            if (!string.IsNullOrEmpty(obj.DependentExemption))
            objSql.AddParameter("@DependentExemption", DbType.Int32, ParameterDirection.Input, 0, obj.DependentExemption);
            if(!string.IsNullOrEmpty(obj.AdditionalExemptions))
            objSql.AddParameter("@AdditionalExemptions", DbType.String, ParameterDirection.Input, 0, obj.AdditionalExemptions);
            objSql.AddParameter("@Numberofboxes", DbType.Int32, ParameterDirection.Input, 0, obj.Numberofboxes);
            if (!string.IsNullOrEmpty(obj.total))
            objSql.AddParameter("@total", DbType.Decimal, ParameterDirection.Input, 0, obj.total);
            if (!string.IsNullOrEmpty(obj.QualifyingDependent))
            objSql.AddParameter("@QualifyingDependent", DbType.Decimal, ParameterDirection.Input, 0, obj.QualifyingDependent);
            if (!string.IsNullOrEmpty(obj.AdditionalState))
            objSql.AddParameter("@AdditionalState", DbType.Decimal, ParameterDirection.Input, 0, obj.AdditionalState);
            if (!string.IsNullOrEmpty(obj.AdditionalCounty))
            objSql.AddParameter("@AdditionalCounty", DbType.Decimal, ParameterDirection.Input, 0, obj.AdditionalCounty);
            objContentId = objSql.ExecuteScalar("p_ApplicantIndianaWH4_ins");
            return albumcontentId = Convert.ToInt32(objContentId);
        }

        public DataSet GetIndianaWH4ByID(int ApplicantId)
        {
            SQL objSql = new SQL();
            DataSet ds;
            objSql.AddParameter("@ApplicantId", DbType.Int32, ParameterDirection.Input, 0, ApplicantId);
            ds = objSql.ExecuteDataSet("p_GetApplicantIndianaWH4ByID");
            return ds;
        }
    }
}
