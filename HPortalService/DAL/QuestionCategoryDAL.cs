﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;


namespace HPortalService.DAL
{
    class QuestionCategoryDAL
    {
        public DataSet GetQuestionCategories()
        {
            SQL objSql = new SQL();
            DataSet ds;
            ds = objSql.ExecuteDataSet("p_LookupQuestionCategory_Get");
            return ds;
        }

    }
}
