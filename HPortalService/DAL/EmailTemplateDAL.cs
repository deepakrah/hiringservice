﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace HPortalService.DAL
{
   public class EmailTemplateDAL
    {
        public int Save(EmailTemplate objET)
        {
            SQL objSql = new SQL();
            objSql.AddParameter("@EmailType", DbType.String, ParameterDirection.Input, 0, objET.EmailType);
            objSql.AddParameter("@BranchId", DbType.Int32, ParameterDirection.Input, 0, objET.BranchId);
            objSql.AddParameter("@Template", DbType.String, ParameterDirection.Input, 0, objET.Template);

            return Convert.ToInt32(objSql.ExecuteNonQuery("p_EmailTemplate_Ins"));

        }


        public DataSet Get(EmailTemplate objET)
        {
            SQL objSql = new SQL();
            DataSet ds = new DataSet();
            objSql.AddParameter("@EmailType", DbType.String, ParameterDirection.Input, 0, objET.EmailType);
            objSql.AddParameter("@BranchId", DbType.String, ParameterDirection.Input, 0, objET.BranchId);
            ds = objSql.ExecuteDataSet("p_EmailTemplateGet");
            return ds;

        }
    }
}
