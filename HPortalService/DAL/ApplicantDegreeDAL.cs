﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;


namespace HPortalService.DAL
{
    public class ApplicantDegreeDAL
    {
        public int UpdateApplicantDegree(ApplicantDegree obj)
        {
            SQL objSql = new SQL();
            object objRowAffected;
            objSql.AddParameter("@JobApplicationId", DbType.Int32, ParameterDirection.Input, 0, obj.JobApplicationId);
            objSql.AddParameter("@HighestEducation", DbType.String, ParameterDirection.Input, 0, obj.HighestEducation);
            objSql.AddParameter("@EducationSchool", DbType.String, ParameterDirection.Input, 0, obj.EducationSchool);
            objSql.AddParameter("@EducationStateId", DbType.Int32, ParameterDirection.Input, 0, obj.EducationStateId);
            objSql.AddParameter("@HighSchoolName", DbType.String, ParameterDirection.Input, 0, obj.HighSchoolName);
            objSql.AddParameter("@Location", DbType.String, ParameterDirection.Input, 0, obj.Location);
            objSql.AddParameter("@GradMonth", DbType.String, ParameterDirection.Input, 0, obj.GradMonth);
            objSql.AddParameter("@GradeYear", DbType.Int32, ParameterDirection.Input, 0, obj.GradeYear);
            objSql.AddParameter("@OtherState", DbType.String, ParameterDirection.Input, 0, obj.OtherState);
            objSql.AddParameter("@DegreeCompleted", DbType.Boolean, ParameterDirection.Input, 0, obj.DegreeCompleted);
            if (!string.IsNullOrEmpty(obj.FileName))
            {
                objSql.AddParameter("@FileName", DbType.String, ParameterDirection.Input, 0, obj.FileName);
            }
            objRowAffected = objSql.ExecuteScalar("p_ApplicantDegree_Ins_test");
            return Convert.ToInt32(objRowAffected);
        }
        public DataSet GetDegreeByJobApplicantId(int JobApplicationId)
        {
            SQL objSql = new SQL();
            DataSet ds;
            objSql.AddParameter("@JobApplicationId", DbType.Int32, ParameterDirection.Input, 0, JobApplicationId);
            ds = objSql.ExecuteDataSet("p_ApplicantDegree_Get");
            return ds;
        }

    }
}
