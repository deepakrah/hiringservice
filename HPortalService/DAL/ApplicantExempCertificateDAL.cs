﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace HPortalService.DAL
{
    public class ApplicantExempCertificateDAL
    {
        public int intApplicantExempCertificate(ApplicantExempCertificate obj)
        {
            SQL objSql = new SQL();
            object objContentId;
            int albumcontentId = 0;
            objSql.AddParameter("@ApplicantId", DbType.Int32, ParameterDirection.Input, 0, obj.ApplicantId);
            objSql.AddParameter("@Address", DbType.String, ParameterDirection.Input, 0, obj.Address);
            objSql.AddParameter("@Zipcode", DbType.String, ParameterDirection.Input, 0, obj.Zipcode);
            objSql.AddParameter("@PublicSchool", DbType.String, ParameterDirection.Input, 0, obj.PublicSchool);
            objSql.AddParameter("@SchoolDistNo", DbType.String, ParameterDirection.Input, 0, obj.SchoolDistNo);
            objSql.AddParameter("@PersonalExemption", DbType.Int32, ParameterDirection.Input, 0, obj.PersonalExemption);
            objSql.AddParameter("@MarriedExemption", DbType.Int32, ParameterDirection.Input, 0, obj.MarriedExemption);
            objSql.AddParameter("@DependentsExemption", DbType.Decimal, ParameterDirection.Input, 0, obj.DependentsExemption);
            objSql.AddParameter("@TotalExemption", DbType.Decimal, ParameterDirection.Input, 0, obj.TotalExemption);
            objSql.AddParameter("@AdditionalWithholding", DbType.Decimal, ParameterDirection.Input, 0, obj.AdditionalWithholding);
            objContentId = objSql.ExecuteScalar("p_ApplicantExempCertificate_ins");
            return albumcontentId = Convert.ToInt32(objContentId);
        }

        public int intApplicantWithholdingCertificate(ApplicantExempCertificate obj)
        {
            SQL objSql = new SQL();
            object objContentId;
            int albumcontentId = 0;
            objSql.AddParameter("@ApplicantId", DbType.Int32, ParameterDirection.Input, 0, obj.ApplicantId);
            objSql.AddParameter("@NoOfResidence", DbType.Int32, ParameterDirection.Input, 0, obj.NoOfResidence);
            objContentId = objSql.ExecuteScalar("p_ApplicantWithholdingCertificate_ins");
            return albumcontentId = Convert.ToInt32(objContentId);
        }

        public DataSet GetApplicantExempCertificateByID(int ID)
        {
            SQL objSql = new SQL();
            DataSet ds;
            objSql.AddParameter("@ApplicantId", DbType.Int32, ParameterDirection.Input, 0, ID);
            ds = objSql.ExecuteDataSet("p_GetApplicantExempCertificateByID");
            return ds;
        }

        public DataSet GetApplicantWithholdingCertificateByID(int ID)
        {
            SQL objSql = new SQL();
            DataSet ds;
            objSql.AddParameter("@ApplicantId", DbType.Int32, ParameterDirection.Input, 0, ID);
            ds = objSql.ExecuteDataSet("p_GetApplicantWithholdingCertificateByID");
            return ds;
        }
    }
}
