﻿using HPortalService.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;


namespace HPortalService.DAL
{
    public class ApplicantDAL
    {



        public int Register(Applicant obj)
        {
            try
            {
                SQL objSql = new SQL();
                object objContentId;
                int albumcontentId = 0;

                objSql.AddParameter("@FirstName", DbType.String, ParameterDirection.Input, 0, obj.FirstName);
                objSql.AddParameter("@LastName", DbType.String, ParameterDirection.Input, 0, obj.LastName);
                objSql.AddParameter("@EmailId", DbType.String, ParameterDirection.Input, 0, obj.EmailID);

                if (!string.IsNullOrEmpty(obj.ZipCode))
                    objSql.AddParameter("@ZipCode", DbType.String, ParameterDirection.Input, 0, obj.ZipCode);

                objSql.AddParameter("@AuthenticationType", DbType.String, ParameterDirection.Input, 0, obj.AuthenticationType);
                objSql.AddParameter("@Password", DbType.String, ParameterDirection.Input, 0, obj.Password);
                objSql.AddParameter("@HowDidYouHear", DbType.String, ParameterDirection.Input, 0, obj.HowDidYouHear);
                objSql.AddParameter("@RefferedBy", DbType.String, ParameterDirection.Input, 0, obj.RefferedBy);
                objSql.AddParameter("@UserType", DbType.Int32, ParameterDirection.Input, 0, obj.UserType);
                objSql.AddParameter("@BranchId", DbType.Int32, ParameterDirection.Input, 0, obj.BranchId);

                objSql.AddParameter("@OrganizationName", DbType.String, ParameterDirection.Input, 0, obj.OrganizationName);
                objSql.AddParameter("@CSCIDNumber", DbType.String, ParameterDirection.Input, 0, obj.CSCIDNumber);
                objSql.AddParameter("@EmployeeLastName", DbType.String, ParameterDirection.Input, 0, obj.EmployeeLastName);
                objSql.AddParameter("@EmployeeFirstName", DbType.String, ParameterDirection.Input, 0, obj.EmployeeFirstName);
                objSql.AddParameter("@EmployeeIDNumber", DbType.String, ParameterDirection.Input, 0, obj.EmployeeIDNumber);
                objSql.AddParameter("@BranchSourceId", DbType.String, ParameterDirection.Input, 0, obj.BranchSourceId);

                objSql.AddParameter("@isCSCReferral", DbType.Boolean, ParameterDirection.Input, 0, obj.isCSCReferral);
                objSql.AddParameter("@isOrganizationReferral", DbType.Boolean, ParameterDirection.Input, 0, obj.isOrganizationReferral);
                objSql.AddParameter("@CSCReferralName", DbType.String, ParameterDirection.Input, 0, obj.CSCReferralName);
                objSql.AddParameter("@OrganizationReferralName", DbType.String, ParameterDirection.Input, 0, obj.OrganizationReferralName);
                if (obj.Rehire)
                {
                    objSql.AddParameter("@RehireSource", DbType.Int32, ParameterDirection.Input, 0, Convert.ToInt32(Applicant.RehireSource.EmailVerifyApplicant));
                    objSql.AddParameter("@Rehire", DbType.String, ParameterDirection.Input, 0, obj.Rehire);
                    objContentId = objSql.ExecuteScalar("p_Applicant_InsReHire");
                }
                else if (obj.ReEmployee)
                {
                    objSql.AddParameter("@RehireSource", DbType.Int32, ParameterDirection.Input, 0, Convert.ToInt32(Applicant.RehireSource.ReturningEmployeeApplicant));
                    objSql.AddParameter("@Rehire", DbType.String, ParameterDirection.Input, 0, obj.ReEmployee);
                    objContentId = objSql.ExecuteScalar("p_Applicant_Ins_test");
                }
                else
                {
                    objContentId = objSql.ExecuteScalar("p_Applicant_Ins_test");
                }
                albumcontentId = Convert.ToInt32(objContentId);
                if (albumcontentId > 0)
                {
                    SQL objSql1 = new SQL();
                    objSql1.AddParameter("@ApplicantId", DbType.Int32, ParameterDirection.Input, 0, albumcontentId);
                    objSql1.AddParameter("@JobPositionId", DbType.String, ParameterDirection.Input, 0, obj.JobPosition);
                    objContentId = objSql1.ExecuteNonQuery("p_InsJobApplicationAndPosition");
                }
                return albumcontentId;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ResetPassword(Applicant obj)
        {
            SQL objSql = new SQL();
            objSql.AddParameter("@ApplicantId", DbType.String, ParameterDirection.Input, 0, obj.ApplicantId);
            objSql.ExecuteDataSet("p_ResetPassword");

        }
        public int ResetApplicant(int ApplicantId, int Type, int LoggedInUserId)
        {
            SQL objSql = new SQL();
            objSql.AddParameter("@ApplicantId", DbType.Int32, ParameterDirection.Input, 0, ApplicantId);
            objSql.AddParameter("@Type", DbType.Int32, ParameterDirection.Input, 0, Type);
            objSql.AddParameter("@UserId", DbType.Int32, ParameterDirection.Input, 0, LoggedInUserId);
            return objSql.ExecuteNonQuery("p_ResetApplicant");
        }
        public int UpdateApplicantBgStatus(int ApplicantId, int StatusId)
        {
            SQL objSql = new SQL();
            objSql.AddParameter("@ApplicantId", DbType.Int32, ParameterDirection.Input, 0, ApplicantId);
            objSql.AddParameter("@StatusId", DbType.Int32, ParameterDirection.Input, 0, StatusId);
            return objSql.ExecuteNonQuery("p_updateApplicantBgStatus");
        }
        public int SelectedResetApplicant(int ApplicantId, int Type, int LoggedInUserId, int ApplicantTaskId)
        {
            SQL objSql = new SQL();
            objSql.AddParameter("@ApplicantId", DbType.Int32, ParameterDirection.Input, 0, ApplicantId);
            objSql.AddParameter("@Type", DbType.Int32, ParameterDirection.Input, 0, Type);
            objSql.AddParameter("@UserId", DbType.Int32, ParameterDirection.Input, 0, LoggedInUserId);
            objSql.AddParameter("@ApplicantTaskId", DbType.Int32, ParameterDirection.Input, 0, ApplicantTaskId);
            return objSql.ExecuteNonQuery("p_ResetApplicantById");

        }

        public int TestResetApplicantById(string ApplicantId)
        {
            SQL objSql = new SQL();
            objSql.AddParameter("@ApplicantId", DbType.String, ParameterDirection.Input, 0, ApplicantId);
            return objSql.ExecuteNonQuery("p_TestResetApplicantById");
        }
        public void UpdateSignatureImageAdmin(CanvasImage obj)
        {
            SQL objSql = new SQL();
            objSql.AddParameter("@UserId", DbType.String, ParameterDirection.Input, 0, obj.UserId);
            objSql.AddParameter("@SignatureImage", DbType.String, ParameterDirection.Input, 0, obj.ImageName);
            objSql.AddParameter("@FirstName", DbType.String, ParameterDirection.Input, 0, obj.FirstName);
            objSql.AddParameter("@LastName", DbType.String, ParameterDirection.Input, 0, obj.LastName);
            objSql.ExecuteDataSet("p_UpdateSignatureImageAdmin");
        }




        public void UpdateSignatureImage(CanvasImage obj)
        {
            SQL objSql = new SQL();
            objSql.AddParameter("@ApplicantId", DbType.String, ParameterDirection.Input, 0, obj.ApplicantId);
            objSql.AddParameter("@SignatureImage", DbType.String, ParameterDirection.Input, 0, obj.ImageName);
            objSql.AddParameter("@FirstName", DbType.String, ParameterDirection.Input, 0, obj.FirstName);
            objSql.AddParameter("@LastName", DbType.String, ParameterDirection.Input, 0, obj.LastName);
            objSql.ExecuteDataSet("p_UpdateSignatureImage");
        }

        public string ResetPassword(string email)
        {
            SQL objSql = new SQL();
            objSql.AddParameter("@email", DbType.String, ParameterDirection.Input, 0, email);
            return Convert.ToString(objSql.ExecuteScalar("p_forgotPwd"));

        }
        public string fnReHiredEmployee(string email)
        {
            SQL objSql = new SQL();
            objSql.AddParameter("@email", DbType.String, ParameterDirection.Input, 0, email);
            return Convert.ToString(objSql.ExecuteScalar("p_ReHiredEmployee"));

        }
        public int fnReHiredEmployeeBySSN(string SSN, int ApplicantId)
        {
            SQL objSql = new SQL();
            objSql.AddParameter("@ApplicantId", DbType.String, ParameterDirection.Input, 0, ApplicantId);
            objSql.AddParameter("@SSN", DbType.String, ParameterDirection.Input, 0, SSN);
            return objSql.ExecuteNonQuery("p_ReHiredEmployeeBySSN");

        }
        public string GetVerificationCode(string EmailID)
        {
            SQL objSql = new SQL();
            objSql.AddParameter("@EmailID", DbType.String, ParameterDirection.Input, 0, EmailID);
            string str = objSql.ExecuteScalar("p_EmailVerification_Insert").ToString();
            return str;
        }

        public string DoVerificationByLink(string VerificationLink)
        {
            SQL objSql = new SQL();
            objSql.AddParameter("@VerificationLink", DbType.String, ParameterDirection.Input, 0, VerificationLink);
            string str = objSql.ExecuteScalar("p_VerifyEmailByLink").ToString();
            return str;
        }


        public string VerifyEmail(string EmailID, string code, int VerifyBy, int UserType = 1)
        {
            SQL objSql = new SQL();
            objSql.AddParameter("@EmailID", DbType.String, ParameterDirection.Input, 0, EmailID);
            objSql.AddParameter("@VerificationCode", DbType.String, ParameterDirection.Input, 0, code);
            objSql.AddParameter("@VerifyBy", DbType.String, ParameterDirection.Input, 0, VerifyBy);
            objSql.AddParameter("@UserType", DbType.String, ParameterDirection.Input, 0, UserType);

            string str = objSql.ExecuteScalar("p_verifyEmail").ToString();
            return str;
        }

        public Boolean CheckVerification(int ApplicantId)
        {
            SQL objSql = new SQL();
            objSql.AddParameter("@ApplicantId", DbType.String, ParameterDirection.Input, 0, ApplicantId);
            return Convert.ToBoolean(objSql.ExecuteScalar("p_CheckProfileVerification"));

        }

        public DataSet GetW4Form(Applicant obj)
        {
            SQL objSql = new SQL();
            DataSet ds = new DataSet();
            objSql.AddParameter("@ApplicantId", DbType.Int32, ParameterDirection.Input, 0, obj.ApplicantId);
            ds = objSql.ExecuteDataSet("p_GetW4FormByApplicantId");
            return ds;
        }


        public DataSet LoginApplicant(Applicant obj)
        {
            SQL objSql = new SQL();
            DataSet ds = new DataSet();
            objSql.AddParameter("@EmailID", DbType.String, ParameterDirection.Input, 0, obj.EmailID);
            objSql.AddParameter("@Password", DbType.String, ParameterDirection.Input, 0, obj.Password);
            objSql.AddParameter("@UserType", DbType.Int32, ParameterDirection.Input, 0, obj.UserType);
            ds = objSql.ExecuteDataSet("p_LoginNew");
            return ds;
        }


        public DataSet AdminValidateUser(Applicant obj)
        {
            SQL objSql = new SQL();
            DataSet ds = new DataSet();
            objSql.AddParameter("@EmailID", DbType.String, ParameterDirection.Input, 0, obj.EmailID);
            objSql.AddParameter("@Password", DbType.String, ParameterDirection.Input, 0, obj.Password);
            objSql.AddParameter("@UserType", DbType.Int32, ParameterDirection.Input, 0, obj.UserType);
            ds = objSql.ExecuteDataSet("p_AdminLogin");
            return ds;
        }

        public DataSet AuditLoginApplicant(Applicant obj)
        {
            SQL objSql = new SQL();
            DataSet ds = new DataSet();
            objSql.AddParameter("@EmailID", DbType.String, ParameterDirection.Input, 0, obj.EmailID);
            objSql.AddParameter("@Password", DbType.String, ParameterDirection.Input, 0, obj.Password);
            objSql.AddParameter("@UserType", DbType.Int32, ParameterDirection.Input, 0, obj.UserType);
            ds = objSql.ExecuteDataSet("p_AuditLoginNew");
            return ds;
        }
        public DataSet ReHireLoginApplicant(Applicant obj)
        {
            SQL objSql = new SQL();
            DataSet ds = new DataSet();
            objSql.AddParameter("@EmailID", DbType.String, ParameterDirection.Input, 0, obj.EmailID);
            objSql.AddParameter("@Password", DbType.String, ParameterDirection.Input, 0, obj.Password);
            objSql.AddParameter("@UserType", DbType.Int32, ParameterDirection.Input, 0, obj.UserType);
            ds = objSql.ExecuteDataSet("p_ReHireLoginNew");
            return ds;

        }

        public DataSet GetApplicantProfile(int ApplicantId)
        {
            SQL objSql = new SQL();
            DataSet ds = new DataSet();
            objSql.AddParameter("@ApplicantId", DbType.Int32, ParameterDirection.Input, 0, ApplicantId);
            ds = objSql.ExecuteDataSet("p_GetApplicantProfileById");
            return ds;


        }



        public DataSet CheckSSNDuplicate(Applicant obj)
        {
            SQL objSql = new SQL();
            DataSet ds = new DataSet();
            objSql.AddParameter("@FirstName", DbType.String, ParameterDirection.Input, 0, obj.FirstName);
            objSql.AddParameter("@LastName", DbType.String, ParameterDirection.Input, 0, obj.LastName);
            objSql.AddParameter("@SSN", DbType.String, ParameterDirection.Input, 0, obj.SSN);
            ds = objSql.ExecuteDataSet("p_checkSSNDuplicate");
            return ds;
        }

        //public DataSet ReHireLoginApplicant(Applicant obj)
        //{
        //    SQL objSql = new SQL();
        //    DataSet ds = new DataSet();
        //    objSql.AddParameter("@EmailID", DbType.String, ParameterDirection.Input, 0, obj.EmailID);
        //    objSql.AddParameter("@Password", DbType.String, ParameterDirection.Input, 0, obj.Password);
        //    objSql.AddParameter("@UserType", DbType.Int32, ParameterDirection.Input, 0, obj.UserType);
        //    ds = objSql.ExecuteDataSet("p_ReHireLoginNew");
        //    return ds;

        //}
        public int UpdateFOrmW14(Applicant obj)
        {
            SQL objSql = new SQL();
            DataSet ds = new DataSet();
            objSql.AddParameter("@ApplicantId", DbType.String, ParameterDirection.Input, 0, obj.ApplicantId);
            objSql.AddParameter("@FirstName", DbType.String, ParameterDirection.Input, 0, obj.FirstName);
            objSql.AddParameter("@MiddleName", DbType.String, ParameterDirection.Input, 0, obj.MiddleName);
            objSql.AddParameter("@LastName", DbType.String, ParameterDirection.Input, 0, obj.LastName);
            objSql.AddParameter("@SSN", DbType.String, ParameterDirection.Input, 0, obj.SSN);
            objSql.AddParameter("@ZipCode", DbType.Int32, ParameterDirection.Input, 0, obj.ZipCode);
            objSql.AddParameter("@City", DbType.String, ParameterDirection.Input, 0, obj.City);
            objSql.AddParameter("@StateId", DbType.Int32, ParameterDirection.Input, 0, obj.StateId);
            objSql.AddParameter("@CountryId", DbType.Int32, ParameterDirection.Input, 0, obj.CountryId);
            objSql.AddParameter("@NoOfAllowances", DbType.Int32, ParameterDirection.Input, 0, obj.NoOfAllowances);
            objSql.AddParameter("@AdditionalAmount", DbType.Decimal, ParameterDirection.Input, 0, obj.WAdditionalAmount);
            objSql.AddParameter("@FedTaxStatus", DbType.String, ParameterDirection.Input, 0, obj.FedTaxStatus);
            objSql.AddParameter("@NameDiffer", DbType.Boolean, ParameterDirection.Input, 0, obj.NameDiffer);
            objSql.AddParameter("@Address1", DbType.String, ParameterDirection.Input, 0, obj.Address1);
            objSql.AddParameter("@Address2", DbType.String, ParameterDirection.Input, 0, obj.Address2);
            objSql.AddParameter("@Exempt", DbType.String, ParameterDirection.Input, 0, obj.Exempt);
            return objSql.ExecuteNonQuery("p_UpdateFOrmW14_test");

        }

        public int UpdateFromWT4(FormWT4 obj)
        {
            SQL objSql = new SQL();
            DataSet ds = new DataSet();
            objSql.AddParameter("@ApplicantId", DbType.Int32, ParameterDirection.Input, 0, obj.ApplicantId);         
            objSql.AddParameter("@HireDate", DbType.String, ParameterDirection.Input, 0, obj.HireDate);
            objSql.AddParameter("@Address", DbType.String, ParameterDirection.Input, 0, obj.Address);
            objSql.AddParameter("@City", DbType.String, ParameterDirection.Input, 0, obj.City);
            objSql.AddParameter("@State", DbType.String, ParameterDirection.Input, 0, obj.State);
            objSql.AddParameter("@ZipCode", DbType.Int32, ParameterDirection.Input, 0, obj.ZipCode);
            objSql.AddParameter("@FedTaxStatus", DbType.Int32, ParameterDirection.Input, 0, obj.FedTaxStatus);
            objSql.AddParameter("@ExemptSelf", DbType.Int32, ParameterDirection.Input, 0, obj.ExemptSelf);
            objSql.AddParameter("@ExemptSpouse", DbType.Int32, ParameterDirection.Input, 0, obj.ExemptSpouse);
            objSql.AddParameter("@ExemptDependent", DbType.Int32, ParameterDirection.Input, 0, obj.ExemptDependent);
            objSql.AddParameter("@TotalExempt", DbType.Int32, ParameterDirection.Input, 0, obj.TotalExempt);
            objSql.AddParameter("@AdditionalAmount", DbType.Decimal, ParameterDirection.Input, 0, obj.AdditionalAmount);
            objSql.AddParameter("@Exempt", DbType.String, ParameterDirection.Input, 0, obj.Exempt);
            return objSql.ExecuteNonQuery("p_UpdateFormWT4");

        }
        

        public DataSet Login(Applicant obj)
        {
            SQL objSql = new SQL();
            DataSet objContentId;
            objSql.AddParameter("@LoginName", DbType.String, ParameterDirection.Input, 0, obj.EmailID);
            objSql.AddParameter("@Password", DbType.String, ParameterDirection.Input, 0, obj.Password);
            objSql.AddParameter("@UserType", DbType.Int32, ParameterDirection.Input, 0, obj.UserType);
            objContentId = objSql.ExecuteDataSet("p_Login");

            return objContentId;
        }


        public DataSet GetDocument(int ID)
        {
            SQL objSql = new SQL();
            DataSet ds;
            objSql.AddParameter("@ApplicantId", DbType.Int32, ParameterDirection.Input, 0, ID);
            ds = objSql.ExecuteDataSet("p_GetDocument");
            return ds;
        }

        public int SaveApplicantDocument(int ApplicantId, string DocumentPath, int ApplicantTaskId, int CreatedBy)
        {
            SQL objSql = new SQL();
            
            objSql.AddParameter("@ApplicantId", DbType.Int32, ParameterDirection.Input, 0, ApplicantId);
            objSql.AddParameter("@DocumentPath", DbType.String, ParameterDirection.Input, 0, DocumentPath);

            objSql.AddParameter("@ApplicantTaskId ", DbType.Int32, ParameterDirection.Input, 0, ApplicantTaskId);
            objSql.AddParameter("@CreatedBy", DbType.Int32, ParameterDirection.Input, 0, @CreatedBy);

            //ds = objSql.ExecuteNonQuery("p_ApplicantDocument_Ins");
            return objSql.ExecuteNonQuery("p_ApplicantDocument_Ins");
        }


        public DataSet GetApplicantProgress(int ID)
        {
            SQL objSql = new SQL();
            DataSet ds;
            objSql.AddParameter("@ApplicantId", DbType.String, ParameterDirection.Input, 0, ID);
            ds = objSql.ExecuteDataSet("p_GetApplicantProgress");
            return ds;
        }

        public DataSet GetApplicantByID(int ID)
        {
            SQL objSql = new SQL();
            DataSet ds;
            objSql.AddParameter("@ApplicantId", DbType.String, ParameterDirection.Input, 0, ID);
            ds = objSql.ExecuteDataSet("p_GetApplicantByID");
            return ds;
        }
        public DataSet GetApplicantForApproveById(int ID)
        {
            SQL objSql = new SQL();
            DataSet ds;
            objSql.AddParameter("@ApplicantId", DbType.String, ParameterDirection.Input, 0, ID);
            ds = objSql.ExecuteDataSet("p_GetApplicantForApproveById");
            return ds;
        }
        public DataSet GetApplicantForWT4ByID(int ID)
        {
            SQL objSql = new SQL();
            DataSet ds;
            objSql.AddParameter("@ApplicantId", DbType.String, ParameterDirection.Input, 0, ID);
            ds = objSql.ExecuteDataSet("p_GetApplicantForWT4ByID");
            return ds;
        }
        public DataSet GetApplicantValidateByID(int ID)
        {
            SQL objSql = new SQL();
            DataSet ds;
            objSql.AddParameter("@ApplicantId", DbType.String, ParameterDirection.Input, 0, ID);
            ds = objSql.ExecuteDataSet("p_GetApplicantValidateById");
            return ds;
        }

        public DataSet GetPwdByApplicantId(int ID)
        {
            SQL objSql = new SQL();
            DataSet ds;
            objSql.AddParameter("@ApplicantId", DbType.String, ParameterDirection.Input, 0, ID);
            ds = objSql.ExecuteDataSet("p_GetPwdByApplicantId");
            return ds;
        }
        public string UpdateWOTC(int ID)
        {
            SQL objSql = new SQL();
            objSql.AddParameter("@ApplicantId", DbType.String, ParameterDirection.Input, 0, ID);
            return Convert.ToString(objSql.ExecuteNonQuery("p_updateWOTCTask"));
        }

        public DataSet GetApplicantPassword(int ID)
        {
            SQL objSql = new SQL();
            DataSet ds;
            objSql.AddParameter("@ApplicantId", DbType.String, ParameterDirection.Input, 0, ID);
            ds = objSql.ExecuteDataSet("p_Applicant_FetchPassword");
            return ds;

        }

        public DataSet GetApplicantAnswer(int ID)
        {
            SQL objSql = new SQL();
            DataSet ds;
            objSql.AddParameter("@ApplicantId", DbType.String, ParameterDirection.Input, 0, ID);
            ds = objSql.ExecuteDataSet("p_GetApplicantAnswer");
            return ds;
        }

        public DataSet GetApplicantBGCheckStatus(int ID)
        {
            SQL objSql = new SQL();
            DataSet ds;
            objSql.AddParameter("@ApplicantId", DbType.String, ParameterDirection.Input, 0, ID);
            ds = objSql.ExecuteDataSet("p_GetApplicantBGCheckStatus");
            return ds;
        }
        public DataSet GetApplicantI9from(int ID)
        {
            SQL objSql = new SQL();
            DataSet ds;
            objSql.AddParameter("@ApplicantId", DbType.String, ParameterDirection.Input, 0, ID);
            ds = objSql.ExecuteDataSet("p_GetApplicantFormI9Status");
            return ds;
        }
        public DataSet GetRejectedApplicant(int branchId, Nullable<int> status = null, string StartDate = null, string enddate = null, bool SearchByHomeBranch = false)
        {
            SQL objSql = new SQL();
            DataSet objRowAffected;
            objSql.AddParameter("@Status", DbType.Int32, ParameterDirection.Input, 0, status);
            objSql.AddParameter("@StartDate", DbType.String, ParameterDirection.Input, 0, StartDate);
            objSql.AddParameter("@EndDate", DbType.String, ParameterDirection.Input, 0, enddate);
            objSql.AddParameter("@BranchId", DbType.Int32, ParameterDirection.Input, 0, branchId);
            if (SearchByHomeBranch)
                objRowAffected = objSql.ExecuteDataSet("p_GetRejectedReport");
            else
                objRowAffected = objSql.ExecuteDataSet("p_GetRejectedReportByChildBranch");
            return objRowAffected;
        }

        public DataSet GetApplicantAllTask(int BranchId, string FirstName, string LastName, bool SearchByHomeBranch, string SSN, int StatusId, string year)
        {
            SQL objSql = new SQL();
            DataSet objRowAffected;
            if (!string.IsNullOrEmpty(FirstName))
                objSql.AddParameter("@FirstName", DbType.String, ParameterDirection.Input, 0, FirstName);
            if (!string.IsNullOrEmpty(LastName))
                objSql.AddParameter("@LastName", DbType.String, ParameterDirection.Input, 0, LastName);
            if (!string.IsNullOrEmpty(SSN))
                objSql.AddParameter("@SSN", DbType.String, ParameterDirection.Input, 0, SSN);
            if (StatusId > 0)
                objSql.AddParameter("@StatusId", DbType.String, ParameterDirection.Input, 0, StatusId);
            objSql.AddParameter("@BranchId", DbType.Int32, ParameterDirection.Input, 0, BranchId);
            if (SearchByHomeBranch)
                objSql.AddParameter("@SearchByHomeBranch", DbType.Boolean, ParameterDirection.Input, 0, SearchByHomeBranch);
            objSql.AddParameter("@year", DbType.String, ParameterDirection.Input, 0, year);
            objRowAffected = objSql.ExecuteDataSet("p_GetApplicantAllTask");

            return objRowAffected;
        }

        public DataSet GetAuditApplicantAllTask(int BranchId, string FirstName, string LastName, bool SearchByHomeBranch, string SSN)
        {
            SQL objSql = new SQL();
            DataSet objRowAffected;
            if (!string.IsNullOrEmpty(FirstName))
                objSql.AddParameter("@FirstName", DbType.String, ParameterDirection.Input, 0, FirstName);
            if (!string.IsNullOrEmpty(LastName))
                objSql.AddParameter("@LastName", DbType.String, ParameterDirection.Input, 0, LastName);
            if (!string.IsNullOrEmpty(SSN))
                objSql.AddParameter("@SSN", DbType.String, ParameterDirection.Input, 0, SSN);
            objSql.AddParameter("@BranchId", DbType.Int32, ParameterDirection.Input, 0, BranchId);
            if (SearchByHomeBranch)
                objSql.AddParameter("@SearchByHomeBranch", DbType.Boolean, ParameterDirection.Input, 0, SearchByHomeBranch);
            objRowAffected = objSql.ExecuteDataSet("p_GetAuditApplicantAllTask");
            return objRowAffected;
        }
        public DataSet GetHiredRehiredReport(string Year)
        {
            SQL objSql = new SQL();
            DataSet objRowAffected;
            objSql.AddParameter("@Year", DbType.String, ParameterDirection.Input, 0, Year);
            objRowAffected = objSql.ExecuteDataSet("p_GetHiredReHireReport");
            return objRowAffected;
        }
        public DataSet UpdateProfile(Applicant obj)
        {
            SQL objSql = new SQL();
            DataSet objRowAffected;

            objSql.AddParameter("@ApplicantId", DbType.Int32, ParameterDirection.Input, 0, obj.ApplicantId);
            objSql.AddParameter("@FirstName", DbType.String, ParameterDirection.Input, 0, obj.FirstName);
            objSql.AddParameter("@MiddleName", DbType.String, ParameterDirection.Input, 0, obj.MiddleName);
            objSql.AddParameter("@LastName", DbType.String, ParameterDirection.Input, 0, obj.LastName);
            objSql.AddParameter("@EmailId", DbType.String, ParameterDirection.Input, 0, obj.EmailID);
            objSql.AddParameter("@ZipCode", DbType.String, ParameterDirection.Input, 0, obj.ZipCode);
            objSql.AddParameter("@Address", DbType.String, ParameterDirection.Input, 0, obj.Address);
            objSql.AddParameter("@City", DbType.String, ParameterDirection.Input, 0, obj.City);
            objSql.AddParameter("@StateId", DbType.Int32, ParameterDirection.Input, 0, obj.StateId);
            objSql.AddParameter("@CountryId", DbType.Int32, ParameterDirection.Input, 0, obj.CountryId);
            objSql.AddParameter("@PrimaryPhone", DbType.String, ParameterDirection.Input, 0, obj.PrimaryPhone);
            objSql.AddParameter("@SecondaryPhone", DbType.String, ParameterDirection.Input, 0, obj.SecondaryPhone);
            objSql.AddParameter("@Unit", DbType.String, ParameterDirection.Input, 0, obj.Unit);
            objRowAffected = objSql.ExecuteDataSet("p_Applicant_Upd");
            return objRowAffected;
        }


        public int UpdateCompleteProfile(Applicant obj)
        {
            try
            {
                SQL objSql = new SQL();
                int objRowAffected;

                objSql.AddParameter("@FirstName", DbType.String, ParameterDirection.Input, 0, obj.FirstName);
                objSql.AddParameter("@MiddleName", DbType.String, ParameterDirection.Input, 0, obj.MiddleName);
                objSql.AddParameter("@LastName", DbType.String, ParameterDirection.Input, 0, obj.LastName);
                objSql.AddParameter("@HowDidYouHear", DbType.String, ParameterDirection.Input, 0, obj.HowDidYouHear);
                objSql.AddParameter("@RefferedBy", DbType.String, ParameterDirection.Input, 0, obj.RefferedBy);
                objSql.AddParameter("@OrganizationName", DbType.String, ParameterDirection.Input, 0, obj.OrganizationName);
                objSql.AddParameter("@CSCIDNumber", DbType.String, ParameterDirection.Input, 0, obj.CSCIDNumber);
                objSql.AddParameter("@EmployeeLastName", DbType.String, ParameterDirection.Input, 0, obj.EmployeeLastName);
                objSql.AddParameter("@EmployeeFirstName", DbType.String, ParameterDirection.Input, 0, obj.EmployeeFirstName);
                objSql.AddParameter("@EmployeeIDNumber", DbType.String, ParameterDirection.Input, 0, obj.EmployeeIDNumber);
                objSql.AddParameter("@ApplicantId", DbType.Int32, ParameterDirection.Input, 0, obj.ApplicantId);
                objSql.AddParameter("@ZipCode", DbType.String, ParameterDirection.Input, 0, obj.ZipCode);
                objSql.AddParameter("@Address", DbType.String, ParameterDirection.Input, 0, obj.Address);
                objSql.AddParameter("@City", DbType.String, ParameterDirection.Input, 0, obj.City);
                objSql.AddParameter("@StateId", DbType.Int32, ParameterDirection.Input, 0, obj.StateId);
                objSql.AddParameter("@CountryId", DbType.Int32, ParameterDirection.Input, 0, obj.CountryId);
                objSql.AddParameter("@PrimaryPhone", DbType.String, ParameterDirection.Input, 0, obj.PrimaryPhone);
                objSql.AddParameter("@SecondaryPhone", DbType.String, ParameterDirection.Input, 0, obj.SecondaryPhone);
                objSql.AddParameter("@DOB", DbType.DateTime, ParameterDirection.Input, 0, obj.DOB != "" ? obj.DOB : null);
                objSql.AddParameter("@SSN", DbType.String, ParameterDirection.Input, 0, !String.IsNullOrEmpty(obj.SSN) ? obj.SSN : null);
                objSql.AddParameter("@TaxStatus", DbType.String, ParameterDirection.Input, 0, obj.TaxStatus);
                objSql.AddParameter("@NoOfDependency", DbType.Int32, ParameterDirection.Input, 0, obj.NoOfDependency);
                objSql.AddParameter("@AdditionalAmount", DbType.Decimal, ParameterDirection.Input, 0, obj.AdditionalAmount);
                objRowAffected = objSql.ExecuteNonQuery("p_Applicant_Up");
                return objRowAffected;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        public int UpdateSupportProfile(Applicant obj, int UserID)
        {
            SQL objSql = new SQL();
            int objRowAffected;

            objSql.AddParameter("@FirstName", DbType.String, ParameterDirection.Input, 0, obj.FirstName);
            objSql.AddParameter("@MiddleName", DbType.String, ParameterDirection.Input, 0, obj.MiddleName);
            objSql.AddParameter("@LastName", DbType.String, ParameterDirection.Input, 0, obj.LastName);
            objSql.AddParameter("@ApplicantId", DbType.Int32, ParameterDirection.Input, 0, obj.ApplicantId);
            objSql.AddParameter("@ZipCode", DbType.String, ParameterDirection.Input, 0, obj.ZipCode);
            objSql.AddParameter("@Address", DbType.String, ParameterDirection.Input, 0, obj.Address);
            objSql.AddParameter("@City", DbType.String, ParameterDirection.Input, 0, obj.City);
            objSql.AddParameter("@StateId", DbType.Int32, ParameterDirection.Input, 0, obj.StateId);
            objSql.AddParameter("@CountryId", DbType.Int32, ParameterDirection.Input, 0, obj.CountryId);
            objSql.AddParameter("@PrimaryPhone", DbType.String, ParameterDirection.Input, 0, obj.PrimaryPhone);
            objSql.AddParameter("@SecondaryPhone", DbType.String, ParameterDirection.Input, 0, obj.SecondaryPhone);
            objSql.AddParameter("@SSN", DbType.String, ParameterDirection.Input, 0, obj.SSN);
            objSql.AddParameter("@Status", DbType.String, ParameterDirection.Input, 0, obj.Status);
            objSql.AddParameter("@UserID", DbType.String, ParameterDirection.Input, 0, UserID);
            objRowAffected = objSql.ExecuteNonQuery("p_Applicant_Support_Up");
            return objRowAffected;
        }

        public int UpdateSupportBgStatus(Applicant obj, int UserID)
        {
            SQL objSql = new SQL();
            int objRowAffected;

            objSql.AddParameter("@BGStatus", DbType.String, ParameterDirection.Input, 0, obj.BGStatus);
            objSql.AddParameter("@BGCHECHID", DbType.String, ParameterDirection.Input, 0, obj.BGCheckID);
            objSql.AddParameter("@Message", DbType.String, ParameterDirection.Input, 0, obj.BGCheckMessage);
            objSql.AddParameter("@ApplicantId", DbType.Int32, ParameterDirection.Input, 0, obj.ApplicantId);
            objSql.AddParameter("@UserID", DbType.String, ParameterDirection.Input, 0, UserID);
            objRowAffected = objSql.ExecuteNonQuery("p_Applicant_Support_UpBG");
            return objRowAffected;
        }

        public DataTable GetLookUpBgStatus()
        {
            DataSet ds;
            SQL objSql = new SQL();
            ds = objSql.ExecuteDataSet("p_LookupBGStatus_Select");
            return ds.Tables[0];
        }

        public DataTable GetLookUpApplicationStatus()
        {
            DataSet ds;
            SQL objSql = new SQL();
            ds = objSql.ExecuteDataSet("p_LookupApplicantStatus_Select");
            return ds.Tables[0];
        }




        public int ChangeProfilePicture(Applicant obj)
        {
            SQL objSql = new SQL();
            object objRowAffected;
            objSql.AddParameter("@ApplicantId", DbType.String, ParameterDirection.Input, 0, obj.ApplicantId);
            objSql.AddParameter("@PicturePath", DbType.String, ParameterDirection.Input, 0, obj.PicturePath);
            objRowAffected = objSql.ExecuteNonQuery("p_ApplicantImage_Upd");
            return Convert.ToInt32(objRowAffected);
        }
        public int UpdateName(Applicant obj)
        {
            SQL objSql = new SQL();
            object objRowAffected;
            objSql.AddParameter("@ApplicantId", DbType.String, ParameterDirection.Input, 0, obj.ApplicantId);
            objSql.AddParameter("@FirstName", DbType.String, ParameterDirection.Input, 0, obj.FirstName);
            objSql.AddParameter("@LastName", DbType.String, ParameterDirection.Input, 0, obj.LastName);
            objSql.AddParameter("@MiddleName", DbType.String, ParameterDirection.Input, 0, obj.MiddleName);
            objRowAffected = objSql.ExecuteNonQuery("p_UpdateApplicantName");
            return Convert.ToInt32(objRowAffected);
        }

        public int UpdateCurrentPage(int ApplicantId, String CurrPage)
        {
            SQL objSql = new SQL();
            object objRowAffected;
            objSql.AddParameter("@ApplicantId", DbType.Int32, ParameterDirection.Input, 0, ApplicantId);
            objSql.AddParameter("@CurrPage", DbType.String, ParameterDirection.Input, 0, CurrPage);
            objRowAffected = objSql.ExecuteNonQuery("p_ApplicantCurrPage_Upd");
            return Convert.ToInt32(objRowAffected);
        }




        public bool CheckSSN(int ApplicantId, String SSN, bool rehire = false)
        {
            SQL objSql = new SQL();
            DataSet objRowAffected;
            objSql.AddParameter("@ApplicantId", DbType.Int32, ParameterDirection.Input, 0, ApplicantId);
            objSql.AddParameter("@SSN", DbType.String, ParameterDirection.Input, 0, SSN);
            objRowAffected = objSql.ExecuteDataSet("p_CheckSSN");
            if (objRowAffected.Tables.Count > 0 && objRowAffected.Tables[0].Rows.Count > 0)
            {
                //if applicant is rehire and already exists in WISH3.0 then don't check duplicacy for SSN.
                //let the system process the application and mark it as a rehire. This applicant will not be moved to
                //hired after orientation. so duplicate ssn will not move to wish3.0 but will remain in hportal db.
                //For wish the applicantID will be 0
                if (rehire && Convert.ToInt32(objRowAffected.Tables[0].Rows[0]["ApplicantID"]) == 0)
                {
                    return true;
                }
                return false;
            }
            else
            {
                return true;
            }
        }

        public DataTable GetApplicantByStatus(Applicant obj)
        {
            SQL objSql = new SQL();
            if (obj.Status > 0)
                objSql.AddParameter("@StatusId", DbType.Int32, ParameterDirection.Input, 0, obj.Status);
            objSql.AddParameter("@BranchId", DbType.Int32, ParameterDirection.Input, 0, obj.BranchId);
            objSql.AddParameter("@PageIndex", DbType.Int32, ParameterDirection.Input, 0, obj.PageIndex);
            objSql.AddParameter("@PageCount", DbType.Int32, ParameterDirection.Input, 0, obj.PageCount);
            objSql.AddParameter("@IsAgree", DbType.Boolean, ParameterDirection.Input, 0, obj.IsAgree);
            objSql.AddParameter("@FirstName", DbType.String, ParameterDirection.Input, 0, obj.FirstName);
            objSql.AddParameter("@LastName", DbType.String, ParameterDirection.Input, 0, obj.LastName);
            objSql.AddParameter("@Email", DbType.String, ParameterDirection.Input, 0, obj.EmailID);
            objSql.AddParameter("@PrimaryPhone", DbType.String, ParameterDirection.Input, 0, obj.PrimaryPhone);
            objSql.AddParameter("@ZipCode", DbType.String, ParameterDirection.Input, 0, obj.ZipCode);
            objSql.AddParameter("@AppliedOnStart", DbType.String, ParameterDirection.Input, 0, obj.StartDate);
            objSql.AddParameter("@AppliedOnEnd", DbType.String, ParameterDirection.Input, 0, obj.EndDate);
            objSql.AddParameter("@sortCol", DbType.String, ParameterDirection.Input, 0, obj.sortExpression);
            objSql.AddParameter("@dir", DbType.String, ParameterDirection.Input, 0, obj.sortOrder);
            objSql.AddParameter("@Duplicate", DbType.String, ParameterDirection.Input, 0, obj.ddlDuplicateFilter);
            objSql.AddParameter("@ShowDuplicate", DbType.Boolean, ParameterDirection.Input, 0, obj.chkShowDuplicate);
            objSql.AddParameter("@year", DbType.String, ParameterDirection.Input, 0, obj.Year);
            objSql.AddParameter("@IncludeAll", DbType.Boolean, ParameterDirection.Input, 0, obj.includeAllStatuses);
            objSql.AddParameter("@JobPositionId", DbType.String, ParameterDirection.Input, 0, obj.JobPosition);
            if (obj.SearchByHomeBranch)
                return objSql.ExecuteDataSet("p_GetApplicantByStatusByHomeBranch").Tables[0];
            else
                return objSql.ExecuteDataSet("p_GetApplicantByStatus").Tables[0];

        }

        public DataTable GetApplicantForTaskDownload(Applicant obj)
        {
            SQL objSql = new SQL();
            if (obj.Status > 0)
                objSql.AddParameter("@StatusId", DbType.Int32, ParameterDirection.Input, 0, obj.Status);
            objSql.AddParameter("@BranchId", DbType.Int32, ParameterDirection.Input, 0, obj.BranchId);
            objSql.AddParameter("@PageIndex", DbType.Int32, ParameterDirection.Input, 0, obj.PageIndex);
            objSql.AddParameter("@PageCount", DbType.Int32, ParameterDirection.Input, 0, obj.PageCount);
            objSql.AddParameter("@IsAgree", DbType.Boolean, ParameterDirection.Input, 0, obj.IsAgree);
            objSql.AddParameter("@FirstName", DbType.String, ParameterDirection.Input, 0, obj.FirstName);
            objSql.AddParameter("@LastName", DbType.String, ParameterDirection.Input, 0, obj.LastName);
            objSql.AddParameter("@Email", DbType.String, ParameterDirection.Input, 0, obj.EmailID);
            objSql.AddParameter("@PrimaryPhone", DbType.String, ParameterDirection.Input, 0, obj.PrimaryPhone);
            objSql.AddParameter("@ZipCode", DbType.String, ParameterDirection.Input, 0, obj.ZipCode);
            objSql.AddParameter("@AppliedOnStart", DbType.String, ParameterDirection.Input, 0, obj.AppliedOnStart);
            objSql.AddParameter("@AppliedOnEnd", DbType.String, ParameterDirection.Input, 0, obj.AppliedOnEnd);
            objSql.AddParameter("@sortCol", DbType.String, ParameterDirection.Input, 0, obj.SortCoumn);
            objSql.AddParameter("@dir", DbType.String, ParameterDirection.Input, 0, obj.sortOrder);
            objSql.AddParameter("@Duplicate", DbType.String, ParameterDirection.Input, 0, obj.ddlDuplicateFilter);
            objSql.AddParameter("@ShowDuplicate", DbType.Boolean, ParameterDirection.Input, 0, obj.chkShowDuplicate);
            objSql.AddParameter("@year", DbType.String, ParameterDirection.Input, 0, obj.Year);
            objSql.AddParameter("@IncludeAll", DbType.Boolean, ParameterDirection.Input, 0, obj.includeAllStatuses);
            return objSql.ExecuteDataSet("p_GetApplicantForTaskDownload_test").Tables[0];

        }

        public int CountApplicationsForTaskDownload(Applicant obj)
        {
            DataSet ds = new DataSet();
            SQL objSql = new SQL();
            if (obj.Status > 0)
                objSql.AddParameter("@StatusId", DbType.Int32, ParameterDirection.Input, 0, obj.Status);
            objSql.AddParameter("@BranchId", DbType.Int32, ParameterDirection.Input, 0, obj.BranchId);
            objSql.AddParameter("@IsAgree", DbType.Boolean, ParameterDirection.Input, 0, obj.IsAgree);
            objSql.AddParameter("@FirstName", DbType.String, ParameterDirection.Input, 0, obj.FirstName);
            objSql.AddParameter("@LastName", DbType.String, ParameterDirection.Input, 0, obj.LastName);
            objSql.AddParameter("@Email", DbType.String, ParameterDirection.Input, 0, obj.EmailID);
            objSql.AddParameter("@ZipCode", DbType.String, ParameterDirection.Input, 0, obj.ZipCode);
            objSql.AddParameter("@AppliedOnStart", DbType.String, ParameterDirection.Input, 0, obj.AppliedOnStart);
            objSql.AddParameter("@AppliedOnEnd", DbType.String, ParameterDirection.Input, 0, obj.AppliedOnEnd);
            objSql.AddParameter("@PrimaryPhone", DbType.String, ParameterDirection.Input, 0, obj.PrimaryPhone);
            objSql.AddParameter("@Duplicate", DbType.String, ParameterDirection.Input, 0, obj.ddlDuplicateFilter);
            objSql.AddParameter("@ShowDuplicate", DbType.Boolean, ParameterDirection.Input, 0, obj.chkShowDuplicate);
            objSql.AddParameter("@IncludeAll", DbType.Boolean, ParameterDirection.Input, 0, obj.includeAllStatuses);
            objSql.AddParameter("@year", DbType.String, ParameterDirection.Input, 0, obj.Year);
            ds = objSql.ExecuteDataSet("p_CountApplicantForTaskDownload");
            if (ds != null)
            {
                if (ds.Tables.Count > 0)
                {
                    return Convert.ToInt16(ds.Tables[0].Rows[0][0]);
                }
            }
            return 0;

        }
        public DataTable GetApplicantByStatusAll(int statusId, int BranchId, int PageIndex, int PageCount, bool includeAllStatuses, string IsAgree = null, string FirstName = null, string LastName = null, string EmailID = null, string PrimaryPhone = null, string ZipCode = null, string AppliedOnStart = null, string AppliedOnEnd = null, String SortCoumn = "", String Dir = "", String Duplicate = null, Boolean showDuplicate = false, string year = null, bool SearchByHomeBranch = false)
        {
            try
            {
                SQL objSql = new SQL();
                if (statusId > 0)
                    objSql.AddParameter("@StatusId", DbType.Int32, ParameterDirection.Input, 0, statusId);
                objSql.AddParameter("@BranchId", DbType.Int32, ParameterDirection.Input, 0, BranchId);
                objSql.AddParameter("@PageIndex", DbType.Int32, ParameterDirection.Input, 0, PageIndex);
                objSql.AddParameter("@PageCount", DbType.Int32, ParameterDirection.Input, 0, PageCount);
                if (!string.IsNullOrEmpty(IsAgree))
                    objSql.AddParameter("@IsAgree", DbType.Boolean, ParameterDirection.Input, 0, IsAgree);
                if (!string.IsNullOrEmpty(FirstName))
                    objSql.AddParameter("@FirstName", DbType.String, ParameterDirection.Input, 0, FirstName);
                if (!string.IsNullOrEmpty(LastName))
                    objSql.AddParameter("@LastName", DbType.String, ParameterDirection.Input, 0, LastName);
                if (!string.IsNullOrEmpty(EmailID))
                    objSql.AddParameter("@Email", DbType.String, ParameterDirection.Input, 0, EmailID);
                if (!string.IsNullOrEmpty(PrimaryPhone))
                    objSql.AddParameter("@PrimaryPhone", DbType.String, ParameterDirection.Input, 0, PrimaryPhone);
                if (!string.IsNullOrEmpty(ZipCode))
                    objSql.AddParameter("@ZipCode", DbType.String, ParameterDirection.Input, 0, ZipCode);
                if (!string.IsNullOrEmpty(AppliedOnStart))
                    objSql.AddParameter("@AppliedOnStart", DbType.String, ParameterDirection.Input, 0, AppliedOnStart);
                if (!string.IsNullOrEmpty(AppliedOnEnd))
                    objSql.AddParameter("@AppliedOnEnd", DbType.String, ParameterDirection.Input, 0, AppliedOnEnd);
                objSql.AddParameter("@sortCol", DbType.String, ParameterDirection.Input, 0, SortCoumn);
                objSql.AddParameter("@dir", DbType.String, ParameterDirection.Input, 0, Dir);
                objSql.AddParameter("@Duplicate", DbType.String, ParameterDirection.Input, 0, Duplicate);
                objSql.AddParameter("@ShowDuplicate", DbType.Boolean, ParameterDirection.Input, 0, showDuplicate);
                objSql.AddParameter("@year", DbType.String, ParameterDirection.Input, 0, year);
                objSql.AddParameter("@IncludeAll", DbType.Boolean, ParameterDirection.Input, 0, includeAllStatuses);
                if (SearchByHomeBranch)
                    return objSql.ExecuteDataSet("p_GetApplicantByStatusByHomeBranchAll").Tables[0];
                else
                    return objSql.ExecuteDataSet("p_GetApplicantByStatusAll").Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int CountApplications(Applicant obj)
        {
            DataSet ds = new DataSet();
            SQL objSql = new SQL();
            if (obj.Status > 0)
                objSql.AddParameter("@StatusId", DbType.Int32, ParameterDirection.Input, 0, obj.Status);
            objSql.AddParameter("@BranchId", DbType.Int32, ParameterDirection.Input, 0, obj.BranchId);
            objSql.AddParameter("@IsAgree", DbType.Boolean, ParameterDirection.Input, 0, obj.IsAgree);
            objSql.AddParameter("@FirstName", DbType.String, ParameterDirection.Input, 0, obj.FirstName);
            objSql.AddParameter("@LastName", DbType.String, ParameterDirection.Input, 0, obj.LastName);
            objSql.AddParameter("@Email", DbType.String, ParameterDirection.Input, 0, obj.EmailID);
            objSql.AddParameter("@ZipCode", DbType.String, ParameterDirection.Input, 0, obj.ZipCode);
            objSql.AddParameter("@AppliedOnStart", DbType.String, ParameterDirection.Input, 0, obj.StartDate);
            objSql.AddParameter("@AppliedOnEnd", DbType.String, ParameterDirection.Input, 0, obj.EndDate);
            objSql.AddParameter("@PrimaryPhone", DbType.String, ParameterDirection.Input, 0, obj.PrimaryPhone);
            objSql.AddParameter("@Duplicate", DbType.String, ParameterDirection.Input, 0, obj.ddlDuplicateFilter);
            objSql.AddParameter("@ShowDuplicate", DbType.Boolean, ParameterDirection.Input, 0, obj.chkShowDuplicate);
            objSql.AddParameter("@IncludeAll", DbType.Boolean, ParameterDirection.Input, 0, obj.includeAllStatuses);
            objSql.AddParameter("@year", DbType.String, ParameterDirection.Input, 0, obj.Year);
            if (obj.SearchByHomeBranch)
                ds = objSql.ExecuteDataSet("p_CountApplicantByHomeBranch");
            else
                ds = objSql.ExecuteDataSet("p_CountApplicant");
            if (ds != null)
            {
                if (ds.Tables.Count > 0)
                {
                    return Convert.ToInt16(ds.Tables[0].Rows[0][0]);
                }
            }
            return 0;

        }

        public DataSet CheckInInterview(int JobApplicationId, int BranchId, int VenueID)
        {
            SQL objSql = new SQL();
            objSql.AddParameter("@JobApplicationId", DbType.Int32, ParameterDirection.Input, 0, JobApplicationId);
            objSql.AddParameter("@BranchId", DbType.Int32, ParameterDirection.Input, 0, BranchId);
            objSql.AddParameter("@VenueId", DbType.Int32, ParameterDirection.Input, 0, VenueID);


            return objSql.ExecuteDataSet("p_CheckInInterview");

        }

        public DataSet GetApplicantStatusByApplicantId(Applicant obj)
        {
            SQL objSql = new SQL();
            DataSet ds;
            objSql.AddParameter("@ApplicantId", DbType.Int32, ParameterDirection.Input, 0, obj.ApplicantId);
            ds = objSql.ExecuteDataSet("p_GetApplicantStatus");
            return ds;

        }


        public int PushToCue(Applicant obj, JobApplication objJobApplication)
        {

            SQL objSql = new SQL();
            object objRowAffected;


            objSql.AddParameter("@Applicantid", DbType.Int32, ParameterDirection.Input, 0, obj.ApplicantId);
            objSql.AddParameter("@FirstName", DbType.String, ParameterDirection.Input, 0, obj.FirstName);
            objSql.AddParameter("@LastName", DbType.String, ParameterDirection.Input, 0, obj.LastName);
            objSql.AddParameter("@MiddleName", DbType.String, ParameterDirection.Input, 0, obj.MiddleName);
            objSql.AddParameter("@EmailID", DbType.String, ParameterDirection.Input, 0, obj.EmailID);
            objSql.AddParameter("@Unit", DbType.String, ParameterDirection.Input, 0, obj.Unit);
            objSql.AddParameter("@ZipCode", DbType.String, ParameterDirection.Input, 0, obj.ZipCode);
            objSql.AddParameter("@Address", DbType.String, ParameterDirection.Input, 0, obj.Address);
            objSql.AddParameter("@City", DbType.String, ParameterDirection.Input, 0, obj.City);
            objSql.AddParameter("@StateId", DbType.Int32, ParameterDirection.Input, 0, obj.StateId);
            objSql.AddParameter("@CountryId", DbType.Int32, ParameterDirection.Input, 0, obj.CountryId);
            objSql.AddParameter("@PrimaryPhone", DbType.String, ParameterDirection.Input, 0, obj.PrimaryPhone);
            objSql.AddParameter("@SecondaryPhone", DbType.String, ParameterDirection.Input, 0, obj.SecondaryPhone);
            objSql.AddParameter("@HowDidYouHear", DbType.String, ParameterDirection.Input, 0, obj.HowDidYouHear);
            objSql.AddParameter("@RefferedBy", DbType.String, ParameterDirection.Input, 0, obj.RefferedBy);
            objSql.AddParameter("@OrganizationName", DbType.String, ParameterDirection.Input, 0, obj.OrganizationName);
            objSql.AddParameter("@CSCIDNumber", DbType.String, ParameterDirection.Input, 0, obj.CSCIDNumber);
            objSql.AddParameter("@EmployeeLastName", DbType.String, ParameterDirection.Input, 0, obj.EmployeeLastName);
            objSql.AddParameter("@EmployeeFirstName", DbType.String, ParameterDirection.Input, 0, obj.EmployeeFirstName);
            objSql.AddParameter("@EmployeeIDNumber", DbType.String, ParameterDirection.Input, 0, obj.EmployeeIDNumber);
            objSql.AddParameter("@ModifiedBy", DbType.Int32, ParameterDirection.Input, 0, objJobApplication.ModifiedBy);
            objSql.AddParameter("@BranchId", DbType.Int32, ParameterDirection.Input, 0, obj.BranchId);


            objRowAffected = objSql.ExecuteScalar("p_PushtoCue");
            return Convert.ToInt32(objRowAffected);
        }


        public int PushToHireInitial(Applicant obj, ApplicantDisclosure objDisc, JobApplication objJobApplication)
        {

            SQL objSql = new SQL();
            object objRowAffected;


            objSql.AddParameter("@Applicantid", DbType.Int32, ParameterDirection.Input, 0, obj.ApplicantId);
            objSql.AddParameter("@FirstName", DbType.String, ParameterDirection.Input, 0, obj.FirstName);
            objSql.AddParameter("@LastName", DbType.String, ParameterDirection.Input, 0, obj.LastName);
            objSql.AddParameter("@MiddleName", DbType.String, ParameterDirection.Input, 0, obj.MiddleName);
            objSql.AddParameter("@EmailID", DbType.String, ParameterDirection.Input, 0, obj.EmailID);
            objSql.AddParameter("@Unit", DbType.String, ParameterDirection.Input, 0, obj.Unit);
            objSql.AddParameter("@ZipCode", DbType.String, ParameterDirection.Input, 0, obj.ZipCode);
            objSql.AddParameter("@Address", DbType.String, ParameterDirection.Input, 0, obj.Address);
            objSql.AddParameter("@City", DbType.String, ParameterDirection.Input, 0, obj.City);
            objSql.AddParameter("@StateId", DbType.Int32, ParameterDirection.Input, 0, obj.StateId);
            objSql.AddParameter("@CountryId", DbType.Int32, ParameterDirection.Input, 0, obj.CountryId);
            objSql.AddParameter("@PrimaryPhone", DbType.String, ParameterDirection.Input, 0, obj.PrimaryPhone);
            objSql.AddParameter("@SecondaryPhone", DbType.String, ParameterDirection.Input, 0, obj.SecondaryPhone);
            objSql.AddParameter("@HowDidYouHear", DbType.String, ParameterDirection.Input, 0, obj.HowDidYouHear);
            objSql.AddParameter("@RefferedBy", DbType.String, ParameterDirection.Input, 0, obj.RefferedBy);
            objSql.AddParameter("@OrganizationName", DbType.String, ParameterDirection.Input, 0, obj.OrganizationName);
            objSql.AddParameter("@CSCIDNumber", DbType.String, ParameterDirection.Input, 0, obj.CSCIDNumber);
            objSql.AddParameter("@EmployeeLastName", DbType.String, ParameterDirection.Input, 0, obj.EmployeeLastName);
            objSql.AddParameter("@EmployeeFirstName", DbType.String, ParameterDirection.Input, 0, obj.EmployeeFirstName);
            objSql.AddParameter("@EmployeeIDNumber", DbType.String, ParameterDirection.Input, 0, obj.EmployeeIDNumber);
            objSql.AddParameter("@DOB", DbType.DateTime, ParameterDirection.Input, 0, obj.DOB != "" ? obj.DOB : null);
            objSql.AddParameter("@SSN", DbType.String, ParameterDirection.Input, 0, obj.SSN);
            objSql.AddParameter("@TaxStatus", DbType.String, ParameterDirection.Input, 0, obj.TaxStatus);
            objSql.AddParameter("@AdditionalAmount", DbType.Decimal, ParameterDirection.Input, 0, obj.AdditionalAmount);
            objSql.AddParameter("@Ethnicity", DbType.String, ParameterDirection.Input, 0, objDisc.Ethnicity);
            objSql.AddParameter("@Gender", DbType.String, ParameterDirection.Input, 0, objDisc.Gender);
            objSql.AddParameter("@DateApplied", DbType.DateTime, ParameterDirection.Input, 0, objJobApplication.AppliedOn != "" ? objJobApplication.AppliedOn : null);
            objSql.AddParameter("@DateInteview", DbType.DateTime, ParameterDirection.Input, 0, objJobApplication.InterviewDate != "" ? objJobApplication.InterviewDate : null);
            objSql.AddParameter("@DateOrientation", DbType.DateTime, ParameterDirection.Input, 0, objJobApplication.OrientationDate != "" ? objJobApplication.OrientationDate : null);
            objSql.AddParameter("@ModifiedBy", DbType.Int32, ParameterDirection.Input, 0, objJobApplication.ModifiedBy);
            objSql.AddParameter("@BranchId", DbType.Int32, ParameterDirection.Input, 0, obj.BranchId);


            objRowAffected = objSql.ExecuteScalar("p_PushtoHireSaveInitial");
            return Convert.ToInt32(objRowAffected);
        }

        public int UpdateApplicantIsAgree(int ApplicantId, Boolean IsAgree, string initials)
        {
            SQL objSql = new SQL();
            object objRowAffected;
            objSql.AddParameter("@ApplicantId", DbType.Int32, ParameterDirection.Input, 0, ApplicantId);
            objSql.AddParameter("@IsAgree", DbType.Boolean, ParameterDirection.Input, 0, IsAgree);
            objSql.AddParameter("@Initials", DbType.String, ParameterDirection.Input, 0, initials);

            objRowAffected = objSql.ExecuteNonQuery("p_ApplicantIsAgree_Ins");
            return Convert.ToInt32(objRowAffected);
        }

        public int saveDownloadPdf(int AuditUserId, int ApplicantId, string DocumentTitle)
        {
            SQL objSql = new SQL();
            objSql.AddParameter("@AuditUserId", DbType.Int32, ParameterDirection.Input, 0, AuditUserId);
            objSql.AddParameter("@ApplicantId", DbType.Int32, ParameterDirection.Input, 0, ApplicantId);
            objSql.AddParameter("@DocumentTitle", DbType.String, ParameterDirection.Input, 0, DocumentTitle);
            return Convert.ToInt32(objSql.ExecuteNonQuery("p_DownloadPdf_ins"));
        }

        public DataSet GetIsAgreeByJApplicantId(int ApplicantId)
        {
            SQL objSql = new SQL();
            DataSet ds;
            objSql.AddParameter("@ApplicantId", DbType.Int32, ParameterDirection.Input, 0, ApplicantId);
            ds = objSql.ExecuteDataSet("p_ApplicantIsAgree_Get");
            return ds;
        }

        public DataSet GetBGCheckByApplicantId(int ApplicantId)
        {
            SQL objSql = new SQL();
            DataSet ds;
            objSql.AddParameter("@ApplicantId", DbType.Int32, ParameterDirection.Input, 0, ApplicantId);
            ds = objSql.ExecuteDataSet("p_GetBGCheckByApplicantId");
            return ds;
        }
        public DataSet UpdateApplicantInformation(Applicant obj)
        {
            SQL objSql = new SQL();
            DataSet objRowAffected;
            objSql.AddParameter("@ApplicantId", DbType.Int32, ParameterDirection.Input, 0, obj.ApplicantId);
            objSql.AddParameter("@DOB", DbType.DateTime, ParameterDirection.Input, 0, obj.DOB != "" ? obj.DOB : null);
            objSql.AddParameter("@SSN", DbType.String, ParameterDirection.Input, 0, obj.SSN);
            objSql.AddParameter("@TaxStatus", DbType.String, ParameterDirection.Input, 0, obj.TaxStatus);
            objSql.AddParameter("@NoOfDependency", DbType.Int32, ParameterDirection.Input, 0, obj.NoOfDependency);
            objSql.AddParameter("@AdditionalAmount", DbType.Decimal, ParameterDirection.Input, 0, obj.AdditionalAmount);
            objSql.AddParameter("@Allowance", DbType.Decimal, ParameterDirection.Input, 0, obj.Allowance);
            objSql.AddParameter("@Rehire", DbType.Boolean, ParameterDirection.Input, 0, obj.Rehire);
            objSql.AddParameter("@RehireSource", DbType.Int32, ParameterDirection.Input, 0, obj.RehireSourceId);
            if (obj.JdpStatus > 0)
                objSql.AddParameter("@BgStatus", DbType.Int32, ParameterDirection.Input, 0, obj.JdpStatus);

            objRowAffected = objSql.ExecuteDataSet("p_ApplicantInfo_Ins");
            return objRowAffected;
        }

        public DataSet UpdateJobApplicationPayrate(JobPosition obj)
        {
            SQL objSql = new SQL();
            DataSet objRowAffected;
            objSql.AddParameter("@ApplicantId", DbType.Int32, ParameterDirection.Input, 0, obj.ApplicantId);
            objSql.AddParameter("@JobPositionId", DbType.String, ParameterDirection.Input, 0, obj.JobPositionId);
            objSql.AddParameter("@Payrate", DbType.Decimal, ParameterDirection.Input, 0, obj.PayRate);
            objSql.AddParameter("@OfferType", DbType.Int32, ParameterDirection.Input, 0, obj.OfferType);
            objSql.AddParameter("@OfferNotes", DbType.String, ParameterDirection.Input, 0, obj.OfferNotes);
            objSql.AddParameter("@JobType", DbType.String, ParameterDirection.Input, 0, obj.JobType);
            objRowAffected = objSql.ExecuteDataSet("p_JobApplication_up");
            return objRowAffected;
        }

        public DataTable GetApplicantForExport(int statusId, int BranchId, string IsAgree = null, string FirstName = null, string LastName = null, string EmailID = null, string ZipCode = null, string AppliedOnStart = null, string AppliedOnEnd = null, string PrimaryPhone = null, String Duplicate = null, Boolean showDuplicate = false, string year = null, bool includeAllStatuses = false, bool SearchByHomeBranch = false)
        {
            SQL objSql = new SQL();
            DataSet ds;
            objSql.AddParameter("@DateFrom", DbType.String, ParameterDirection.Input, 0, AppliedOnStart);
            objSql.AddParameter("@DateTo", DbType.String, ParameterDirection.Input, 0, AppliedOnEnd);
            if (statusId > 0)
                objSql.AddParameter("@StatusId", DbType.Int32, ParameterDirection.Input, 0, statusId);
            objSql.AddParameter("@BranchId", DbType.Int32, ParameterDirection.Input, 0, BranchId);
            objSql.AddParameter("@IsAgree", DbType.Boolean, ParameterDirection.Input, 0, IsAgree);
            objSql.AddParameter("@FirstName", DbType.String, ParameterDirection.Input, 0, FirstName);
            objSql.AddParameter("@LastName", DbType.String, ParameterDirection.Input, 0, LastName);
            objSql.AddParameter("@Email", DbType.String, ParameterDirection.Input, 0, EmailID);
            objSql.AddParameter("@ZipCode", DbType.String, ParameterDirection.Input, 0, ZipCode);
            objSql.AddParameter("@PrimaryPhone", DbType.String, ParameterDirection.Input, 0, PrimaryPhone);
            objSql.AddParameter("@Duplicate", DbType.String, ParameterDirection.Input, 0, Duplicate);
            objSql.AddParameter("@ShowDuplicate", DbType.Boolean, ParameterDirection.Input, 0, showDuplicate);
            objSql.AddParameter("@year", DbType.String, ParameterDirection.Input, 0, year);
            objSql.AddParameter("@IncludeAll", DbType.Boolean, ParameterDirection.Input, 0, includeAllStatuses);
            if (SearchByHomeBranch)
                return objSql.ExecuteDataSet("p_GetHiredApplicantByHomeBranchAll").Tables[0];
            else
                return objSql.ExecuteDataSet("p_GetHiredApplicant").Tables[0];

        }

        public DataSet GetDuplicates(int branchId)
        {
            SQL objSql = new SQL();
            objSql.AddParameter("@BranchId", DbType.Int32, ParameterDirection.Input, 0, branchId);
            return objSql.ExecuteDataSet("p_DuplicateApplicants");

        }

        public int MarkPrinted(string appIds)
        {
            SQL objSql = new SQL();
            objSql.AddParameter("@ApplicantIds", DbType.String, ParameterDirection.Input, 0, appIds);
            return objSql.ExecuteNonQuery("p_ApplicantMarkPrinted");
        }

        public DataSet GetSummary(int branchId, string Year, bool SearchByHomeBranch = false)
        {
            SQL objSql = new SQL();
            objSql.AddParameter("@BranchId", DbType.Int32, ParameterDirection.Input, 0, branchId);
            objSql.AddParameter("@year", DbType.Int32, ParameterDirection.Input, 0, Year);
            if (SearchByHomeBranch)
                return objSql.ExecuteDataSet("p_ApplicationDashBoardByHomeBranch");
            else
                return objSql.ExecuteDataSet("p_ApplicationDashBoard");
        }
        public DataSet ViewLog(int applicantId)
        {
            SQL objSql = new SQL();
            objSql.AddParameter("@applicantId", DbType.Int32, ParameterDirection.Input, 0, applicantId);

            return objSql.ExecuteDataSet("p_viewLog");
        }

        public int UpdateBGCheckOrderId(int applicantId, string orderId)
        {
            SQL objSql = new SQL();
            objSql.AddParameter("@applicantId", DbType.Int32, ParameterDirection.Input, 0, applicantId);
            objSql.AddParameter("@orderid", DbType.String, ParameterDirection.Input, 0, orderId);
            return objSql.ExecuteNonQuery("p_UpdateBGCheckOrder");
        }
        public int UpdateBGCheckStatus(int applicantId, int statusId, string message)
        {
            SQL objSql = new SQL();
            objSql.AddParameter("@applicantId", DbType.Int32, ParameterDirection.Input, 0, applicantId);
            objSql.AddParameter("@statusId", DbType.Int32, ParameterDirection.Input, 0, statusId);
            objSql.AddParameter("@message", DbType.String, ParameterDirection.Input, 0, message);
            return objSql.ExecuteNonQuery("p_UpdateBGCheckStatus");
        }
        public int TransferLocation(string applicantId, int destination, int UserId)
        {
            SQL objSql = new SQL();
            objSql.AddParameter("@applicantId", DbType.String, ParameterDirection.Input, 0, applicantId);
            objSql.AddParameter("@branchId", DbType.Int32, ParameterDirection.Input, 0, destination);
            objSql.AddParameter("@UserId", DbType.Int32, ParameterDirection.Input, 0, UserId);
            return objSql.ExecuteNonQuery("p_TransferLocation");
        }

        //Changes done by vijay on 30 May 2017
        public DataSet GeteVerifyLookUp()
        {
            SQL objSql = new SQL();
            DataSet ds = new DataSet();
            //objSql.AddParameter("@EmailID", DbType.String, ParameterDirection.Input, 0, obj.brancid);
            //objSql.AddParameter("@Password", DbType.String, ParameterDirection.Input, 0, obj.Password);
            //objSql.AddParameter("@UserType", DbType.Int32, ParameterDirection.Input, 0, obj.UserType);
            ds = objSql.ExecuteDataSet("p_GetLookupEverifyDocument");
            return ds;
        }
        public int InsEVerifyDoc(EverifyDoc obj)
        {
            SQL objSql = new SQL();
            DataSet ds = new DataSet();
            objSql.AddParameter("@ApplicantId", DbType.String, ParameterDirection.Input, 0, obj.ApplicantId);
            objSql.AddParameter("@DocumentPath", DbType.String, ParameterDirection.Input, 0, obj.DocumentPath);
            objSql.AddParameter("@EverifyDocumentId", DbType.Int32, ParameterDirection.Input, 0, obj.EverifyDocumentId);
            objSql.AddParameter("@CardNo", DbType.String, ParameterDirection.Input, 0, obj.CardNo);
            objSql.AddParameter("@AliensNo", DbType.String, ParameterDirection.Input, 0, obj.AliensNo);
            objSql.AddParameter("@DocumentNo", DbType.String, ParameterDirection.Input, 0, obj.DocumentNo);
            objSql.AddParameter("@IssuingAuthority", DbType.String, ParameterDirection.Input, 0, obj.IssuingAuthority);
            objSql.AddParameter("@ExpirationDate", DbType.DateTime, ParameterDirection.Input, 0, obj.ExpirationDate);
            objSql.AddParameter("@isExpirationDate", DbType.Int16, ParameterDirection.Input, 0, obj.isExpirationDate);
            objSql.AddParameter("@CitizenFlag", DbType.Boolean, ParameterDirection.Input, 0, obj.CitizenFlag);
            objSql.AddParameter("@DocType", DbType.String, ParameterDirection.Input, 0, obj.DocType);
            objSql.AddParameter("@UserId", DbType.Int32, ParameterDirection.Input, 0, obj.UserId);
            objSql.AddParameter("@ApplicantEverifyDocId", DbType.Int32, ParameterDirection.Input, 0, obj.ApplicantEverifyDocId);
            if (obj.IdDocType > 0)
                objSql.AddParameter("@IdDocType", DbType.Int32, ParameterDirection.Input, 0, obj.IdDocType);
            return objSql.ExecuteNonQuery("p_InsApplicantEverifyDoc");
        }

        public int UpEVerifyDoc(EverifyDoc obj)
        {
            SQL objSql = new SQL();
            DataSet ds = new DataSet();
            objSql.AddParameter("@ApplicantId", DbType.String, ParameterDirection.Input, 0, obj.ApplicantId);
            objSql.AddParameter("@DocumentPath", DbType.String, ParameterDirection.Input, 0, obj.DocumentPath);
            objSql.AddParameter("@EverifyDocumentId", DbType.Int32, ParameterDirection.Input, 0, obj.EverifyDocumentId);
            objSql.AddParameter("@CardNo", DbType.String, ParameterDirection.Input, 0, obj.CardNo);
            objSql.AddParameter("@AliensNo", DbType.String, ParameterDirection.Input, 0, obj.AliensNo);
            objSql.AddParameter("@DocumentNo", DbType.String, ParameterDirection.Input, 0, obj.DocumentNo);
            objSql.AddParameter("@IssuingAuthority", DbType.String, ParameterDirection.Input, 0, obj.IssuingAuthority);
            objSql.AddParameter("@isExpirationDate", DbType.Int16, ParameterDirection.Input, 0, obj.isExpirationDate);
            objSql.AddParameter("@ExpirationDate", DbType.DateTime, ParameterDirection.Input, 0, obj.ExpirationDate);
            objSql.AddParameter("@CitizenFlag", DbType.Boolean, ParameterDirection.Input, 0, obj.CitizenFlag);
            objSql.AddParameter("@DocType", DbType.String, ParameterDirection.Input, 0, obj.DocType);
            objSql.AddParameter("@UserId", DbType.Int32, ParameterDirection.Input, 0, obj.UserId);
            if (obj.IdDocType > 0)
                objSql.AddParameter("@IdDocType", DbType.Int32, ParameterDirection.Input, 0, obj.IdDocType);
            return objSql.ExecuteNonQuery("p_UpApplicantEverifyDoc");
        }
        //End

        public int SaveApplicantUniform(ApplicantUniform obj)
        {
            SQL objSql = new SQL();
            // DataSet ds = new DataSet();
            objSql.AddParameter("@Height", DbType.String, ParameterDirection.Input, 0, obj.Height);
            objSql.AddParameter("@HeightInche", DbType.String, ParameterDirection.Input, 0, obj.HeightInche);
            objSql.AddParameter("@Weight", DbType.String, ParameterDirection.Input, 0, obj.Weight);
            if (!string.IsNullOrEmpty(obj.JacketSize))
                objSql.AddParameter("@JacketSize", DbType.String, ParameterDirection.Input, 0, obj.JacketSize.Trim());
            if (!string.IsNullOrEmpty(obj.BlazerSize))
                objSql.AddParameter("@BlazerSize", DbType.String, ParameterDirection.Input, 0, obj.BlazerSize.Trim());
            if (!string.IsNullOrEmpty(obj.ShoeSize))
                objSql.AddParameter("@ShoeSize", DbType.String, ParameterDirection.Input, 0, obj.ShoeSize.Trim());
            if (!string.IsNullOrEmpty(obj.PantSize))
                objSql.AddParameter("@PantSize", DbType.String, ParameterDirection.Input, 0, obj.PantSize.Trim());
            if (!string.IsNullOrEmpty(obj.GloveSize))
                objSql.AddParameter("@GloveSize", DbType.String, ParameterDirection.Input, 0, obj.GloveSize.Trim());
            if (!string.IsNullOrEmpty(obj.PoloShirtSize))
                objSql.AddParameter("@PoloShirtSize", DbType.String, ParameterDirection.Input, 0, obj.PoloShirtSize.Trim());
            if (!string.IsNullOrEmpty(obj.ButtonDownShirtSize))
                objSql.AddParameter("@ButtonDownShirtSize", DbType.String, ParameterDirection.Input, 0, obj.ButtonDownShirtSize.Trim());
            objSql.AddParameter("@ApplicantId", DbType.Int32, ParameterDirection.Input, 0, obj.ApplicantId);
            return objSql.ExecuteNonQuery("p_ApplicantUniform_Ins");
        }
        public DataSet GetApplicantUniform(ApplicantUniform obj)
        {
            SQL objSql = new SQL();
            // DataSet ds = new DataSet();
            objSql.AddParameter("@ApplicantId", DbType.Int32, ParameterDirection.Input, 0, obj.ApplicantId);
            return objSql.ExecuteDataSet("p_GetApplicantUniform");
        }
        public DataSet GetApplicantFormI9(int ApplicantId)
        {
            SQL objSql = new SQL();
            // DataSet ds = new DataSet();
            objSql.AddParameter("@ApplicantId", DbType.Int32, ParameterDirection.Input, 0, ApplicantId);
            return objSql.ExecuteDataSet("p_GetApplicantFormI9");
        }
        public DataSet fnGetApplicantEVerifyList(int ApplicantId)
        {
            SQL objSql = new SQL();
            objSql.AddParameter("@ApplicantId", DbType.Int32, ParameterDirection.Input, 0, ApplicantId);
            return objSql.ExecuteDataSet("p_GetApplicantEVerifyList");
        }
        public DataSet GetApplicantFormI9(ApplicantFormI9 obj)
        {
            SQL objSql = new SQL();
            // DataSet ds = new DataSet();
            objSql.AddParameter("@ApplicantId", DbType.Int32, ParameterDirection.Input, 0, obj.ApplicantId);
            return objSql.ExecuteDataSet("p_GetApplicantFormI9");
        }
        public DataSet GetW4Form(ApplicantFormI9 obj)
        {
            SQL objSql = new SQL();
            // DataSet ds = new DataSet();
            objSql.AddParameter("@ApplicantId", DbType.Int32, ParameterDirection.Input, 0, obj.ApplicantId);
            return objSql.ExecuteDataSet("p_GetW4Form");
        }

        public int SaveApplicantFormI9(ApplicantFormI9 obj)
        {
            SQL objSql = new SQL();
            // DataSet ds = new DataSet();
            objSql.AddParameter("@ApplicantId", DbType.Int32, ParameterDirection.Input, 0, obj.ApplicantId);
            objSql.AddParameter("@FirstName", DbType.String, ParameterDirection.Input, 0, obj.FirstName);
            objSql.AddParameter("@MiddleInitial", DbType.String, ParameterDirection.Input, 0, obj.MiddleInitial);
            objSql.AddParameter("@LastName", DbType.String, ParameterDirection.Input, 0, obj.LastName);
            objSql.AddParameter("@OtherLastName", DbType.String, ParameterDirection.Input, 0, obj.OtherLastName);
            objSql.AddParameter("@Address", DbType.String, ParameterDirection.Input, 0, obj.Address);
            objSql.AddParameter("@AptNumber", DbType.String, ParameterDirection.Input, 0, obj.AptNumber);
            objSql.AddParameter("@City", DbType.String, ParameterDirection.Input, 0, obj.City);
            objSql.AddParameter("@State", DbType.String, ParameterDirection.Input, 0, obj.State);
            objSql.AddParameter("@ZipCode", DbType.String, ParameterDirection.Input, 0, obj.ZipCode);
            if (!string.IsNullOrEmpty(obj.DateOfBirth))
                objSql.AddParameter("@DateOfBirth", DbType.DateTime, ParameterDirection.Input, 0, obj.DateOfBirth);
            objSql.AddParameter("@SSN", DbType.String, ParameterDirection.Input, 0, obj.SSN);
            objSql.AddParameter("@Email", DbType.String, ParameterDirection.Input, 0, obj.Email);
            objSql.AddParameter("@Telephone", DbType.String, ParameterDirection.Input, 0, obj.Telephone);
            objSql.AddParameter("@AttestPerjury", DbType.String, ParameterDirection.Input, 0, obj.AttestPerjury);
            objSql.AddParameter("@RegistrationNumber", DbType.String, ParameterDirection.Input, 0, obj.RegistrationNumber);
            objSql.AddParameter("@RegistrationNumberType", DbType.String, ParameterDirection.Input, 0, obj.RegistrationNumberType);
            objSql.AddParameter("@ExpirationDate", DbType.String, ParameterDirection.Input, 0, obj.ExpirationDate);
            objSql.AddParameter("@FormI94AddminssionNumber", DbType.String, ParameterDirection.Input, 0, obj.FormI94AddminssionNumber);
            objSql.AddParameter("@ForeignPassportNumber", DbType.String, ParameterDirection.Input, 0, obj.ForeignPassportNumber);
            objSql.AddParameter("@IssuanceCountry", DbType.String, ParameterDirection.Input, 0, obj.IssuanceCountry);
            objSql.AddParameter("@EmployeeSignature", DbType.String, ParameterDirection.Input, 0, obj.EmployeeSignature);
            if (!string.IsNullOrEmpty(obj.EmployeeSignatureDate))
                objSql.AddParameter("@EmployeeSignatureDate", DbType.String, ParameterDirection.Input, 0, obj.EmployeeSignatureDate);
            objSql.AddParameter("@Preparer", DbType.String, ParameterDirection.Input, 0, obj.Preparer);
            objSql.AddParameter("@PreparerNumber", DbType.String, ParameterDirection.Input, 0, obj.PreparerNumber);
            objSql.AddParameter("@SignaturePreparer", DbType.String, ParameterDirection.Input, 0, obj.SignaturePreparer);
            if (!string.IsNullOrEmpty(obj.PreparerSignatureDate))
                objSql.AddParameter("@PreparerSignatureDate", DbType.DateTime, ParameterDirection.Input, 0, obj.PreparerSignatureDate);
            objSql.AddParameter("@PFirstName", DbType.String, ParameterDirection.Input, 0, obj.PFirstName);
            objSql.AddParameter("@PLastName", DbType.String, ParameterDirection.Input, 0, obj.PLastName);

            objSql.AddParameter("@PAddress", DbType.String, ParameterDirection.Input, 0, obj.PAddress);
            objSql.AddParameter("@PCity", DbType.String, ParameterDirection.Input, 0, obj.PCity);
            objSql.AddParameter("@PState", DbType.String, ParameterDirection.Input, 0, obj.PState);
            objSql.AddParameter("@PZipCode", DbType.String, ParameterDirection.Input, 0, obj.PZipCode);

            return objSql.ExecuteNonQuery("p_ApplicantFormI9_Ins");
        }
        public int UpdateApplicantFormI9(int ApplicantId, int UserId, DateTime FirstDayEmployment, string fileName, int Type)
        {
            SQL objSql = new SQL();
            // DataSet ds = new DataSet();
            objSql.AddParameter("@ApplicantId", DbType.Int32, ParameterDirection.Input, 0, ApplicantId);
            objSql.AddParameter("@UserId", DbType.Int32, ParameterDirection.Input, 0, UserId);
            objSql.AddParameter("@FirstDayEmployment", DbType.DateTime, ParameterDirection.Input, 0, FirstDayEmployment);
            objSql.AddParameter("@AdminDocPath", DbType.String, ParameterDirection.Input, 0, fileName);
            objSql.AddParameter("@Type", DbType.Int32, ParameterDirection.Input, 0, Type);

            return objSql.ExecuteNonQuery("p_UpApplicantFormI9");
        }
        public DataSet SelectDocuemntI9(EverifyDoc obj)
        {
            SQL objSql = new SQL();
            DataSet ds = new DataSet();
            objSql.AddParameter("@CitizenID", DbType.String, ParameterDirection.Input, 0, obj.CitizenId);
            objSql.AddParameter("@type", DbType.String, ParameterDirection.Input, 0, obj.Type);

            ds = objSql.ExecuteDataSet("p_SelectDocuemntI9_sel");
            return ds;
        }

        public DataSet SelectIssuingAuth(IssuingAuthority obj)
        {
            SQL objSql = new SQL();
            DataSet ds = new DataSet();
            objSql.AddParameter("@EverifyDocumentId", DbType.String, ParameterDirection.Input, 0, obj.EverifyDocumentId);
            ds = objSql.ExecuteDataSet("p_SelectIssuingAuth_sel");
            return ds;
        }

        public DataSet GetApplicantInterviewVenue(int ApplicantId)
        {
            SQL objSql = new SQL();
            DataSet ds = new DataSet();
            objSql.AddParameter("@ApplicantId", DbType.Int32, ParameterDirection.Input, 0, ApplicantId);
            ds = objSql.ExecuteDataSet("p_GetApplicantInterviewVenue");
            return ds;
        }
        public DataSet fnGetApplicationDetails(int ApplicantId)
        {
            SQL objSql = new SQL();
            DataSet ds = new DataSet();
            objSql.AddParameter("@ApplicantId", DbType.Int32, ParameterDirection.Input, 0, ApplicantId);
            ds = objSql.ExecuteDataSet("p_GetApplicationDetails");
            return ds;
        }
        public DataSet GetApplicantZurichDetails(int ApplicantId)
        {
            SQL objSql = new SQL();
            DataSet ds = new DataSet();
            objSql.AddParameter("@ApplicantId", DbType.Int32, ParameterDirection.Input, 0, ApplicantId);
            ds = objSql.ExecuteDataSet("p_GetApplicantZurichDetails");
            return ds;
        }
        public DataSet GetDataforVerification(int ApplicantId)
        {
            SQL objSql = new SQL();
            DataSet ds = new DataSet();
            objSql.AddParameter("@ApplicantId", DbType.Int32, ParameterDirection.Input, 0, ApplicantId);
            ds = objSql.ExecuteDataSet("p_GetDataforVerification");
            return ds;
        }
        public int FormI9Verification_Ins(string queuepk, int ResultId, int RequestId, string ResultErrorCode, string STATUS,
            string CaseNumber, string InvoiceAmount, string B2BStatus, string B2BSubStatus, string ProcessStatus, string InvoicingStatus)
        {
            SQL objSql = new SQL();
            DataSet ds = new DataSet();
            objSql.AddParameter("@queuepk", DbType.String, ParameterDirection.Input, 0, queuepk);
            objSql.AddParameter("@ResultId", DbType.Int32, ParameterDirection.Input, 0, ResultId);
            objSql.AddParameter("@RequestId", DbType.Int32, ParameterDirection.Input, 0, RequestId);
            objSql.AddParameter("@ResultErrorCode", DbType.String, ParameterDirection.Input, 0, ResultErrorCode);
            objSql.AddParameter("@STATUS", DbType.String, ParameterDirection.Input, 0, STATUS);
            objSql.AddParameter("@CaseNumber", DbType.String, ParameterDirection.Input, 0, CaseNumber);
            objSql.AddParameter("@InvoiceAmount", DbType.String, ParameterDirection.Input, 0, InvoiceAmount);
            objSql.AddParameter("@B2BStatus", DbType.String, ParameterDirection.Input, 0, B2BStatus);
            objSql.AddParameter("@B2BSubStatus", DbType.String, ParameterDirection.Input, 0, B2BSubStatus);
            objSql.AddParameter("@ProcessStatus", DbType.String, ParameterDirection.Input, 0, ProcessStatus);
            objSql.AddParameter("@InvoicingStatus", DbType.String, ParameterDirection.Input, 0, InvoicingStatus);
            return Convert.ToInt32(objSql.ExecuteNonQuery("p_FormI9Verification_Ins"));
        }
        public DataSet GetESignature(int ApplicantId)
        {
            SQL objSql = new SQL();
            objSql.AddParameter("@ApplicantId", DbType.Int32, ParameterDirection.Input, 0, ApplicantId);
            DataSet ds = objSql.ExecuteDataSet("p_GetApplicantSignature_test");
            if (ds != null && ds.Tables.Count > 0)
            {
                return ds;
            }
            else
            {
                return null;
            }
        }

        public int SaveApplicantZurich(Applicant obj)
        {
            SQL objSql = new SQL();
            DataSet ds = new DataSet();
            objSql.AddParameter("@ApplicantId", DbType.Int32, ParameterDirection.Input, 0, obj.ApplicantId);
            objSql.AddParameter("@Date", DbType.DateTime, ParameterDirection.Input, 0, DateTime.Now);
            objSql.AddParameter("@PrintedName", DbType.String, ParameterDirection.Input, 0, obj.ZurichPrintedName);
            objSql.AddParameter("@HomeAddress", DbType.String, ParameterDirection.Input, 0, obj.ZurichAddress);
            objSql.AddParameter("@City", DbType.String, ParameterDirection.Input, 0, obj.ZurichCity);
            objSql.AddParameter("@STATE", DbType.String, ParameterDirection.Input, 0, obj.ZurichStateName);
            objSql.AddParameter("@Zip", DbType.String, ParameterDirection.Input, 0, obj.ZurichZipCode);
            objSql.AddParameter("@AppTaskId", DbType.Int32, ParameterDirection.Input, 0, obj.AppTaskId);
            return objSql.ExecuteNonQuery("p_ApplicantZurich_Ins");
        }

        public DataSet GetBranch(int ID)
        {
            SQL objSql = new SQL();
            DataSet ds;
            objSql.AddParameter("@BranchId", DbType.String, ParameterDirection.Input, 0, ID);
            return objSql.ExecuteDataSet("p_GetBranch");
        }
        public DataTable GetConfiguration(string Name)
        {
            SQL objSql = new SQL();
            objSql.AddParameter("@Name", DbType.String, ParameterDirection.Input, 0, Name);
            return objSql.ExecuteDataSet("p_getConfiguration_sel").Tables[0];
        }
        public DataTable GetHiredRehiredReport(int UserId, DateTime StartDate, DateTime EndDate)
        {
            SQL objSql = new SQL();
            DataTable objRowAffected;
            objSql.AddParameter("@UserId", DbType.Int32, ParameterDirection.Input, 0, UserId);
            objSql.AddParameter("@StartDate", DbType.DateTime, ParameterDirection.Input, 0, StartDate);
            objSql.AddParameter("@EndDate", DbType.DateTime, ParameterDirection.Input, 0, EndDate.AddHours(23).AddMinutes(59).AddSeconds(59));
            objRowAffected = objSql.ExecuteDataSet("p_GetApplicantInfo").Tables[0];
            return objRowAffected;
        }
        public DataTable GetApplicantRehireDetails(int ApplicantId)
        {
            SQL objSql = new SQL();
            DataTable objRowAffected;
            objSql.AddParameter("@ApplicantId", DbType.Int32, ParameterDirection.Input, 0, ApplicantId);
            objRowAffected = objSql.ExecuteDataSet("p_GetApplicantRehireDetails").Tables[0];
            return objRowAffected;
        }
        public DataSet GetApplicantDocumentById(int ApplicantId, int ApplicantTaskId)
        {
            SQL objSql = new SQL();
            DataSet ds;
            objSql.AddParameter("@ApplicantId", DbType.String, ParameterDirection.Input, 0, ApplicantId);
            objSql.AddParameter("@ApplicantTaskId", DbType.String, ParameterDirection.Input, 0, ApplicantTaskId);
            ds = objSql.ExecuteDataSet("p_GetApplicantDocumentById");
            return ds;
        }
    }
}