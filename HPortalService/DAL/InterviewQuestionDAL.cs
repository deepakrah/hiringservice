﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;


namespace HPortalService.DAL
{
    public class InterviewQuestionDAL
    {
        public int Save(InterviewQuestion obj)
        {
            SQL objSql = new SQL();
            object objRowAffected;
            objSql.AddParameter("@InterviewQuestionId", DbType.Int32, ParameterDirection.Input, 0, obj.InterviewQuestionId);
            objSql.AddParameter("@QuesDesc", DbType.String, ParameterDirection.Input, 0, obj.QuesDesc);
            objSql.AddParameter("@BranchId", DbType.Int32, ParameterDirection.Input, 0, obj.BranchId);

            objRowAffected = objSql.ExecuteScalar("p_InterviewQuestions_Ins");
            return Convert.ToInt32(objRowAffected);
        }
        public void SaveOption(InterviewQuestion obj)
        {
            SQL objSql = new SQL();
            objSql.AddParameter("@InterviewQuestionId", DbType.Int32, ParameterDirection.Input, 0, obj.InterviewQuestionId);
            objSql.AddParameter("@OptionDesc", DbType.String, ParameterDirection.Input, 0, obj.OptionDesc);
            objSql.ExecuteNonQuery("p_InterviewOptions_Ins");

        }

        public int SaveApplicantOption(InterviewQuestion obj)
        {
            SQL objSql = new SQL();
            object objRowAffected;
            objSql.AddParameter("@InterviewQuestionId", DbType.Int32, ParameterDirection.Input, 0, obj.InterviewQuestionId);
            objSql.AddParameter("@InterviewOptionId", DbType.String, ParameterDirection.Input, 0, obj.InterviewOptionId);
            objSql.AddParameter("@ApplicantId", DbType.Int32, ParameterDirection.Input, 0, obj.ApplicantId);
            objSql.AddParameter("@rubics", DbType.Boolean, ParameterDirection.Input, 0, obj.rubics);
            objSql.AddParameter("@remarks", DbType.String, ParameterDirection.Input, 0, obj.Remarks);
            objSql.AddParameter("@Score", DbType.String, ParameterDirection.Input, 0, obj.Score);


            objRowAffected = objSql.ExecuteNonQuery("p_ApplicantInterviewAnser_Ins");
            return Convert.ToInt32(objRowAffected);
        }


        public DataSet GetQuestions(InterviewQuestion obj)
        {
            SQL objSql = new SQL();
            objSql.AddParameter("@BranchId", DbType.Int32, ParameterDirection.Input, 0, obj.BranchId);
            objSql.AddParameter("@InterviewQuestionId", DbType.Int32, ParameterDirection.Input, 0, obj.InterviewQuestionId);
            DataSet ds = objSql.ExecuteDataSet("p_InterviewQuestions_Get");
            return ds;
        }


        public DataSet GetOptions(InterviewQuestion obj)
        {
            SQL objSql = new SQL();
            objSql.AddParameter("@InterviewQuestionId", DbType.Int32, ParameterDirection.Input, 0, obj.InterviewQuestionId);
            DataSet ds = objSql.ExecuteDataSet("p_InterviewOptions_Get");
            return ds;
        }


        public int Delete(InterviewQuestion obj)
        {
            SQL objSql = new SQL();
            object objRowAffected;
            objSql.AddParameter("@InterviewQuestionId", DbType.Int32, ParameterDirection.Input, 0, obj.InterviewQuestionId);
            objRowAffected = objSql.ExecuteNonQuery("p_InterviewQuestions_Delete");
            return Convert.ToInt32(objRowAffected);
        }

           public DataSet GetApplicantInterviewAnswers(InterviewQuestion obj)
        {
            SQL objSql = new SQL();
            objSql.AddParameter("@ApplicantId", DbType.Int32, ParameterDirection.Input, 0, obj.ApplicantId);
            DataSet ds = objSql.ExecuteDataSet("p_ApplicantInterviewAnser_Get");
            return ds;
        }

        
    }
}
