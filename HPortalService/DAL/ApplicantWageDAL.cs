﻿
using System;
using System.Data;

namespace HPortalService.DAL
{
   public class ApplicantWageDAL
    {
        public int intApplicantWage(ApplicantWage obj)
        {
            SQL objSql = new SQL();
            object objContentId;
            int albumcontentId = 0;
            objSql.AddParameter("@ApplicantId", DbType.Int32, ParameterDirection.Input, 0, obj.ApplicantId);
            objSql.AddParameter("@Name", DbType.String, ParameterDirection.Input, 0, obj.Name);
            objSql.AddParameter("@SoleProprietor", DbType.Boolean, ParameterDirection.Input, 0, obj.SoleProprietor);
            objSql.AddParameter("@Corporation", DbType.Boolean, ParameterDirection.Input, 0, obj.Corporation);
            objSql.AddParameter("@LimitedLiabilityCompany", DbType.Boolean, ParameterDirection.Input, 0, obj.LimitedLiabilityCompany);
            objSql.AddParameter("@GeneralPartnership", DbType.Boolean, ParameterDirection.Input, 0, obj.GeneralPartnership);
            objSql.AddParameter("@MainOfficeAddress", DbType.String, ParameterDirection.Input, 0, obj.MainOfficeAddress);
            objSql.AddParameter("@MailingAddress", DbType.String, ParameterDirection.Input, 0, obj.MailingAddress);
            objSql.AddParameter("@Phone", DbType.String, ParameterDirection.Input, 0, obj.Phone);
            objSql.AddParameter("@BName", DbType.String, ParameterDirection.Input, 0, obj.BName);
            objSql.AddParameter("@Agreement", DbType.String, ParameterDirection.Input, 0, obj.Agreement);
            objSql.AddParameter("@Allowances", DbType.String, ParameterDirection.Input, 0, obj.Allowances);
            objSql.AddParameter("@InsuranceCarrierName", DbType.String, ParameterDirection.Input, 0, obj.InsuranceCarrierName);
            objSql.AddParameter("@InsuranceCarrierAddress", DbType.String, ParameterDirection.Input, 0, obj.InsuranceCarrierAddress);
            objSql.AddParameter("@InsuranceCarrierPhone", DbType.String, ParameterDirection.Input, 0, obj.InsuranceCarrierPhone);
            objSql.AddParameter("@PolicyNo", DbType.String, ParameterDirection.Input, 0, obj.PolicyNo);
            objContentId = objSql.ExecuteScalar("p_insApplicantWage");            
            return albumcontentId = Convert.ToInt32(objContentId);
        }

        public DataSet GetApplicantWageByID(int ID)
        {
            SQL objSql = new SQL();
            DataSet ds;
            objSql.AddParameter("@ApplicantId", DbType.String, ParameterDirection.Input, 0, ID);
            ds = objSql.ExecuteDataSet("p_GetApplicantWage");
            return ds;
        }
    }
}
