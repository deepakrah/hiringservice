﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;


namespace HPortalService.DAL
{
    public class PrivacyActDAL
    {

        public DataSet fnGetApplicantPrivacyAct(int ApplicantId)
        {
            SQL objSql = new SQL();
            DataSet ds = new DataSet();
            objSql.AddParameter("@ApplicantId", DbType.Int32, ParameterDirection.Input, 0, ApplicantId);
            ds = objSql.ExecuteDataSet("p_GetApplicantPrivacyAct");
            return ds;
        }
        public DataSet fnGetRelationShip(int ApplicantId)
        {
            SQL objSql = new SQL();
            DataSet ds = new DataSet();
            ds = objSql.ExecuteDataSet("p_GetLookupEmergencyRelationshipTypes");
            return ds;
        }

        public int fnInsApplicantValidate(PrivacyAct obj)
        {
           SQL objSql = new SQL();
            DataSet ds = new DataSet();
            objSql.AddParameter("@ApplicantId", DbType.Int32, ParameterDirection.Input, 0, obj.ApplicantId);
            if (!string.IsNullOrEmpty(obj.ApplicantInfo))
                objSql.AddParameter("@ApplicantInfo", DbType.String, ParameterDirection.Input, 0, obj.ApplicantInfo);
            if (!string.IsNullOrEmpty(obj.Name))
                objSql.AddParameter("@Name", DbType.String, ParameterDirection.Input, 0, obj.Name);
            if (!string.IsNullOrEmpty(obj.SSN))
                objSql.AddParameter("@SSN", DbType.String, ParameterDirection.Input, 0, obj.SSN);
            if (obj.DateOfBirth != null)
                objSql.AddParameter("@DateOfBirth", DbType.DateTime, ParameterDirection.Input, 0, obj.DateOfBirth);
            objSql.AddParameter("@Gender", DbType.String, ParameterDirection.Input, 0, obj.Gender);
            objSql.AddParameter("@HomeAddress", DbType.String, ParameterDirection.Input, 0, obj.HomeAddress);
            objSql.AddParameter("@City", DbType.String, ParameterDirection.Input, 0, obj.City);
            objSql.AddParameter("@State", DbType.String, ParameterDirection.Input, 0, obj.State);
            objSql.AddParameter("@PostalCode", DbType.String, ParameterDirection.Input, 0, obj.PostalCode);
            objSql.AddParameter("@PrimaryTelephone", DbType.String, ParameterDirection.Input, 0, obj.PrimaryTelephone);
            objSql.AddParameter("@PrimaryEmail", DbType.String, ParameterDirection.Input, 0, obj.PrimaryEmail);
            objSql.AddParameter("@AlternateTelephone", DbType.String, ParameterDirection.Input, 0, obj.AlternateTelephone);
            objSql.AddParameter("@AlternateEMail", DbType.String, ParameterDirection.Input, 0, obj.AlternateEMail);
            objSql.AddParameter("@Allergies", DbType.String, ParameterDirection.Input, 0, obj.Allergies);
            objSql.AddParameter("@Medications", DbType.String, ParameterDirection.Input, 0, obj.Medications);
            objSql.AddParameter("@PrimaryContactName", DbType.String, ParameterDirection.Input, 0, obj.PrimaryContactName);
            objSql.AddParameter("@Relationship", DbType.String, ParameterDirection.Input, 0, obj.Relationship);
            objSql.AddParameter("@PrimaryContactTelephone", DbType.String, ParameterDirection.Input, 0, obj.PrimaryContactTelephone);
            objSql.AddParameter("@PrimaryContactEmail", DbType.String, ParameterDirection.Input, 0, obj.PrimaryContactEmail);
            objSql.AddParameter("@ContactAlternateTelephone", DbType.String, ParameterDirection.Input, 0, obj.ContactAlternateTelephone);
            objSql.AddParameter("@ContactAlternateEMail", DbType.String, ParameterDirection.Input, 0, obj.ContactAlternateEMail);
            objSql.AddParameter("@AlternateContactName1", DbType.String, ParameterDirection.Input, 0, obj.AlternateContactName1);
            objSql.AddParameter("@AlternateRelationship1", DbType.String, ParameterDirection.Input, 0, obj.AlternateRelationship1);
            objSql.AddParameter("@AlternatePTelephone1", DbType.String, ParameterDirection.Input, 0, obj.AlternatePTelephone1);
            objSql.AddParameter("@AlternatePEmailAddress1", DbType.String, ParameterDirection.Input, 0, obj.AlternatePEmailAddress1);
            objSql.AddParameter("@AlternateATelephone1", DbType.String, ParameterDirection.Input, 0, obj.AlternateATelephone1);
            objSql.AddParameter("@AlternateAEmailAddress1", DbType.String, ParameterDirection.Input, 0, obj.AlternateAEmailAddress1);
            objSql.AddParameter("@AlternateContactName2", DbType.String, ParameterDirection.Input, 0, obj.AlternateContactName2);
            objSql.AddParameter("@AlternateRelationship2", DbType.String, ParameterDirection.Input, 0, obj.AlternateRelationship2);
            objSql.AddParameter("@AlternatePTelephone2", DbType.String, ParameterDirection.Input, 0, obj.AlternatePTelephone2);
            objSql.AddParameter("@AlternatePEmailAddress2", DbType.String, ParameterDirection.Input, 0, obj.AlternatePEmailAddress2);
            objSql.AddParameter("@AlternateATelephone2", DbType.String, ParameterDirection.Input, 0, obj.AlternateATelephone2);
            objSql.AddParameter("@AlternateAEmailAddress2", DbType.String, ParameterDirection.Input, 0, obj.AlternateAEmailAddress2);
            return objSql.ExecuteNonQuery("p_ApplicantPrivacyAct_ins");

        }

    }
}