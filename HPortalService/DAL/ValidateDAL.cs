﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;



namespace HPortalService.DAL
{
    public class ValidateDAL
    {

        public int fnInsApplicantValidate(Validate obj)
        {
            SQL objSql = new SQL();
            DataSet ds = new DataSet();
            objSql.AddParameter("@ApplicantId", DbType.Int32, ParameterDirection.Input, 0, obj.ApplicantId);
            objSql.AddParameter("@Type", DbType.Int32, ParameterDirection.Input, 0, obj.Type);
            if (!string.IsNullOrEmpty(obj.FirstName))
                objSql.AddParameter("@FirstName", DbType.String, ParameterDirection.Input, 0, obj.FirstName);
            if (!string.IsNullOrEmpty(obj.MiddleName) && obj.MiddleName != "null")
                objSql.AddParameter("@MiddleName", DbType.String, ParameterDirection.Input, 0, obj.MiddleName);
            if (!string.IsNullOrEmpty(obj.LastName))
                objSql.AddParameter("@LastName", DbType.String, ParameterDirection.Input, 0, obj.LastName);
            if (!string.IsNullOrEmpty(obj.SSN))
                objSql.AddParameter("@SSN", DbType.String, ParameterDirection.Input, 0, obj.SSN);
            if (obj.DateOfBirth != null)
                objSql.AddParameter("@DateOfBirth", DbType.DateTime, ParameterDirection.Input, 0, obj.DateOfBirth);
            if (obj.Finish > 0)
                objSql.AddParameter("@Finish", DbType.Int32, ParameterDirection.Input, 0, obj.Finish);
            return objSql.ExecuteNonQuery("p_insApplicantValidate");

        }
        public DataSet fnGetApplicantValidate(int ApplicantId)
        {
            SQL objSql = new SQL();
            DataSet ds = new DataSet();
            objSql.AddParameter("@ApplicantId", DbType.Int32, ParameterDirection.Input, 0, ApplicantId);
            return ds = objSql.ExecuteDataSet("p_GetApplicantValidate");

        }
        public DataSet GetApplicantValidatebySSN(string SSN)
        {
            SQL objSql = new SQL();
            DataSet ds = new DataSet();
            objSql.AddParameter("@SSN", DbType.String, ParameterDirection.Input, 0, SSN);
            return ds = objSql.ExecuteDataSet("p_GetApplicantValidatebySSN");

        }

    }
}
