﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;


namespace HPortalService.DAL
{
    public class ApplicantTaskDAL
    {

        public DataSet GetApplicantTask(ApplicantTask obj)
        {
            SQL objSql = new SQL();
            DataSet ds = new DataSet();
            objSql.AddParameter("@ApplicantId", DbType.Int32, ParameterDirection.Input, 0, obj.ApplicantId);
            ds = objSql.ExecuteDataSet("p_GetApplicantTask");
            return ds;
        }
        public DataSet getApplicantSigninAddress(ApplicantTask obj)
        {
            SQL objSql = new SQL();
            DataSet ds = new DataSet();
            objSql.AddParameter("@ApplicantId", DbType.Int32, ParameterDirection.Input, 0, obj.ApplicantId);
            ds = objSql.ExecuteDataSet("p_getApplicantSigninAddress");
            return ds;
        }
        public int InsApplicantTask(ApplicantTask obj)
        {
            SQL objSql = new SQL();
            DataSet ds = new DataSet();
            objSql.AddParameter("@ApplicantId", DbType.Int32, ParameterDirection.Input, 0, obj.ApplicantId);
            objSql.AddParameter("@StatusId", DbType.Int32, ParameterDirection.Input, 0, obj.StatusId);
            int temp = -1;
            if (obj.Type == 1)
            {
                temp = objSql.ExecuteNonQuery("p_insApplicantTaskform1");
            }
            else
            {
                temp = objSql.ExecuteNonQuery("p_insApplicantTask");
            }
            return temp;
        }
        public int GetApplicantCheckInStatus(ApplicantTask obj)
        {
            SQL objSQL = new SQL();
            objSQL.AddParameter("@JobApplicationId", DbType.Int32, ParameterDirection.Input, 0, obj.JobApplicationId);
            DataTable dt = objSQL.ExecuteDataSet("p_GetApplicantInterviewIsAttend").Tables[0];
            if (dt.Rows.Count > 0)
            {
                return Convert.ToInt32(dt.Rows[0]["isAttend"]);
            }
            return 0;
        }
        public int UpApplicantResultId(ApplicantTask obj)
        {
            SQL objSql = new SQL();
            DataSet ds = new DataSet();
            objSql.AddParameter("@ApplicantId", DbType.Int32, ParameterDirection.Input, 0, obj.ApplicantId);
            objSql.AddParameter("@ResultId", DbType.Int32, ParameterDirection.Input, 0, obj.ResultId);
            int temp = -1;
            temp = objSql.ExecuteNonQuery("p_UpApplicantResultId");
            return temp;
        }
        public DataSet GetApplicantTaskDocs(ApplicantTask obj)
        {
            SQL objSql = new SQL();
            DataSet ds = new DataSet();
            objSql.AddParameter("@ApplicantId", DbType.Int32, ParameterDirection.Input, 0, obj.ApplicantId);
            if (obj.Phase > 0)
                objSql.AddParameter("@Phase", DbType.Int32, ParameterDirection.Input, 0, obj.Phase);
            ds = objSql.ExecuteDataSet("p_GetApplicantTaskForDoc");
            return ds;
        }

        public DataSet GetEVerifyHistory(ApplicantTask obj)
        {
            SQL objSql = new SQL();
            DataSet ds = new DataSet();
            objSql.AddParameter("@ApplicantId", DbType.Int32, ParameterDirection.Input, 0, obj.ApplicantId);
            ds = objSql.ExecuteDataSet("p_GetEVerifyHistroy");
            return ds;
        }

        public DataSet GetApplicantAllTaskDocs(ApplicantTask obj)
        {
            SQL objSql = new SQL();
            DataSet ds = new DataSet();
            objSql.AddParameter("@ApplicantId", DbType.Int32, ParameterDirection.Input, 0, obj.ApplicantId);
            if (obj.Phase > 0)
                objSql.AddParameter("@Phase", DbType.Int32, ParameterDirection.Input, 0, obj.Phase);
            ds = objSql.ExecuteDataSet("p_GetApplicantAllTaskForDoc");
            return ds;
        }

        public DataSet GetAuditApplicantAllTaskForDoc(ApplicantTask obj)
        {
            SQL objSql = new SQL();
            DataSet ds = new DataSet();
            objSql.AddParameter("@ApplicantId", DbType.Int32, ParameterDirection.Input, 0, obj.ApplicantId);
            if (obj.Phase > 0)
                objSql.AddParameter("@Phase", DbType.Int32, ParameterDirection.Input, 0, obj.Phase);
            ds = objSql.ExecuteDataSet("p_GetAuditApplicantAllTaskForDoc");
            return ds;
        }

        public DataSet GetApplicantTaskForDocDownload(ApplicantTask obj)
        {
            SQL objSql = new SQL();
            DataSet ds = new DataSet();
            objSql.AddParameter("@ApplicantId", DbType.Int32, ParameterDirection.Input, 0, obj.ApplicantId);
            ds = objSql.ExecuteDataSet("p_GetApplicantTaskForDocDownload");
            return ds;
        }
        public DataSet ApplicantTaskUpdate(ApplicantTask obj)
        {
            SQL objSql = new SQL();
            DataSet ds = new DataSet();
            objSql.AddParameter("@ApplicantTaskId", DbType.Int32, ParameterDirection.Input, 0, obj.ApplicantTaskId);
            objSql.AddParameter("@ApplicantId", DbType.Int32, ParameterDirection.Input, 0, obj.ApplicantId);
            objSql.AddParameter("@VerifiedBy", DbType.Int32, ParameterDirection.Input, 0, obj.VerifiedBy);
            return objSql.ExecuteDataSet("p_ApplicantTaskUpdate");

        }
        public int ApplicantUpdate(ApplicantTask obj)
        {
            SQL objSql = new SQL();
            objSql.AddParameter("@ApplicantId", DbType.Int32, ParameterDirection.Input, 0, obj.ApplicantId);
            return objSql.ExecuteNonQuery("p_ApplicantUpdate");

        }

        public DataSet ApplicantTaskStatusUpdate(ApplicantTask obj)
        {
            SQL objSql = new SQL();
            DataSet ds = new DataSet();
            objSql.AddParameter("@ApplicantTaskId", DbType.Int32, ParameterDirection.Input, 0, obj.ApplicantTaskId);
            return objSql.ExecuteDataSet("p_ApplicantTaskStatusUpdate");

        }


    }
}
