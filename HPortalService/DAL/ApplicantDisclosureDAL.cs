﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;


namespace HPortalService.DAL
{
   public class ApplicantDisclosureDAL
    {
       public int UpdateApplicantDisclosure(ApplicantDisclosure obj)
        {
            SQL objSql = new SQL();
            object objRowAffected;
            objSql.AddParameter("@JobApplicationId", DbType.Int32, ParameterDirection.Input, 0, obj.JobApplicationId);
            objSql.AddParameter("@Ethnicity", DbType.String, ParameterDirection.Input, 0, obj.Ethnicity);
            objSql.AddParameter("@Gender",  DbType.String, ParameterDirection.Input, 0, obj.Gender);
            objSql.AddParameter("@VeteranStatus", DbType.String, ParameterDirection.Input, 0, obj.VeteranStatus);
            objRowAffected = objSql.ExecuteNonQuery("p_ApplicantDisclosure_Ins");
            return Convert.ToInt32(objRowAffected);
        }


       public DataSet GetDisclosureByJobApplicantId(int JobApplicationId)
       {
           SQL objSql = new SQL();
           DataSet ds;
           objSql.AddParameter("@JobApplicationId", DbType.Int32, ParameterDirection.Input, 0, JobApplicationId);
           ds = objSql.ExecuteDataSet("p_ApplicantDisclosure_Get");
           return ds;
       }

    }
}
