﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;




namespace HPortalService.DAL
{
    public class LookupDAL
    {

        public DataSet GetAllCountry()
        {
            SQL objSql = new SQL();
            DataSet ds = new DataSet();
            ds = objSql.ExecuteDataSet("p_GetCountry");
            return ds;

        }

        public DataSet GetStateByCountryId(Lookup obj)
        {
            SQL objSql = new SQL();
            DataSet ds = new DataSet();
            objSql.AddParameter("@CountryId", DbType.Int32, ParameterDirection.Input, 0, obj.CountryId);
            ds = objSql.ExecuteDataSet("p_GetStateByCountryId");
            return ds;

        }

    }
}
