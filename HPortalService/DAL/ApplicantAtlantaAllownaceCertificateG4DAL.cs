﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace HPortalService.DAL
{
    public class ApplicantAtlantaAllownaceCertificateG4DAL
    {
        public int intApplicantAtlantaAllownaceCertificateG4(ApplicantAtlantaAllownaceCertificateG4 obj)
        {
            SQL objSql = new SQL();
            object objContentId;
            int albumcontentId = 0;
            objSql.AddParameter("@ApplicantId", DbType.Int32, ParameterDirection.Input, 0, obj.ApplicantId);
            objSql.AddParameter("@Address1", DbType.String, ParameterDirection.Input, 0, obj.Address1);
            objSql.AddParameter("@Address2", DbType.String, ParameterDirection.Input, 0, obj.Address2);
            objSql.AddParameter("@MaritalStatus", DbType.Int32, ParameterDirection.Input, 0, obj.MaritalStatus);
            objSql.AddParameter("@MarriedFilingJointbothSpouses", DbType.Int32, ParameterDirection.Input, 0, obj.MarriedFilingJointbothSpouses);
            objSql.AddParameter("@MarriedFilingJointoneSpouse", DbType.Int32, ParameterDirection.Input, 0, obj.MarriedFilingJointoneSpouse);
            objSql.AddParameter("@MarriedFilingSeparate", DbType.Int32, ParameterDirection.Input, 0, obj.MarriedFilingSeparate);
            objSql.AddParameter("@HeadofHousehold", DbType.Int32, ParameterDirection.Input, 0, obj.HeadofHousehold);
            objSql.AddParameter("@DependentAllowances", DbType.String, ParameterDirection.Input, 0, obj.DependentAllowances);
            objSql.AddParameter("@AddtionalAllowances", DbType.String, ParameterDirection.Input, 0, obj.AddtionalAllowances);
            if (!string.IsNullOrEmpty(obj.AddtionalWithholding))
                objSql.AddParameter("@AddtionalWithholding", DbType.Decimal, ParameterDirection.Input, 0, obj.AddtionalWithholding);
            objSql.AddParameter("@StandardDeductionYourselfAge", DbType.Boolean, ParameterDirection.Input, 0, obj.StandardDeductionYourselfAge);
            objSql.AddParameter("@StandardDeductionYourselfBlind", DbType.Boolean, ParameterDirection.Input, 0, obj.StandardDeductionYourselfBlind);
            objSql.AddParameter("@StandardDeductionSpouseAge", DbType.Boolean, ParameterDirection.Input, 0, obj.StandardDeductionSpouseAge);
            objSql.AddParameter("@StandardDeductionSpouseBlind", DbType.Boolean, ParameterDirection.Input, 0, obj.StandardDeductionSpouseBlind);
            objSql.AddParameter("@Boxeschecked", DbType.Int32, ParameterDirection.Input, 0, obj.Boxeschecked);
            if (!string.IsNullOrEmpty(obj.Amount))
                objSql.AddParameter("@Amount", DbType.Decimal, ParameterDirection.Input, 0, obj.Amount);
            if (!string.IsNullOrEmpty(obj.ItemizedDeductions))
                objSql.AddParameter("@ItemizedDeductions", DbType.Decimal, ParameterDirection.Input, 0, obj.ItemizedDeductions);
            if (!string.IsNullOrEmpty(obj.GeorgiaStandardDeduction))
                objSql.AddParameter("@GeorgiaStandardDeduction", DbType.Decimal, ParameterDirection.Input, 0, obj.GeorgiaStandardDeduction);
            if (!string.IsNullOrEmpty(obj.SubtractLineB))
                objSql.AddParameter("@SubtractLineB", DbType.Decimal, ParameterDirection.Input, 0, obj.SubtractLineB);
            if (!string.IsNullOrEmpty(obj.AllowableDeductions))
                objSql.AddParameter("@AllowableDeductions", DbType.Decimal, ParameterDirection.Input, 0, obj.AllowableDeductions);
            if (!string.IsNullOrEmpty(obj.AddAmounts))
                objSql.AddParameter("@AddAmounts", DbType.Decimal, ParameterDirection.Input, 0, obj.AddAmounts);
            if (!string.IsNullOrEmpty(obj.EstimateofTaxable))
                objSql.AddParameter("@EstimateofTaxable", DbType.Decimal, ParameterDirection.Input, 0, obj.EstimateofTaxable);
            if (!string.IsNullOrEmpty(obj.SubtractLineF))
                objSql.AddParameter("@SubtractLineF", DbType.Decimal, ParameterDirection.Input, 0, obj.SubtractLineF);
            if (!string.IsNullOrEmpty(obj.DividetheAmount))
                objSql.AddParameter("@DividetheAmount", DbType.Decimal, ParameterDirection.Input, 0, obj.DividetheAmount);
            if (!string.IsNullOrEmpty(obj.LetterUsed))
                objSql.AddParameter("@LetterUsed", DbType.String, ParameterDirection.Input, 0, obj.LetterUsed);
            if (!string.IsNullOrEmpty(obj.TotalAllowances))
                objSql.AddParameter("@TotalAllowances", DbType.Decimal, ParameterDirection.Input, 0, obj.TotalAllowances);
            objSql.AddParameter("@IsIncomeTaxLiability", DbType.Boolean, ParameterDirection.Input, 0, obj.IsIncomeTaxLiability);
            if (!string.IsNullOrEmpty(obj.Residence))
                objSql.AddParameter("@Residence", DbType.String, ParameterDirection.Input, 0, obj.Residence);
            if (!string.IsNullOrEmpty(obj.SpouseResidence))
                objSql.AddParameter("@SpouseResidence", DbType.String, ParameterDirection.Input, 0, obj.SpouseResidence);
            objSql.AddParameter("@IsResidenceExempt", DbType.Boolean, ParameterDirection.Input, 0, obj.IsResidenceExempt);

            objContentId = objSql.ExecuteScalar("p_intApplicantAtlantaAllownaceCertificateG4_ins");
            return albumcontentId = Convert.ToInt32(objContentId);
        }

        public DataSet GetApplicantAtlantaAllownaceCertificateG4ByID(int ID)
        {
            SQL objSql = new SQL();
            DataSet ds;
            objSql.AddParameter("@ApplicantId", DbType.Int32, ParameterDirection.Input, 0, ID);
            ds = objSql.ExecuteDataSet("p_GetApplicantAtlantaAllownaceCertificateG4ByID");
            return ds;
        }
    }
}
