﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;


namespace HPortalService.DAL
{
    class ApplicantCTW4EmpWithholdingCertificateDAL
    {
        public int IntApplicantCTW4EmpWithholdingCertificate(ApplicantCTW4EmpWithholdingCertificate obj)
        {

            SQL objSql = new SQL();
            object objContentId;
            int albumcontentId = 0;
            objSql.AddParameter("@ApplicantId", DbType.Int32, ParameterDirection.Input, 0, obj.ApplicantId);
            objSql.AddParameter("@ApplicantCTW4EmpWithholdingCertificateId", DbType.Int32, ParameterDirection.Input, 0, obj.ApplicantCTW4EmpWithholdingCertificateId);
            objSql.AddParameter("@WithholdingCode", DbType.String, ParameterDirection.Input, 0, obj.WithholdingCode);
            if(!string.IsNullOrEmpty(obj.AdditionalAmount))
            objSql.AddParameter("@AdditionalAmount", DbType.Decimal, ParameterDirection.Input, 0, obj.AdditionalAmount);
            if (!string.IsNullOrEmpty(obj.ReducedAmount))
            objSql.AddParameter("@ReducedAmount", DbType.Decimal, ParameterDirection.Input, 0, obj.ReducedAmount);
            objSql.AddParameter("@LegalResidence", DbType.String, ParameterDirection.Input, 0, obj.LegalResidence);
            objSql.AddParameter("@IsRehired", DbType.Boolean, ParameterDirection.Input, 0, obj.IsRehired);
            if(!string.IsNullOrEmpty(obj.RehiredDate))
            objSql.AddParameter("@RehiredDate", DbType.DateTime, ParameterDirection.Input, 0, Convert.ToDateTime(obj.RehiredDate));
            objContentId = objSql.ExecuteScalar("p_ApplicantCTW4EmpWithholdingCertificate_ins");
            return albumcontentId = Convert.ToInt32(objContentId);
        }

        public DataSet GetApplicantCTW4EmpWithholdingCertificateByID(int ApplicantId)
        {
            SQL objSql = new SQL();
            DataSet ds;
            objSql.AddParameter("@ApplicantId", DbType.Int32, ParameterDirection.Input, 0, ApplicantId);
            ds = objSql.ExecuteDataSet("p_GetApplicantCTW4EmpWithholdingCertificateByID");
            return ds;
        }
    }
}
