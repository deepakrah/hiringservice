﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;




namespace HPortalService.DAL
{
    public class JobApplicationDAL
    {
        public int jobapplictionlog_ins_upd(JobApplication obj)
        {
            SQL objSql = new SQL();
            objSql.AddParameter("@JobApplicationId", DbType.Int32, ParameterDirection.Input, 0, obj.JobApplicationId);
            objSql.AddParameter("@StatusId", DbType.Int32, ParameterDirection.Input, 0, obj.StatusID);
            objSql.AddParameter("@ModifiedBy", DbType.Int32, ParameterDirection.Input, 0, obj.ModifiedBy);

            return Convert.ToInt32(objSql.ExecuteNonQuery("p_jobapplictionlog_ins_upd"));
        }
        public int BackToQueue(JobApplication obj)
        {
            SQL objSql = new SQL();
            objSql.AddParameter("@JobApplicationId", DbType.Int32, ParameterDirection.Input, 0, obj.JobApplicationId);
            objSql.AddParameter("@ModifiedBy", DbType.Int32, ParameterDirection.Input, 0, obj.ModifiedBy);

            return Convert.ToInt32(objSql.ExecuteScalar("p_BackToQueue"));
        }



        public bool ScheduleForInterview(int jobApplicationId, int interviewVenueId, bool schedByAdmin, bool schedByUser, int schedById, int InterviewSessionId, int isOrientation)
        {
            SQL objSql = new SQL();
            objSql.AddParameter("@interviewVenueId", DbType.Int32, ParameterDirection.Input, 0, interviewVenueId);
            objSql.AddParameter("@JobApplicationId", DbType.Int32, ParameterDirection.Input, 0, jobApplicationId);
            objSql.AddParameter("@ScheduledByAdmin", DbType.Boolean, ParameterDirection.Input, 0, schedByAdmin);
            objSql.AddParameter("@ScheduledByUser", DbType.Boolean, ParameterDirection.Input, 0, schedByUser);
            objSql.AddParameter("@ScheduledBy", DbType.Int32, ParameterDirection.Input, 0, schedById);
            objSql.AddParameter("@InterviewSessionId", DbType.Int32, ParameterDirection.Input, 0, InterviewSessionId);
            objSql.AddParameter("@isOrientation", DbType.Int32, ParameterDirection.Input, 0, isOrientation);

            return Convert.ToInt32(objSql.ExecuteScalar("p_ApplicantInterview_ins")) == 1;

        }


        public int Mark_Publish(InterviewVenues obj)
        {
            SQL objSql = new SQL();
            objSql.AddParameter("@InterviewVenueId", DbType.Int32, ParameterDirection.Input, 0, obj.InterviewVenueId);
            objSql.AddParameter("@isPublished", DbType.Int32, ParameterDirection.Input, 0, obj.isPublished);

            return Convert.ToInt32(objSql.ExecuteScalar("p_markPublish_InterviewVenues"));
        }



        public int InterviewVenue_Ins(InterviewVenues obj)
        {
            SQL objSql = new SQL();
            objSql.AddParameter("@VenueID", DbType.Int32, ParameterDirection.Input, 0, obj.VenueId);
            objSql.AddParameter("@VenueName", DbType.String, ParameterDirection.Input, 100, obj.VenueName);
            objSql.AddParameter("@BranchID", DbType.Int32, ParameterDirection.Input, 0, obj.BranchId);
            if (!string.IsNullOrEmpty(obj.Date))
                objSql.AddParameter("@Date", DbType.DateTime, ParameterDirection.Input, 0, Convert.ToDateTime(obj.Date));
            objSql.AddParameter("@InterviewType", DbType.String, ParameterDirection.Input, 0, obj.InterviewType);
            objSql.AddParameter("@StartTime", DbType.String, ParameterDirection.Input, 100, obj.StartTime);
            objSql.AddParameter("@Duration", DbType.String, ParameterDirection.Input, 0, obj.Duration);
            objSql.AddParameter("@Sessions", DbType.Int32, ParameterDirection.Input, 0, obj.Sessions);
            objSql.AddParameter("@Interviewers", DbType.Int32, ParameterDirection.Input, 0, obj.Interviewers);
            objSql.AddParameter("@Room", DbType.String, ParameterDirection.Input, 0, obj.RoomNo);
            objSql.AddParameter("@Parking", DbType.String, ParameterDirection.Input, 0, obj.ParkingInstructions);
            objSql.AddParameter("@isPublished", DbType.Boolean, ParameterDirection.Input, 0, obj.isPublished);
            objSql.AddParameter("@ShiftRoleId", DbType.Int32, ParameterDirection.Input, 0, obj.ShiftRoleId);
            objSql.AddParameter("@Slot", DbType.Int32, ParameterDirection.Input, 0, obj.Slot);
            return Convert.ToInt32(objSql.ExecuteScalar("p_EventScheduler_Ins"));
        }

        public int Interview_MarkHired(int JobApplicationId, string JobNo, string ShiftNumber)
        {
            SQL objSql = new SQL();
            objSql.AddParameter("@JobApplicationId", DbType.Int32, ParameterDirection.Input, 0, JobApplicationId);
            objSql.AddParameter("@JobNo", DbType.String, ParameterDirection.Input, 0, JobNo);
            objSql.AddParameter("@ShiftNumber", DbType.String, ParameterDirection.Input, 0, ShiftNumber);
            return Convert.ToInt32(objSql.ExecuteScalar("p_markHire"));
        }

        public bool VerifyShiftNo(string JobNo, string ShiftNo, int BranchId)
        {
            SQL objSql = new SQL();
            objSql.AddParameter("@JobNo", DbType.String, ParameterDirection.Input, 0, JobNo);
            objSql.AddParameter("@ShiftNo", DbType.String, ParameterDirection.Input, 0, ShiftNo);
            objSql.AddParameter("@BranchId", DbType.Int32, ParameterDirection.Input, 0, BranchId);
            return Convert.ToBoolean(objSql.ExecuteScalar("p_verifyShiftNo"));
        }

        public bool VerifyJobNo(string JobNo, int BranchId)
        {
            SQL objSql = new SQL();
            objSql.AddParameter("@JobNo", DbType.String, ParameterDirection.Input, 0, JobNo);
            objSql.AddParameter("@BranchId", DbType.Int32, ParameterDirection.Input, 0, BranchId);
            return Convert.ToBoolean(objSql.ExecuteScalar("p_verifyJobNo"));
        }

        public int Interview_MarkAttend(int JobApplicationID, int isOrientation, int ScheduledBy)
        {
            SQL objSql = new SQL();
            objSql.AddParameter("@JobApplicationId", DbType.Int32, ParameterDirection.Input, 0, JobApplicationID);
            objSql.AddParameter("@isOrientation", DbType.Int32, ParameterDirection.Input, 0, isOrientation);
            objSql.AddParameter("@ScheduledBy", DbType.Int32, ParameterDirection.Input, 0, ScheduledBy);
            return Convert.ToInt32(objSql.ExecuteScalar("p_ApplicantInterview_MarkAttend"));
        }

        public DataSet EditInterviewVenue(InterviewVenues obj)
        {
            SQL objSql = new SQL();
            DataSet ds;
            objSql.AddParameter("@InterviewVenueId", DbType.Int32, ParameterDirection.Input, 0, obj.InterviewVenueId);
            objSql.AddParameter("@VenueId", DbType.Int32, ParameterDirection.Input, 0, obj.VenueId);
            objSql.AddParameter("@VenueName", DbType.String, ParameterDirection.Input, 0, obj.VenueName);
            objSql.AddParameter("@RoomNo", DbType.String, ParameterDirection.Input, 0, obj.RoomNo);
            objSql.AddParameter("@InterviewType", DbType.String, ParameterDirection.Input, 0, obj.InterviewType);
            objSql.AddParameter("@Parking", DbType.String, ParameterDirection.Input, 0, obj.ParkingInstructions);
            objSql.AddParameter("@Slot", DbType.String, ParameterDirection.Input, 0, obj.Slot);
            if (obj.ShiftRoleId > 0)
                objSql.AddParameter("@ShiftRoleId", DbType.String, ParameterDirection.Input, 0, obj.ShiftRoleId);
            ds = objSql.ExecuteDataSet("p_InterviewVenues_Edit");
            return ds;
        }

        public DataSet GetInterviewVenueBranchId(int BranchID, string Type = null, bool SearchByHomeBranch = false)
        {
            SQL objSql = new SQL();
            DataSet ds;
            objSql.AddParameter("@BranchId", DbType.Int32, ParameterDirection.Input, 0, BranchID);
            if (!string.IsNullOrEmpty(Type))
                objSql.AddParameter("@InterViewType", DbType.String, ParameterDirection.Input, 0, Type);
            if (SearchByHomeBranch)
                ds = objSql.ExecuteDataSet("p_InterviewScheduler_Get");
            else
                ds = objSql.ExecuteDataSet("p_InterviewScheduler_GetByChildBranch");
            return ds;
        }

        public int RejectApplicant(int JobApplicationID, string reason, string Remrks)
        {
            SQL objSql = new SQL();
            objSql.AddParameter("@JobApplicationId", DbType.Int32, ParameterDirection.Input, 0, JobApplicationID);
            objSql.AddParameter("@Reason", DbType.String, ParameterDirection.Input, 0, reason);
            objSql.AddParameter("@Remarks", DbType.String, ParameterDirection.Input, 0, Remrks);
            return Convert.ToInt32(objSql.ExecuteScalar("p_ApplicantRejection_Ins"));
        }

        public DataSet GetApplicantRejection(int JobApplicationId)
        {
            SQL objSql = new SQL();
            DataSet ds;
            objSql.AddParameter("@JobApplicationId", DbType.Int32, ParameterDirection.Input, 0, JobApplicationId);
            ds = objSql.ExecuteDataSet("p_ApplicantRejection_Get");
            return ds;
        }

        public void InsertBulk(DataTable dt)
        {

            JobApplicationDAL obj = new JobApplicationDAL();
            SQL objSql = new SQL();
            objSql.ExecuteBatchQuery("p_Update_ExportStatus", dt);

        }


        public DataSet GetCSVData(int branchId, bool ShowOnlyDups, string from = null, string to = null)
        {
            SQL objSql = new SQL();
            DataSet ds;
            objSql.AddParameter("@DateFrom", DbType.String, ParameterDirection.Input, 0, from);
            objSql.AddParameter("@DateTo", DbType.String, ParameterDirection.Input, 0, to);
            objSql.AddParameter("@ShowOnlyDups", DbType.Boolean, ParameterDirection.Input, 0, ShowOnlyDups);

            if (branchId > 0)
            {
                objSql.AddParameter("@BranchId", DbType.Int16, ParameterDirection.Input, 0, branchId);

            }
            ds = objSql.ExecuteDataSet("p_GetCSVData");
            return ds;
        }


        public DataSet GetHiredApplicant(int branchId, string from = null, string to = null, bool SearchByHomeBranch = false)
        {
            SQL objSql = new SQL();
            DataSet ds;
            if (!string.IsNullOrEmpty(from))
                objSql.AddParameter("@DateFrom", DbType.String, ParameterDirection.Input, 0, from);
            if (!string.IsNullOrEmpty(to))
                objSql.AddParameter("@DateTo", DbType.String, ParameterDirection.Input, 0, to);
            objSql.AddParameter("@BranchId", DbType.Int16, ParameterDirection.Input, 0, branchId);
            if (SearchByHomeBranch)
                ds = objSql.ExecuteDataSet("p_GetHiredApplicant_Obsolate");
            else
                ds = objSql.ExecuteDataSet("p_GetHiredApplicant_ObsolateByChildBranch");
            return ds;
        }

        public DataSet GetHiredApplicantSummary(int branchId, string from = null, string to = null, string source = null, bool SearchByHomeBranch = false)
        {
            SQL objSql = new SQL();
            DataSet ds;
            objSql.AddParameter("@DateFrom", DbType.String, ParameterDirection.Input, 0, from);
            objSql.AddParameter("@DateTo", DbType.String, ParameterDirection.Input, 0, to);
            objSql.AddParameter("@Source", DbType.String, ParameterDirection.Input, 0, source);

            objSql.AddParameter("@BranchId", DbType.Int16, ParameterDirection.Input, 0, branchId);
            if (SearchByHomeBranch)
                ds = objSql.ExecuteDataSet("p_SystemSummary");
            else
                ds = objSql.ExecuteDataSet("p_SystemSummaryByChildBranch");
            return ds;
        }

        public DataSet GetRegistrationReportSummary(int branchId, string from = null, string to = null, string source = null, bool SearchByHomeBranch = false)
        {
            SQL objSql = new SQL();
            DataSet ds;
            objSql.AddParameter("@DateFrom", DbType.String, ParameterDirection.Input, 0, from);
            objSql.AddParameter("@DateTo", DbType.String, ParameterDirection.Input, 0, to);
            objSql.AddParameter("@Source", DbType.String, ParameterDirection.Input, 0, source);

            objSql.AddParameter("@BranchId", DbType.Int16, ParameterDirection.Input, 0, branchId);
            if (SearchByHomeBranch)
                ds = objSql.ExecuteDataSet("p_GetRegistrationReportSummary");
            else
                ds = objSql.ExecuteDataSet("p_GetRegistrationReportSummaryByChildBranch");
            return ds;
        }



        public DataSet GetAvailInterviewVenueByDate(string Date, int BranchID, int Orientation = 0, int isAdmin = 0, int VenueId = 0)
        {
            SQL objSql = new SQL();
            objSql.AddParameter("@Date", DbType.String, ParameterDirection.Input, 0, Date);
            objSql.AddParameter("@BranchId", DbType.Int32, ParameterDirection.Input, 0, BranchID);
            objSql.AddParameter("@isOrientation", DbType.Int32, ParameterDirection.Input, 0, Orientation);
            objSql.AddParameter("@isAdmin", DbType.Int32, ParameterDirection.Input, 0, isAdmin);
            if (VenueId > 0)
                objSql.AddParameter("@VenueId", DbType.Int32, ParameterDirection.Input, 0, VenueId);
            DataSet ds;
            ds = objSql.ExecuteDataSet("P_Get_InterviewVenueByDate_test");
            return ds;
        }


        //public DataSet GetAvailInterviewVenueByDateVenue(string Date, int BranchID, int Orientation = 0, int isAdmin = 0, int VenueId = 0)
        //{
        //    SQL objSql = new SQL();
        //    objSql.AddParameter("@Date", DbType.String, ParameterDirection.Input, 0, Date);
        //    objSql.AddParameter("@BranchId", DbType.Int32, ParameterDirection.Input, 0, BranchID);
        //    objSql.AddParameter("@isOrientation", DbType.Int32, ParameterDirection.Input, 0, Orientation);
        //    objSql.AddParameter("@isAdmin", DbType.Int32, ParameterDirection.Input, 0, isAdmin);
        //    objSql.AddParameter("@VenueId", DbType.Int32, ParameterDirection.Input, 0, VenueId);

        //    DataSet ds;
        //    ds = objSql.ExecuteDataSet("P_Get_InterviewVenueByDateVenue");
        //    return ds;
        //}


        public DataSet GetJobByDate(DateTime Date, int BranchID)
        {
            SQL objSql = new SQL();
            objSql.AddParameter("@Date", DbType.DateTime, ParameterDirection.Input, 0, Date);
            objSql.AddParameter("@BranchId", DbType.Int32, ParameterDirection.Input, 0, BranchID);
            DataSet ds;
            ds = objSql.ExecuteDataSet("P_GetJobByDate");
            return ds;
        }
        public DataSet GetShiftRole(int JobId, int BranchID)
        {
            SQL objSql = new SQL();
            objSql.AddParameter("@JobId", DbType.Int32, ParameterDirection.Input, 0, JobId);
            objSql.AddParameter("@BranchId", DbType.Int32, ParameterDirection.Input, 0, BranchID);
            DataSet ds;
            ds = objSql.ExecuteDataSet("P_GetShiftRole");
            return ds;
        }
        public DataSet GetAllVenues(int applicantId)
        {
            SQL objSql = new SQL();
            DataSet ds;
            objSql.AddParameter("@applicantId", DbType.Int32, ParameterDirection.Input, 0, applicantId);

            ds = objSql.ExecuteDataSet("P_GetAllVenues");
            return ds;

        }


        public DataSet GetInterviewVenueDate(int BranchID, int type = 0, int isAdmin = 1, Nullable<int> VenueId = null, string zipCode = null, bool getUnpublished = false, int YearFilter = 0)
        {
            SQL objSql = new SQL();
            DataSet ds;
            objSql.AddParameter("@BranchId", DbType.Int32, ParameterDirection.Input, 0, BranchID);
            objSql.AddParameter("@isOrientation", DbType.Int32, ParameterDirection.Input, 0, type);
            objSql.AddParameter("@isAdmin", DbType.Int32, ParameterDirection.Input, 0, isAdmin);
            objSql.AddParameter("@VenueID", DbType.Int32, ParameterDirection.Input, 0, VenueId);
            objSql.AddParameter("@ZipCode", DbType.String, ParameterDirection.Input, 0, zipCode);
            objSql.AddParameter("@getUnpublished", DbType.Boolean, ParameterDirection.Input, 0, getUnpublished);
            if (YearFilter > 0)
                objSql.AddParameter("@Year", DbType.Int32, ParameterDirection.Input, 0, YearFilter);
            ds = objSql.ExecuteDataSet("p_Get_AvailableScheduleDate");
            return ds;
        }


        public DataSet GetInterviewVenueDateVenue(int BranchID, int type = 0, int isAdmin = 1, Nullable<int> VenueId = null, string zipCode = null, bool getUnpublished = false, int YearFilter = 0)
        {
            SQL objSql = new SQL();
            DataSet ds;
            objSql.AddParameter("@BranchId", DbType.Int32, ParameterDirection.Input, 0, BranchID);
            objSql.AddParameter("@isOrientation", DbType.Int32, ParameterDirection.Input, 0, type);
            objSql.AddParameter("@isAdmin", DbType.Int32, ParameterDirection.Input, 0, isAdmin);
            objSql.AddParameter("@VenueID", DbType.Int32, ParameterDirection.Input, 0, VenueId);
            objSql.AddParameter("@ZipCode", DbType.String, ParameterDirection.Input, 0, zipCode);
            objSql.AddParameter("@getUnpublished", DbType.Boolean, ParameterDirection.Input, 0, getUnpublished);
            if (YearFilter > 0)
                objSql.AddParameter("@Year", DbType.Int32, ParameterDirection.Input, 0, YearFilter);
            ds = objSql.ExecuteDataSet("p_Get_AvailableScheduleDateVenue");
            return ds;
        }





        public DataSet GetApplicantForBatchInterview(int BranchId, Nullable<int> VenueID = null, string date = null, Nullable<int> JobApplicationId = null)
        {
            SQL objSql = new SQL();
            DataSet ds;
            objSql.AddParameter("@BranchId", DbType.Int32, ParameterDirection.Input, 0, BranchId);
            objSql.AddParameter("@VenueID", DbType.Int32, ParameterDirection.Input, 0, VenueID);
            if (date != "null")
                objSql.AddParameter("@Date", DbType.Date, ParameterDirection.Input, 0, date);
            if (JobApplicationId > 0)
                objSql.AddParameter("@JobApplicationID", DbType.Int32, ParameterDirection.Input, 0, JobApplicationId);

            ds = objSql.ExecuteDataSet("p_Get_ApplicantForBatchInterview");
            return ds;
        }


        public DataSet GetApplicantInterview(int BranchId, Nullable<int> VenueID = null, string date = null, Nullable<int> JobApplicationId = null, int type = 0, bool includeAttended = true, bool checkedIn = false, bool SearchByHomeBranch = false)
        {
            SQL objSql = new SQL();
            DataSet ds;
            objSql.AddParameter("@BranchId", DbType.Int32, ParameterDirection.Input, 0, BranchId);
            objSql.AddParameter("@VenueID", DbType.Int32, ParameterDirection.Input, 0, VenueID);
            objSql.AddParameter("@Date", DbType.Date, ParameterDirection.Input, 0, date);
            if (JobApplicationId > 0)
                objSql.AddParameter("@JobApplicationID", DbType.Int32, ParameterDirection.Input, 0, JobApplicationId);
            objSql.AddParameter("@includeAttended", DbType.Boolean, ParameterDirection.Input, 0, includeAttended);
            objSql.AddParameter("@isOrientation", DbType.Int32, ParameterDirection.Input, 0, type);
            objSql.AddParameter("@CheckedIn", DbType.Boolean, ParameterDirection.Input, 0, checkedIn);
            if (SearchByHomeBranch)
                ds = objSql.ExecuteDataSet("p_Get_ApplicantInterview_test");
            else
                ds = objSql.ExecuteDataSet("p_Get_ApplicantInterviewByChildBranch_test");

            return ds;
        }
        public DataSet GetApplicantInterview1(int BranchId, Nullable<int> VenueID = null, string date = null, Nullable<int> JobApplicationId = null, int type = 0, bool includeAttended = true, bool checkedIn = false, bool SearchByHomeBranch = false)
        {
            SQL objSql = new SQL();
            DataSet ds;
            objSql.AddParameter("@BranchId", DbType.Int32, ParameterDirection.Input, 0, BranchId);
            objSql.AddParameter("@VenueID", DbType.Int32, ParameterDirection.Input, 0, VenueID);
            objSql.AddParameter("@Date", DbType.Date, ParameterDirection.Input, 0, date);
            if (JobApplicationId > 0)
                objSql.AddParameter("@JobApplicationID", DbType.Int32, ParameterDirection.Input, 0, JobApplicationId);
            objSql.AddParameter("@includeAttended", DbType.Boolean, ParameterDirection.Input, 0, includeAttended);
            objSql.AddParameter("@isOrientation", DbType.Int32, ParameterDirection.Input, 0, type);
            objSql.AddParameter("@CheckedIn", DbType.Boolean, ParameterDirection.Input, 0, checkedIn);
            if (SearchByHomeBranch)
                ds = objSql.ExecuteDataSet("p_Get_ApplicantInterview_test1");
            else
                ds = objSql.ExecuteDataSet("p_Get_ApplicantInterviewByChildBranch_test1");

            return ds;
        }
        public int DeleteInterviewVenueByInterviewVenueId(int InterviewVenueId)
        {
            SQL objSql = new SQL();
            object objRowAffected;
            objSql.AddParameter("@InterviewVenueId", DbType.Int32, ParameterDirection.Input, 0, InterviewVenueId);
            objRowAffected = objSql.ExecuteScalar("p_EventScheduler_Del");
            return Convert.ToInt32(objRowAffected);
        }


        public DataSet GetJobApplicantByID(int ApplicantID)
        {
            SQL objSql = new SQL();
            DataSet ds;
            objSql.AddParameter("@ApplicantId", DbType.Int32, ParameterDirection.Input, 0, ApplicantID);
            ds = objSql.ExecuteDataSet("p_GetJobApplicantByID");
            return ds;
        }
        public DataSet GetVenueSummary(int branchId, String StartDate = null, String EndDate = null)
        {
            SQL objSql = new SQL();
            DataSet ds;
            objSql.AddParameter("@StartDate", DbType.String, ParameterDirection.Input, 0, StartDate);
            objSql.AddParameter("@EndDate", DbType.String, ParameterDirection.Input, 0, EndDate);
            objSql.AddParameter("@BranchID", DbType.Int32, ParameterDirection.Input, 0, branchId);

            ds = objSql.ExecuteDataSet("p_VenueSummaryByDate");
            return ds;
        }


        public DataSet GetApplicantBySessionId(int InterviewSessionId)
        {
            SQL objSql = new SQL();
            DataSet ds;
            objSql.AddParameter("@InterviewSessionId", DbType.Int32, ParameterDirection.Input, 0, InterviewSessionId);
            ds = objSql.ExecuteDataSet("p_GetApplicantBySessionId");
            return ds;
        }



        public DataSet GetApplicantNotAttendedInt(int BranchID, int VenueID, string VenueDate)
        {
            SQL objSql = new SQL();
            DataSet ds;

            objSql.AddParameter("@BranchID", DbType.Int32, ParameterDirection.Input, 0, BranchID);
            objSql.AddParameter("@VenueID", DbType.Int32, ParameterDirection.Input, 0, VenueID);
            objSql.AddParameter("@VenueDate", DbType.String, ParameterDirection.Input, 0, VenueDate);
            ds = objSql.ExecuteDataSet("p_ApplicantNotAttendedInt_Get");
            return ds;
        }
    }
}
