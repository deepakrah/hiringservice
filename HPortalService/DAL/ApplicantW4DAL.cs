﻿using HPortalService.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;



namespace HPortalService.DAL
{
    public class ApplicantW4DAL
    {
        public int UpdateFormW4(ApplicantW4 obj)
        {
            SQL objSql = new SQL();
            DataSet ds = new DataSet();
            objSql.AddParameter("@ApplicantId", DbType.String, ParameterDirection.Input, 0, obj.ApplicantId);
            objSql.AddParameter("@FirstName", DbType.String, ParameterDirection.Input, 0, obj.FirstName);
            objSql.AddParameter("@MiddleName", DbType.String, ParameterDirection.Input, 0, obj.MiddleName);
            objSql.AddParameter("@LastName", DbType.String, ParameterDirection.Input, 0, obj.LastName);
            objSql.AddParameter("@SSN", DbType.String, ParameterDirection.Input, 0, obj.SSN);
            objSql.AddParameter("@ZipCode", DbType.Int32, ParameterDirection.Input, 0, obj.ZipCode);
            objSql.AddParameter("@City", DbType.String, ParameterDirection.Input, 0, obj.City);
            objSql.AddParameter("@StateId", DbType.Int32, ParameterDirection.Input, 0, obj.StateId);
            objSql.AddParameter("@CountryId", DbType.Int32, ParameterDirection.Input, 0, obj.CountryId);
            objSql.AddParameter("@NoOfAllowances", DbType.Int32, ParameterDirection.Input, 0, obj.NoOfAllowances);
            objSql.AddParameter("@AdditionalAmount", DbType.Decimal, ParameterDirection.Input, 0, obj.WAdditionalAmount);
            objSql.AddParameter("@FedTaxStatus", DbType.String, ParameterDirection.Input, 0, obj.FedTaxStatus);
            objSql.AddParameter("@NameDiffer", DbType.Boolean, ParameterDirection.Input, 0, obj.NameDiffer);
            objSql.AddParameter("@Address1", DbType.String, ParameterDirection.Input, 0, obj.Address1);
            objSql.AddParameter("@Address2", DbType.String, ParameterDirection.Input, 0, obj.Address2);
            objSql.AddParameter("@Exempt", DbType.String, ParameterDirection.Input, 0, obj.Exempt);

            objSql.AddParameter("@IsMultipleJob", DbType.Boolean, ParameterDirection.Input, 0, obj.IsMultipleJob);
            objSql.AddParameter("@ClaimDepUnderAge", DbType.Int32, ParameterDirection.Input, 0, obj.ClaimDepUnderAge);
            objSql.AddParameter("@ClaimDepOther", DbType.Int32, ParameterDirection.Input, 0, obj.ClaimDepOther);
            objSql.AddParameter("@TotalClaim", DbType.Int32, ParameterDirection.Input, 0, obj.TotalClaim);
            objSql.AddParameter("@OtherIncome", DbType.Int32, ParameterDirection.Input, 0, obj.OtherIncome);
            objSql.AddParameter("@Deductions", DbType.Int32, ParameterDirection.Input, 0, obj.Deductions);
            objSql.AddParameter("@ExtraWithholding", DbType.Int32, ParameterDirection.Input, 0, obj.ExtraWithholding);
            objSql.AddParameter("@TwoJobsAmount", DbType.Int32, ParameterDirection.Input, 0, obj.TwoJobsAmount);
            objSql.AddParameter("@ThreeJobsAmountA", DbType.Int32, ParameterDirection.Input, 0, obj.ThreeJobsAmountA);
            objSql.AddParameter("@ThreeJobsAmountB", DbType.Int32, ParameterDirection.Input, 0, obj.ThreeJobsAmountB);
            objSql.AddParameter("@ThreeJobsAmountC", DbType.Int32, ParameterDirection.Input, 0, obj.ThreeJobsAmountC);
            objSql.AddParameter("@PayPeriod", DbType.Int32, ParameterDirection.Input, 0, obj.PayPeriod);
            objSql.AddParameter("@DivideAnnualAmount", DbType.Int32, ParameterDirection.Input, 0, obj.DivideAnnualAmount);
            objSql.AddParameter("@DeductionsWorksheet1", DbType.Int32, ParameterDirection.Input, 0, obj.DeductionsWorksheet1);
            objSql.AddParameter("@DeductionsWorksheet2", DbType.Int32, ParameterDirection.Input, 0, obj.DeductionsWorksheet2);
            objSql.AddParameter("@DeductionsWorksheet3", DbType.Int32, ParameterDirection.Input, 0, obj.DeductionsWorksheet3);
            objSql.AddParameter("@DeductionsWorksheet4", DbType.Int32, ParameterDirection.Input, 0, obj.DeductionsWorksheet4);
            objSql.AddParameter("@DeductionsWorksheet5", DbType.Int32, ParameterDirection.Input, 0, obj.DeductionsWorksheet5);
            return objSql.ExecuteNonQuery("p_UpdateFOrmW14_test");

        }


        public DataSet GetW4Form(ApplicantW4 obj)
        {
            SQL objSql = new SQL();
            DataSet ds = new DataSet();
            objSql.AddParameter("@ApplicantId", DbType.Int32, ParameterDirection.Input, 0, obj.ApplicantId);
            ds = objSql.ExecuteDataSet("p_GetW4FormByApplicantId");
            return ds;
        }
    }
}
