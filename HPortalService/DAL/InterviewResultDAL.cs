﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HPortalService.DAL
{
    public class InterviewResultDAL
    {
        public int SaveResult(int applicantId, int jobApplicationId, int takenBy, string result, string remarks)
        {

            SQL objSql = new SQL();
            objSql.AddParameter("@applicantId", System.Data.DbType.Int16, System.Data.ParameterDirection.Input, 0, applicantId);
            objSql.AddParameter("@jobApplicationId", System.Data.DbType.Int16, System.Data.ParameterDirection.Input, 0, jobApplicationId);

            objSql.AddParameter("@takenBy", System.Data.DbType.Int16, System.Data.ParameterDirection.Input, 0, takenBy);
            objSql.AddParameter("@result", System.Data.DbType.String, System.Data.ParameterDirection.Input, 0, result);
            objSql.AddParameter("@remarks", System.Data.DbType.String, System.Data.ParameterDirection.Input, 0, remarks);

            return objSql.ExecuteNonQuery("p_result_ins");

        }


        public bool GetResult(int jobApplicationId)
        {
            SQL objSql = new SQL();
            objSql.AddParameter("@jobApplicationId", System.Data.DbType.Int16, System.Data.ParameterDirection.Input, 0, jobApplicationId);
            return objSql.ExecuteNonQuery("p_Result_sel") > 0;

        }
    }
}
