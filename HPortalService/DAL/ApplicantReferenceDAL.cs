﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;



namespace HPortalService.DAL
{
    class ApplicantReferenceDAL
    {
        public int SaveReference(ApplicantReference obj)
        {
            SQL objSql = new SQL();
            object objContentId;
            int albumcontentId = 0;

            objSql.AddParameter("@JobApplicationId", DbType.Int32, ParameterDirection.Input, 0, obj.JobApplicationId);
            objSql.AddParameter("@Name", DbType.String, ParameterDirection.Input, 0, obj.Name);
            objSql.AddParameter("@RelationShip", DbType.String, ParameterDirection.Input, 0, obj.RelationShip);
            objSql.AddParameter("@PrimaryPhone", DbType.String, ParameterDirection.Input, 0, obj.PrimaryPhone);
            objSql.AddParameter("@AlternatePhone", DbType.String, ParameterDirection.Input, 0, obj.AlternatePhone);
            objContentId = objSql.ExecuteScalar("p_ApplicantReference_Ins");
            return albumcontentId = Convert.ToInt32(objContentId);
        }


        public DataSet GetReferenceByJobApplicantId(int JobApplicationId)
        {
            SQL objSql = new SQL();
            DataSet ds;
            objSql.AddParameter("@JobApplicationId", DbType.Int32, ParameterDirection.Input, 0, JobApplicationId);
            ds = objSql.ExecuteDataSet("p_ApplicantReference_Get");
            return ds;
        }


        public int DeleteApplicantReferenceById(int ApplicantReferenceID)
        {
            SQL objSql = new SQL();
            object objRowAffected;
            objSql.AddParameter("@ApplicantReferenceID", DbType.Int32, ParameterDirection.Input, 0, ApplicantReferenceID);
            objRowAffected = objSql.ExecuteNonQuery("p_ApplicantReference_Del");
            return Convert.ToInt32(objRowAffected);
        }

        public int UpdateReference(ApplicantReference obj)
        {
            SQL objSql = new SQL();
            object objContentId;
            objSql.AddParameter("@ApplicantReferenceID", DbType.Int32, ParameterDirection.Input, 0, obj.ApplicantReferenceID);
            objSql.AddParameter("@JobApplicationId", DbType.Int32, ParameterDirection.Input, 0, obj.JobApplicationId);
            objSql.AddParameter("@Name", DbType.String, ParameterDirection.Input, 0, obj.Name);
            objSql.AddParameter("@RelationShip", DbType.String, ParameterDirection.Input, 0, obj.RelationShip);
            objSql.AddParameter("@PrimaryPhone", DbType.String, ParameterDirection.Input, 0, obj.PrimaryPhone);
            objSql.AddParameter("@AlternatePhone", DbType.String, ParameterDirection.Input, 0, obj.AlternatePhone);
            objContentId = objSql.ExecuteScalar("p_ApplicantReference_Upd");
            return  Convert.ToInt32(objContentId);
        }

    }
}
