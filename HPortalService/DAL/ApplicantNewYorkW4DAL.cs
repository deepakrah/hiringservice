﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;


namespace HPortalService.DAL
{
    class ApplicantNewYorkW4DAL
    {
        public int IntApplicantNewYorkW4(ApplicantNewYorkW4 obj)
        {

            SQL objSql = new SQL();
            object objContentId;
            int albumcontentId = 0;
            objSql.AddParameter("@ApplicantId", DbType.Int32, ParameterDirection.Input, 0, obj.ApplicantId);
            if(!string.IsNullOrEmpty( obj.FedTaxStatus))
            objSql.AddParameter("@FedTaxStatus", DbType.String, ParameterDirection.Input, 0, obj.FedTaxStatus);
            objSql.AddParameter("@IsResidentNewYork", DbType.Boolean, ParameterDirection.Input, 0, obj.IsResidentNewYork);
            objSql.AddParameter("@IsResidentYonkers", DbType.Boolean, ParameterDirection.Input, 0, obj.IsResidentYonkers);
            if (!string.IsNullOrEmpty(obj.NumberofAllowances))
            objSql.AddParameter("@NumberofAllowances", DbType.Int32, ParameterDirection.Input, 0, obj.NumberofAllowances);
            if (!string.IsNullOrEmpty(obj.NumberofAllowancesforNewYork))
            objSql.AddParameter("@NumberofAllowancesforNewYork", DbType.Int32, ParameterDirection.Input, 0, obj.NumberofAllowancesforNewYork);
            if (!string.IsNullOrEmpty(obj.StateAmount))
                objSql.AddParameter("@StateAmount", DbType.Decimal, ParameterDirection.Input, 0, obj.StateAmount);
            if (!string.IsNullOrEmpty(obj.CityAmount))
                objSql.AddParameter("@CityAmount", DbType.Decimal, ParameterDirection.Input, 0, obj.CityAmount);
            //objSql.AddParameter("@Numberofboxes", DbType.Int32, ParameterDirection.Input, 0, obj.Numberofboxes);
            if (!string.IsNullOrEmpty(obj.YonkersAmount))
                objSql.AddParameter("@YonkersAmount", DbType.Decimal, ParameterDirection.Input, 0, obj.YonkersAmount);
            objSql.AddParameter("@IsClaimedExemption", DbType.Boolean, ParameterDirection.Input, 0, obj.IsClaimedExemption);
            objSql.AddParameter("@IsRehire", DbType.Boolean, ParameterDirection.Input, 0, obj.IsRehire);
            if (!string.IsNullOrEmpty(obj.FirstDate))
            objSql.AddParameter("@FirstDate", DbType.DateTime, ParameterDirection.Input, 0, obj.FirstDate);
            objSql.AddParameter("@IsDependent", DbType.Boolean, ParameterDirection.Input, 0, obj.IsDependent);
            if (!string.IsNullOrEmpty(obj.QualifiesDate))
            objSql.AddParameter("@QualifiesDate", DbType.DateTime, ParameterDirection.Input, 0, obj.QualifiesDate);
            if (!string.IsNullOrEmpty(obj.ApartmentNumber))
                objSql.AddParameter("@ApartmentNumber", DbType.String, ParameterDirection.Input, 0, obj.ApartmentNumber);

            if (!string.IsNullOrEmpty(obj.NumberofDependents))
                objSql.AddParameter("@NumberofDependents", DbType.Int32, ParameterDirection.Input, 0, obj.NumberofDependents);

            if (!string.IsNullOrEmpty(obj.CollegeTuitionCr))
                objSql.AddParameter("@CollegeTuitionCr", DbType.Decimal, ParameterDirection.Input, 0, obj.CollegeTuitionCr);

            if (!string.IsNullOrEmpty(obj.StateHouseholdCr))
                objSql.AddParameter("@StateHouseholdCr", DbType.Decimal, ParameterDirection.Input, 0, obj.StateHouseholdCr);

            if (!string.IsNullOrEmpty(obj.PropertyTaxCr))
                objSql.AddParameter("@PropertyTaxCr", DbType.Decimal, ParameterDirection.Input, 0, obj.PropertyTaxCr);

            if (!string.IsNullOrEmpty(obj.DependentCareCr))
                objSql.AddParameter("@DependentCareCr", DbType.Decimal, ParameterDirection.Input, 0, obj.DependentCareCr);

            if (!string.IsNullOrEmpty(obj.EarnedIncomeCr))
                objSql.AddParameter("@EarnedIncomeCr", DbType.Decimal, ParameterDirection.Input, 0, obj.EarnedIncomeCr);

            if (!string.IsNullOrEmpty(obj.StateChildCr))
                objSql.AddParameter("@StateChildCr", DbType.Decimal, ParameterDirection.Input, 0, obj.StateChildCr);

            if (!string.IsNullOrEmpty(obj.SchoolTaxCr))
                objSql.AddParameter("@SchoolTaxCr", DbType.Decimal, ParameterDirection.Input, 0, obj.SchoolTaxCr);

            if (!string.IsNullOrEmpty(obj.OtherCr))
                objSql.AddParameter("@OtherCr", DbType.Decimal, ParameterDirection.Input, 0, obj.OtherCr);

            if (!string.IsNullOrEmpty(obj.HeadofHouseHoldStatus))
                objSql.AddParameter("@HeadofHouseHoldStatus", DbType.Decimal, ParameterDirection.Input, 0, obj.HeadofHouseHoldStatus);

            if (!string.IsNullOrEmpty(obj.FederalAdjustments))
                objSql.AddParameter("@FederalAdjustments", DbType.Decimal, ParameterDirection.Input, 0, obj.FederalAdjustments);

            if (!string.IsNullOrEmpty(obj.DivideEstimate))
                objSql.AddParameter("@DivideEstimate", DbType.Decimal, ParameterDirection.Input, 0, obj.DivideEstimate);

            if (!string.IsNullOrEmpty(obj.StateTaxReturn))
                objSql.AddParameter("@StateTaxReturn", DbType.Decimal, ParameterDirection.Input, 0, obj.StateTaxReturn);

            if (!string.IsNullOrEmpty(obj.TaxpayersSpousesWorking))
                objSql.AddParameter("@TaxpayersSpousesWorking", DbType.Decimal, ParameterDirection.Input, 0, obj.TaxpayersSpousesWorking);

            if (!string.IsNullOrEmpty(obj.FederalItemizedDeductions))
                objSql.AddParameter("@FederalItemizedDeductions", DbType.Decimal, ParameterDirection.Input, 0, obj.FederalItemizedDeductions);

            if (!string.IsNullOrEmpty(obj.EstimatedStateLocalForeignTax))
                objSql.AddParameter("@EstimatedStateLocalForeignTax", DbType.Decimal, ParameterDirection.Input, 0, obj.EstimatedStateLocalForeignTax);

            if (!string.IsNullOrEmpty(obj.Subtractline20FromLine19))
                objSql.AddParameter("@Subtractline20FromLine19", DbType.Decimal, ParameterDirection.Input, 0, obj.Subtractline20FromLine19);

            if (!string.IsNullOrEmpty(obj.EstimatedCollegeTuitionDeduction))
                objSql.AddParameter("@EstimatedCollegeTuitionDeduction", DbType.Decimal, ParameterDirection.Input, 0, obj.EstimatedCollegeTuitionDeduction);

            if (!string.IsNullOrEmpty(obj.AddLines21and22))
                objSql.AddParameter("@AddLines21and22", DbType.Decimal, ParameterDirection.Input, 0, obj.AddLines21and22);

            if (!string.IsNullOrEmpty(obj.FederalFilingStatus))
                objSql.AddParameter("@FederalFilingStatus", DbType.Decimal, ParameterDirection.Input, 0, obj.FederalFilingStatus);

            if (!string.IsNullOrEmpty(obj.SubtractLine24fromLine23))
                objSql.AddParameter("@SubtractLine24fromLine23", DbType.Decimal, ParameterDirection.Input, 0, obj.SubtractLine24fromLine23);

            if (!string.IsNullOrEmpty(obj.DivideLine25))
                objSql.AddParameter("@DivideLine25", DbType.Decimal, ParameterDirection.Input, 0, obj.DivideLine25);

            if (!string.IsNullOrEmpty(obj.AmountFromLine6Above))
                objSql.AddParameter("@AmountFromLine6Above", DbType.Decimal, ParameterDirection.Input, 0, obj.AmountFromLine6Above);

            if (!string.IsNullOrEmpty(obj.AddLines15through17above))
                objSql.AddParameter("@AddLines15through17above", DbType.Decimal, ParameterDirection.Input, 0, obj.AddLines15through17above);

            if (!string.IsNullOrEmpty(obj.AddLines27and28))
                objSql.AddParameter("@AddLines27and28", DbType.Decimal, ParameterDirection.Input, 0, obj.AddLines27and28);
           
            objContentId = objSql.ExecuteScalar("p_ApplicantNewYorkW4_ins");
            return albumcontentId = Convert.ToInt32(objContentId);
        }

        public DataSet GetApplicantNewYorkW4ByID(int ApplicantId)
        {
            SQL objSql = new SQL();
            DataSet ds;
            objSql.AddParameter("@ApplicantId", DbType.Int32, ParameterDirection.Input, 0, ApplicantId);
            ds = objSql.ExecuteDataSet("p_GetApplicantNewYorkW4ByID");
            return ds;
        }
    }
}
