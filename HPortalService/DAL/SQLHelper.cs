﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace HPortalService.DAL
{
    public class SQLHelper
    {



        /// <summary>
        /// on the basis of type passed in "T", construct list from dataset
        /// </summary>




        public static List<T> ContructList<T>(DataSet ds)
        {
            List<T> objList = new List<T>();
            foreach (DataTable dt in ds.Tables)
            {
                foreach (DataRow row in dt.Rows)
                {
                    T objT;
                    Type t = typeof(T);
                    BindingFlags bflags = BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public;
                    ConstructorInfo cInfo = typeof(T).GetConstructor(bflags, null, new Type[0] { }, null);
                    if (cInfo != null)
                    {
                        objT = (T)cInfo.Invoke(null);
                    }
                    else
                        objT = Activator.CreateInstance<T>();



                    PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(T));
                    for (int col = 0; col <= dt.Columns.Count - 1; col++)
                    {
                        foreach (PropertyDescriptor prop in properties)
                        {
                            if (prop.Name.ToUpper() == dt.Columns[col].ColumnName.ToUpper() && !string.IsNullOrEmpty(Convert.ToString(row[col])))
                            {
                                var underlyingType = Nullable.GetUnderlyingType(prop.PropertyType);
                                if (underlyingType == null)
                                {
                                    prop.SetValue(objT, Convert.ChangeType(row[col], prop.PropertyType));

                                }
                                else
                                {
                                    prop.SetValue(objT, Convert.ChangeType(row[col], underlyingType));
                                }
                            }
                        }
                    }

                    objList.Add(objT);
                }
            }
            return objList;
        }

        public static List<T> ContructList<T>(DataTable dt)
        {
            List<T> objList = new List<T>();
            foreach (DataRow row in dt.Rows)
            {
                T objT;
                Type t = typeof(T);
                BindingFlags bflags = BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public;
                ConstructorInfo cInfo = typeof(T).GetConstructor(bflags, null, new Type[0] { }, null);
                if (cInfo != null)
                {
                    objT = (T)cInfo.Invoke(null);
                }
                else
                    objT = Activator.CreateInstance<T>();



                PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(T));
                for (int col = 0; col <= dt.Columns.Count - 1; col++)
                {
                    foreach (PropertyDescriptor prop in properties)
                    {
                        if (prop.Name.ToUpper() == dt.Columns[col].ColumnName.ToUpper() && !string.IsNullOrEmpty(Convert.ToString(row[col])))
                        {
                            var underlyingType = Nullable.GetUnderlyingType(prop.PropertyType);
                            if (underlyingType == null)
                            {
                                prop.SetValue(objT, Convert.ChangeType(row[col], prop.PropertyType));

                            }
                            else
                            {
                                prop.SetValue(objT, Convert.ChangeType(row[col], underlyingType));
                            }
                        }
                    }
                }

                objList.Add(objT);
            }

            return objList;
        }

        /// <summary>
        /// on the basis of type passed in "T", construct class from dataset
        /// </summary>

        public static T ContructClass<T>(DataSet ds)
        {
            T objT;

            Type t = typeof(T);
            BindingFlags bflags = BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public;
            ConstructorInfo cInfo = typeof(T).GetConstructor(bflags, null, new Type[0] { }, null);
            objT = (T)cInfo.Invoke(null);
            foreach (DataTable dt in ds.Tables)
            {
                foreach (DataRow row in dt.Rows)
                {
                    if (cInfo != null)
                    {
                        objT = (T)cInfo.Invoke(null);
                    }
                    else
                        objT = Activator.CreateInstance<T>();

                    PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(T));
                    for (int col = 0; col <= dt.Columns.Count - 1; col++)
                    {
                        foreach (PropertyDescriptor prop in properties)
                        {
                            if (prop.Name.ToUpper() == dt.Columns[col].ColumnName.ToUpper() && !string.IsNullOrEmpty(Convert.ToString(row[col])))
                            {
                                var underlyingType = Nullable.GetUnderlyingType(prop.PropertyType);
                                if (underlyingType == null)
                                {
                                    prop.SetValue(objT, Convert.ChangeType(row[col], prop.PropertyType));

                                }
                                else
                                {
                                    prop.SetValue(objT, Convert.ChangeType(row[col], underlyingType));


                                }
                            }
                        }
                    }
                }
            }

            return objT;

        }

        public static DataTable CopyDataTable(DataTable dtSource, int iRowsNeeded)
        {

            if (dtSource.Rows.Count > iRowsNeeded)
            {
                // cloned to get the structure of source
                DataTable dtDestination = dtSource.Clone();
                for (int i = 0; i < iRowsNeeded; i++)
                {
                    dtDestination.ImportRow(dtSource.Rows[i]);
                }
                return dtDestination;
            }
            else
                return dtSource;
        }

    }
}
