﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;




namespace HPortalService.DAL
{
   
  public class QuestionDAL
    {
      public DataSet GetAnswerSetByQuesId(Question obj)
        {
            SQL objSql = new SQL();
            DataSet ds = new DataSet();
            objSql.AddParameter("@QuesId", DbType.Int32, ParameterDirection.Input, 0, obj.QuesId);
            objSql.AddParameter("@AppMasterId", DbType.Int32, ParameterDirection.Input, 0, obj.AppMasterId);
            ds = objSql.ExecuteDataSet("p_GetAnswerSetByQuesId");
            return ds;

        }


      public DataSet GetAnswerSetByApplicantId(Question obj)
      {
          SQL objSql = new SQL();
          DataSet ds = new DataSet();
          objSql.AddParameter("@QuesId", DbType.Int32, ParameterDirection.Input, 0, obj.QuesId);
          objSql.AddParameter("@ApplicantID", DbType.Int32, ParameterDirection.Input, 0, obj.ApplicantId);
          ds = objSql.ExecuteDataSet("GetAnswerByQuestionID");
          return ds;

      }


      public int InsAnswerByApplicant(Question obj)
      {
          SQL objSql = new SQL();
          int insertedRowCount = 0;
          objSql.AddParameter("@ApplicantId", DbType.Int32, ParameterDirection.Input, 0, obj.ApplicantId);
          objSql.AddParameter("@AppMasterId", DbType.Int32, ParameterDirection.Input, 0, obj.AppMasterId);
          objSql.AddParameter("@QuesId", DbType.Int32, ParameterDirection.Input, 0, obj.QuesId);
          objSql.AddParameter("@AnswerId", DbType.Int32, ParameterDirection.Input, 0, obj.AnswerId);
          objSql.AddParameter("@IsSubmit", DbType.Int32, ParameterDirection.Input, 0, obj.IsSubmit);
          objSql.AddParameter("@isReAttempt", DbType.Boolean, ParameterDirection.Input, 0, obj.isReAttempt);
          return insertedRowCount = objSql.ExecuteNonQuery("p_Answer_Ins_ByApplicat_test");

      }
      public DataSet GetQuestionByCategory(int QuestionCategoryId,int ApplicantID)
      {
          SQL objSql = new SQL();
          DataSet ds = new DataSet();
          objSql.AddParameter("@QuestionCategoryId", DbType.Int32, ParameterDirection.Input, 0, QuestionCategoryId);
          objSql.AddParameter("@ApplicantId", DbType.Int32, ParameterDirection.Input, 0, ApplicantID);
          ds = objSql.ExecuteDataSet("p_Question_GetByCategory");
          return ds;
      }

      public DataSet GetFollowupQuestions(int AnswerId)
      {
          SQL objSql = new SQL();
          DataSet ds = new DataSet();
          objSql.AddParameter("@AnswerId", DbType.Int32, ParameterDirection.Input, 0, AnswerId);
          ds = objSql.ExecuteDataSet("p_GetFollowupQuestions_sel");
          return ds;
      }

  public DataSet GetLookupQuestionCategory()
      {
          SQL objSql = new SQL();
          DataSet ds = new DataSet();
          //objSql.AddParameter("@QuestionCategoryId", DbType.Int32, ParameterDirection.Input, 0, QuestionCategoryId);
          ds = objSql.ExecuteDataSet("p_LookupQuestionCategory_Get");
          return ds;
      }

        public Decimal GetCorrectAnswerCount(int QuestionCategoryId, int ApplicantID)
        {
            Decimal CorrectCount = 0;
            SQL objSql = new SQL();

            objSql.AddParameter("@QuestionCategoryId", DbType.Int32, ParameterDirection.Input, 0, QuestionCategoryId);
            objSql.AddParameter("@ApplicantID", DbType.Int32, ParameterDirection.Input, 0, ApplicantID);
            CorrectCount = Convert.ToDecimal(objSql.ExecuteScalar("p_GetCorrectAnswerByApplicantID"));
            return CorrectCount;
        }

        public Decimal GetMaxPoint()
        {
            Decimal CorrectCount = 0;
            SQL objSql = new SQL();
            CorrectCount = Convert.ToDecimal(objSql.ExecuteScalar("p_GetMaxPoint"));
            return CorrectCount;
        }
        public DataSet GetFAQuestion(int BranchId, int TaskId, int ApplicantID)
        {
            SQL objSql = new SQL();
            DataSet ds = new DataSet();
            objSql.AddParameter("@BranchId", DbType.Int32, ParameterDirection.Input, 0, BranchId);
            objSql.AddParameter("@TaskId", DbType.Int32, ParameterDirection.Input, 0, TaskId);
            ds = objSql.ExecuteDataSet("p_getFAQ");
            return ds;
        }
        public DataSet GetApplicantFAQ(int TaskId, int ApplicantID, int QuestionNo)
        {
            SQL objSql = new SQL();
            DataSet ds = new DataSet();           
            objSql.AddParameter("@TaskId", DbType.Int32, ParameterDirection.Input, 0, TaskId);
            objSql.AddParameter("@ApplicantId", DbType.Int32, ParameterDirection.Input, 0, ApplicantID);
            objSql.AddParameter("@QuestionNo", DbType.Int32, ParameterDirection.Input, 0, QuestionNo);
            ds = objSql.ExecuteDataSet("p_getApplicantFAQ");
            return ds;
        }
        public DataSet GetApplicantAllFAQ(int TaskId, int ApplicantID)
        {
            SQL objSql = new SQL();
            DataSet ds = new DataSet();
            objSql.AddParameter("@TaskId", DbType.Int32, ParameterDirection.Input, 0, TaskId);
            objSql.AddParameter("@ApplicantId", DbType.Int32, ParameterDirection.Input, 0, ApplicantID);
            ds = objSql.ExecuteDataSet("p_getApplicantAllFAQ");
            return ds;
        }
        public DataSet GetAllApplicantFAQ(int BranchId, int TaskId, int ApplicantID, int Phase)
        {
            SQL objSql = new SQL();
            DataSet ds = new DataSet();           
            objSql.AddParameter("@BranchId", DbType.Int32, ParameterDirection.Input, 0, BranchId);
            objSql.AddParameter("@TaskId", DbType.Int32, ParameterDirection.Input, 0, TaskId);
            objSql.AddParameter("@ApplicantId", DbType.Int32, ParameterDirection.Input, 0, ApplicantID);
            objSql.AddParameter("@Phase", DbType.Int32, ParameterDirection.Input, 0, Phase);
            ds = objSql.ExecuteDataSet("p_getAllApplicantFAQ");
            return ds;
        }
        public int fnInsApplicantFAQ(FAQuestion obj)
        {
            SQL objSql = new SQL();
            int insertedRowCount = 0;            
            objSql.AddParameter("@ApplicantId", DbType.Int32, ParameterDirection.Input, 0, obj.ApplicantId);
            objSql.AddParameter("@TaskId", DbType.Int32, ParameterDirection.Input, 0, obj.TaskId);
            objSql.AddParameter("@Question", DbType.String, ParameterDirection.Input, 0, obj.Question);
            objSql.AddParameter("@Answer", DbType.String, ParameterDirection.Input, 0, obj.Answer);
            objSql.AddParameter("@AnswerGivenBy", DbType.Int32, ParameterDirection.Input, 0, obj.AnswerGivenBy);
            objSql.AddParameter("@QuestionNo", DbType.Int32, ParameterDirection.Input, 0, obj.QuestionNo);
            if (!string.IsNullOrEmpty(obj.QuestionTittle))
                objSql.AddParameter("@QuestionTittle", DbType.String, ParameterDirection.Input, 0, obj.QuestionTittle);
            if (obj.IAgreed == 1)
            {
                insertedRowCount = objSql.ExecuteNonQuery("p_upApplicantFAQ");
            }
            else
            {
            insertedRowCount = objSql.ExecuteNonQuery("p_insApplicantFAQ");
            }
            return insertedRowCount;
        }
        public int fnUpApplicantFAQ(FAQuestion obj)
        {
            SQL objSql = new SQL();
            int insertedRowCount = 0;
            objSql.AddParameter("@ApplicantId", DbType.Int32, ParameterDirection.Input, 0, obj.ApplicantId);
            objSql.AddParameter("@TaskId", DbType.Int32, ParameterDirection.Input, 0, obj.TaskId);
            objSql.AddParameter("@Answer", DbType.String, ParameterDirection.Input, 0, obj.Answer);
            objSql.AddParameter("@AnswerGivenBy", DbType.Int32, ParameterDirection.Input, 0, obj.AnswerGivenBy);
            objSql.AddParameter("@QuestionNo", DbType.Int32, ParameterDirection.Input, 0, obj.QuestionNo);
            insertedRowCount = objSql.ExecuteNonQuery("p_upApplicantFAQAns");        
            return insertedRowCount;
        }
    }


}
