﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;


namespace HPortalService.DAL
{
    public class SourceDAL
    {


        public int Save(Source obj)
        {
            SQL objSql = new SQL();
            object objRowAffected;
            objSql.AddParameter("@SourceId", DbType.Int32, ParameterDirection.Input, 0, obj.SourceId);
            objSql.AddParameter("@SourceName", DbType.String, ParameterDirection.Input, 0, obj.SourceName);
            objSql.AddParameter("@promptForValue", DbType.String, ParameterDirection.Input, 0, obj.promptForValue);
            objSql.AddParameter("@BranchId", DbType.String, ParameterDirection.Input, 0, obj.BranchId);

            objRowAffected = objSql.ExecuteNonQuery("p_Source_Ins");
            return Convert.ToInt32(objRowAffected);
        }

        public DataSet Get(Source obj)
        {
            DataSet ds;

            SQL objSql = new SQL();
            objSql.AddParameter("@SourceId", DbType.Int32, ParameterDirection.Input, 0, obj.SourceId);
            objSql.AddParameter("@BranchId", DbType.Int32, ParameterDirection.Input, 0, obj.BranchId);
            ds = objSql.ExecuteDataSet("p_Source_Get");
            return ds;
        }


        public int Disable(Source obj)
        {
            object objRowAffected;
            SQL objSql = new SQL();
            objSql.AddParameter("@SourceId", DbType.Int32, ParameterDirection.Input, 0, obj.SourceId);
            objRowAffected = objSql.ExecuteNonQuery("p_Source_Deactive");
            return Convert.ToInt32(objRowAffected);
        }

        public int InsertExpense(Source obj)
        {
            object objRowAffected;
            SQL objSql = new SQL();
            objSql.AddParameter("@BranchId", DbType.Int32, ParameterDirection.Input, 0, obj.BranchId);
            objSql.AddParameter("@SourceCategoryId", DbType.Int32, ParameterDirection.Input, 0, obj.SourceId);
            objSql.AddParameter("@Amount", DbType.Decimal, ParameterDirection.Input, 0, obj.Amount);
            objSql.AddParameter("@Month", DbType.Int32, ParameterDirection.Input, 0, obj.Month);
            objSql.AddParameter("@year", DbType.Int32, ParameterDirection.Input, 0, obj.Year);
            objSql.AddParameter("@CreatedBy", DbType.Int32, ParameterDirection.Input, 0, obj.CreatedBy);
            objSql.AddParameter("@DateFrom", DbType.String, ParameterDirection.Input, 0, obj.DateFrom);
            objSql.AddParameter("@DateTo", DbType.String, ParameterDirection.Input, 0, obj.DateTo);
            objSql.AddParameter("@Method", DbType.Int32, ParameterDirection.Input, 0, obj.Method);
            objRowAffected = objSql.ExecuteNonQuery("P_SourceExpense_Insert");
            return Convert.ToInt32(objRowAffected);
        }


        public DataSet GetExpense(int? branchId, string from, string to)
        {
            DataSet ds;
            SQL objSql = new SQL();
            objSql.AddParameter("@From", DbType.String, ParameterDirection.Input, 0, from);
            objSql.AddParameter("@To", DbType.String, ParameterDirection.Input, 0, to);
            objSql.AddParameter("@BranchId", DbType.Int32, ParameterDirection.Input, 0, branchId);

            ds = objSql.ExecuteDataSet("p_SourceExpense_Get");
            return ds;
        }
        public DataSet GetSourceReport(int YEAR)
        {
            DataSet ds;
            SQL objSql = new SQL();
            objSql.AddParameter("@YEAR", DbType.Int32, ParameterDirection.Input, 0, YEAR);
            ds = objSql.ExecuteDataSet("p_getvw_SourceReport");
            return ds;
        }
    }
}
