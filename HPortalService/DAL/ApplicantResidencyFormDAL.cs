﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;


namespace HPortalService.DAL
{
    class ApplicantResidencyFormDAL
    {
        public int InsApplicantResidencyForm(ApplicantResidencyForm obj)
        {

            SQL objSql = new SQL();
            object objContentId;
            int albumcontentId = 0;
            objSql.AddParameter("@ApplicantId", DbType.Int32, ParameterDirection.Input, 0, obj.ApplicantId);
            if (!string.IsNullOrEmpty(obj.StreetAddress))
                objSql.AddParameter("@StreetAddress", DbType.String, ParameterDirection.Input, 0, obj.StreetAddress);
            if (!string.IsNullOrEmpty(obj.SecondAddress))
                objSql.AddParameter("@SecondAddress", DbType.String, ParameterDirection.Input, 0, obj.SecondAddress);
            if (!string.IsNullOrEmpty(obj.City))
                objSql.AddParameter("@City", DbType.String, ParameterDirection.Input, 0, obj.City);
            if (!string.IsNullOrEmpty(obj.STATE))
                objSql.AddParameter("@State", DbType.String, ParameterDirection.Input, 0, obj.STATE);
            if (!string.IsNullOrEmpty(obj.ZipCode))
                objSql.AddParameter("@ZipCode", DbType.String, ParameterDirection.Input, 0, obj.ZipCode);
            if (!string.IsNullOrEmpty(obj.Phone))
                objSql.AddParameter("@Phone", DbType.String, ParameterDirection.Input, 0, obj.Phone);
            if (!string.IsNullOrEmpty(obj.Municipality))
                objSql.AddParameter("@Municipality", DbType.String, ParameterDirection.Input, 0, obj.Municipality);
            if (!string.IsNullOrEmpty(obj.County))
                objSql.AddParameter("@County", DbType.String, ParameterDirection.Input, 0, obj.County);
            if (!string.IsNullOrEmpty(obj.PsdCode))
                objSql.AddParameter("@PsdCode", DbType.String, ParameterDirection.Input, 0, obj.PsdCode);
            if (!string.IsNullOrEmpty(obj.TotalResident))
                objSql.AddParameter("@TotalResident", DbType.String, ParameterDirection.Input, 0, obj.TotalResident);
            objContentId = objSql.ExecuteScalar("p_ApplicantResidencyForm_ins");
            return albumcontentId = Convert.ToInt32(objContentId);
        }

        public DataSet GetApplicantResidencyForm(int ApplicantId)
        {
            SQL objSql = new SQL();
            DataSet ds;
            objSql.AddParameter("@ApplicantId", DbType.Int32, ParameterDirection.Input, 0, ApplicantId);
            ds = objSql.ExecuteDataSet("p_getApplicantResidencyForm");
            return ds;
        }
    }
}
