﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;



namespace HPortalService.DAL
{
    class EmailLogDAL
    {

        public int SaveLog(EmailLog obj)
        {           
            SQL objSql = new SQL();       
            objSql.AddParameter("@UserId", DbType.Int32, ParameterDirection.Input, 0, obj.UserId);
            objSql.AddParameter("@ApplicantId", DbType.Int32, ParameterDirection.Input, 0, obj.ApplicantId);
                objSql.AddParameter("@Subject", DbType.String, ParameterDirection.Input, 0, obj.Subject);
                objSql.AddParameter("@Body", DbType.String, ParameterDirection.Input, 0, obj.Body);
         int i=  objSql.ExecuteNonQuery("p_EmailLog_Insert");
            return i;

        }
    }
}
