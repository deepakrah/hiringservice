﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;


namespace HPortalService.DAL
{
   public class ApplicantGeneralInfoDAL
    {
       public int UpdateApplicantGeneralInfo(ApplicantGeneralInfo obj)
       {
           SQL objSql = new SQL();
           object objRowAffected;
           objSql.AddParameter("@JobApplicationId", DbType.Int32, ParameterDirection.Input, 0, obj.JobApplicationId);
           objSql.AddParameter("@IsAdult", DbType.Boolean, ParameterDirection.Input, 0, obj.IsAdult);
           objSql.AddParameter("@SubmitVerification", DbType.Boolean, ParameterDirection.Input, 0, obj.SubmitVerification);
           objSql.AddParameter("@HiredByCSC", DbType.Boolean, ParameterDirection.Input, 0, obj.HiredByCSC);
           objSql.AddParameter("@Location", DbType.String, ParameterDirection.Input, 0, obj.Location);
           objSql.AddParameter("@StartDate", DbType.String, ParameterDirection.Input, 0, obj.StartDate == "" ? null : obj.StartDate);
           objSql.AddParameter("@EndDate", DbType.String, ParameterDirection.Input, 0, obj.EndDate == "" ? null : obj.EndDate);
           objSql.AddParameter("@RelativeCSC", DbType.Boolean, ParameterDirection.Input, 0, obj.RelativeCSC);
           objSql.AddParameter("@RelativeName", DbType.String, ParameterDirection.Input, 0, obj.RelativeName);
            objSql.AddParameter("@ForeignLanguage", DbType.Boolean, ParameterDirection.Input, 0, obj.ForeignLanguage);
           objSql.AddParameter("@LanguageName", DbType.String, ParameterDirection.Input, 0, obj.LanguageName);
           objSql.AddParameter("@AvailableDate", DbType.String, ParameterDirection.Input, 0, obj.AvailableDate != "" ? obj.AvailableDate : null);
           objSql.AddParameter("@WorkedOnNightWeekend", DbType.Boolean, ParameterDirection.Input, 0, obj.WorkedOnNightWeekend);
           objSql.AddParameter("@RellableTransport", DbType.Boolean, ParameterDirection.Input, 0, obj.RellableTransport);
           objSql.AddParameter("@PrevFirstName", DbType.String, ParameterDirection.Input, 0, obj.PrevFirstName);
           objSql.AddParameter("@PrevMiddleName", DbType.String, ParameterDirection.Input, 0, obj.PrevMiddleName);
           objSql.AddParameter("@PrevLastName", DbType.String, ParameterDirection.Input, 0, obj.PrevLastName);
           objSql.AddParameter("@Suffix", DbType.String, ParameterDirection.Input, 0, obj.Suffix);
           objSql.AddParameter("@ArmedForce", DbType.Boolean, ParameterDirection.Input, 0, obj.ArmedForce);
           objSql.AddParameter("@Rank", DbType.String, ParameterDirection.Input, 0, obj.Rank);
           objSql.AddParameter("@Duties", DbType.String, ParameterDirection.Input, 0, obj.Duties);
           objSql.AddParameter("@EmployeeID", DbType.String, ParameterDirection.Input, 0, obj.EmployeeID);
           objSql.AddParameter("@ReEmployee", DbType.Boolean, ParameterDirection.Input, 0, obj.ReEmployee);
           objSql.AddParameter("@RehireSource", DbType.Int32, ParameterDirection.Input, 0, obj.RehireSourceId);   
        
           objRowAffected = objSql.ExecuteNonQuery("p_ApplicantGeneralInfo_Ins_test");
           return Convert.ToInt32(objRowAffected);
       }
       
           
       public DataSet SearchApplicantSupport(String Fname,String Lname,String Mobile,String EmailID,String SSN)
       {
           SQL objSql = new SQL();
           DataSet ds;
           if (Fname != "")
               objSql.AddParameter("@FirstName", DbType.String, ParameterDirection.Input, 0, Fname);
           if (Lname != "")
               objSql.AddParameter("@LastName", DbType.String, ParameterDirection.Input, 0, Lname);
           if (EmailID != "")
               objSql.AddParameter("@Email", DbType.String, ParameterDirection.Input, 0, EmailID);
           if (SSN != "")
               objSql.AddParameter("@SSN", DbType.String, ParameterDirection.Input, 0, SSN);
           if (Mobile != "")
               objSql.AddParameter("@PhoneNo", DbType.String, ParameterDirection.Input, 0, Mobile);
           ds = objSql.ExecuteDataSet("p_Support_search");
           return ds;
       }

       public DataSet GetGeneralInfoByJobApplicantId(int JobApplicationId)
       {
           SQL objSql = new SQL();
           DataSet ds;
           objSql.AddParameter("@JobApplicationId", DbType.Int32, ParameterDirection.Input, 0, JobApplicationId);
           ds = objSql.ExecuteDataSet("p_GetGeneralInfoById");
           return ds;
       }
    }
}
