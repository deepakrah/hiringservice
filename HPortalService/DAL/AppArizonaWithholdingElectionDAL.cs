﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace HPortalService.DAL
{
    class AppArizonaWithholdingElectionDAL
    {
        public int IntAppArizonaWithholdingElection(AppArizonaWithholdingElection obj)
        {
            SQL objSql = new SQL();
            object objContentId;
            int albumcontentId = 0;
            objSql.AddParameter("@ApplicantId", DbType.Int32, ParameterDirection.Input, 0, obj.ApplicantId);

            objSql.AddParameter("@SSN", DbType.String, ParameterDirection.Input, 0, obj.SSN);
            objSql.AddParameter("@HomeAddress", DbType.String, ParameterDirection.Input, 0, obj.HomeAddress);
            objSql.AddParameter("@Street", DbType.String, ParameterDirection.Input, 0, obj.Street);
            objSql.AddParameter("@RuralRoute", DbType.String, ParameterDirection.Input, 0, obj.RuralRoute);
            objSql.AddParameter("@City", DbType.String, ParameterDirection.Input, 0, obj.City);
            objSql.AddParameter("@State", DbType.String, ParameterDirection.Input, 0, obj.State);

            if(!string.IsNullOrEmpty(obj.ZIPCode))
                objSql.AddParameter("@ZIPCode", DbType.Int32, ParameterDirection.Input, 0, Convert.ToInt32(obj.ZIPCode));

            objSql.AddParameter("@IsTaxableWages", DbType.Boolean, ParameterDirection.Input, 0, obj.IsTaxableWages);

            if (!string.IsNullOrEmpty(obj.TaxableWagesPer))
                objSql.AddParameter("@TaxableWagesPer", DbType.String, ParameterDirection.Input, 0, obj.TaxableWagesPer);

            if (!string.IsNullOrEmpty(obj.ExtraAmount))
            objSql.AddParameter("@ExtraAmount", DbType.Decimal, ParameterDirection.Input, 0,Convert.ToDecimal(obj.ExtraAmount));

            objSql.AddParameter("@IsElect", DbType.Boolean, ParameterDirection.Input, 0, obj.IsElect);

            objContentId = objSql.ExecuteScalar("p_AppArizonaWithholdingElection_ins");
            return albumcontentId = Convert.ToInt32(objContentId);
        }

        public DataSet GetAppArizonaWithholdingElectionByID(int ApplicantId)
        {
            SQL objSql = new SQL();
            DataSet ds;
            objSql.AddParameter("@ApplicantId", DbType.Int32, ParameterDirection.Input, 0, ApplicantId);
            ds = objSql.ExecuteDataSet("p_GetAppArizonaWithholdingElectionByID");
            return ds;
        }
    }
}
