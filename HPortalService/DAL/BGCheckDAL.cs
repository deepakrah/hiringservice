﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Globalization;

namespace HPortalService.DAL
{
    public class BGCheckDAL
    {
        public int SaveApplicantBGCheck(BGCheck obj)
        {
            SQL objSql = new SQL();
            object objRowAffected;
            if (!string.IsNullOrEmpty(obj.BGCheckId))
                objSql.AddParameter("@BGCheckId", DbType.Int32, ParameterDirection.Input, 0, Convert.ToInt32(obj.BGCheckId));
            if (!string.IsNullOrEmpty(obj.ApplicantId))
                objSql.AddParameter("@ApplicantId", DbType.Int32, ParameterDirection.Input, 0, Convert.ToInt32(obj.ApplicantId));
            objSql.AddParameter("@FirstName", DbType.String, ParameterDirection.Input, 0, obj.FirstName);
            objSql.AddParameter("@MiddleName", DbType.String, ParameterDirection.Input, 0, obj.MiddleName);
            objSql.AddParameter("@LastName", DbType.String, ParameterDirection.Input, 0, obj.LastName);
            objSql.AddParameter("@MaidenName", DbType.String, ParameterDirection.Input, 0, obj.MaidenName);
            if (!string.IsNullOrEmpty(obj.YearUsed))
                objSql.AddParameter("@YearUsed", DbType.Int32, ParameterDirection.Input, 0, Convert.ToInt32(obj.YearUsed));
            if (!string.IsNullOrEmpty(obj.YearUsedTo))
                objSql.AddParameter("@YearUsedTo", DbType.Int32, ParameterDirection.Input, 0, Convert.ToInt32(obj.YearUsedTo));
            objSql.AddParameter("@SSN", DbType.String, ParameterDirection.Input, 0, obj.SSN);

            objSql.AddParameter("@DLNumber", DbType.String, ParameterDirection.Input, 0, obj.DLNumber);
            objSql.AddParameter("@State", DbType.String, ParameterDirection.Input, 0, obj.State);
            if (!string.IsNullOrEmpty(obj.DOB))
                objSql.AddParameter("@DOB", DbType.Date, ParameterDirection.Input, 0, Convert.ToDateTime(obj.DOB).Date);

            if (!string.IsNullOrEmpty(obj.IsFelony))
                objSql.AddParameter("@IsFelony", DbType.Boolean, ParameterDirection.Input, 0, (obj.IsFelony == "True" ? 1 : 0));
            if (!string.IsNullOrEmpty(obj.IsMisdemeanor))
                objSql.AddParameter("@IsMisdemeanor", DbType.Boolean, ParameterDirection.Input, 0, (obj.IsMisdemeanor == "True" ? 1 : 0));
            objSql.AddParameter("@ConvictionNote", DbType.String, ParameterDirection.Input, 0, obj.ConvictionNote);
            if (!string.IsNullOrEmpty(obj.ConvictionDate))
                objSql.AddParameter("@ConvictionDate", DbType.Date, ParameterDirection.Input, 0, Convert.ToDateTime(obj.ConvictionDate).Date);

            if (!string.IsNullOrEmpty(obj.IsParole))
                objSql.AddParameter("@IsParole", DbType.Boolean, ParameterDirection.Input, 0, (obj.IsParole == "True" ? 1 : 0));
            if (!string.IsNullOrEmpty(obj.IsProbation))
                objSql.AddParameter("@IsProbation", DbType.Boolean, ParameterDirection.Input, 0, (obj.IsProbation == "True" ? 1 : 0));
            objSql.AddParameter("@ParoleProbationNote", DbType.String, ParameterDirection.Input, 0, obj.ParoleProbationNote);
            if (!string.IsNullOrEmpty(obj.IsPendingTrial))
                objSql.AddParameter("@IsPendingTrial", DbType.Boolean, ParameterDirection.Input, 0, (obj.IsPendingTrial == "True" ? 1 : 0));
            objSql.AddParameter("@PendingTrialNote", DbType.String, ParameterDirection.Input, 0, obj.PendingTrialNote);
            objSql.AddParameter("@PresentStreetAddress", DbType.String, ParameterDirection.Input, 0, obj.PresentStreetAddress);
            //objSql.AddParameter("@PresentCityStateZip", DbType.String, ParameterDirection.Input, 0, obj.PresentCityStateZip);

            if (!string.IsNullOrEmpty(obj.PFromDate))
                objSql.AddParameter("@PFromDate", DbType.Date, ParameterDirection.Input, 0, Convert.ToDateTime(obj.PFromDate).Date);

            if (!string.IsNullOrEmpty(obj.PToDate))
                objSql.AddParameter("@PToDate", DbType.Date, ParameterDirection.Input, 0, Convert.ToDateTime(obj.PToDate).Date);

            objSql.AddParameter("@PCity", DbType.String, ParameterDirection.Input, 0, obj.PCity);
            objSql.AddParameter("@PState", DbType.String, ParameterDirection.Input, 0, obj.PState);
            objSql.AddParameter("@PZip", DbType.String, ParameterDirection.Input, 0, obj.PZip);
            objSql.AddParameter("@isBGchkFreeCopy", DbType.Boolean, ParameterDirection.Input, 0, obj.isBGchkFreeCopy);

            objSql.AddParameter("@CrimeState", DbType.String, ParameterDirection.Input, 0, obj.CrimeState);
            objSql.AddParameter("@CrimeCity", DbType.String, ParameterDirection.Input, 0, obj.CrimeCity);
            objSql.AddParameter("@CrimeCounty", DbType.String, ParameterDirection.Input, 0, obj.CrimeCounty);
            objSql.AddParameter("@CrimeZip", DbType.String, ParameterDirection.Input, 0, obj.CrimeZip);
            objRowAffected = objSql.ExecuteScalar("p_ApplicantBGCheck_ins_test");



            if (string.IsNullOrEmpty(obj.BGCheckId))
            {
                obj.BGCheckId = objRowAffected.ToString();
            }
            if (!string.IsNullOrEmpty(obj.BGCheckAddressId1) || !string.IsNullOrEmpty(obj.PriorStreetAddress1) || !string.IsNullOrEmpty(obj.PriorCity1) || !string.IsNullOrEmpty(obj.PriorState1) ||
                !string.IsNullOrEmpty(obj.PriorZip1) ||
                !string.IsNullOrEmpty(obj.FromDate1) ||
                !string.IsNullOrEmpty(obj.ToDate1))
            {
                SaveApplicantBGCheckAddress(obj.BGCheckAddressId1, obj.BGCheckId, obj.PriorStreetAddress1, obj.PriorCity1, obj.PriorState1, obj.PriorZip1, obj.FromDate1, obj.ToDate1);
            }
            if (!string.IsNullOrEmpty(obj.BGCheckAddressId2) || !string.IsNullOrEmpty(obj.PriorStreetAddress2) || !string.IsNullOrEmpty(obj.PriorCity2) || !string.IsNullOrEmpty(obj.PriorState2) ||
                !string.IsNullOrEmpty(obj.PriorZip2) ||
                !string.IsNullOrEmpty(obj.FromDate2) ||
                !string.IsNullOrEmpty(obj.ToDate2))
            {
                SaveApplicantBGCheckAddress(obj.BGCheckAddressId2, obj.BGCheckId, obj.PriorStreetAddress2, obj.PriorCity2, obj.PriorState2, obj.PriorZip2, obj.FromDate2, obj.ToDate2);
            }
            if (!string.IsNullOrEmpty(obj.BGCheckAddressId3) || !string.IsNullOrEmpty(obj.PriorStreetAddress3) || !string.IsNullOrEmpty(obj.PriorCity3) || !string.IsNullOrEmpty(obj.PriorState3) ||
                !string.IsNullOrEmpty(obj.PriorZip3) ||
                !string.IsNullOrEmpty(obj.FromDate3) ||
                !string.IsNullOrEmpty(obj.ToDate3))
            {
                SaveApplicantBGCheckAddress(obj.BGCheckAddressId3, obj.BGCheckId, obj.PriorStreetAddress3, obj.PriorCity3, obj.PriorState3, obj.PriorZip3, obj.FromDate3, obj.ToDate3);
            }
            if (!string.IsNullOrEmpty(obj.BGCheckAddressId4) || !string.IsNullOrEmpty(obj.PriorStreetAddress4) || !string.IsNullOrEmpty(obj.PriorCity4) || !string.IsNullOrEmpty(obj.PriorState4) ||
                !string.IsNullOrEmpty(obj.PriorZip4) ||
                !string.IsNullOrEmpty(obj.FromDate4) ||
                !string.IsNullOrEmpty(obj.ToDate4))
            {
                SaveApplicantBGCheckAddress(obj.BGCheckAddressId4, obj.BGCheckId, obj.PriorStreetAddress4, obj.PriorCity4, obj.PriorState4, obj.PriorZip4, obj.FromDate4, obj.ToDate4);
            }
            if (!string.IsNullOrEmpty(obj.PriorStreetAddress5) || !string.IsNullOrEmpty(obj.PriorCity5) || !string.IsNullOrEmpty(obj.PriorState5) ||
                !string.IsNullOrEmpty(obj.PriorZip5) ||
                !string.IsNullOrEmpty(obj.FromDate5) ||
                !string.IsNullOrEmpty(obj.ToDate5))
            {
                SaveApplicantBGCheckAddress(obj.BGCheckAddressId5, obj.BGCheckId, obj.PriorStreetAddress5, obj.PriorCity5, obj.PriorState5, obj.PriorZip5, obj.FromDate4, obj.ToDate5);
            }
            if (!string.IsNullOrEmpty(obj.PriorStreetAddress6) || !string.IsNullOrEmpty(obj.PriorCity6) || !string.IsNullOrEmpty(obj.PriorState6) ||
                !string.IsNullOrEmpty(obj.PriorZip6) ||
                !string.IsNullOrEmpty(obj.FromDate1) ||
                !string.IsNullOrEmpty(obj.ToDate6))
            {
                SaveApplicantBGCheckAddress(obj.BGCheckAddressId6, obj.BGCheckId, obj.PriorStreetAddress6, obj.PriorCity6, obj.PriorState6, obj.PriorZip6, obj.FromDate6, obj.ToDate6);
            }
            if (!string.IsNullOrEmpty(obj.BGCheckAddressId7) || !string.IsNullOrEmpty(obj.PriorStreetAddress7) || !string.IsNullOrEmpty(obj.PriorCity7) || !string.IsNullOrEmpty(obj.PriorState7) ||
                !string.IsNullOrEmpty(obj.PriorZip7) ||
                !string.IsNullOrEmpty(obj.FromDate7) ||
                !string.IsNullOrEmpty(obj.ToDate7))
            {
                SaveApplicantBGCheckAddress(obj.BGCheckAddressId7, obj.BGCheckId, obj.PriorStreetAddress7, obj.PriorCity7, obj.PriorState7, obj.PriorZip7, obj.FromDate7, obj.ToDate7);
            }
            return Convert.ToInt32(objRowAffected);
        }

        public int SaveApplicantBGCheckAddress(string BGCheckAddressId, string BGCheckId, string PriorStreetAddress, string PriorCity, string PriorState, string PriorZip, string FromDate, string ToDate)
        {
            SQL objSql = new SQL();
            object objRowAffected;
            if (!string.IsNullOrEmpty(BGCheckAddressId))
                objSql.AddParameter("@BGCheckAddressId", DbType.Int32, ParameterDirection.Input, 0, Convert.ToInt32(BGCheckAddressId));
            if (!string.IsNullOrEmpty(BGCheckId))
                objSql.AddParameter("@BGCheckId", DbType.Int32, ParameterDirection.Input, 0, Convert.ToInt32(BGCheckId));
            if (!string.IsNullOrEmpty(PriorStreetAddress))
                objSql.AddParameter("@PriorStreetAddress", DbType.String, ParameterDirection.Input, 0, PriorStreetAddress);
            if (!string.IsNullOrEmpty(PriorCity))
                objSql.AddParameter("@PriorCity", DbType.String, ParameterDirection.Input, 0, PriorCity);
            if (!string.IsNullOrEmpty(PriorState))
                objSql.AddParameter("@PriorState", DbType.String, ParameterDirection.Input, 0, PriorState);
            if (!string.IsNullOrEmpty(PriorZip))
                objSql.AddParameter("@PriorZip", DbType.String, ParameterDirection.Input, 0, PriorZip);
            if (!string.IsNullOrEmpty(FromDate))
                objSql.AddParameter("@FromDate", DbType.Date, ParameterDirection.Input, 0, Convert.ToDateTime(FromDate).Date);
            if (!string.IsNullOrEmpty(ToDate))
                objSql.AddParameter("@ToDate", DbType.Date, ParameterDirection.Input, 0, Convert.ToDateTime(ToDate).Date);
            objRowAffected = objSql.ExecuteScalar("p_ApplicantBGCheckAddress_ins");
            return Convert.ToInt32(objRowAffected);
        }


        public int SaveApplicantBGCheckAddress(BGCheck obj)
        {
            SQL objSql = new SQL();
            object objRowAffected;
            if (!string.IsNullOrEmpty(obj.BGCheckAddressId))
                objSql.AddParameter("@BGCheckAddressId", DbType.Int32, ParameterDirection.Input, 0, Convert.ToInt32(obj.BGCheckAddressId));
            if (!string.IsNullOrEmpty(obj.BGCheckId))
                objSql.AddParameter("@BGCheckId", DbType.Int32, ParameterDirection.Input, 0, Convert.ToInt32(obj.BGCheckId));
            objSql.AddParameter("@PriorStreetAddress", DbType.String, ParameterDirection.Input, 0, obj.PriorStreetAddress1);
            objSql.AddParameter("@PriorCity", DbType.String, ParameterDirection.Input, 0, obj.PriorCity1);
            if (!string.IsNullOrEmpty(obj.FromDate1))
                objSql.AddParameter("@FromDate", DbType.DateTime, ParameterDirection.Input, 0, DateTime.Today);
            if (!string.IsNullOrEmpty(obj.ToDate1))
                objSql.AddParameter("@ToDate", DbType.DateTime, ParameterDirection.Input, 0, DateTime.Today);
            objRowAffected = objSql.ExecuteScalar("p_ApplicantBGCheckAddress_ins");
            return Convert.ToInt32(objRowAffected);
        }

        public DataSet GetApplicantBGCheckById(int ApplicantId)
        {
            DataSet ds;

            SQL objSql = new SQL();
            objSql.AddParameter("@ApplicantId", DbType.Int32, ParameterDirection.Input, 0, ApplicantId);
            ds = objSql.ExecuteDataSet("p_GetApplicantBGCheckById");
            return ds;
        }

        public DataSet GetBGCheckAddressById(int ApplicantId)
        {
            DataSet ds;

            SQL objSql = new SQL();
            objSql.AddParameter("@ApplicantId", DbType.Int32, ParameterDirection.Input, 0, ApplicantId);
            ds = objSql.ExecuteDataSet("p_GetBGCheckAddressById");
            return ds;
        }

        public DataSet GetApplicantBGCheckAddressById(int ApplicantId)
        {
            DataSet ds;

            SQL objSql = new SQL();
            objSql.AddParameter("@ApplicantId", DbType.Int32, ParameterDirection.Input, 0, ApplicantId);
            ds = objSql.ExecuteDataSet("p_GetApplicantBGCheckAddressById");
            return ds;
        }
        public int DeleteBgChecksAddressById(int BGCheckAddressId)
        {
            //DataSet ds;

            SQL objSql = new SQL();
            objSql.AddParameter("@BGCheckAddressId", DbType.Int32, ParameterDirection.Input, 0, BGCheckAddressId);
            // ds = objSql.ExecuteDataSet("p_deleteBGCheckAddressById");
            return objSql.ExecuteNonQuery("p_deleteBGCheckAddressById");
        }


    }
}
