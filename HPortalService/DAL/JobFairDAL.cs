﻿using System;
using System.Data;

namespace HPortalService.DAL
{
    public class JobFairDAL
    {
        public int Register(JobFair obj)
        {
            try
            {
                SQL objSql = new SQL();
                object objContentId;
                int albumcontentId = 0;

                objSql.AddParameter("@JobFairID", DbType.Int32, ParameterDirection.Input, 0, obj.JobFairID);
                objSql.AddParameter("@FirstName", DbType.String, ParameterDirection.Input, 0, obj.FirstName);
                objSql.AddParameter("@LastName", DbType.String, ParameterDirection.Input, 0, obj.LastName);
                objSql.AddParameter("@Email", DbType.String, ParameterDirection.Input, 0, obj.Email);
                objSql.AddParameter("@Phone", DbType.String, ParameterDirection.Input, 0, obj.PrimaryPhone);
                objSql.AddParameter("@Comment", DbType.String, ParameterDirection.Input, 0, obj.Comments);
                objSql.AddParameter("@SignInTime", DbType.DateTime, ParameterDirection.Input, 0, obj.SignInTime);
                objSql.AddParameter("@SignOutTime", DbType.DateTime, ParameterDirection.Input, 0, obj.SignOutTime);
                objContentId = objSql.ExecuteScalar("p_JobFairApplicant_ins");
                return albumcontentId = Convert.ToInt32(objContentId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int InsJFPApplicants(JobFair obj)
        {
            try
            {
                SQL objSql = new SQL();
                object objContentId;
                int albumcontentId = 0;
                objSql.AddParameter("@JobFairID", DbType.Int32, ParameterDirection.Input, 0, obj.JobFairID);
                objSql.AddParameter("@JFApplicantID", DbType.String, ParameterDirection.Input, 0, obj.JFApplicantID);
                objSql.AddParameter("@JFParticipantID", DbType.String, ParameterDirection.Input, 0, obj.JFParticipantID);
                objContentId = objSql.ExecuteNonQuery("p_JFPApplicants_ins");
                return albumcontentId = Convert.ToInt32(objContentId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int SignInSave(JobFair obj)
        {
            try
            {
                SQL objSql = new SQL();
                object objContentId;
                int albumcontentId = 0;               
                objSql.AddParameter("@JFApplicantID", DbType.String, ParameterDirection.Input, 0, obj.JFApplicantID);               
                objContentId = objSql.ExecuteNonQuery("p_JFApplicant_up");
                return albumcontentId = Convert.ToInt32(objContentId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int SignOutSave(JobFair obj)
        {
            try
            {
                SQL objSql = new SQL();
                object objContentId;
                int albumcontentId = 0;
                objSql.AddParameter("@JFApplicantID", DbType.String, ParameterDirection.Input, 0, obj.JFApplicantID);
                objSql.AddParameter("@JFParticipant", DbType.String, ParameterDirection.Input, 0, obj.JFParticipant.Trim(','));
                objContentId = objSql.ExecuteNonQuery("p_JFApplicant_SignOut");
                return albumcontentId = Convert.ToInt32(objContentId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataSet GetJobFairApplicant(JobFair obj)
        {
            SQL objSql = new SQL();           
            objSql.AddParameter("@JobFairID", DbType.String, ParameterDirection.Input, 0, obj.JobFairID);
            objSql.AddParameter("@PageIndex", DbType.Int32, ParameterDirection.Input, 0, obj.PageIndex);
            objSql.AddParameter("@PageCount", DbType.Int32, ParameterDirection.Input, 0, obj.PageCount);
            objSql.AddParameter("@sortCol", DbType.String, ParameterDirection.Input, 0, obj.sortCol);
            objSql.AddParameter("@SortOrder", DbType.String, ParameterDirection.Input, 0, obj.SortOrder);
            if (!string.IsNullOrEmpty(obj.FirstName))
                objSql.AddParameter("@FirstName", DbType.String, ParameterDirection.Input, 0, obj.FirstName);
            if (!string.IsNullOrEmpty(obj.LastName))
                objSql.AddParameter("@LastName", DbType.String, ParameterDirection.Input, 0, obj.LastName);
            if (!string.IsNullOrEmpty(obj.PrimaryPhone))
                objSql.AddParameter("@Phone", DbType.String, ParameterDirection.Input, 0, obj.PrimaryPhone);
            if (!string.IsNullOrEmpty(obj.Email))
                objSql.AddParameter("@Email", DbType.String, ParameterDirection.Input, 0, obj.Email);
            if (!string.IsNullOrEmpty(obj.Comments))
                objSql.AddParameter("@Comment", DbType.String, ParameterDirection.Input, 0, obj.Comments);
            if (obj.SignIn > 0)
                objSql.AddParameter("@SignIn", DbType.String, ParameterDirection.Input, 0, obj.SignIn);
            if (obj.sendMail)
                objSql.AddParameter("@sendMail", DbType.String, ParameterDirection.Input, 0, obj.sendMail);
            DataSet ds = objSql.ExecuteDataSet("p_GetJobFairApplicant");
            return ds;

        }

        public DataSet GetJobFairParticipant(JobFair obj)
        {
            SQL objSql = new SQL();
            DataSet dt;
            objSql.AddParameter("@JobFairID", DbType.String, ParameterDirection.Input, 0, obj.JobFairID);
            objSql.AddParameter("@JFApplicantID", DbType.String, ParameterDirection.Input, 0, obj.JFApplicantID);
            dt = objSql.ExecuteDataSet("p_GetJobFairParticipant");
            return dt;

        }
        //public DataSet Get(EmailTemplate objET)
        //{
        //    SQL objSql = new SQL();
        //    DataSet ds = new DataSet();
        //    objSql.AddParameter("@EmailType", DbType.String, ParameterDirection.Input, 0, objET.EmailType);
        //    objSql.AddParameter("@BranchId", DbType.String, ParameterDirection.Input, 0, objET.BranchId);
        //    ds = objSql.ExecuteDataSet("p_EmailTemplateGet");
        //    return ds;

        //}
        public DataSet GetApplicantByID(int ID)
        {
            SQL objSql = new SQL();
            DataSet ds;
            objSql.AddParameter("@JFApplicantID", DbType.String, ParameterDirection.Input, 0, ID);
            ds = objSql.ExecuteDataSet("p_GetJobFairApplicantForEMail");
            return ds;
        }
        public SendEmailUserInfo GetUserInfo(int ApplicantId)
        {
            SendEmailUserInfo objuserInfo = new SendEmailUserInfo();
            DataSet ds = GetApplicantByID(ApplicantId);
            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                objuserInfo.ApplicationNumber = ds.Tables[0].Rows[0]["JFApplicantID"].ToString();
                objuserInfo.Password = ds.Tables[0].Rows[0]["JobFairID"].ToString();
                objuserInfo.FullName = ds.Tables[0].Rows[0]["FirstName"] + " " + ds.Tables[0].Rows[0]["LastName"];
                objuserInfo.Email = ds.Tables[0].Rows[0]["Email"].ToString();
                objuserInfo.FirstName = ds.Tables[0].Rows[0]["FirstName"].ToString();
                objuserInfo.LastName = ds.Tables[0].Rows[0]["LastName"].ToString();
                objuserInfo.Comment = ds.Tables[0].Rows[0]["Comment"].ToString();
               //objuserInfo.StartTime = ds.Tables[0].Rows[0]["SignInTime"].ToString();
               //objuserInfo.EndTime = ds.Tables[0].Rows[0]["SignOutTime"].ToString();
                var SignInTime = ds.Tables[0].Rows[0]["SignInTime"];
                if (!String.IsNullOrEmpty(Convert.ToString(SignInTime)))
                {
                    objuserInfo.StartTime = GetTime(ds.Tables[0].Rows[0]["SignInTime"].ToString());
                }
                var SignOutTime = ds.Tables[0].Rows[0]["SignOutTime"];
                if (!String.IsNullOrEmpty(Convert.ToString(SignOutTime)))
                {
                    objuserInfo.EndTime = GetTime(ds.Tables[0].Rows[0]["SignOutTime"].ToString());
                }
            }
            return objuserInfo;
        }
        public string GetTime(String time)
        {
            //string str = Convert.ToDateTime(DateTime.Now).ToShortDateString() + " " + time.Substring(0, 5) + " " + time.Substring(time.Length - 2);
            DateTime objDate = Convert.ToDateTime(time);
            return objDate.ToString("HH:mm");
        }

        public DataSet GetJobFairApplicantById(JobFair obj)
        {
            SQL objSql = new SQL();
            DataSet ds;
            objSql.AddParameter("@JFApplicantID", DbType.Int32, ParameterDirection.Input, 0, obj.JFApplicantID);
            ds = objSql.ExecuteDataSet("p_GetJobFairApplicantById");
            return ds;
        }
    }
}
