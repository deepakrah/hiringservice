﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;


namespace HPortalService.DAL
{
    class ApplicantRenewalDAL
    {
        public int IntEmpRegRenewal(ApplicantRenewal obj)
        {
            SQL objSql = new SQL();
            object objContentId;
            int albumcontentId = 0;
            objSql.AddParameter("@ApplicantId", DbType.Int32, ParameterDirection.Input, 0, obj.ApplicantId);
            objSql.AddParameter("@CompanyName", DbType.String, ParameterDirection.Input, 0, obj.CompanyName);
            objSql.AddParameter("@LicenseeFileNo", DbType.String, ParameterDirection.Input, 0, obj.LicenseeFileNo);
            objSql.AddParameter("@TradeName", DbType.String, ParameterDirection.Input, 0, obj.TradeName);
            objSql.AddParameter("@Address", DbType.String, ParameterDirection.Input, 0, obj.Address);
            objSql.AddParameter("@City", DbType.String, ParameterDirection.Input, 0, obj.City);
            objSql.AddParameter("@State", DbType.String, ParameterDirection.Input, 0, obj.State);
            objSql.AddParameter("@zipcode", DbType.String, ParameterDirection.Input, 0, obj.zipcode);
            objSql.AddParameter("@DayTimePhone", DbType.String, ParameterDirection.Input, 0, obj.DayTimePhone);
            objSql.AddParameter("@FAX", DbType.String, ParameterDirection.Input, 0, obj.FAX);
            objSql.AddParameter("@Email", DbType.String, ParameterDirection.Input, 0, obj.Email);
            objSql.AddParameter("@FirstName", DbType.String, ParameterDirection.Input, 0, obj.FirstName);
            objSql.AddParameter("@MiddleName", DbType.String, ParameterDirection.Input, 0, obj.MiddleName);
            objSql.AddParameter("@LastName", DbType.String, ParameterDirection.Input, 0, obj.LastName);
            objSql.AddParameter("@Suffix", DbType.String, ParameterDirection.Input, 0, obj.Suffix);
            objSql.AddParameter("@EmployeeRegCardNo", DbType.String, ParameterDirection.Input, 0, obj.EmployeeRegCardNo);
            objSql.AddParameter("@HomeAddress", DbType.String, ParameterDirection.Input, 0, obj.HomeAddress);
            objSql.AddParameter("@Phone", DbType.String, ParameterDirection.Input, 0, obj.@Phone);
            objSql.AddParameter("@DOB", DbType.DateTime, ParameterDirection.Input, 0, obj.DOB);
            objSql.AddParameter("@SSN", DbType.String, ParameterDirection.Input, 0, obj.SSN);
            objSql.AddParameter("@EmpCity", DbType.String, ParameterDirection.Input, 0, obj.EmpCity);
            objSql.AddParameter("@EmpState", DbType.String, ParameterDirection.Input, 0, obj.EmpState);
            objSql.AddParameter("@EmpZipCode", DbType.String, ParameterDirection.Input, 0, obj.EmpZipCode);
            objSql.AddParameter("@EmpCountry", DbType.String, ParameterDirection.Input, 0, obj.EmpCountry);
            objSql.AddParameter("@ScarsMarks", DbType.String, ParameterDirection.Input, 0, obj.ScarsMarks);
            objSql.AddParameter("@Height", DbType.String, ParameterDirection.Input, 0, obj.Height);
            objSql.AddParameter("@Weight", DbType.String, ParameterDirection.Input, 0, obj.Weight);
            objSql.AddParameter("@HairColor", DbType.String, ParameterDirection.Input, 0, obj.HairColor);
            objSql.AddParameter("@EyeColor", DbType.String, ParameterDirection.Input, 0, obj.EyeColor);
            objSql.AddParameter("@FormerFirstName", DbType.String, ParameterDirection.Input, 0, obj.FormerFirstName);
            objSql.AddParameter("@FormerMiddleName", DbType.String, ParameterDirection.Input, 0, obj.FormerMiddleName);
            objSql.AddParameter("@FormerLastName", DbType.String, ParameterDirection.Input, 0, obj.FormerLastName);
            objSql.AddParameter("@IsVeteran", DbType.String, ParameterDirection.Input, 0, obj.IsVeteran);
            objSql.AddParameter("@IsAvailability", DbType.String, ParameterDirection.Input, 0, obj.IsAvailability);
            objSql.AddParameter("@IsConvicted", DbType.String, ParameterDirection.Input, 0, obj.IsConvicted);
            objSql.AddParameter("@IsCertify", DbType.String, ParameterDirection.Input, 0, obj.IsCertify);

            objContentId = objSql.ExecuteScalar("insApplicantRenewal");
            return albumcontentId = Convert.ToInt32(objContentId);
        }

        public DataSet GettApplicantRenewalByID(int ApplicantId)
        {
            SQL objSql = new SQL();
            DataSet ds;
            objSql.AddParameter("@ApplicantId", DbType.Int32, ParameterDirection.Input, 0, ApplicantId);
            ds = objSql.ExecuteDataSet("p_GetApplicantRenewalByID");
            return ds;
        }
    }
}
