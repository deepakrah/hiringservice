﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;


namespace HPortalService.DAL
{
   public  class ApplicantAddressDAL
    {
       public int UpdateApplicantAddress(ApplicantAddress obj)
        {
            SQL objSql = new SQL();
            object objRowAffected;
            objSql.AddParameter("@JobApplicationId", DbType.Int32, ParameterDirection.Input, 0, obj.JobApplicationId);
            objSql.AddParameter("@isMailingDiff", DbType.Boolean, ParameterDirection.Input, 0, obj.isMailingDiff);
            objSql.AddParameter("@AddressType", DbType.String, ParameterDirection.Input, 0, obj.AddressType);
            objSql.AddParameter("@Address", DbType.String, ParameterDirection.Input, 0, obj.Address);
            objSql.AddParameter("@Unit", DbType.String, ParameterDirection.Input, 0, obj.Unit);
            objSql.AddParameter("@City", DbType.String, ParameterDirection.Input, 0, obj.City);
            objSql.AddParameter("@ZipCode", DbType.String, ParameterDirection.Input, 0, obj.ZipCode);           
            objSql.AddParameter("@StateId", DbType.Int32, ParameterDirection.Input, 0, obj.StateId);
            objSql.AddParameter("@CountryId", DbType.Int32, ParameterDirection.Input, 0, obj.CountryId);
            objRowAffected = objSql.ExecuteNonQuery("p_ApplicantAddress_Ins");
            return Convert.ToInt32(objRowAffected);
        }



       public DataSet GetApplicantAddress(int JobApplicationID)
       { 
           DataSet ds;

              SQL objSql = new SQL();
            objSql.AddParameter("@JobApplicationId", DbType.Int32, ParameterDirection.Input, 0, JobApplicationID);
            ds = objSql.ExecuteDataSet("p_GetApplicantAddress_test");
            return ds;
       }



    }
}
