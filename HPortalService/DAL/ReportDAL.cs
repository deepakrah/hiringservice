﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace HPortalService.DAL
{
    public class ReportDAL
    {
        public DataSet GetApplicantByID_rpt(int ApplicantId, int AppTaskId)
        {
            SQL objSql = new SQL();
            objSql.AddParameter("@ApplicantId", DbType.Int32, ParameterDirection.Input, 0, ApplicantId);
            objSql.AddParameter("@ApplicantTaskId", DbType.Int32, ParameterDirection.Input, 0, AppTaskId);
            return objSql.ExecuteDataSet("p_GetApplicantByID_rpt");
        }
    }
}
