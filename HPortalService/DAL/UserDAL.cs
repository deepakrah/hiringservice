﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace HPortalService.DAL
{
    public class UserDAL
    {

        public DataTable GetSettings(string section, int userId)
        {
            SQL objSql = new SQL();
            objSql.AddParameter("@Section", DbType.String, ParameterDirection.Input, 0, section);
            objSql.AddParameter("@UserId", DbType.Int32, ParameterDirection.Input, 0, userId);
            DataSet ds = objSql.ExecuteDataSet(GETSETTINGS);
            if (ds != null && ds.Tables.Count > 0)
            {
                return ds.Tables[0];
            }
            else
                return null;

        }


        const string GETSETTINGS = "p_UserSettings_sel";

        public DataSet GetESignature(int userId)
        {
            SQL objSql = new SQL();
            objSql.AddParameter("@UserId", DbType.Int32, ParameterDirection.Input, 0, userId);
            DataSet ds = objSql.ExecuteDataSet("p_GetUserSignature");
            if (ds != null && ds.Tables.Count > 0)
            {
                return ds;
            }
            else
            { 
                return null;
            }

        }
    }
}
