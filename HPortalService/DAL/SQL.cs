﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Reflection;
namespace HPortalService.DAL
{

    public class SQL : IDisposable
    {
        SqlCommand cmd = null;
        SqlConnection sqlCon = null;
        string sqlSTR = Startup.ConnectionString;  //= ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString;
        public SQL()
        {
            InitialzeConnection();
        }

        private void InitialzeConnection()
        {

            if (sqlCon == null)
                sqlCon = new SqlConnection(sqlSTR);

            if (sqlCon.State != ConnectionState.Closed)
                sqlCon.Close();
            // sqlCon.Open();
            cmd = new SqlCommand();
            cmd.Connection = sqlCon;

        }

        public void Dispose()
        {
            if (sqlCon.State != ConnectionState.Closed)
                sqlCon.Close();
            sqlCon.Dispose();
            cmd.Dispose();
        }

        public void AddParameter(string pName, DbType type, ParameterDirection direction, int size, object value)
        {
            DbParameter p = new SqlParameter();
            p.DbType = type;
            p.ParameterName = pName;
            p.Direction = direction;
            p.Size = size;
            p.Value = value;
            cmd.Parameters.Add(p);
        }

        public DataSet ExecuteDataSet(string pName)
        {
            SetCMDName(pName);
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = cmd;
            DataSet ds = new DataSet();
            da.Fill(ds);
            OutputParameters();
            da.Dispose();
            return ds;
        }

        DbParameter[] param = null;

        public DbParameter[] OutParams
        {
            get { return param; }
        }

        private DbParameter[] OutputParameters()
        {
            param = new DbParameter[cmd.Parameters.Count];
            int i = 0;
            foreach (DbParameter dp in cmd.Parameters)
            {
                if (dp.Direction == ParameterDirection.Output)
                {
                    param[i] = dp;
                }
                i++;
            }
            return param;
        }

        private void SetCMDName(string pName)
        {
            cmd.CommandText = pName;
            cmd.CommandType = CommandType.StoredProcedure;
        }

        public IDataReader ExecuteReader(string pName)
        {
            SetCMDName(pName);
            IDataReader dr = cmd.ExecuteReader();
            OutputParameters();
            return dr;
        }

        public int ExecuteNonQuery(string pName)
        {
            SetCMDName(pName);
            cmd.Connection.Open();
            int value = cmd.ExecuteNonQuery();
            OutputParameters();
            Dispose();
            return value;
        }

        public object ExecuteScalar(string pName)
        {
            SetCMDName(pName);
            cmd.Connection.Open();
            object retvalue = cmd.ExecuteScalar();
            OutputParameters();
            Dispose();
            return retvalue;
        }

        public static List<T> ContructList<T>(DataSet ds)
        {
            List<T> objList = new List<T>();
            foreach (DataTable dt in ds.Tables)
            {
                foreach (DataRow row in dt.Rows)
                {
                    T objT;
                    Type t = typeof(T);
                    BindingFlags bflags = BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public;
                    ConstructorInfo cInfo = typeof(T).GetConstructor(bflags, null, new Type[0] { }, null);
                    if (cInfo != null)
                    {
                        objT = (T)cInfo.Invoke(null);
                    }
                    else
                        objT = Activator.CreateInstance<T>();



                    PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(T));
                    for (int col = 0; col <= dt.Columns.Count - 1; col++)
                    {
                        foreach (PropertyDescriptor prop in properties)
                        {
                            //if (prop.Name.ToUpper() == dt.Columns[col].ColumnName.ToUpper() && !string.IsNullOrEmpty(Convert.ToString(row[col])))
                            //{
                            //    prop.SetValue(objT, Convert.ChangeType(row[col], prop.PropertyType));
                            //}

                            if (prop.Name.ToUpper() == dt.Columns[col].ColumnName.ToUpper() && !string.IsNullOrEmpty(Convert.ToString(row[col])))
                            {
                                var underlyingType = Nullable.GetUnderlyingType(prop.PropertyType);
                                if (underlyingType == null)
                                {
                                    prop.SetValue(objT, Convert.ChangeType(row[col], prop.PropertyType));

                                }
                                else
                                {
                                    prop.SetValue(objT, Convert.ChangeType(row[col], underlyingType));


                                }
                            }
                        }
                    }

                    objList.Add(objT);
                }
            }
            ds.Dispose();
            return objList;
        }

        public static T ContructClass<T>(DataSet ds)
        {
            T objT;

            Type t = typeof(T);
            BindingFlags bflags = BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public;
            ConstructorInfo cInfo = typeof(T).GetConstructor(bflags, null, new Type[0] { }, null);
            objT = (T)cInfo.Invoke(null);
            foreach (DataTable dt in ds.Tables)
            {
                foreach (DataRow row in dt.Rows)
                {
                    if (cInfo != null)
                    {
                        objT = (T)cInfo.Invoke(null);
                    }
                    else
                        objT = Activator.CreateInstance<T>();



                    PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(T));
                    for (int col = 0; col <= dt.Columns.Count - 1; col++)
                    {
                        foreach (PropertyDescriptor prop in properties)
                        {
                            //if (prop.Name.ToUpper() == dt.Columns[col].ColumnName.ToUpper() && !string.IsNullOrEmpty(Convert.ToString(row[col])))
                            //{
                            //    prop.SetValue(objT, Convert.ChangeType(row[col], prop.PropertyType));
                            //}
                            if (prop.Name.ToUpper() == dt.Columns[col].ColumnName.ToUpper() && !string.IsNullOrEmpty(Convert.ToString(row[col])))
                            {
                                var underlyingType = Nullable.GetUnderlyingType(prop.PropertyType);
                                if (underlyingType == null)
                                {
                                    prop.SetValue(objT, Convert.ChangeType(row[col], prop.PropertyType));

                                }
                                else
                                {
                                    prop.SetValue(objT, Convert.ChangeType(row[col], underlyingType));


                                }
                            }
                        }
                    }
                }
            }
 ds.Dispose();
            return objT;

        }

        /// <summary>
        /// execute Batch which performing some DML operations
        /// </summary>
        public int ExecuteBatchQuery(string pName, DataTable dt)
        {
            try
            {
                InitialzeConnection();
                SetCMDName(pName);
                cmd.Parameters.AddWithValue("@tblJobApplication", dt);
                int value = cmd.ExecuteNonQuery();
                OutputParameters();
                // Dispose();
                return value;
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                Dispose();
            }
        }      

        /// <summary>
        /// calling this method which return only one value
        /// </summary>
        /// 

        public int ExecuteBulkInsert(IEnumerable<Type> objList, String TableName)
        {


            //using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["SomeConnectionString"].ConnectionString))
            using(var connection = new  SqlConnection(sqlSTR))
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();

                using (var bulkCopy = new SqlBulkCopy(connection, SqlBulkCopyOptions.Default, transaction))
                {
                    bulkCopy.BatchSize = 100;
                    bulkCopy.DestinationTableName = TableName;
                    try
                    {
                        bulkCopy.WriteToServer(objList.AsDataTable());
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        connection.Close();
                    }
                }

                transaction.Commit();
            }
            return 1;
        }      


    }
    public static class IEnumerableExtensions
    {
        public static DataTable AsDataTable<T>(this IEnumerable<T> data)
        {
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(T));
            var table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            foreach (T item in data)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                table.Rows.Add(row);
            }
            return table;
        }
    }
}