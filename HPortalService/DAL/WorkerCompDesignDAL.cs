﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace HPortalService.DAL
{
   public class WorkerCompDesignDAL
    {
       public int insApplicantCompDesign(ApplicantCompDesign obj)
        {
            SQL objSql = new SQL();
            object objContentId;
            int albumcontentId = 0;
            objSql.AddParameter("@ApplicantId", DbType.Int32, ParameterDirection.Input, 0, obj.ApplicantId);
            objSql.AddParameter("@Physician", DbType.String, ParameterDirection.Input, 0, obj.Physician);
            objSql.AddParameter("@Address", DbType.String, ParameterDirection.Input, 0, obj.Address);
            objSql.AddParameter("@Phone", DbType.String, ParameterDirection.Input, 0, obj.Phone);
            objSql.AddParameter("@City", DbType.String, ParameterDirection.Input, 0, obj.City);
            objSql.AddParameter("@State", DbType.String, ParameterDirection.Input, 0, obj.State);
            objSql.AddParameter("@Zip", DbType.String, ParameterDirection.Input, 0, obj.Zip);
            objContentId = objSql.ExecuteScalar("p_insApplicantCompDesign");
            return albumcontentId = Convert.ToInt32(objContentId);
        }

        public DataSet GetApplicantCompDesignByID(int ID)
        {
            SQL objSql = new SQL();
            DataSet ds;
            objSql.AddParameter("@ApplicantId", DbType.String, ParameterDirection.Input, 0, ID);
            ds = objSql.ExecuteDataSet("p_GetApplicantCompDesignByID");
            return ds;
        }
    }
}
