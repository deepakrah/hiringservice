﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;



namespace HPortalService.DAL
{
    class CallLogDAL
    {
        public int SaveLog(CallLog obj)
        {
            SQL objSql = new SQL();
            objSql.AddParameter("@CallAction", DbType.String, ParameterDirection.Input, 0, obj.CallAction);
            objSql.AddParameter("@ApplicantId", DbType.Int32, ParameterDirection.Input, 0, obj.ApplicantId);
            objSql.AddParameter("@Remarks", DbType.String, ParameterDirection.Input, 0, obj.Remarks);
            objSql.AddParameter("@UpdatedBy", DbType.Int32, ParameterDirection.Input, 0, obj.UpdatedBy);
            int i = objSql.ExecuteNonQuery("p_CallLog_Ins");
            return i;

        }

        public DataSet GetLog(int ApplicantId)
        {
            SQL objSql = new SQL();
            objSql.AddParameter("@ApplicantId", DbType.Int32, ParameterDirection.Input, 0, ApplicantId);
            DataSet ds = objSql.ExecuteDataSet("p_CallLog_Get");
           return ds;
        }

    }
}
