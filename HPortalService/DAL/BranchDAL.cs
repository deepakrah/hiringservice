﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;




namespace HPortalService.DAL
{
    class BranchDAL
    {

        public DataSet GetBranchCoordinatate()
        {
            SQL objSql = new SQL();
            DataSet ds = new DataSet();
            return objSql.ExecuteDataSet("p_GetBranchCoordinate");
        }

        public int SaveBranchInterview(Branch obj)
        {
            SQL objSql = new SQL();
            DataSet ds = new DataSet();
            if (obj.BranchId > 0)
            {
                objSql.AddParameter("@BranchId", DbType.Int32, ParameterDirection.Input, 0, obj.BranchId);
                objSql.AddParameter("@Enable", DbType.Boolean, ParameterDirection.Input, 0, obj.EnableOnline);
                objSql.AddParameter("@OnlineInterview", DbType.String, ParameterDirection.Input, 0, obj.OnlineInterview);
                return objSql.ExecuteNonQuery("p_BranchInterview_Ins");
            }
            return 0;
        }
        public int SaveBranchTask(Branch obj)
        {
            SQL objSql = new SQL();
            DataSet ds = new DataSet();

            if (obj.BranchId > 0)
                objSql.AddParameter("@BranchId", DbType.Int32, ParameterDirection.Input, 0, obj.BranchId);
            if (obj.TaskId > 0)
                objSql.AddParameter("@TaskId", DbType.Int32, ParameterDirection.Input, 0, obj.TaskId);
            if (obj.OrderNo > 0)
                objSql.AddParameter("@OrderNo", DbType.Int32, ParameterDirection.Input, 0, obj.OrderNo);
            if (obj.TaskLevel > 0)
                objSql.AddParameter("@TaskLevel", DbType.Int32, ParameterDirection.Input, 0, obj.TaskLevel);
            if (obj.IsExist)
                objSql.AddParameter("@IsExist", DbType.Boolean, ParameterDirection.Input, 0, obj.IsExist);
            if (obj.Phase > 0)
                objSql.AddParameter("@Phase", DbType.Int32, ParameterDirection.Input, 0, obj.Phase);


            //ds = objSql.ExecuteDataSet("p_BranchTask_Ins");
            return objSql.ExecuteNonQuery("p_BranchTask_Ins");

        }
        public DataSet GetMangeBranchTask(Branch obj)
        {
            SQL objSql = new SQL();
            DataSet ds = new DataSet();

            if (obj.BranchId > 0)
                objSql.AddParameter("@BranchId", DbType.Int32, ParameterDirection.Input, 0, obj.BranchId);
            if (obj.TaskId > 0)
                objSql.AddParameter("@TaskId", DbType.Int32, ParameterDirection.Input, 0, obj.TaskId);
            ds = objSql.ExecuteDataSet("p_MangeBranchTask");
            return ds;

        }

        public DataSet GetBranchInterview(int BranchId)
        {
            SQL objSql = new SQL();
            DataSet ds = new DataSet();

            if (BranchId > 0)
                objSql.AddParameter("@BranchId", DbType.Int32, ParameterDirection.Input, 0, BranchId);
            ds = objSql.ExecuteDataSet("p_GetBranchInterview");
            return ds;
        }

        public DataSet GetBranchTask(Branch obj)
        {
            SQL objSql = new SQL();
            DataSet ds = new DataSet();

            if (obj.BranchId > 0)
                objSql.AddParameter("@BranchId", DbType.Int32, ParameterDirection.Input, 0, obj.BranchId);
            if (obj.TaskId > 0)
                objSql.AddParameter("@TaskId", DbType.Int32, ParameterDirection.Input, 0, obj.TaskId);
            ds = objSql.ExecuteDataSet("p_GetBranchTask");
            return ds;

        }
        public DataSet GetBranchAllTask(Branch obj)
        {
            SQL objSql = new SQL();
            DataSet ds = new DataSet();

            if (obj.BranchId > 0)
                objSql.AddParameter("@BranchId", DbType.Int32, ParameterDirection.Input, 0, obj.BranchId);
            if (obj.TaskId > 0)
                objSql.AddParameter("@TaskId", DbType.Int32, ParameterDirection.Input, 0, obj.TaskId);
            ds = objSql.ExecuteDataSet("p_GetBranchAllTask");
            return ds;
        }
        public DataSet GetQuestionByBranchId(Branch obj)
        {
            SQL objSql = new SQL();
            DataSet ds = new DataSet();


            objSql.AddParameter("@BranchId", DbType.Int32, ParameterDirection.Input, 0, obj.BranchId);
            objSql.AddParameter("@ApplicantId", DbType.Int32, ParameterDirection.Input, 0, obj.ApplicantId);

            objSql.AddParameter("@ZipCode", DbType.Int32, ParameterDirection.Input, 0, obj.ZipCode);

            ds = objSql.ExecuteDataSet("p_GetQuestionByBranchId");
            return ds;

        }

        public DataSet GetInterviewVenues(int branchId)
        {
            SQL objSql = new SQL();
            DataSet ds = new DataSet();
            objSql.AddParameter("@BranchId", DbType.Int32, ParameterDirection.Input, 0, branchId);
            ds = objSql.ExecuteDataSet("p_GetInterviewVenuesForScheduling");
            return ds;
        }
        public DataSet GetWorkingYear(int branchId)
        {
            SQL objSql = new SQL();
            DataSet ds = new DataSet();
            objSql.AddParameter("@BranchId", DbType.Int32, ParameterDirection.Input, 0, branchId);
            ds = objSql.ExecuteDataSet("p_GetWorkingYear");
            return ds;
        }



        public DataSet GetAllSouceCodes(int branchId)
        {
            SQL objSql = new SQL();
            DataSet ds = new DataSet();
            objSql.AddParameter("@BranchId", DbType.Int32, ParameterDirection.Input, 0, branchId);
            ds = objSql.ExecuteDataSet("p_BranchSourceCodes_selall");
            return ds;
        }

        public DataSet GetSourceCode(string branchsourceCodeId)
        {
            SQL objSql = new SQL();
            DataSet ds = new DataSet();
            objSql.AddParameter("@BranchSourceCodeId", DbType.String, ParameterDirection.Input, 0, branchsourceCodeId);
            ds = objSql.ExecuteDataSet("p_BranchSourceCodes_sel");
            return ds;
        }

        public DataSet GetBranchSourceByBranchId(string branchId)
        {
            SQL objSql = new SQL();
            DataSet ds = new DataSet();
            objSql.AddParameter("@BranchId", DbType.String, ParameterDirection.Input, 0, branchId);
            ds = objSql.ExecuteDataSet("p_GetBranchSource");
            return ds;
        }
        public DataSet GetAllBranch()
        {
            SQL objSql = new SQL();
            DataSet ds = new DataSet();
            ds = objSql.ExecuteDataSet("p_GetAllBranch");
            return ds;
        }
        public DataSet GetBranchByUserId(int UserId)
        {
            SQL objSql = new SQL();
            DataSet ds = new DataSet();
            objSql.AddParameter("@UserId", DbType.Int32, ParameterDirection.Input, 0, UserId);
            ds = objSql.ExecuteDataSet("p_SharedBranches_Get_test");
            return ds;
        }

        public DataSet GetAuditBranchByUserId(int UserId)
        {
            SQL objSql = new SQL();
            DataSet ds = new DataSet();
            objSql.AddParameter("@UserId", DbType.Int32, ParameterDirection.Input, 0, UserId);
            ds = objSql.ExecuteDataSet("p_AuditSharedBranches_Get");
            return ds;
        }
        public string GetEmailId(int BranchId)
        {
            SQL objSql = new SQL();
            DataSet ds = new DataSet();
            string branchSourceId = Security.Encrypt(Guid.NewGuid().ToString());
            objSql.AddParameter("@BranchId", DbType.Int32, ParameterDirection.Input, 0, BranchId);
            return objSql.ExecuteScalar("p_getEmailId_byBranch").ToString();

        }

        public DataTable GetBranchDetails(int BranchId)
        {
            SQL objSql = new SQL();
            DataSet ds = new DataSet();
            string branchSourceId = Security.Encrypt(Guid.NewGuid().ToString());
            objSql.AddParameter("@BranchId", DbType.Int32, ParameterDirection.Input, 0, BranchId);
            return objSql.ExecuteDataSet("p_GetBranchDetails").Tables[0];
        }
        public int UpdateBranch(int branchId, string emailId, string Address1, string CityName, string state, string Country, string Zip,string primaryPhone, string HiringManager, string Designation, string PayRate)
        {
            SQL objSql = new SQL();
            objSql.AddParameter("@BranchId", DbType.Int32, ParameterDirection.Input, 0, branchId);
            objSql.AddParameter("@Email", DbType.String, ParameterDirection.Input, 0, emailId);
            objSql.AddParameter("@Address1", DbType.String, ParameterDirection.Input, 0, Address1);
            objSql.AddParameter("@CityName", DbType.String, ParameterDirection.Input, 0, CityName);
            objSql.AddParameter("@State", DbType.String, ParameterDirection.Input, 0, state);
            objSql.AddParameter("@Country", DbType.String, ParameterDirection.Input, 0, Country);
            objSql.AddParameter("@Zip", DbType.String, ParameterDirection.Input, 0, Zip);
            objSql.AddParameter("@Phone1", DbType.String, ParameterDirection.Input, 0, primaryPhone);
            objSql.AddParameter("@HiringManager", DbType.String, ParameterDirection.Input, 0, HiringManager);
            objSql.AddParameter("@Designation", DbType.String, ParameterDirection.Input, 0, Designation);
            objSql.AddParameter("@PayRate", DbType.String, ParameterDirection.Input, 0, PayRate);
            return objSql.ExecuteNonQuery("p_updateBranch");
        }
        public int AddCode(int sourceCategory, int branchId, string source, int ceatedBy)
        {
            SQL objSql = new SQL();
            DataSet ds = new DataSet();
            string branchSourceId = Security.Encrypt(Guid.NewGuid().ToString());
            objSql.AddParameter("@BranchSourceId", DbType.String, ParameterDirection.Input, 0, branchSourceId);
            objSql.AddParameter("@SourceCategory", DbType.Int32, ParameterDirection.Input, 0, sourceCategory);
            objSql.AddParameter("@Source", DbType.String, ParameterDirection.Input, 0, source);
            objSql.AddParameter("@BranchId", DbType.Int32, ParameterDirection.Input, 0, branchId);
            objSql.AddParameter("@CratedBy", DbType.Int32, ParameterDirection.Input, 0, ceatedBy);

            return objSql.ExecuteNonQuery("p_BranchSourceCodes_ins");
        }
        public DataSet GetSourceCategory(int branchId)
        {
            SQL objSql = new SQL();
            DataSet ds = new DataSet();
            objSql.AddParameter("@BranchId", DbType.Int32, ParameterDirection.Input, 0, branchId);

            return objSql.ExecuteDataSet("p_SourceCategory_sel");
        }

        public DataSet GetBranchInfo(int branchId)
        {
            SQL objSql = new SQL();
            DataSet ds = new DataSet();
            objSql.AddParameter("@BranchId", DbType.Int32, ParameterDirection.Input, 0, branchId);
            return objSql.ExecuteDataSet("p_GetBranchInfo");
        }


        public DataSet GetAllBranches()
        {
            SQL objSql = new SQL();
            DataSet ds = new DataSet();

            return objSql.ExecuteDataSet("p_Branch_selAll");
        }

        public void AddSouceCategory(int branchId, int sourceCategoryId, Boolean ShowOnWeb)
        {
            SQL objSql = new SQL();
            DataSet ds = new DataSet();
            objSql.AddParameter("@SourceCategory", DbType.String, ParameterDirection.Input, 0, sourceCategoryId);
            objSql.AddParameter("@BranchId", DbType.String, ParameterDirection.Input, 0, branchId);
            objSql.AddParameter("@ShowOnWeb", DbType.Boolean, ParameterDirection.Input, 0, ShowOnWeb);

            objSql.ExecuteNonQuery("p_AddSouceCategory");
        }

        public void AddLookupSouceCategory(string Name, string GroupName)
        {
            SQL objSql = new SQL();
            DataSet ds = new DataSet();
            objSql.AddParameter("@Name", DbType.String, ParameterDirection.Input, 0, Name);
            objSql.AddParameter("@GroupName", DbType.String, ParameterDirection.Input, 0, GroupName);
            objSql.ExecuteNonQuery("p_addSourceCategory");
        }


        public DataSet GetAllSourceCategory()
        {
            SQL objSql = new SQL();
            return objSql.ExecuteDataSet("p_AllSourceCategory_sel");
        }


        public DataSet GetSourceCategoryGroup()
        {
            SQL objSql = new SQL();
            return objSql.ExecuteDataSet("GetSourceCategoryGroup");
        }

        public int UpdateEmail(int branchId, string emailId)
        {
            SQL objSql = new SQL();
            objSql.AddParameter("@BranchId", DbType.Int32, ParameterDirection.Input, 0, branchId);
            objSql.AddParameter("@Email", DbType.String, ParameterDirection.Input, 0, emailId);
            return objSql.ExecuteNonQuery("p_updateBranchDefautlEmail");
        }

        public DataSet GetBranchIsTask(int BranchId)
        {
            SQL objSql = new SQL();
            DataSet ds;
            objSql.AddParameter("@BranchId", DbType.Int32, ParameterDirection.Input, 0, BranchId);
            ds = objSql.ExecuteDataSet("p_GetBranchIsTask");
            return ds;
        }

        public DataSet GetBranch(int ID)
        {
            SQL objSql = new SQL();
            DataSet ds;
            objSql.AddParameter("@BranchId", DbType.String, ParameterDirection.Input, 0, ID);
            return objSql.ExecuteDataSet("p_GetBranch");
        }

        public DataSet GetJobPositionByBranchId(int BranchId, int JobPositionId)
        {
            SQL objSql = new SQL();
            DataSet ds;
            if (BranchId > 0)
                objSql.AddParameter("@BranchId", DbType.Int32, ParameterDirection.Input, 0, BranchId);
            if (JobPositionId > 0)
                objSql.AddParameter("@JobPositionId", DbType.Int32, ParameterDirection.Input, 0, JobPositionId);
            ds = objSql.ExecuteDataSet("p_GetJobPositionByBranchId");
            return ds;
        }

        public int UpJobPosition(int ApplicantId, string JobPositionId)
        {
            SQL objSql = new SQL();
            objSql.AddParameter("@ApplicantId", DbType.String, ParameterDirection.Input, 0, ApplicantId);
            objSql.AddParameter("@JobPositionId", DbType.String, ParameterDirection.Input, 0, JobPositionId);
            return objSql.ExecuteNonQuery("p_JobApplication_upd");
        }
    }
}
