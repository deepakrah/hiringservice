﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;


namespace HPortalService.DAL
{
    public class BranchSettingDAL
    {
        public DataSet GetBranchSetting(int BranchId)
        {
            DataSet ds;

            SQL objSql = new SQL();
            objSql.AddParameter("@BranchId", DbType.Int32, ParameterDirection.Input, 0, BranchId);
            ds = objSql.ExecuteDataSet("GetBranchSettings");
            return ds;
        }

        public int UpdateBranchSetting(BranchSetting obj)
        {
            SQL objSql = new SQL();
            object objRowAffected;
            objSql.AddParameter("@BranchId", DbType.Int32, ParameterDirection.Input, 0, obj.BranchId);
            objSql.AddParameter("@SettingKey", DbType.String, ParameterDirection.Input, 0, obj.Key);
            objSql.AddParameter("@SettingValue", DbType.String, ParameterDirection.Input, 0, obj.Value);
            objSql.AddParameter("@Section", DbType.String, ParameterDirection.Input, 0, obj.Section);

            objRowAffected = objSql.ExecuteNonQuery("UpdateBranchSetting");
            return Convert.ToInt32(objRowAffected);
        }

        public int SaveJobPosition(string JobPositionName, decimal PayRate, int BranchId, int JobPositionId, string JobType, string Description, string PayrateDescription)
        {
            SQL objSql = new SQL();
            objSql.AddParameter("@JobPosition", DbType.String, ParameterDirection.Input, 0, JobPositionName);
            objSql.AddParameter("@Payrate", DbType.Decimal, ParameterDirection.Input, 0, PayRate);
            objSql.AddParameter("@BranchId", DbType.Int32, ParameterDirection.Input, 0, BranchId);
            objSql.AddParameter("@JobType", DbType.String, ParameterDirection.Input, 0, JobType);
            objSql.AddParameter("@Description", DbType.String, ParameterDirection.Input, 0, Description);
            objSql.AddParameter("@PayrateDescription", DbType.String, ParameterDirection.Input, 0, PayrateDescription);
            if (JobPositionId > 0)
                objSql.AddParameter("@JobPositionId", DbType.Int32, ParameterDirection.Input, 0, JobPositionId);
            return Convert.ToInt32(objSql.ExecuteNonQuery("p_JobPosition_ins"));
        }

        public int UpdateJobPosition(int JobPositionId, int Status)
        {
            SQL objSql = new SQL();
            objSql.AddParameter("@JobPositionId", DbType.Int32, ParameterDirection.Input, 0, JobPositionId);
            objSql.AddParameter("@Status", DbType.Int32, ParameterDirection.Input, 0, Status);
            return Convert.ToInt32(objSql.ExecuteNonQuery("p_UpdatePositionStatus"));
        }

        public DataSet GetAllPositionByBranchId(int BranchId)
        {
            DataSet ds;
            SQL objSql = new SQL();
            objSql.AddParameter("@BranchId", DbType.Int32, ParameterDirection.Input, 0, BranchId);
            ds = objSql.ExecuteDataSet("p_GetAllPositionByBranchId");
            return ds;
        }
    }
}
