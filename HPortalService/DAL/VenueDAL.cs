﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;



namespace HPortalService.DAL
{
    public class VenueDAL
    {
        public DataSet GetVenueByBranchId(int BranchId)
        {
            SQL objSql = new SQL();
            DataSet ds;
            objSql.AddParameter("@BranchId", DbType.Int32, ParameterDirection.Input, 0, BranchId);
            ds = objSql.ExecuteDataSet("p_Venue_Get");
            return ds;
        }

        public DataSet GetVenueMapByVenueId(int VenueId)
        {
            SQL objSql = new SQL();
            DataSet ds;
            objSql.AddParameter("@VenueId", DbType.Int32, ParameterDirection.Input, 0, VenueId);
            ds = objSql.ExecuteDataSet("p_GetVenueMapByVenueId");
            return ds;
        }
        public DataSet GetInterviewVenuesById(int InterviewVenueId)
        {
            SQL objSql = new SQL();
            DataSet ds;
            objSql.AddParameter("@InterviewVenueId", DbType.Int32, ParameterDirection.Input, 0, InterviewVenueId);
            ds = objSql.ExecuteDataSet("p_InterviewVenuesById");
            return ds;
        }
    }
}
