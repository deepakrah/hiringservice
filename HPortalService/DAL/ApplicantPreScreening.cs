﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;


namespace HPortalService.DAL
{
    class ApplicantPreScreeningDAL
    {
        public int InsApplicantPreScreening(ApplicantPreScreening obj)
        {

            SQL objSql = new SQL();
            object objContentId;
            int albumcontentId = 0;
            objSql.AddParameter("@ApplicantId", DbType.Int32, ParameterDirection.Input, 0, obj.ApplicantId);
            objSql.AddParameter("@County", DbType.String, ParameterDirection.Input, 0, obj.County);
            objSql.AddParameter("@Telephone", DbType.String, ParameterDirection.Input, 0, obj.Telephone);
            objSql.AddParameter("@StateWorkforceAgency", DbType.String, ParameterDirection.Input, 0, obj.StateWorkforceAgency);

            objContentId = objSql.ExecuteScalar("p_ApplicantPreScreening_ins_test");
            return albumcontentId = Convert.ToInt32(objContentId);
        }

        public DataSet GetApplicantPreScreening(int ApplicantId)
        {
            SQL objSql = new SQL();
            DataSet ds;
            objSql.AddParameter("@ApplicantId", DbType.Int32, ParameterDirection.Input, 0, ApplicantId);
            ds = objSql.ExecuteDataSet("p_getApplicantPreScreening");
            return ds;
        }
    }
}
