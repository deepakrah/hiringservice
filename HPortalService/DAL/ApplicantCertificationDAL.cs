﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;



namespace HPortalService.DAL
{
    class ApplicantCertificationDAL
    {

        public int SaveCertificate(ApplicantCertification obj)
        {
            SQL objSql = new SQL();
            object objContentId;
            int albumcontentId = 0;

            objSql.AddParameter("@JobApplicationId", DbType.Int32, ParameterDirection.Input, 0, obj.JobApplicationId);
            objSql.AddParameter("@SecurityLicence", DbType.Boolean, ParameterDirection.Input, 0, obj.SecurityLicence);
            objSql.AddParameter("@LicenseType", DbType.String, ParameterDirection.Input, 0, obj.LicenseType);
            objSql.AddParameter("@LicenseNumber", DbType.String, ParameterDirection.Input, 0, obj.LicenseNumber);
            objSql.AddParameter("@Agency", DbType.String, ParameterDirection.Input, 0, obj.Agency);
            objSql.AddParameter("@IssueMonth", DbType.String, ParameterDirection.Input, 0, obj.IssueMonth);
            objSql.AddParameter("@IssueYear", DbType.Int32, ParameterDirection.Input, 0, obj.IssueYear);
            objSql.AddParameter("@ExpirationMonth", DbType.String, ParameterDirection.Input, 0, obj.ExpirationMonth);
            objSql.AddParameter("@ExpirationYear", DbType.Int32, ParameterDirection.Input, 0, obj.ExpirationYear);
            objSql.AddParameter("@Description", DbType.String, ParameterDirection.Input, 0, obj.Description);
            objContentId = objSql.ExecuteNonQuery("p_ApplicantCertification_Ins");
            return albumcontentId = Convert.ToInt32(objContentId);
        }

        public int UpdateCertificate(ApplicantCertification obj)
        {
            SQL objSql = new SQL();
            object objContentId;
            int albumcontentId = 0;
            objSql.AddParameter("@CertificationID", DbType.Int32, ParameterDirection.Input, 0, obj.CertificationID);
            objSql.AddParameter("@JobApplicationId", DbType.Int32, ParameterDirection.Input, 0, obj.JobApplicationId);
            objSql.AddParameter("@SecurityLicence", DbType.Boolean, ParameterDirection.Input, 0, obj.SecurityLicence);
            objSql.AddParameter("@LicenseType", DbType.String, ParameterDirection.Input, 0, obj.LicenseType);
            objSql.AddParameter("@LicenseNumber", DbType.String, ParameterDirection.Input, 0, obj.LicenseNumber);
            objSql.AddParameter("@Agency", DbType.String, ParameterDirection.Input, 0, obj.Agency);
            objSql.AddParameter("@IssueMonth", DbType.String, ParameterDirection.Input, 0, obj.IssueMonth);
            objSql.AddParameter("@IssueYear", DbType.Int32, ParameterDirection.Input, 0, obj.IssueYear);
            objSql.AddParameter("@ExpirationMonth", DbType.String, ParameterDirection.Input, 0, obj.ExpirationMonth);
            objSql.AddParameter("@ExpirationYear", DbType.Int32, ParameterDirection.Input, 0, obj.ExpirationYear);
            objSql.AddParameter("@Description", DbType.String, ParameterDirection.Input, 0, obj.Description);
            objContentId = objSql.ExecuteNonQuery("p_ApplicantCertification_Upd");
            return albumcontentId = Convert.ToInt32(objContentId);
        }


        public DataSet GetCertificateByJobApplicantId(int JobApplicationId)
        {
            SQL objSql = new SQL();
            DataSet ds;
            objSql.AddParameter("@JobApplicationId", DbType.Int32, ParameterDirection.Input, 0, JobApplicationId);
            ds = objSql.ExecuteDataSet("p_ApplicantCertification_Get");
            return ds;
        }

        public int DeleteApplicantCertificationById(int CertificationID)
        {
            SQL objSql = new SQL();
            object objRowAffected;
            objSql.AddParameter("@CertificationID", DbType.Int32, ParameterDirection.Input, 0, CertificationID);
            objRowAffected = objSql.ExecuteNonQuery("p_ApplicantCertification_Del");
            return Convert.ToInt32(objRowAffected);
        }
    }
}
