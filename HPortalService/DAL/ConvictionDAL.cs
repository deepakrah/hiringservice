﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;



namespace HPortalService.DAL
{
    class ConvictionDAL
    {
        public List<Conviction> GetConvictionByID(int ID)
        {
            SQL objSql = new SQL();
            DataSet ds;
            objSql.AddParameter("@ApplicantId", DbType.String, ParameterDirection.Input, 0, ID);
            ds = objSql.ExecuteDataSet("p_Get_Conviction");
            List<Conviction> objList = SQLHelper.ContructList<Conviction>(ds);
            return objList;
        }


        public int SaveConviction(Conviction obj)
        {
            SQL objSql = new SQL();
            object objContentId;
            int albumcontentId = 0;
            objSql.AddParameter("@name", DbType.String, ParameterDirection.Input, 0, obj.name);
            objSql.AddParameter("@Year", DbType.String, ParameterDirection.Input, 0, obj.Year);
            objSql.AddParameter("@Type", DbType.String, ParameterDirection.Input, 0, obj.Type);
            objSql.AddParameter("@DisplayOrder", DbType.Int32, ParameterDirection.Input, 0, obj.DisplayOrder);
            objSql.AddParameter("@ApplicantId", DbType.String, ParameterDirection.Input, 0, obj.ApplicantId);
            objContentId = objSql.ExecuteScalar("p_Save_Conviction");
            return albumcontentId = Convert.ToInt32(objContentId);
        }

        public int DeleteConviction(int ID)
        {
            SQL objSql = new SQL();
            object objRowAffected;
            objSql.AddParameter("@ApplicantId", DbType.Int32, ParameterDirection.Input, 0, ID);
            objRowAffected = objSql.ExecuteNonQuery("p_Delete_Conviction");
            return Convert.ToInt32(objRowAffected);
        }
    }
}
