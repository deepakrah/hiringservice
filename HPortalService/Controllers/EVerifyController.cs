﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Net.Http;
using System.Xml;
using System.Text;
using System.IO;
using HPortalService.DAL;
using System.Data;
using System.Net.Mail;
using System.Net;

namespace HPortalService.Controllers
{
    [Produces("application/json")]
    [Route("api/EVerify")]
    public class EVerifyController : Controller
    {
        string HPortalResponseValue = @"D:\Inetpub\wwwHportalWSRService\wwwroot\EverifyResponses\";
        string LogPath = @"D:\Inetpub\wwwHportalWSRService\wwwroot\ErrorLog\" + DateTime.Now.ToString("MM-dd-yyyy hh-mm") + "-logfile.txt";
        [AcceptVerbs("GET", "POST")]
        [HttpPost]

        public string Post([FromBody]XmlDocument doc)
        {
            try
            {
                //string fileName = DateTime.Now.ToFileTime().ToString() + ".xml";
                //string locationPath = HPortalResponseValue + fileName;
                //// return "Received Request" + fileName;

                //var content = request.Content;
                //string outPut = content.ReadAsStringAsync().Result;

                //var Form = request.Content.ReadAsFormDataAsync().Result;
                //string xml = Form["Status"].ToString();

                //// Save Received Response
                //System.IO.File.WriteAllText(locationPath, xml, Encoding.ASCII);

                //string[] str = outPut.Split('&');


                string fileName = DateTime.Now.ToFileTime().ToString() + ".xml";
                string locationPath = HPortalResponseValue + fileName;
                // return "Received Request" + fileName;

                //doc.Save(locationPath);


                string strqueuepk = "";
                string strResultId = "";
                string strRequestId = "";
                string strResultErrorCode = "";
                string strSTATUS = "";
                string strCaseNumber = "";
                string strInvoiceAmount = "";
                string strB2BStatus = "";
                string strB2BSubStatus = "";
                string strProcessStatus = "";
                string strInvoicingStatus = "";

                XmlNodeList xnList = doc.SelectNodes("/FormI9Data[@*]");
                foreach (XmlNode xn in xnList)
                {
                    XmlNode requestnode = xn.SelectSingleNode("request");
                    if (requestnode != null)
                    {
                        strResultId = requestnode["resultid"].InnerText;
                        strRequestId = requestnode["requestid"].InnerText;


                    }

                    XmlNode resultnode = xn.SelectSingleNode("result");

                    if (resultnode != null)
                    {
                        strSTATUS = resultnode["status"].InnerText;
                        strCaseNumber = resultnode["casenumber"].InnerText;
                        strInvoiceAmount = resultnode["invoiceamount"].InnerText;
                        strB2BStatus = resultnode["b2bstatus"].InnerText;
                        strB2BSubStatus = resultnode["b2bsubstatus"].InnerText;
                        strProcessStatus = resultnode["processstatus"].InnerText;
                        strInvoicingStatus = resultnode["invoicingstatus"].InnerText;


                    }
                }


                //string strqueuepk = "";
                //string strResultId = str[2].Split('=')[1] == "" ? "0" : str[2].Split('=')[1];
                //string strRequestId = str[5].Split('=')[1] == "" ? "0" : str[5].Split('=')[1];
                //string strResultErrorCode = str[1].Split('=')[1];
                //string strSTATUS = str[12].Split('=')[1];
                //string strCaseNumber = str[15].Split('=')[1];
                //string strInvoiceAmount = str[14].Split('=')[1];
                //string strB2BStatus = str[16].Split('=')[1];
                //string strB2BSubStatus = str[17].Split('=')[1];
                //string strProcessStatus = str[18].Split('=')[1];
                //string strInvoicingStatus = str[21].Split('=')[1];

                DataTable dt = FormI9Verification_Ins(strqueuepk, Convert.ToInt32(strResultId), Convert.ToInt32(strRequestId), strResultErrorCode, strSTATUS,
            strCaseNumber, strInvoiceAmount, strB2BStatus, strB2BSubStatus, strProcessStatus, strInvoicingStatus);
                if (dt.Rows.Count > 0)
                {
                    fnSendMail(dt.Rows[0]["FirstName"].ToString(), dt.Rows[0]["LastName"].ToString(), dt.Rows[0]["Email"].ToString(), strCaseNumber, strB2BStatus, dt.Rows[0]["CCemailId"].ToString());
                }

                return "Success";
            }

            catch (Exception exx)
            {
                //var content = request.Content;
                //string outPut = content.ReadAsStringAsync().Result;
                string str = exx.Message.ToString();
                StreamWriter log;
                if (!System.IO.File.Exists(LogPath))
                //}
                {
                    log = new StreamWriter(LogPath);
                }
                else
                {
                    log = System.IO.File.AppendText(LogPath);
                }
                log.Close();
                using (StreamWriter writer = new StreamWriter(LogPath, true))
                {
                    writer.WriteLine("xml realted:  " + exx.Message.ToString());
                    writer.Close();
                }

                return "error";
            }


        }


        public DataTable FormI9Verification_Ins(string queuepk, int ResultId, int RequestId, string ResultErrorCode, string STATUS,
            string CaseNumber, string InvoiceAmount, string B2BStatus, string B2BSubStatus, string ProcessStatus, string InvoicingStatus)
        {
            SQL objSql = new SQL();
            DataSet ds = new DataSet();
            objSql.AddParameter("@queuepk", DbType.String, ParameterDirection.Input, 0, queuepk);
            objSql.AddParameter("@ResultId", DbType.Int32, ParameterDirection.Input, 0, ResultId);
            objSql.AddParameter("@RequestId", DbType.Int32, ParameterDirection.Input, 0, RequestId);
            objSql.AddParameter("@ResultErrorCode", DbType.String, ParameterDirection.Input, 0, ResultErrorCode);
            objSql.AddParameter("@STATUS", DbType.String, ParameterDirection.Input, 0, STATUS);
            objSql.AddParameter("@CaseNumber", DbType.String, ParameterDirection.Input, 0, CaseNumber);
            objSql.AddParameter("@InvoiceAmount", DbType.String, ParameterDirection.Input, 0, InvoiceAmount);
            objSql.AddParameter("@B2BStatus", DbType.String, ParameterDirection.Input, 0, B2BStatus);
            objSql.AddParameter("@B2BSubStatus", DbType.String, ParameterDirection.Input, 0, B2BSubStatus);
            objSql.AddParameter("@ProcessStatus", DbType.String, ParameterDirection.Input, 0, ProcessStatus);
            objSql.AddParameter("@InvoicingStatus", DbType.String, ParameterDirection.Input, 0, InvoicingStatus);
            //return Convert.ToInt32(objSql.ExecuteNonQuery("p_FormI9Verification_Ins"));

            ds = objSql.ExecuteDataSet("p_FormI9Verification_Ins");
            if (ds != null && ds.Tables.Count > 0)
            {
                return ds.Tables[0];
            }
            return null;
        }

        public void fnSendMail(string FirstName, string LastName, string EmailId, string CaseNumber, string BStatus, string CCMailId)
        //var dataFile = System.Web.HttpContext.Current.Server.MapPath("~/App_Data/data.txt");
        {
            if (!string.IsNullOrEmpty(FirstName) && !string.IsNullOrEmpty(LastName))
            {
                string body = "Dear Hiring Manager"
                + "<br/><br/>"

                + "Case no: " + CaseNumber + " for the below applicant has Changed to " + BStatus
                + "<br/><br/>"
                + "FirstName : " + FirstName
                + "<br/>"
                + "LastName : " + LastName

                + "<br/><br/>Thank you,"
                + "<br/>Woodside Support.";
                this.SendMail("HR@woodsideranch.com", EmailId, "Everify-Employee Case Update", body, CCMailId, "PDevadoss@protatechindia.com");
            }
        }

        public bool SendMail(string strFrom, string strTo, string strSub, string strBody, string strCc, string strBcc)
        {
            SmtpClient smtpClient = new SmtpClient();
            MailMessage message = new MailMessage();

            string userId = Convert.ToString(Startup.userId); //MAIL ID FOR AUTHENTICATION
            string password = Convert.ToString(Startup.password); ;//PASSWORD FOR AUTHENTICATION
            string EnableSsl = Convert.ToString(Startup.EnableSsl);

            bool flag = false;
            if (!string.IsNullOrEmpty(Startup.authenticate))
            {
                flag = Convert.ToBoolean(Startup.authenticate);
            }


            ////  string addMessage = Convert.ToString(Startup.Subject"]);
            String host = Startup.mailServer;
            MailAddress FromAddress = new MailAddress(strFrom);
            try
            {
                smtpClient.EnableSsl = Convert.ToBoolean(EnableSsl);
                smtpClient.Host = host;
                message.From = FromAddress;
                message.To.Add(strTo);
                if (!string.IsNullOrEmpty(strCc))
                    message.CC.Add(strCc);
                if (!string.IsNullOrEmpty(strBcc))
                    message.Bcc.Add(strBcc);
                message.Subject = strSub;
                message.Body = strBody;
                message.IsBodyHtml = true;
                if (flag)
                {
                    NetworkCredential oCredential = new NetworkCredential(userId, password);
                    smtpClient.UseDefaultCredentials = false;
                    smtpClient.Credentials = oCredential;
                }
                else
                {
                    smtpClient.UseDefaultCredentials = true;

                }
                smtpClient.Send(message);
                return true;
            }
            catch (Exception exx)
            {
                string str = exx.Message.ToString();
                return false;
                //}
            }

            //SendMail("support@protatech.com", "draheja@protatechindia.com", "Evrify", outPut, "", "");

        }

    }
}