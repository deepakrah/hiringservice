﻿using HPortalService.DAL;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Data;

namespace HPortalService.Controllers
{
    [Produces("application/json")]
    [Authorize]
    [Route("api/Branch")]
    public class BranchController : Controller
    {

        [HttpGet]
        [Route("VerifySource/{code}")]
        public DataTable VerifySource(string code)
        {
            Branch objBranch = new Branch();
            DataSet ds = objBranch.GetSourceCode(code);

            return ds.Tables[0];
        }



        [HttpGet]
        public string Get()
        {
            return "Default Method";
            //return new string[] { "value1", "value2" };
        }  

        [HttpGet]


        [Route("GetBranchByUser/{UserId}")]
        public List<Branch> GetBranchByUser(int UserId)
        {

            Branch objBranch = new Branch();
            DataSet ds = objBranch.GetBranchByUserId(UserId);
            return SQLHelper.ContructList<Branch>(ds);
        }

        [HttpGet]
        [Route("GetAllBranches")]
        public List<Branch> GetAllBranches()
        {
            DataSet ds = Branch.GetAllBranches();
            return SQLHelper.ContructList<Branch>(ds);
        }
        [HttpGet]
        [AllowAnonymous]
        [Route("GetBranchById/{BranchId}")]
        public List<Branch> GetBranchById(string BranchId)
        {
            if (!string.IsNullOrEmpty(BranchId) && BranchId != "null")
            {
                Branch obj = new Branch();
                DataSet ds = obj.fnGetBranch(Convert.ToInt32(BranchId));
                return SQLHelper.ContructList<Branch>(ds);
            }
            return null;
        }

        [AllowAnonymous]
        [HttpGet]
        [Route("GetBranchSourceByBranchId/{BranchId}")]
        public List<BranchSource> GetBranchSourceByBranchId(string branchId = "10")
        {
            try
            {
                Branch _Branch = new Branch();
                DataSet ds = _Branch.GetBranchSourceByBranchId(branchId);
                return SQLHelper.ContructList<BranchSource>(ds);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet]
        [Route("GetBranchIsTask/{BranchId}")]
        public List<Branch> GetBranchIsTask(int BranchId)
        {
            Branch obj = new Branch();
            DataSet ds = obj.GetBranchIsTask(BranchId);
            SQLHelper objHelper = new SQLHelper();
            List<Branch> lstSources = SQLHelper.ContructList<Branch>(ds);
            return lstSources;
        }
        [AllowAnonymous]
        [HttpGet]
        [Route("fnGetData/{BranchId}")]
        public DataTable fnGetData(int BranchId)
        {
            Branch objBranch = new Branch();
            DataSet ds = objBranch.GetBranchInterview(BranchId);
            return ds.Tables[0];
        }

        [HttpPost]
        [Route("SaveBranchInterview")]
        public int SaveBranchInterview([FromBody]Branch objBranch)
        {
            Branch obj = new Branch();
            obj.BranchId = objBranch.BranchId;
            obj.EnableOnline = objBranch.EnableOnline;
            obj.OnlineInterview = objBranch.OnlineInterview;
            return obj.SaveBranchInterview(obj);
        }
        [AllowAnonymous]
        [HttpGet]
        [Route("GetJobPositionByBranchId/{BranchId}/{JobPositionId}")]
        public DataTable GetJobPositionByBranchId(string BranchId, string JobPositionId)
        {
            Branch objBranch = new Branch();
            DataSet ds = objBranch.GetJobPositionByBranchId(Convert.ToInt32(BranchId), Convert.ToInt32(JobPositionId));
            return ds.Tables[0];
        }
    }
}