﻿using HPortalService.DAL;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Data;
using HPortalService.Model;
using Microsoft.AspNetCore.Authorization;

namespace HPortalService.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [Route("api/Question")]
    public class QuestionController : Controller
    {
        [HttpGet]
        [Route("GetQuestionByBranchId/{BranchId}/{ApplicantId}/{ZipCode}")]
        public List<Question> GetQuestionByBranchId(int BranchId, int ApplicantId, string ZipCode)
        {
            Branch branch = new Branch() { BranchId = BranchId, ApplicantId = ApplicantId, ZipCode = ZipCode };
            try
            {
                return SQLHelper.ContructList<Question>(branch.GetQuestionByBranchId(branch));
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.Message);
                ErrorLogger.Log(ex.StackTrace);
                throw ex;
            }
        }

        [HttpGet]
        [Route("GetAnswerSetByQuesId/{QuesId}/{AppMasterId}")]
        public List<Question> GetAnswerSetByQuesId(int QuesId, int AppMasterId)
        {
            Question lstQues = new Question() { QuesId = QuesId, AppMasterId = AppMasterId };
            try
            {
                return SQLHelper.ContructList<Question>(lstQues.GetAnswerSetByQuesId(lstQues));
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.Message);
                ErrorLogger.Log(ex.StackTrace);
                throw ex;
            }

        }

        [HttpPost]
        [Route("SaveAnswer")]
        public int SaveAnswer([FromBody]Question objQuestion)
        {
            try
            {
                Question lstQues = new Question();
                return lstQues.SaveAnswer(objQuestion);
            }
            catch (Exception )
            {
                return -1;
            }
        }

        [HttpGet]
        [Route("GetQuestionCategory")]
        public List<QuestionCategory> GetQuestionCategory()
        {
            QuestionCategory _QuestionCategory = new QuestionCategory();
            List<QuestionCategory> lstLogin = _QuestionCategory.GetQuestionCategory();
            return lstLogin;
        }

        [HttpGet]
        [Route("GetQuestionByCategory/{QuestionCategoryId}/{ApplicantId}")]
        public List<Question> GetQuestionByCategory(int QuestionCategoryId, int ApplicantId)
        {
            Question _Question = new Question();
            List<Question> lst = _Question.GetQuestionByCategory(QuestionCategoryId, ApplicantId);
            return lst;
        }

        [HttpGet]
        [Route("GetFollowupQuestions/{AnswerId}")]
        public List<Answer> GetFollowupQuestions(int AnswerId)
        {
            Question _Question = new Question();
            List<Answer> lst = _Question.GetFollowupQuestions(AnswerId);
            return lst;
        }

        [HttpGet]
        [Route("GetQuestionByBranchIdQuesId/{BranchId}/{InterviewQuestionId}")]
        public DataSet GetQuestionByBranchIdQuesId(int BranchId, int InterviewQuestionId)
        {
            InterviewQuestion objInterviewQuestion = new InterviewQuestion();
            objInterviewQuestion.BranchId = BranchId;
            return objInterviewQuestion.GetQuestions();
        }

        [HttpPost]
        [Route("SaveApplicantOption")]
        public int SaveApplicantOption([FromBody]InterviewQuestion objQuestion)
        {
            try
            {
                InterviewQuestion objInterviewQuestion = new InterviewQuestion();
                objInterviewQuestion.InterviewOptionId = objQuestion.InterviewOptionId;
                objInterviewQuestion.rubics = objQuestion.rubics;
                objInterviewQuestion.Remarks = objQuestion.Remarks;
                objInterviewQuestion.InterviewQuestionId = objQuestion.InterviewQuestionId;
                objInterviewQuestion.ApplicantId = objQuestion.ApplicantId;
                objInterviewQuestion.Score = objQuestion.Score;
                return objInterviewQuestion.SaveApplicantOption();
            }
            catch (Exception )
            {
                return -1;
            }
        }

        [HttpPost]
        [Route("SaveApplicantResultOption")]
        public int SaveApplicantResultOption([FromBody]InterviewResult obj)
        {
            try
            {
                InterviewResult objResult = new InterviewResult();
                objResult.ApplicantId = obj.ApplicantId;
                objResult.JobApplicationId = obj.JobApplicationId;
                objResult.Remarks = obj.Remarks;
                objResult.Result = obj.Result;
                objResult.TakenBy = obj.TakenBy;
                objResult.SaveResult();

                #region UpdateStatusToApprovedByManager
                JobApplication _JobApplication = new JobApplication();
                _JobApplication.ApplicantId = obj.ApplicantId;
                _JobApplication.JobApplicationId = obj.JobApplicationId;
                _JobApplication.StatusID = obj.StatusID;//Approved/reject
                _JobApplication.ModifiedBy = obj.TakenBy;
                _JobApplication.JobApplictionlog_InsUpd(_JobApplication);
                //insert rejection remarks
                if (_JobApplication.StatusID == 6)
                {
                    _JobApplication.RejectApplicant(_JobApplication.JobApplicationId, obj.Reason, obj.Remarks);
                }
                #endregion

                return 0;

            }
            catch (Exception )
            {
                return -1;
            }
        }
    }
}