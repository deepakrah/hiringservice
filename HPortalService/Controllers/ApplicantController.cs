﻿using HPortalService.DAL;
using HPortalService.Helpers;
using HPortalService.Model;
using HPortalService.Service;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Drawing.Text;
using System.Globalization;
using System.IO;
using System.Linq;

namespace HPortalService.Controllers
{


    [Produces("application/json")]
    [Authorize]
    [Route("api/Applicant")]
    public class ApplicantController : Controller
    {
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly AppSettings _appSettings;
        private IAuthorizationService _authorizationService;
        public ApplicantController(IHostingEnvironment hostingEnvironment, IOptions<AppSettings> appSettings, IAuthorizationService authorizationService)
        {
            _hostingEnvironment = hostingEnvironment;
            _appSettings = appSettings.Value;
            _authorizationService = authorizationService;
        }

        [Route("GetStatusList/{id}")]
        public List<StatusList> GetStatusList(int UserId)
        {

            StatusList obj = new StatusList();
            List<StatusList> lst = obj.ListData();
            return lst;
        }

        [HttpPost]
        [Route("PostDashboardApplicant")]
        public List<ApplicantDashboard> DashboardApplicant([FromBody]Applicant obj)
        {
            if (obj == null)
            {
                return null;
            }
            Applicant _Applicant = new Applicant();
            DataTable dt = new DataTable();
            int totalApplications = 0;
            obj.SearchByHomeBranch = false;
            if (obj.ChildBranchId > 0)
            {
                obj.SearchByHomeBranch = true;
                if (obj.BranchId != obj.ChildBranchId)
                {

                    obj.BranchId = obj.ChildBranchId;
                }

            }

            if (!string.IsNullOrEmpty(obj.StartDate))
            {
                try
                {
                    obj.StartDate = Convert.ToDateTime(obj.StartDate).ToShortDateString();
                }
                catch
                {
                    obj.StartDate = null;
                }
            }
            if (!string.IsNullOrEmpty(obj.EndDate))
            {
                try
                {
                    obj.EndDate = Convert.ToDateTime(obj.EndDate).ToShortDateString();
                }
                catch
                {
                    obj.EndDate = null;
                }
            }
            if (obj.sortExpression.ToLower() == "noshowinterview")
            {   //sort order on orientation will be applied in stored procedure.as it concatenates the order in sql as a string.
                obj.sortExpression = "NoShowInterview " + obj.sortOrder + ",NoShowOrientation";
            }
            if (obj.sortExpression.ToLower() == "noshoworientation")
            {   //sort order on orientation will be applied in stored procedure.as it concatenates the order in sql as a string.
                obj.sortExpression = "NoShowOrientation " + obj.sortOrder + ",NoShowInterview";
            }
            //Search all years
            if (obj.chkShowAllYears)
            {
                obj.Year = null;
                //dt = _Applicant.GetApplicant_Status(obj.Status, obj.BranchId, obj.PageIndex, obj.PageCount, null, obj.includeAllStatuses, obj.FirstName != "" ? obj.FirstName : null, obj.LastName != "" ? obj.LastName : null, obj.EmailID != "" ? obj.EmailID : null, obj.PrimaryPhone != "" ? obj.PrimaryPhone : null, obj.ZipCode != "" ? obj.ZipCode : null, obj.StartDate, obj.EndDate, obj.sortExpression, obj.sortOrder, obj.ddlDuplicateFilter, obj.chkShowDuplicate, null, SearchByHomeBranch);
                dt = _Applicant.GetApplicant_Status(obj);
                if (!string.IsNullOrEmpty(obj.FirstName)
                     || !string.IsNullOrEmpty(obj.LastName)
                     || !string.IsNullOrEmpty(obj.PrimaryPhone)
                     || !string.IsNullOrEmpty(obj.EmailID)
                     || obj.ddlDuplicateFilter != "0"
                     || !string.IsNullOrEmpty(obj.StartDate)
                     || !string.IsNullOrEmpty(obj.EndDate)
                     || obj.chkShowDuplicate
                     || obj.chkShowAllYears
                     || obj.includeAllStatuses
                     )
                {
                    totalApplications = _Applicant.CountApplications(obj);
                }

            }
            else
            {

                dt = _Applicant.GetApplicant_Status(obj);
                if (!string.IsNullOrEmpty(obj.FirstName)
                    || !string.IsNullOrEmpty(obj.LastName)
                    || !string.IsNullOrEmpty(obj.PrimaryPhone)
                    || !string.IsNullOrEmpty(obj.EmailID)
                    || obj.ddlDuplicateFilter != "0"
                    || !string.IsNullOrEmpty(obj.StartDate)
                    || !string.IsNullOrEmpty(obj.EndDate)
                    || obj.chkShowDuplicate
                    || obj.chkShowAllYears
                    || obj.includeAllStatuses
                    )
                {
                    totalApplications = _Applicant.CountApplications(obj);
                }

            }
            dt.Columns.Add("TotalCount", typeof(System.Int32));
            dt.Columns["TotalCount"].Expression = totalApplications.ToString();
            List<ApplicantDashboard> lst = SQLHelper.ContructList<ApplicantDashboard>(dt);
            return lst;
        }

        [HttpGet]
        [Route("GetSummary/{BranchId}/{ChildBranchId}/{yrs}")]
        public List<StatusSummary> GetSummary(int BranchId, int ChildBranchId, string yrs)

        {
            Applicant _Applicant = new Applicant();
            bool SearchByHomeBranch = false;
            if (ChildBranchId > 0)
            {
                SearchByHomeBranch = true;
                if (BranchId != ChildBranchId)
                {
                    BranchId = ChildBranchId;
                }
            }
            DataSet ds = _Applicant.GetSummary(BranchId, yrs, SearchByHomeBranch);
            return SQLHelper.ContructList<StatusSummary>(ds);
        }

        [HttpPost]
        [Route("LoginApplicant")]
        public List<Applicant> LoginApplicant([FromBody]Applicant obj)
        {
            Applicant _Application = new Applicant();
            DataSet ds = _Application.LoginApplicant(obj);
            return SQLHelper.ContructList<Applicant>(ds);
        }

        [HttpPost]
        [Route("PostChangeStatus")]
        public ShowMessage ChangeStatus([FromBody]List<Applicant> AppObj)
        {
            ShowMessage ShowMessage = new ShowMessage();
            try
            {

                Applicant objReturn = new Applicant();
                ChangeStatus objChgStatus = new ChangeStatus();
                int statusToChange = AppObj[0].ddlChangeStatus;
                string UserType = AppObj[0].UserType;
                int UserId = AppObj[0].UserId;


                string flag = "";
                bool valid = true;
                bool futureDate = false;
                int failureReason = 0;
                string Everification = "";
                Applicant Obj = new Applicant();
                DataTable dt = Obj.fnGetConfiguration("Everification");
                if (dt.Rows.Count > 0)
                {
                    Everification = dt.Rows[0]["Value"].ToString();
                }
                foreach (Applicant item in AppObj)
                {
                    StatusList objLst = new StatusList();
                    List<StatusList> objList = objLst.ListData();
                    //Do Some Thing Here                   
                    // foreach (var obj in DictgrdEmp)
                    //{
                    //string[] strArry = obj.Value.Split('|');
                    //int prevStatus = Convert.ToInt32(strArry[0]);
                    int prevStatus = item.Status;
                    int bgStatus = item.BGStatus;
                    int eVerify = item.EVerify;
                    string InterviewDate = item.InterviewDate;
                    //if (strArry.Length > 4)
                    //    {
                    //        //bgStatus = Convert.ToInt32(strArry[4]);
                    //    }
                    //if (strArry.Length > 5)
                    //{
                    //    //eVerify = Convert.ToInt32(strArry[5]);
                    //}

                    if (!string.IsNullOrEmpty(Everification))
                    {
                        if (Everification == "1")
                        {
                            if ((statusToChange == 5 || statusToChange == 20) && (eVerify == 0 || bgStatus != 2))
                            {
                                failureReason = 4;
                                valid = false;
                            }
                        }
                        else
                        {
                            if ((statusToChange == 5 || statusToChange == 20) && bgStatus != 2)
                            {
                                failureReason = 4;
                                valid = false;
                            }
                            else
                            {
                                if (statusToChange == 5 || statusToChange == 20)
                                {
                                    SQL objSql = new SQL();
                                    DataTable dtVerify;
                                    objSql.AddParameter("@ApplicantId", DbType.String, ParameterDirection.Input, 0, Convert.ToInt32(item.ApplicantId));
                                    dtVerify = objSql.ExecuteDataSet("p_ApplicantEverifyDocByAppId").Tables[0];
                                    if (dtVerify.Rows.Count == 0)
                                    {
                                        failureReason = 5;
                                        valid = false;
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        if ((statusToChange == 5 || statusToChange == 20) && (eVerify == 0 || bgStatus != 2))
                        {
                            failureReason = 4;
                            valid = false;
                        }
                    }
                    //if (objList.FirstOrDefault(u => u.StatusListID == prevStatus).ChildList == null || objList.FirstOrDefault(u => u.StatusListID == prevStatus).ChildList.FirstOrDefault(a => a.StatusListID == newStatus) == null)
                    if (prevStatus == statusToChange || statusToChange == 0)
                    {

                        valid = false;

                    }
                    //if ((statusToChange == 15 || statusToChange == 16 || statusToChange == 13 || statusToChange == 14) && strArry[2] != "" && (Convert.ToDateTime(strArry[2]).Date > DateTime.Now.Date))
                    if ((statusToChange == 15 || statusToChange == 16 || statusToChange == 13 || statusToChange == 14) && InterviewDate != "" && (Convert.ToDateTime(InterviewDate).Date > DateTime.Now.Date))
                    {
                        valid = false;
                        futureDate = true;

                    }

                    if (statusToChange == 9 && bgStatus != 2)
                    {
                        failureReason = 2;
                        valid = false;

                    }

                    if (!valid && failureReason == 2)
                    {
                        //System.Web.UI.ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "AlertBox", "alert('selected applicant can not be scheduled for orientatoin.Their background check must be passed.')", true);
                        return ShowMessage.Show("selected applicant can not be scheduled for orientatoin.Their background check must be passed.", ShowMessage.messageType.Error);
                    }
                    /// Hire 
                    if (!valid && failureReason == 4 && Everification == "1" && statusToChange == 5)
                    {
                        return ShowMessage.Show("Applicant cannot be moved to hired status without a background check and e-verify.", ShowMessage.messageType.Error);
                    }
                    if (!valid && failureReason == 4 && Everification == "0" && statusToChange == 5)
                    {
                        return ShowMessage.Show("Applicant cannot be moved to hired status without a background check.", ShowMessage.messageType.Error);
                    }
                    if (!valid && failureReason == 5 && statusToChange == 5)
                    {
                        return ShowMessage.Show("Applicant cannot be moved to hired status without uploading E-verify documents.", ShowMessage.messageType.Error);
                    }

                    // Rehire
                    if (!valid && failureReason == 4 && Everification == "1" && statusToChange == 20)
                    {
                        return ShowMessage.Show("Applicant cannot be moved to rehired status without a background check and e-verify.", ShowMessage.messageType.Error);
                    }
                    if (!valid && failureReason == 4 && Everification == "0" && statusToChange == 20)
                    {
                        return ShowMessage.Show("Applicant cannot be moved to rehired status without a background check.", ShowMessage.messageType.Error);
                    }
                    if (!valid && failureReason == 5 && statusToChange == 20)
                    {
                        return ShowMessage.Show("Applicant cannot be moved to rehired status without uploading E-verify documents.", ShowMessage.messageType.Error);
                    }

                    //if (!valid && failureReason == 4)
                    //{
                    //    return ShowMessage.Show("Applicant cannot be moved to hired status without a background check and e-verify.", ShowMessage.messageType.Error);
                    //}
                    else if (!valid && UserType != "4")
                    {
                        //int i = Convert.ToInt32(ddlChangeStatus.SelectedValue);
                        List<StatusList> objvalidList = objList.Where(u => u.StatusListID != 0 && u.ChildList != null && u.ChildList.Where(a => a.StatusListID == statusToChange).ToList().Count > 0).ToList();
                        string str = string.Join(",", objvalidList.Select(x => x.Name));
                        if (futureDate)
                        {
                            //System.Web.UI.ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "AlertBox", "alert('Applicant Status can not be changed to " + objList.SingleOrDefault(u => u.StatusListID == statusToChange).Name + " for future date')", true);
                            return ShowMessage.Show("Applicant Status can not be changed to " + objList.SingleOrDefault(u => u.StatusListID == statusToChange).Name + " for future date", ShowMessage.messageType.Error);
                        }
                        else
                        {
                            if (str == "")
                            {
                                return ShowMessage.Show("Applicant Status can not be change now", ShowMessage.messageType.Error);
                                //System.Web.UI.ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "AlertBox", "alert('Applicant Status can not be change now')", true);
                            }
                            else
                            {
                                return ShowMessage.Show("Applicant Status should be in " + str, ShowMessage.messageType.Error);
                                //System.Web.UI.ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "AlertBox", "alert('Applicant Status should be in " + str + "')", true);
                            }
                        }
                    }
                    else
                    {

                        switch (statusToChange)
                        {
                            case 2:
                                objChgStatus.ChangeToInterview();
                                break;
                            case 9:
                                objChgStatus.changeToOrientation();
                                break;
                            case 3:
                                if (UserType == "2")
                                {
                                    if (AppObj.Count > 1)
                                    {
                                        return ShowMessage.Show("Please choose one applicant at a time", ShowMessage.messageType.Error);
                                    }

                                }

                                objChgStatus.ChangeStatusToOffer();
                                break;

                            case 4:
                            case 6:
                                objChgStatus.ChangeStatusReject();
                                break;
                            case 1:
                            case 7:
                            case 8:
                            case 10:
                            case 12:
                            case 11:
                            case 13:
                            case 14:
                            case 15:
                            case 16:
                            case 17:
                            case 18:
                            case 19:
                            case 20:
                            case 22:
                            case 23:
                                flag = objChgStatus.UpdateApplicationStatus(item.ApplicantId, item.JobApplicationId, statusToChange, item.Status, UserId, 0, 0, 0, 0, 1, (statusToChange == 17 ? item.subject : null), (statusToChange == 17 ? item.emailBody : null), item.SSN);

                                break;
                            //case 17:
                            //    objChgStatus.ChangeToRescind(item);
                            //    break;
                            case 5:
                                //objChgStatus.ChangeStatusToHire(item);
                                //break;
                                if (item.Completed != 1)
                                {
                                    return ShowMessage.Show("Only completed profile can be marked as hired.", ShowMessage.messageType.Error);
                                }
                                else
                                {
                                    return ShowMessage.Show("", ShowMessage.messageType.Warning); // return here for show hired popup
                                }
                        }

                    }




                }
                StatusList lstStatus = new StatusList();
                string msg = "";
                if (statusToChange == 19)
                {
                    msg = "Status successfully updated to 'Scheduled for an interview'.";
                }
                else
                {
                    msg = "Status successfully updated to '" + lstStatus.ListData().FirstOrDefault(u => u.StatusListID == statusToChange).Name + "'.";
                }

                if (flag == "true")
                {
                    return ShowMessage.Show(msg, ShowMessage.messageType.Success);
                }
                else
                {
                    return ShowMessage.Show("Invalid Status", ShowMessage.messageType.Error);
                }

            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.Message);
                ErrorLogger.Log(ex.StackTrace);

            }
            return ShowMessage.Show("Error", ShowMessage.messageType.Error); ;
        }

        [HttpPost]
        [Route("PostChangeStatusToHire")]
        public DataTable ChangeStatusToHire([FromBody]List<Applicant> DictgrdEmp)
        {
            ChangeStatus obj = new ChangeStatus();
            return obj.ChangeStatusToHire(DictgrdEmp);
        }

        [HttpPost]
        [Route("PostMakeHire")]
        public string MakeHire([FromBody]List<Applicant> DictgrdEmp)
        {
            bool flag = false;
            string msg = "";
            SendEmails sendEmails = new SendEmails();
            int UserId = DictgrdEmp[0].UserId;
            foreach (var item in DictgrdEmp)
            {
                ChangeStatus objChgStatus = new ChangeStatus();
                //flag = objChgStatus.fnChangeStatus(item.ApplicantId, item.JobApplicationId, 5, item.Status, item.UserId);

                JobApplication _JobApplication = new JobApplication();
                _JobApplication.JobApplicationId = item.JobApplicationId;
                _JobApplication.StatusID = 5;
                _JobApplication.ModifiedBy = UserId;
                _JobApplication.JobApplictionlog_InsUpd(_JobApplication);

                sendEmails.setMailContent(item.ApplicantId, SendEmails.EStatus.Hired.ToString(), null, null);

                StatusList lstStatus = new StatusList();
                msg = "Status successfully updated to '" + lstStatus.ListData().FirstOrDefault(u => u.StatusListID == item.ddlChangeStatus).Name + "'.";

                //if (flag)
                //{
                //    return msg;
                //}
            }
            return msg;
        }

        [HttpPost]
        [Route("PostBindInterviewDate")]
        public List<InterviewVenues> BindInterviewDate([FromBody]InterviewVenues IntObj)
        {
            int branchId = IntObj.BranchId;
            //Get hiring Locations values if it is selected.
            // Added By Sowmya
            if (IntObj.ChildBranchId > 0)
            {
                if (branchId != IntObj.ChildBranchId)
                {
                    branchId = IntObj.ChildBranchId;
                }
            }
            int type = IntObj.ScheduleType;
            InterviewVenues obj = new InterviewVenues();

            Branch objBranch = new Branch(IntObj.BranchId);
            int YearFilter = 0;
            if (IntObj.Year != "All")
            {
                YearFilter = Convert.ToInt32(IntObj.Year);
            }
            DataSet objDs = obj.GetInterviewVenueDate(branchId, type, (IntObj.UserType), null, null, objBranch.VenueSetting.AllowSchedOnUnPublished, YearFilter);
            List<InterviewVenues> lst = new List<InterviewVenues>();
            lst = SQLHelper.ContructList<InterviewVenues>(objDs);
            return lst;
        }

        [HttpPost]
        [Route("PostBindVenueDate")]
        public List<InterviewVenues> BindVenueDate([FromBody]InterviewVenues IntObj)
        {
            if (IntObj.Date != null)
            {
                int branchId = IntObj.BranchId;
                if (IntObj.ChildBranchId > 0)
                {
                    if (branchId != IntObj.ChildBranchId)
                    {
                        branchId = IntObj.ChildBranchId;
                    }
                }
                int type = IntObj.ScheduleType;
                InterviewVenues objVenues = new InterviewVenues();
                DataSet objDs = objVenues.GetAvailInterviewVenueByDate(IntObj.Date, branchId, type, 1);
                List<InterviewVenues> lst = new List<InterviewVenues>();
                lst = SQLHelper.ContructList<InterviewVenues>(objDs);
                return lst;

            }

            return null;
        }

        [HttpPost]
        [Route("PostApplySchedForIntStatus")]
        public ShowMessage ApplySchedForIntStatus([FromBody]List<Applicant> AppObj)
        {
            string interviewVenueId = AppObj[0].InterviewVenueId.ToString();
            string InterviewSession = AppObj[0].InterviewSession.ToString();
            int statusToChange = AppObj[0].ddlChangeStatus;
            ChangeStatus objChgStatus = new ChangeStatus();
            ShowMessage Message = new ShowMessage();
            StatusList lstStatus = new StatusList();
            ApplicantDAL _objApp = new ApplicantDAL();
            //GridViewRow gr = (GridViewRow)((LinkButton)sender).NamingContainer;
            //HiddenField hfSessionId = (HiddenField)gr.FindControl("hfSessionId");
            //HiddenField available = (HiddenField)gr.FindControl("hfAvailable");
            if (!String.IsNullOrEmpty(interviewVenueId))
            {

                if (AppObj == null || AppObj.Count == 0)
                {
                    //System.Web.UI.ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "AlertBox1", "alert('Please Select atleast one applicant.')", true);
                    return Message.Show("Please Select atleast one applicant.", ShowMessage.messageType.Error);

                }
                else
                {
                    string flag = "";
                    foreach (Applicant item in AppObj)
                    {
                        if (statusToChange == 9)
                        {
                            DataTable dt = _objApp.GetApplicantI9from(item.ApplicantId).Tables[0];
                            if (dt.Rows.Count == 0)
                            {
                                string msg = "Status cannot be updated to '" + lstStatus.ListData().FirstOrDefault(u => u.StatusListID == statusToChange).Name + "' as applicant 'Form I-9' is not completed.";
                                return Message.Show(msg, ShowMessage.messageType.Error);
                            }
                        }
                        flag = objChgStatus.UpdateApplicationStatus(item.ApplicantId, item.JobApplicationId, statusToChange, item.Status, item.UserId, 0, Convert.ToInt32(interviewVenueId), Convert.ToInt32(InterviewSession), item.BranchId);
                        if (flag != "true")
                        {
                            return Message.Show(flag, ShowMessage.messageType.Error);
                        }
                    }
                    if (flag == "true")
                    {
                        // return Message.Show("Applicant status has been changed successfully", ShowMessage.messageType.Success);
                        //StatusList lstStatus = new StatusList();
                        string msg = "Status successfully updated to '" + lstStatus.ListData().FirstOrDefault(u => u.StatusListID == statusToChange).Name + "'.";
                        return Message.Show(msg, ShowMessage.messageType.Success);
                    }
                    else
                    {
                        return Message.Show("Invalid Status", ShowMessage.messageType.Error);
                    }
                }
            }
            return Message.Show("Invalid", ShowMessage.messageType.Error);
        }

        [HttpPost]
        [Route("PostBindOfferDetails")]
        public List<Applicant> BindOfferDetails([FromBody]List<Applicant> AppObj)
        {

            DataTable dt = new DataTable();
            dt.Columns.Add("ApplicantId");
            dt.Columns.Add("JobApplicationId");
            dt.Columns.Add("FirstName");
            dt.Columns.Add("LastName");
            dt.Columns.Add("DOB");
            dt.Columns.Add("SSN");
            dt.Columns.Add("TaxStatus");
            dt.Columns.Add("Allowance");
            dt.Columns.Add("AdditionalAmount");
            dt.Columns.Add("MiddleName");
            dt.Columns.Add("Rehire");
            dt.Columns.Add("status");
            foreach (Applicant item in AppObj)
            {
                int ApplicantID = Convert.ToInt32(item.ApplicantId);
                Applicant objApplicant = new Applicant();
                DataSet ds = objApplicant.GetApplicantByID(ApplicantID);
                dt.Rows.Add(new object[] { ds.Tables[0].Rows[0]["ApplicantId"],
                    ds.Tables[0].Rows[0]["JobApplicationId"],
                    ds.Tables[0].Rows[0]["FirstName"],
                    ds.Tables[0].Rows[0]["LastName"],
                    ds.Tables[0].Rows[0]["WDOB"].ToString() != "" ? Convert.ToDateTime(ds.Tables[0].Rows[0]["WDOB"]).ToString("MM/dd/yyyy") : "",
                    ds.Tables[0].Rows[0]["WSSN"], ds.Tables[0].Rows[0]["FedTaxStatuses"], ds.Tables[0].Rows[0]["NoOfAllowances"],
                    ds.Tables[0].Rows[0]["WAdditionalAmount"],
                    ds.Tables[0].Rows[0]["MiddleName"],
                    ds.Tables[0].Rows[0]["Rehire"],
                    ds.Tables[0].Rows[0]["STATUS"]
                });
            }
            List<Applicant> lst = new List<Applicant>();
            lst = SQLHelper.ContructList<Applicant>(dt);
            return lst;
        }

        [HttpPost]
        [Route("PostMakeOffer")]
        public ShowMessage MakeOffer([FromBody]List<Applicant> AppObj)
        {
            int statusToChange = AppObj[0].ddlChangeStatus;
            ChangeStatus objChgStatus = new ChangeStatus();
            ApplicantDAL objApp = new ApplicantDAL();
            ShowMessage Message = new ShowMessage();
            StatusList lstStatus = new StatusList();
            string flag = "";
            foreach (Applicant row in AppObj)
            {


                Applicant lstLoginInfo = new Applicant() { ApplicantId = Convert.ToInt32(row.ApplicantId), DOB = row.DOB, Allowance = row.Allowance != 0 ? Convert.ToDecimal(row.Allowance) : 0, SSN = row.SSN, TaxStatus = row.TaxStatus, AdditionalAmount = row.AdditionalAmount != 0 ? Convert.ToDecimal(row.AdditionalAmount) : 0 };
                lstLoginInfo.FirstName = row.FirstName.Trim();
                lstLoginInfo.LastName = row.LastName.Trim();
                //if (statusToChange == 3)
                //{
                //    DataTable dt = objApp.GetApplicantBGCheckStatus(row.ApplicantId).Tables[0];
                //    if (dt.Rows.Count == 0)
                //    {
                //        string msg = "Status cannot be updated to '" + lstStatus.ListData().FirstOrDefault(u => u.StatusListID == statusToChange).Name + "' as applicant 'Background Check Disclosure and Authorization Form' is not verified.";
                //        return Message.Show(msg, ShowMessage.messageType.Error);
                //    }
                //}
                if (!String.IsNullOrEmpty(row.MiddleName))
                {
                    lstLoginInfo.MiddleName = row.MiddleName.Trim();
                }
                lstLoginInfo.Rehire = row.Rehire;
                lstLoginInfo.RehireSourceId = Convert.ToInt32(Applicant.RehireSource.OfferedPopupAdmin);
                lstLoginInfo.UpdateApplicantInfo(lstLoginInfo);

                if (!String.IsNullOrEmpty(lstLoginInfo.FirstName) && !String.IsNullOrEmpty(lstLoginInfo.LastName))
                {
                    lstLoginInfo.UpdateName(row);
                }
                flag = objChgStatus.UpdateApplicationStatus(row.ApplicantId, row.JobApplicationId, statusToChange, row.Status, row.UserId, 0, 0, 0, row.BranchId, 1, null, null, row.SSN);
                if (flag != "true")
                {
                    return Message.Show(flag, ShowMessage.messageType.Error);
                }

            }
            if (flag == "true")
            {
                //return Message.Show("Applicant status has been changed successfully", ShowMessage.messageType.Success);              
                string msg = "Status successfully updated to '" + lstStatus.ListData().FirstOrDefault(u => u.StatusListID == statusToChange).Name + "'.";
                return Message.Show(msg, ShowMessage.messageType.Success);
            }
            else
            {
                return Message.Show("Invalid Status", ShowMessage.messageType.Error);
            }
        }

        [HttpPost]
        [Route("BindApproveDetails")]
        public DataTable BindApproveDetails([FromBody]List<Applicant> AppObj)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("ApplicantId");
            dt.Columns.Add("JobApplicationId");
            dt.Columns.Add("JobPositionId");
            dt.Columns.Add("FirstName");
            dt.Columns.Add("LastName");
            dt.Columns.Add("BranchPayrate");
            dt.Columns.Add("Payrate");
            dt.Columns.Add("status");
            foreach (Applicant item in AppObj)
            {
                int ApplicantID = Convert.ToInt32(item.ApplicantId);
                Applicant objApplicant = new Applicant();
                DataSet ds = objApplicant.GetApplicantForApproveById(ApplicantID);
                dt.Rows.Add(new object[] { ds.Tables[0].Rows[0]["ApplicantId"],
                    ds.Tables[0].Rows[0]["JobApplicationId"],
                   ds.Tables[0].Rows[0]["JobPositionId"],
                    ds.Tables[0].Rows[0]["FirstName"],
                    ds.Tables[0].Rows[0]["LastName"],
                    ds.Tables[0].Rows[0]["BranchPayrate"],
                    ds.Tables[0].Rows[0]["Payrate"],
                    ds.Tables[0].Rows[0]["STATUS"]
                });
            }
            return dt;
        }
        [HttpPost]
        [Route("MakeApprove")]
        public ShowMessage MakeApprove([FromBody]List<JobPosition> AppObj)
        {
            int statusToChange = AppObj[0].ddlChangeStatus;
            ChangeStatus objChgStatus = new ChangeStatus();
            ApplicantDAL objApp = new ApplicantDAL();
            ShowMessage Message = new ShowMessage();
            StatusList lstStatus = new StatusList();
            string flag = "";
            foreach (JobPosition row in AppObj)
            {
                //JobPosition lstLoginInfo = new JobPosition()
                //{
                //    ApplicantId = Convert.ToInt32(row.ApplicantId),
                //    PayRate = row.PayRate != 0 ? Convert.ToDecimal(row.PayRate) : 0,
                //    JobPositionId = row.JobPositionId
                //};
                //lstLoginInfo.FirstName = row.FirstName.Trim();
                //lstLoginInfo.LastName = row.LastName.Trim();
                objApp.UpdateJobApplicationPayrate(row);
                flag = objChgStatus.UpdateApplicationStatus(row.ApplicantId, row.JobApplicationId, statusToChange, row.Status, row.UserId, 0, 0, 0, row.BranchId, 1, null, null, null);
                if (flag != "true")
                {
                    return Message.Show(flag, ShowMessage.messageType.Error);
                }

            }
            if (flag == "true")
            {
                //return Message.Show("Applicant status has been changed successfully", ShowMessage.messageType.Success);              
                string msg = "Status successfully updated to '" + lstStatus.ListData().FirstOrDefault(u => u.StatusListID == statusToChange).Name + "'.";
                return Message.Show(msg, ShowMessage.messageType.Success);
            }
            else
            {
                return Message.Show("Invalid Status", ShowMessage.messageType.Error);
            }
        }

        [HttpGet]
        [Route("GetApplicantTask/{ApplicantId}/{Phase}")]
        public List<ApplicantTask> GetApplicantTask(int ApplicantId, int Phase)
        {
            ApplicantTask objApplicantTask = new ApplicantTask
            {
                ApplicantId = ApplicantId,
                Phase = Phase
            };
            DataSet dsApplicantTask = new DataSet();
            dsApplicantTask = objApplicantTask.GetApplicantTaskDocs();
            List<ApplicantTask> lst = new List<ApplicantTask>();
            lst = SQLHelper.ContructList<ApplicantTask>(dsApplicantTask);
            return lst;
        }

        [HttpPost]
        [Route("PostRejectApplicant")]
        public ShowMessage RejectApplicant([FromBody]List<Applicant> AppObj)
        {
            int statusToChange = AppObj[0].ddlChangeStatus;
            int UserId = AppObj[0].UserId;
            ChangeStatus objChgStatus = new ChangeStatus();
            ShowMessage Message = new ShowMessage();
            string flag = "";
            foreach (Applicant item in AppObj)
            {
                JobApplication objJobApp = new JobApplication();
                objJobApp.RejectApplicant(Convert.ToInt32(item.JobApplicationId), item.ddlReason, item.RejectRemarks);
                flag = objChgStatus.UpdateApplicationStatus(item.ApplicantId, item.JobApplicationId, statusToChange, item.Status, UserId, 0, 0, 0, 0, 1, item.subject, item.emailBody);
                if (flag != "true")
                {
                    return Message.Show(flag, ShowMessage.messageType.Error);
                }
            }
            if (flag == "true")
            {
                //return Message.Show("Applicant status has been changed successfully", ShowMessage.messageType.Success);
                StatusList lstStatus = new StatusList();
                string msg = "Status successfully updated to '" + lstStatus.ListData().FirstOrDefault(u => u.StatusListID == statusToChange).Name + "'.";
                return Message.Show(msg, ShowMessage.messageType.Success);
            }
            else
            {
                return Message.Show("Invalid Status", ShowMessage.messageType.Error);
            }
        }

        [HttpPost]
        [Route("PostAppTaskReset")]
        public ShowMessage ApplicantTaskReset([FromBody]ApplicantTask AppTaskObj)
        {
            ShowMessage Message = new ShowMessage();

            if (AppTaskObj.ApplicantId > 0)
            {

                if (AppTaskObj.ApplicantTaskId > 0)
                {
                    Applicant objAp = new Applicant
                    {
                        ApplicantId = AppTaskObj.ApplicantId
                    };
                    int type = 0;
                    int status = objAp.SelectedResetApplicant(AppTaskObj.ApplicantId, AppTaskObj.Type, AppTaskObj.UserId, AppTaskObj.ApplicantTaskId);
                    if (status > 0)
                    {
                        return Message.Show("Record has been reset successfully.", ShowMessage.messageType.Success);
                    }
                    return Message.Show("Invalid Task", ShowMessage.messageType.Error);
                }
            }

            return Message.Show("Invalid Status", ShowMessage.messageType.Error);
        }

        [HttpPost]
        [Route("PostRegenerate")]
        public ShowMessage PostRegenerate([FromBody]ApplicantTask AppTaskObj)
        {


            ShowMessage Message = new ShowMessage();
            //GenerateTaskReport _taskRep = new GenerateTaskReport();
            if (AppTaskObj.ApplicantId > 0)
            {
                int ApplicantId = AppTaskObj.ApplicantId;
                int ApplicantTaskId = AppTaskObj.ApplicantTaskId;
                if (ApplicantTaskId > 0)
                {

                    string RName = "";

                    RName = AppTaskObj.TaskId.ToString();
                    string BId = AppTaskObj.BranchId.ToString();
                    string TaskId = AppTaskObj.TaskId.ToString();

                    if (TaskId == "11")
                    {
                        if (Convert.ToInt32(AppTaskObj.ReverifyBy) > 0)
                        {
                            RName = "18";
                        }
                        else if (Convert.ToInt32(AppTaskObj.CertifyBy) > 0)
                        {
                            RName = "19";
                        }

                    }
                    if (!string.IsNullOrEmpty(TaskId))
                    {
                        if (Convert.ToInt32(TaskId) >= 19)
                        {
                            RName = (Convert.ToInt32(TaskId) + 1).ToString();
                        }
                    }


                    //ReportSettings setting = new ReportSettings();
                    //ReportViewer viewer = new ReportViewer(setting);

                    //ReportRequest req = new ReportRequest();
                    //req.Path = "";

                    //viewer.Execute(req)
                    //string imagePath;
                    ////byte[] bytes;
                    ////string fileName = "";
                    //string ServerPath = Startup.ServerPath;
                    //viewer.ProcessingMode = ProcessingMode.Local;
                    //viewer.LocalReport.ReportPath = ServerPath + @"\Report\" + RName + ".rdlc";

                    //viewer.LocalReport.EnableExternalImages = true;

                    //imagePath = ServerPath + @"\ApplicantForm\" + AppTaskObj.ApplicantId + "/" + ((AppTaskObj.ApplicantId > 0) ? AppTaskObj.SignatureImage : "").ToString();
                    //ReportParameter parameter = new ReportParameter("Path", imagePath);




                    // string _param = ApplicantId.ToString() + "," + RName + "," + BId + "," + ApplicantTaskId.ToString() + "," + TaskId + "," + AppTaskObj.UserId;
                    //ApplicantTask obj = new ApplicantTask();

                    //_taskRep.GenerateReport(AppTaskObj);


                    //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "script1", "GenerateReport('" + _param + "');", true);
                    //mpeTask.Show();
                    //hdnApplicantTaskId.Value = "";
                    //hdnTaskId.Value = "";
                    return Message.Show("Record has been reset successfully.", ShowMessage.messageType.Success);
                }
            }
            return Message.Show("Invalid Status", ShowMessage.messageType.Error);
        }

        [HttpPost]
        [Route("PostVerifyTask")]
        public ShowMessage VerifyTask([FromBody]ApplicantTask AppTaskObj)
        {
            ShowMessage Message = new ShowMessage();
            string MessageText = "";
            if (AppTaskObj.ApplicantId > 0)
            {

                if (AppTaskObj.ApplicantTaskId > 0)
                {

                    int TaskVefiedCount = 0;
                    bool AllTaskVerifyCount = false;

                    ApplicantTask objApplicantTask = new ApplicantTask();
                    objApplicantTask.ApplicantTaskId = AppTaskObj.ApplicantTaskId;
                    objApplicantTask.ApplicantId = AppTaskObj.ApplicantId;
                    objApplicantTask.VerifiedBy = AppTaskObj.UserId;
                    DataSet AllTask = objApplicantTask.ApplicantTaskUpdate();
                    if (AllTask != null && AllTask.Tables.Count > 0)
                    {
                        TaskVefiedCount = Convert.ToInt32(AllTask.Tables[0].Rows[0]["TaskVerifiedCount"]);

                        if (AllTask.Tables[0].Rows[0]["TaskVerifiedCount"].ToString() == AllTask.Tables[2].Rows[0]["TotalPhase1Task"].ToString())
                        {
                            AllTaskVerifyCount = true;
                        }
                        else
                        {
                            AllTaskVerifyCount = false;
                        }
                    }


                    if (TaskVefiedCount > 0)
                    {
                        if (AllTaskVerifyCount)
                        {
                            if (AllTask.Tables.Count > 1 && AllTask.Tables[1].Rows.Count > 0)
                            {
                                if (Convert.ToInt32(AllTask.Tables[1].Rows[0]["BgCheckid"]) <= 0)
                                {
                                    ApplicantTask objApplicantTask1 = new ApplicantTask();
                                    SendEmails sendEmails = new SendEmails();
                                    objApplicantTask1.ApplicantId = AppTaskObj.ApplicantId;
                                    objApplicantTask1.ApplicantUpdate();

                                    JobApplication _JobApplication = new JobApplication();
                                    _JobApplication.StatusID = 3; //offered stataus
                                                                  //this.fnInsApplicantTask(ApplicantId, _JobApplication.StatusID); // commnented by deepak on 15 jan 2018 (Second phase Task should be inserted when we change the Status Schedule for orientation).
                                                                  // sendEmails.setMailContent(Convert.ToInt32(ApplicantId), EStatus.Manager_Offered, null, null);// commnented by deepak on 2 apr 2018 (Second phase Task mail should be sent when we change the Status Schedule for orientation).
                                                                  //objApplicantTask1.SendBGCheckRequest(AppTaskObj.ApplicantId);  //Send bg CheckRequest on offer
                                    ChangeStatus obj = new ChangeStatus();
                                    obj.SendBGCheckRequest(AppTaskObj.ApplicantId, AppTaskObj.UserId, AppTaskObj.BranchId);

                                }
                            }

                        }
                        return Message.Show("Document has been verified successfully.", ShowMessage.messageType.Success);
                        //FillApplicantDocs(ApplicantId);
                    }
                    else
                    {
                        return Message.Show("Pending document can not be verified.", ShowMessage.messageType.Error);

                    }

                }
            }

            return Message.Show("Invalid Status", ShowMessage.messageType.Error);
        }

        [AllowAnonymous]
        [HttpGet]
        [Route("GetVerificationCode/{EmailID}/{ApplicantId}/{BranchId}")]
        public string GetVerificationCode(string EmailID, int ApplicantId, int BranchId)
        {
            string webRootPath = _hostingEnvironment.WebRootPath;

            Applicant obj = new Applicant();
            string code = obj.GetVerificationCode(EmailID);
            Branch objBranch = null;
            if (BranchId > 0)
            {
                objBranch = new Branch(BranchId);
            }
            try
            {
                if (code == "3")
                {
                }
                else if (code != "1")
                {
                    //SendEmails objSendEmail = new SendEmails();
                    //EmailParameters emailParameters = new EmailParameters()
                    //{
                    //    UserName = EmailID,
                    //    Password = code.Substring(0, 5),
                    //    XMLFilePath = "EmailVerification.xsl",
                    //    BranchEmail = objBranch != null ? objBranch.EmailId : "",
                    //    Branch = objBranch != null ? objBranch.BranchName : "",
                    //    VerificationLink = code
                    //};
                    //string xmlData = emailParameters.GetXML();
                    //string strBody = MailerUtility.GetMailBody(webRootPath + "\\xslt\\" + emailParameters.XMLFilePath, xmlData);
                    //MailContent objMailContent = new MailContent() { From = "HR@woodsideranch.com", toEmailaddress = EmailID, displayName = "Woodside Recruiting Staff", subject = "Woodside Verification Code", emailBody = strBody };
                    //objSendEmail.SendEmailInBackgroundThread(objMailContent);
                }
            }
            catch (Exception ex)
            {   //verification code not sent
                code = "2";
                // ErrorLogger.Log(ex.Message);
            }
            return code;
        }

        [HttpPost]
        [Route("GetHiredRehiredReport")]
        public DataTable GetHiredRehiredReport([FromBody]Applicant objApp)
        {
            Applicant obj = new Applicant();
            DateTime StartDate = new DateTime();
            DateTime EndDate = new DateTime();
            if (!string.IsNullOrEmpty(objApp.StartDate))
            {
                StartDate = Convert.ToDateTime(objApp.StartDate);
            }
            if (!string.IsNullOrEmpty(objApp.EndDate))
            {
                EndDate = Convert.ToDateTime(objApp.EndDate);
            }
            return obj.GetHiredRehiredReport(objApp.UserId, StartDate, EndDate);
        }

        [HttpPost]
        [Route("GetYearlyHiredRehiredReport")]
        public DataTable GetYearlyHiredRehiredReport([FromBody]Applicant objApp)
        {
            Applicant obj = new Applicant();
            return obj.GetHiredRehiredReport(objApp.Year).Tables[0];
        }
        [AllowAnonymous]
        [HttpGet]
        [Route("VerifyEmail/{EmailID}/{code}/{VerifyBy}")]
        public string VerifyEmail(string EmailID, string code, int VerifyBy)
        {
            int userType = 1;
            //string strAdmin = HttpContext.Current.Session["UserName"] != null ? HttpContext.Current.Session["UserName"].ToString() : "";
            //if (strAdmin != "")
            //{
            //    string[] _strSession = strAdmin.Split(',');
            //    VerifyBy = Convert.ToInt32(_strSession[2]);
            //    userType = Convert.ToInt32(_strSession[3]);

            //}
            Applicant obj = new Applicant();
            string str = obj.VerifyEmail(EmailID, code, VerifyBy, userType);
            return str;
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("PostRegister")]
        public ApplicantToken Register([FromBody]Applicant objApplicant)
        {
            ApplicantToken objToken = new ApplicantToken();
            Applicant _Application = new Applicant();
            int id = _Application.Save(objApplicant);
            if (id == 0)
            {
                return null;
            }

            SendEmails sendEmails = new SendEmails();
            sendEmails.setMailContent(id, SendEmails.EStatus.Registration.ToString());
            sendEmails.setMailContent(id, SendEmails.EStatus.NewApplicant.ToString());
            AuthorizeService auth = new AuthorizeService();
            string _token = auth.Authenticate(objApplicant.EmailID, _appSettings);
            objToken.Token = _token;
            objToken.ApplicantId = id;
            return objToken;
            //return 1;
        }

        [HttpGet]
        [Route("GetApplicantByID/{ApplicantId}")]
        public List<Applicant> GetApplicantByID(int ApplicantId)
        {
            Applicant obj = new Applicant();
            return SQLHelper.ContructList<Applicant>(obj.GetApplicantByID(ApplicantId));
        }

        [HttpGet]
        [Route("GetApplicantForWT4ByID/{ApplicantId}")]
        public DataTable GetApplicantForWT4ByID(int ApplicantId)
        {
            Applicant obj = new Applicant();
            return obj.GetApplicantForWT4ByID(ApplicantId).Tables[0];
        }


        [Route("GetApplicantValidateByID/{ApplicantId}")]
        public DataTable GetApplicantValidateByID(int ApplicantId)
        {
            Applicant obj = new Applicant();
            DataSet ds = obj.GetApplicantValidateByID(ApplicantId);
            if (ds != null && ds.Tables.Count > 0)
            {
                return ds.Tables[0];
            }
            return null;
        }

        [HttpGet]
        [Route("GetApplicantAnswerPre/{ApplicantId}")]
        public List<Question> GetApplicantAnswerPre(int ApplicantId)
        {
            Applicant obj = new Applicant();
            DataSet ds = obj.GetApplicantAnswer(Convert.ToInt32(ApplicantId));
            List<Question> lstApplicant = SQLHelper.ContructList<Question>(ds);
            List<Question> lstPre = lstApplicant.Where(u => u.QuestionCategoryId == 1).Select(u => u).ToList();
            return lstPre;

        }

        [HttpGet]
        [Route("GetApplicantAnswerAssessment/{ApplicantId}")]
        public List<Question> GetApplicantAnswerAssessment(int ApplicantId)
        {
            Applicant obj = new Applicant();
            DataSet ds = obj.GetApplicantAnswer(Convert.ToInt32(ApplicantId));
            List<Question> lstApplicant = SQLHelper.ContructList<Question>(ds);
            List<Question> lstPre = lstApplicant.Where(u => u.QuestionCategoryId == 2).Select(u => u).ToList();
            return lstPre;

        }


        [HttpGet]
        [Route("BindGeneralAssessment/{ApplicantId}")]
        public List<LookupQuestionCategory> BindGeneralAssessment(int ApplicantId)
        {
            Question objQuestion = new Question();
            objQuestion.ApplicantId = Convert.ToInt32(ApplicantId);

            LookupQuestionCategory objLookQuesCat = new LookupQuestionCategory();

            List<LookupQuestionCategory> lLookupQuestionByCat = objLookQuesCat.GetLookupQuestionCategory();
            List<LookupQuestionCategory> parentCat = lLookupQuestionByCat.Where(u => u.Parent == 0).Select(u => u).ToList();
            List<LookupQuestionCategory> ChilCat = new List<LookupQuestionCategory>();
            foreach (LookupQuestionCategory objParent in parentCat)
            {

                ChilCat = lLookupQuestionByCat.Where(u => u.Parent == objParent.QuestionCategoryId).Select(u => u).ToList();
                if (ChilCat.Count > 0)
                {
                    return ChilCat;
                }

            }
            return ChilCat;

        }


        [HttpGet]
        [Route("GetQuestionByCategory/{ApplicantId}/{QuesCatId}")]
        public List<Question> GetQuestionByCategory(int ApplicantId, int QuesCatId)
        {
            Question objQuestion = new Question();
            List<Question> lstQuestions = objQuestion.GetQuestionByCategory(QuesCatId, ApplicantId);
            return lstQuestions;
        }

        [HttpGet]
        [Route("GetGeneralInfo/{JobApplicantId}")]
        public ApplicantGeneralInfo GetGeneralInfo(int JobApplicantId)
        {
            ApplicantGeneralInfo objApplicantGeneralInfo = new ApplicantGeneralInfo();
            ApplicantGeneralInfo AppGeneralInfo = objApplicantGeneralInfo.GetGeneralInfoByJobApplicantId(JobApplicantId).FirstOrDefault();
            return AppGeneralInfo;
        }





        [HttpPost]
        [Route("SaveApplicantIsAgree")]
        public string SaveApplicantIsAgree([FromBody]Applicant objApplicant)
        {
            Applicant AppAgree = objApplicant;
            try
            {
                int id = AppAgree.SaveApplicantAgree();
                if (id == 0)
                {
                    return "0";
                }
                else
                {
                    if (AppAgree.IsAgree)
                    {
                        SendEmails sendEmails = new SendEmails();
                        sendEmails.setMailContent(AppAgree.ApplicantId, SendEmails.EStatus.Applied.ToString());
                        sendEmails.setMailContent(AppAgree.ApplicantId, SendEmails.EStatus.JobApplication.ToString());
                    }
                }
                return "1";
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        [HttpGet]
        [Route("GetApplicantAddress/{JobApplicationId}")]
        public List<ApplicantAddress> GetApplicantAddress(int JobApplicationId)
        {
            ApplicantAddress objAddress = new ApplicantAddress();
            List<ApplicantAddress> objList = objAddress.GetApplicantAddress(JobApplicationId);
            return objList;
        }

        [HttpPost]
        [Route("SaveApplicantAddress")]
        public string SaveApplicantAddress([FromBody]ApplicantAddress objAddress)
        {
            ApplicantAddress AppAddress = objAddress;
            try
            {
                int id = AppAddress.SaveApplicantAddress(AppAddress);
                if (id == 0)
                {
                    return "0";
                }
                return "1";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        [Route("UpdateProfile")]
        public List<Applicant> UpdateProfile([FromBody]Applicant objApplicant)
        {
            Applicant lstLoginInfo = objApplicant;
            try
            {
                List<Applicant> lstLogin = new List<Applicant>();
                DataSet ds = lstLoginInfo.UpdateProfile(lstLoginInfo);
                if (ds.Tables.Count == 0)
                {
                    return lstLogin;
                }
                lstLogin = SQLHelper.ContructList<Applicant>(ds);
                return lstLogin;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet]
        [Route("GetApplicanGeneralInfo/{JobApplicationId}")]
        public List<ApplicantGeneralInfo> GetApplicanGeneralInfo(int JobApplicationId)
        {
            ApplicantGeneralInfo objGeneralInfo = new ApplicantGeneralInfo();
            List<ApplicantGeneralInfo> objList = objGeneralInfo.GetGeneralInfoByJobApplicantId(JobApplicationId);
            return objList;
        }

        [HttpPost]
        [Route("SaveApplicantGeneralInfo")]
        public string SaveApplicantGeneralInfo([FromBody]ApplicantGeneralInfo objInfo)
        {
            ApplicantGeneralInfo AppGeneralInfo = objInfo;
            try
            {
                AppGeneralInfo.RehireSourceId = Convert.ToInt32(Applicant.RehireSource.HiredByCSCApplicant);
                int id = AppGeneralInfo.SaveApplicantGeneralInfo(AppGeneralInfo);
                if (id == 0)
                {
                    return "0";
                }
                return "1";
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.Message);
                ErrorLogger.Log(ex.StackTrace);
                throw ex;
            }
        }

        [HttpGet]
        [Route("GetApplicantOccupation/{JobApplicationId}")]
        public List<ApplicantOccupation> GetApplicantOccupation(int JobApplicationId)
        {
            ApplicantOccupation objOccupation = new ApplicantOccupation();
            List<ApplicantOccupation> objList = objOccupation.GetOccupationByJobApplicantId(JobApplicationId);
            return objList;
        }

        [HttpGet]
        [Route("GetApplicantDegree/{JobApplicationId}")]
        public List<ApplicantDegree> GetApplicantDegree(int JobApplicationId)
        {
            ApplicantDegree objDegree = new ApplicantDegree();
            List<ApplicantDegree> objList = objDegree.GetDegreeByJobApplicantId(JobApplicationId);
            return objList;
        }

        [HttpGet]
        [Route("DeleleApplicantOccupationById/{OccupationId}")]
        public string DeleleApplicantOccupationById(int OccupationId)
        {
            ApplicantOccupation AppOccupation = new ApplicantOccupation() { OccupationId = OccupationId };
            try
            {
                int id = AppOccupation.DeleteApplicantOccupation();
                if (id == 0)
                {
                    return "0";
                }
                return "1";
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.Message);
                ErrorLogger.Log(ex.StackTrace);
                throw ex;
            }
        }

        [HttpPost]
        [Route("SaveApplicantOccupation")]
        public string SaveApplicantOccupation([FromBody]ApplicantOccupation _ApplicantOccupation)
        {
            ApplicantOccupation AppOccupation = _ApplicantOccupation;
            try
            {
                if (_ApplicantOccupation != null)
                {
                    if (_ApplicantOccupation.StartMonth != "0")
                    {
                        _ApplicantOccupation.StartMonth = _ApplicantOccupation.StartMonth != null ? Convert.ToDateTime(_ApplicantOccupation.StartMonth).ToString("MMMM yyyy") : null;
                    }
                    else
                    {
                        _ApplicantOccupation.StartMonth = "";
                    }
                    if (_ApplicantOccupation.EndMonth != "0")
                    {
                        _ApplicantOccupation.EndMonth = _ApplicantOccupation.EndMonth != null ? Convert.ToDateTime(_ApplicantOccupation.EndMonth).ToString("MMMM yyyy") : null;
                    }
                    else
                    {
                        _ApplicantOccupation.EndMonth = "";
                    }
                    //_ApplicantOccupation.StartMonth = _ApplicantOccupation.StartMonth != null ? Convert.ToDateTime(_ApplicantOccupation.StartMonth).ToString("MMMM yyyy") : null;
                    //_ApplicantOccupation.EndMonth = _ApplicantOccupation.EndMonth != null ? Convert.ToDateTime(_ApplicantOccupation.EndMonth).ToString("MMMM yyyy") : null;
                    //_ApplicantOccupation.GradMonth = Degree.GradMonth != null ? Convert.ToDateTime(Degree.GradMonth).ToString("MMMM yyyy") : null;
                    int id = AppOccupation.SaveApplicantOccupation(AppOccupation);
                    if (id == 0)
                    {
                        return "0";
                    }
                    return "1";
                }
                else
                {
                    return "0";
                }
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.Message);
                ErrorLogger.Log(ex.StackTrace);
                throw ex;
            }
        }

        [HttpPost]
        [Route("UpdateApplicantOccupation")]
        public string UpdateApplicantOccupation([FromBody]ApplicantOccupation _ApplicantOccupation)
        {
            ApplicantOccupation AppOccupation = _ApplicantOccupation;
            try
            {
                if (_ApplicantOccupation != null)
                {
                    if (_ApplicantOccupation.StartMonth != "0")
                    {
                        _ApplicantOccupation.StartMonth = _ApplicantOccupation.StartMonth != null ? Convert.ToDateTime(_ApplicantOccupation.StartMonth).ToString("MMMM yyyy") : null;
                    }
                    else
                    {
                        _ApplicantOccupation.StartMonth = "";
                    }
                    if (_ApplicantOccupation.EndMonth != "0")
                    {
                        _ApplicantOccupation.EndMonth = _ApplicantOccupation.EndMonth != null ? Convert.ToDateTime(_ApplicantOccupation.EndMonth).ToString("MMMM yyyy") : null;
                    }
                    else
                    {
                        _ApplicantOccupation.EndMonth = "";
                    }
                    int id = AppOccupation.UpdateApplicantOccupation(AppOccupation);
                    if (id == 0)
                    {
                        return "0";
                    }
                    return "1";
                }
                else
                {
                    return "0";
                }
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.Message);
                ErrorLogger.Log(ex.StackTrace);
                throw ex;
            }
        }

        [HttpPost]
        [Route("SaveApplicantDegree")]
        public string SaveApplicantDegree([FromBody]ApplicantDegree Degree)
        {
            ApplicantDegree AppDegree = Degree;
            try
            {
                Degree.GradeYear = (Degree.GradeYear != null && Degree.GradeYear != "") ? Convert.ToDateTime(Degree.GradMonth).ToString("yyyy") : null;
                Degree.GradDate = (Degree.GradDate != null && Degree.GradDate != "") ? Convert.ToDateTime(Degree.GradDate).ToString("MM/dd/yyyy") : null;
                Degree.GradMonth = (Degree.GradMonth != null && Degree.GradMonth != "") ? Convert.ToDateTime(Degree.GradMonth).ToString("MMMM yyyy") : null;
                if (Degree.AcademicCompletionDate != "")
                {
                    Degree.AcademicCompletionDate = Degree.AcademicCompletionDate != null ? Convert.ToDateTime(Degree.AcademicCompletionDate).ToString("MMMM yyyy") : null;
                }
                else
                {
                    Degree.AcademicCompletionDate = "";
                }

                //file Name
                string Ext = ".doc";
                if (Degree.FileName.Contains("pdf"))
                {
                    Ext = ".pdf";
                }
                else if (Degree.FileName.Contains("doc"))
                {
                    Ext = ".doc";
                }

                else if (Degree.FileName.Contains("docx"))
                {
                    Ext = ".docx";
                }
                string filename = "Resume_" + DateTime.Now.ToString("yyyyMMddHHmmss") + Ext;
                if (!string.IsNullOrEmpty(Degree.FileData))
                {
                    Degree.FileName = filename;
                }
                else
                {
                    Degree.FileName = null;
                }
                int ApplicantId = AppDegree.SaveApplicantDegree(AppDegree);
                if (ApplicantId <= 0)
                {
                    return "0";
                }

                //File Saved
                if (!string.IsNullOrEmpty(Degree.FileData))
                {
                    string webRootPath = @"P:";//_hostingEnvironment.WebRootPath;
                    bool folderExists = System.IO.Directory.Exists(webRootPath + "\\WSRApplicantForm\\" + ApplicantId + "\\");
                    if (!folderExists)
                        Directory.CreateDirectory(webRootPath + "\\WSRApplicantForm\\" + ApplicantId + "\\");


                    string fileNameWitPath = webRootPath + "\\WSRApplicantForm\\" + ApplicantId + "\\" + filename;
                    Degree.FileName = filename;
                    using (FileStream fs = new FileStream(fileNameWitPath, FileMode.Create))
                    {

                        using (BinaryWriter bw = new BinaryWriter(fs))
                        {

                            byte[] data = Convert.FromBase64String(Degree.FileData);
                            bw.Write(data);
                            bw.Close();
                        }

                    }
                }
                return "https://hportalwsrsvc.schedulingsite.com/ApplicantForm" + "\\" + ApplicantId + "\\" + filename;
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.Message);
                ErrorLogger.Log(ex.StackTrace);
                return "0";
            }
        }

        [HttpPost]
        [Route("UpdateApplicantReferences")]
        public string UpdateApplicantReferences([FromBody]ApplicantReference refrence)
        {
            ApplicantReference AppReference = refrence;
            try
            {
                int id = AppReference.UpdateReferences();
                if (id == 0)
                {
                    return "0";
                }
                return "1";
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.Message);
                ErrorLogger.Log(ex.StackTrace);
                throw ex;
            }
        }

        [HttpPost]
        [Route("SaveApplicantReferences")]
        public string SaveApplicantReferences([FromBody]ApplicantReference reference)
        {
            ApplicantReference AppOccupation = reference;
            try
            {
                int id = AppOccupation.SaveReferences();
                if (id == 0)
                {
                    return "0";
                }
                return "1";
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.Message);
                ErrorLogger.Log(ex.StackTrace);
                throw ex;
            }
        }

        [HttpGet]
        [Route("GetApplicantReferences/{JobApplicationId}")]
        public List<ApplicantReference> GetApplicantReferences(int JobApplicationId)
        {
            ApplicantReference objDegree = new ApplicantReference();
            List<ApplicantReference> objList = objDegree.GetReferenceByJobApplicantId(JobApplicationId);
            return objList;
        }

        [HttpGet]
        [Route("DeleleApplicantReferenceById/{ApplicantReferenceID}")]
        public string DeleleApplicantReferenceById(int ApplicantReferenceID)
        {
            ApplicantReference AppReference = new ApplicantReference() { ApplicantReferenceID = ApplicantReferenceID };
            try
            {
                int id = AppReference.DeleteApplicantReference();
                if (id == 0)
                {
                    return "0";
                }
                return "1";
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.Message);
                ErrorLogger.Log(ex.StackTrace);
                throw ex;
            }
        }

        [HttpGet]
        [Route("DeleleApplicantCertificationById/{CertificationID}")]
        public string DeleleApplicantCertificationById(int CertificationID)
        {
            ApplicantCertification AppCertification = new ApplicantCertification() { CertificationID = CertificationID };
            try
            {
                int id = AppCertification.DeleteApplicantCertification();
                if (id == 0)
                {
                    return "0";
                }
                return "1";
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.Message);
                ErrorLogger.Log(ex.StackTrace);
                throw ex;
            }

        }

        [HttpGet]
        [Route("GetApplicantCertification/{JobApplicationId}")]
        public List<ApplicantCertification> GetApplicantCertification(int JobApplicationId)
        {
            ApplicantCertification objGeneralInfo = new ApplicantCertification();
            List<ApplicantCertification> objList = objGeneralInfo.GetCertificationByJobApplicantId(JobApplicationId);
            return objList;
        }

        [HttpPost]
        [Route("SaveCertification")]
        public string SaveCertification([FromBody]ApplicantCertification Cetificate)
        {
            if (Cetificate != null)
            {
                ApplicantCertification AppGeneralInfo = Cetificate;

                if (AppGeneralInfo.SecurityLicence == false)
                {
                    //delete the license infor from the db.
                    return "1";
                }
                try
                {
                    Cetificate.IssueYear = string.IsNullOrEmpty(Cetificate.IssueMonth) ? null : Convert.ToDateTime(Cetificate.IssueMonth).ToString("yyyy");
                    Cetificate.ExpirationYear = string.IsNullOrEmpty(Cetificate.ExpirationMonth) ? null : Convert.ToDateTime(Cetificate.ExpirationMonth).ToString("yyyy");
                    Cetificate.IssueMonth = string.IsNullOrEmpty(Cetificate.IssueMonth) ? null : Convert.ToDateTime(Cetificate.IssueMonth).ToString("MMMM yyyy");
                    Cetificate.ExpirationMonth = string.IsNullOrEmpty(Cetificate.ExpirationMonth) ? null : Convert.ToDateTime(Cetificate.ExpirationMonth).ToString("MMMM yyyy");
                    int id = AppGeneralInfo.SaveCertification(AppGeneralInfo);
                    if (id == 0)
                    {
                        return "0";
                    }
                    return id.ToString();
                }
                catch (Exception ex)
                {
                    ErrorLogger.Log(ex.Message);
                    ErrorLogger.Log(ex.StackTrace);
                    throw ex;
                }
            }

            return "0";
        }

        [HttpPost]
        [Route("UpdateCertification")]
        public string UpdateCertification([FromBody]ApplicantCertification Cetificate)
        {
            if (Cetificate != null)
            {
                ApplicantCertification AppGeneralInfo = Cetificate;
                if (AppGeneralInfo.SecurityLicence == false)
                {
                    //delete the license infor from the db.
                    return "1";
                }
                try
                {
                    Cetificate.IssueYear = string.IsNullOrEmpty(Cetificate.IssueMonth) ? null : Convert.ToDateTime(Cetificate.IssueMonth).ToString("yyyy");
                    Cetificate.ExpirationYear = string.IsNullOrEmpty(Cetificate.ExpirationMonth) ? null : Convert.ToDateTime(Cetificate.ExpirationMonth).ToString("yyyy");
                    Cetificate.IssueMonth = string.IsNullOrEmpty(Cetificate.IssueMonth) ? null : Convert.ToDateTime(Cetificate.IssueMonth).ToString("MMMM yyyy");
                    Cetificate.ExpirationMonth = string.IsNullOrEmpty(Cetificate.ExpirationMonth) ? null : Convert.ToDateTime(Cetificate.ExpirationMonth).ToString("MMMM yyyy");
                    int id = AppGeneralInfo.UpdateCertification(AppGeneralInfo);
                    if (id == 0)
                    {
                        return "0";
                    }
                    return "1";
                }
                catch (Exception ex)
                {
                    ErrorLogger.Log(ex.Message);
                    ErrorLogger.Log(ex.StackTrace);
                    throw ex;
                }
            }
            return "0";
        }

        [HttpPost]
        [Route("SaveApplicantDisclosure")]
        public string SaveApplicantDisclosure([FromBody]ApplicantDisclosure Disclosure)
        {
            ApplicantDisclosure AppDisclosure = Disclosure;
            try
            {
                int id = AppDisclosure.SaveApplicantDisclosure(AppDisclosure);
                if (id == 0)
                {
                    return "0";
                }
                return "1";

            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.Message);
                ErrorLogger.Log(ex.StackTrace);
                throw ex;
            }
        }

        [HttpGet]
        [Route("GetApplicantDisclosure/{JobApplicationId}")]
        public List<ApplicantDisclosure> GetApplicantDisclosure(int JobApplicationId)
        {
            ApplicantDisclosure objDisclosre = new ApplicantDisclosure();
            List<ApplicantDisclosure> objList = objDisclosre.GetDisclosureByJobApplicantId(JobApplicationId);
            return objList;
        }

        [HttpGet]
        [Route("GetApplicantSignature/{ApplicantId}")]
        public CanvasImage GetApplicantSignature(int ApplicantId)
        {
            Applicant obj = new Applicant();
            DataSet ds = obj.fnGetESignature(ApplicantId);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                CanvasImage img = new CanvasImage();
                img.FirstName = ds.Tables[0].Rows[0]["FirstName"].ToString();
                img.LastName = ds.Tables[0].Rows[0]["LastName"].ToString();
                img.ApplicantId = ApplicantId;
                img.UserId = Convert.ToInt32(ds.Tables[0].Rows[0]["UserId"].ToString());
                img.ImageName = ds.Tables[0].Rows[0]["SignatureImage"].ToString();
                return img;
            }
            else if (ds != null && ds.Tables.Count > 1 && ds.Tables[1].Rows.Count > 0)
            {
                CanvasImage img = new CanvasImage();
                img.FirstName = ds.Tables[1].Rows[0]["FirstName"].ToString();
                img.LastName = ds.Tables[1].Rows[0]["LastName"].ToString();
                img.ApplicantId = ApplicantId;
                img.UserId = Convert.ToInt32(ds.Tables[1].Rows[0]["UserId"].ToString());
                string Name = img.FirstName + " " + img.LastName;
                img.ImageName = SignConvert(Name.ToLower(), ApplicantId.ToString());
                obj.UpdateSignatureImage(img);
                return img;
            }
            else
            {
                return null;
            }
        }

        protected string SignConvert(string Name, string ApplicantId)
        {
            //string SignaturePath = Startup.SignaturePath;

            string webRootPath = "P:"; //_hostingEnvironment.WebRootPath;

            var random = new Random();
            var list = new List<string> { "Gabriola", "Windsong", "Amanda Signature", "Alfrida Signature" };
            int index = random.Next(list.Count);
            string FullName = ToTitleCase(Name);
            Bitmap bitmap = new Bitmap(1, 1);
            Font font = new Font(list[index], 30, FontStyle.Italic, GraphicsUnit.Pixel);
            Graphics graphics = Graphics.FromImage(bitmap);
            int width = (int)graphics.MeasureString(FullName, font).Width;
            int height = (int)graphics.MeasureString(FullName, font).Height;
            bitmap = new Bitmap(bitmap, new Size(width, height));
            graphics = Graphics.FromImage(bitmap);
            graphics.SmoothingMode = SmoothingMode.AntiAlias;
            graphics.TextRenderingHint = TextRenderingHint.AntiAlias;
            graphics.DrawString(FullName, font, new SolidBrush(Color.FromArgb(0, 0, 0)), 0, 0);
            graphics.Flush();
            graphics.Dispose();
            string filename = ApplicantId.ToString() + '_' + DateTime.Now.ToString("yyyyMMddHHmmss") + ".png";
            bool folderExists = System.IO.Directory.Exists(webRootPath + "\\WSRApplicantForm\\" + ApplicantId + "\\");
            if (!folderExists)
                System.IO.Directory.CreateDirectory(webRootPath + "\\WSRApplicantForm\\" + ApplicantId + "\\");
            string fileNameWitPath = webRootPath + "\\WSRApplicantForm\\" + ApplicantId + "\\" + filename;
            bitmap.Save(fileNameWitPath, ImageFormat.Png);
            return filename;
        }

        public string ToTitleCase(string str)
        {
            return CultureInfo.CurrentCulture.TextInfo.ToTitleCase(str.ToLower());
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("ValidateUser")]
        public List<Applicant> ValidateUser([FromBody]Applicant objApplicant)
        {
            Applicant LoginInfo = objApplicant;
            List<Applicant> lstLogin = new List<Applicant>();
            try
            {
                DataSet ds = LoginInfo.LoginApplicant(LoginInfo);
                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    DataSet ds1 = new DataSet();
                    ds1.Tables.Add(ds.Tables[0].Copy());
                    lstLogin = SQLHelper.ContructList<Applicant>(ds1);
                    foreach (Applicant obj in lstLogin)
                    {
                        obj.DOB = (obj.DOB != null && obj.DOB != "") ? Convert.ToDateTime(obj.DOB).ToString("MM/dd/yyyy") : null;
                    }
                    AuthorizeService auth = new AuthorizeService();
                    string _token = auth.Authenticate(objApplicant.EmailID, _appSettings);
                    lstLogin[0].Token = _token;
                    return lstLogin;
                }
                else if (ds.Tables.Count > 1 && ds.Tables[1].Rows.Count > 0)
                {
                    LoginInfo.ApplicantId = -1;
                    LoginInfo.BranchId = Convert.ToInt32(ds.Tables[1].Rows[0]["BranchId"]);
                    LoginInfo.Rehire = Convert.ToBoolean(Convert.ToInt32(ds.Tables[1].Rows[0]["ReHire"]));
                    LoginInfo.UserId = Convert.ToInt32(ds.Tables[1].Rows[0]["UserId"]);
                    lstLogin.Add(LoginInfo);
                    AuthorizeService auth = new AuthorizeService();
                    string _token = auth.Authenticate(objApplicant.EmailID, _appSettings);
                    lstLogin[0].Token = _token;
                    return lstLogin;
                }
                else
                {
                    throw new Exception("Either username or password incorrect");
                }
            }
            catch (Exception ex)
            {
                return lstLogin;
            }

        }


        //[HttpPost]
        //[Route("AdminValidateUser")]
        //public List<Applicant> AdminValidateUser([FromBody]Applicant objApplicant)
        //{
        //    Applicant LoginInfo = objApplicant;
        //    List<Applicant> lstLogin = new List<Applicant>();
        //    try
        //    {
        //        DataSet ds = LoginInfo.AdminLoginApplicant(LoginInfo);
        //        if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
        //        {
        //            DataSet ds1 = new DataSet();
        //            ds1.Tables.Add(ds.Tables[0].Copy());
        //            lstLogin = SQLHelper.ContructList<Applicant>(ds1);
        //            foreach (Applicant obj in lstLogin)
        //            {
        //                obj.DOB = (obj.DOB != null && obj.DOB != "") ? Convert.ToDateTime(obj.DOB).ToString("MM/dd/yyyy") : null;
        //            }
        //            return lstLogin;
        //        }
        //        else if (ds.Tables.Count > 1 && ds.Tables[1].Rows.Count > 0)
        //        {
        //            return lstLogin;
        //        }
        //        else
        //        {
        //            throw new Exception("Either username or password incorrect");
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        return lstLogin;
        //    }

        //}


        [HttpGet]
        [Route("UpdateCurrentPage/{ApplicantId}/{CurrPage}")]
        public string UpdateCurrentPage(int ApplicantId, string CurrPage)
        {
            if (!CurrPage.Contains('/'))
            {
                CurrPage = "/" + CurrPage;
            }
            Applicant lstCurrPage = new Applicant() { ApplicantId = ApplicantId, CurrPage = CurrPage };
            try
            {
                int id = lstCurrPage.UpdateCurrentPage(ApplicantId, CurrPage);
                if (id == 0)
                {
                    return "0";
                }
                return "1";
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.Message);
                ErrorLogger.Log(ex.StackTrace);
                throw ex;
            }
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("GetApplicationEmailPwd")]
        public List<Applicant> GetApplicationEmailPwd([FromBody]Applicant objApplicant)
        {
            Applicant objApp = new Applicant();
            Applicant LoginInfo = objApplicant;
            List<Applicant> lstLogin = SQLHelper.ContructList<Applicant>(objApp.GetApplicantPassword(LoginInfo.ApplicantId));
            return lstLogin;
        }

        [HttpGet]
        [Route("GetApplicantTask/{ApplicantId}")]
        public List<ApplicantTask> GetApplicantTask(int ApplicantId)
        {
            List<ApplicantTask> lstLogin = new List<ApplicantTask>();
            ApplicantTask LoginInfo = new ApplicantTask();
            LoginInfo.ApplicantId = ApplicantId;
            try
            {
                DataSet ds = LoginInfo.GetApplicantTask();

                DataTable dtTaskLevel = ds.Tables[0].AsEnumerable().GroupBy(r => r.Field<int>("TaskLevel")).Select(g => g.First()).CopyToDataTable();

                DataView dtTaskLevel1 = dtTaskLevel.DefaultView;
                // dtTaskLevel1.RowFilter = "TaskId != 3";
                dtTaskLevel1.Sort = "TaskLevel ASC";
                dtTaskLevel = dtTaskLevel1.ToTable();
                DataTable dtTask = new DataTable();
                foreach (DataRow dr in dtTaskLevel.Rows)
                {
                    DataView vwTaskComplete = ds.Tables[0].DefaultView; //For  Complete task
                    vwTaskComplete.RowFilter = "TaskLevel = " + dr["TaskLevel"].ToString() + " AND (StatusId = 2 OR  StatusId = 3) ";
                    if (vwTaskComplete.Count > 0)
                    {
                        if (dtTask.Rows.Count > 0)
                            dtTask.Merge(vwTaskComplete.ToTable());
                        else
                            dtTask = vwTaskComplete.ToTable();
                    }

                    DataView vwTaskPending = ds.Tables[0].DefaultView; //For  Pending task
                    vwTaskPending.RowFilter = "StatusId = 1 AND TaskLevel= " + dr["TaskLevel"].ToString();
                    if (vwTaskPending.Count > 0)
                    {
                        if (dtTask.Rows.Count > 0)
                            dtTask.Merge(vwTaskPending.ToTable());
                        else
                            dtTask = vwTaskPending.ToTable();

                        break;  //if pending status then  complete first 
                    }
                }

                ds.Reset();
                ds.Tables.Add(dtTask);
                if (ds.Tables.Count > 0)
                {
                    lstLogin = SQLHelper.ContructList<ApplicantTask>(ds);
                    return lstLogin;
                }
                else
                {
                    throw new Exception("0");
                }
            }
            catch (Exception ex)
            {
                return lstLogin;
            }

        }

        [HttpGet]
        [Route("GetApplicantStatusByApplicantId/{ApplicantId}")]
        public List<Applicant> GetApplicantStatusByApplicantId(int ApplicantId)
        {
            List<Applicant> lstApplicant = new List<Applicant>();
            Applicant _Applicant = new Applicant() { ApplicantId = ApplicantId };
            try
            {
                lstApplicant = SQLHelper.ContructList<Applicant>(_Applicant.ApplicantStatusByApplicantId(_Applicant));
                string Status = Enum.GetName(typeof(EStatus), lstApplicant.FirstOrDefault().Status);
                lstApplicant.FirstOrDefault().ApplicantStatus = Status;
                return lstApplicant;
            }
            catch (Exception ex)
            {
                return lstApplicant;
            }

        }

        [HttpGet]
        [Route("GetInterviewVenueDates/{BranchId}/{VenueId}/{IsOrientation}")]
        public List<VenueAvail> GetInterviewVenueDates(int BranchId, int VenueId, string ZipCode = "", int IsOrientation = 0)
        {
            InterviewVenues _Branch = new InterviewVenues();
            _Branch.BranchId = BranchId;
            Nullable<int> VID = null;
            if (ZipCode == "")
                ZipCode = null;
            if (VenueId != 0)
                VID = VenueId;

            DataSet ds = _Branch.GetInterviewVenueDate(BranchId, IsOrientation, 2, VID, ZipCode);
            List<VenueAvail> lstVenueAvail = SQLHelper.ContructList<VenueAvail>(ds);

            return lstVenueAvail;

        }


        [HttpGet]
        [Route("GetInterviewVenueDatesVenue/{BranchId}/{VenueId}/{IsOrientation}")]
        public List<VenueAvail> GetInterviewVenueDatesVenue(int BranchId, int VenueId, int IsOrientation = 0)
        {
            InterviewVenues _Branch = new InterviewVenues();
            _Branch.BranchId = BranchId;
            Nullable<int> VID = null;
            //if (ZipCode == "")
            //    ZipCode = null;
            if (VenueId != 0)
                VID = VenueId;

            DataSet ds = _Branch.GetInterviewVenueDateVenue(BranchId, IsOrientation, 2, VID, null);
            List<VenueAvail> lstVenueAvail = SQLHelper.ContructList<VenueAvail>(ds);

            return lstVenueAvail;

        }

        [HttpGet]
        [Route("GetInterviewVenues1/{Date}/{BranchId}/{IsOrientation}")]
        public List<VenueAvail> GetInterviewVenues1(String Date, int BranchId, int IsOrientation = 0)
        {
            InterviewVenues _Branch = new InterviewVenues();
            _Branch.BranchId = BranchId;
            DataSet ds = _Branch.GetAvailInterviewVenueByDate(Date, BranchId, IsOrientation);
            List<VenueAvail> lstLogin = SQLHelper.ContructList<VenueAvail>(ds);
            return lstLogin;
        }


        [HttpPost]
        [Route("GetInterviewVenues")]
        public List<VenueAvail> GetInterviewVenues([FromBody]VenueAvail obj)
        {
            VenueAvail objConv = obj;
            InterviewVenues _Branch = new InterviewVenues();
            _Branch.BranchId = obj.BranchId;
            DataSet ds = _Branch.GetAvailInterviewVenueByDate(obj.Date, obj.BranchId, obj.IsOrientation, 0, obj.VenueId);
            List<VenueAvail> lstLogin = SQLHelper.ContructList<VenueAvail>(ds);
            return lstLogin;
        }



        //[HttpPost]
        //[Route("GetInterviewVenuesByVenue")]
        //public List<VenueAvail> GetInterviewVenuesByVenue([FromBody]VenueAvail obj)
        //{
        //    VenueAvail objConv = obj;
        //    InterviewVenues _Branch = new InterviewVenues();
        //    _Branch.BranchId = obj.BranchId;
        //    DataSet ds = _Branch.GetAvailInterviewVenueByDateVenue(obj.Date, obj.BranchId, obj.IsOrientation, obj.VenueId);
        //    List<VenueAvail> lstLogin = SQLHelper.ContructList<VenueAvail>(ds);
        //    return lstLogin;
        //}

        [HttpPost]
        [Route("UpdateApplicationStatus")]
        public string UpdateApplicationStatus([FromBody]JobApplication obj)
        {
            string status = "0";
            JobApplication _JobApplication = new JobApplication();
            _JobApplication.JobApplicationId = obj.JobApplicationId;
            _JobApplication.StatusID = 2;

            if (_JobApplication.ScheduleForInterview(obj.interviewVenueId, false, true, obj.ModifiedBy, obj.InterviewSession, obj.IsOrientation))
            {
                if (obj.StatusID == 1)
                    _JobApplication.JobApplictionlog_InsUpd(_JobApplication);

                status = "1";
                SendEmails sendEmails = new SendEmails();
                sendEmails.setMailContent(obj.ApplicantId, EStatus.Schedule_for_Interview.ToString());
            }
            return status;
        }

        [HttpPost]
        [Route("GetBranchHiringSetting")]
        public HiringSettings GetBranchHiringSetting([FromBody]Branch obj)
        {
            Branch objBranch = new Branch(obj.BranchId);
            HiringSettings lstLogin = objBranch.HiringSetting;

            // string jsonString = JsonConvert.SerializeObject(lstLogin);
            return lstLogin;
        }

        [HttpPost]
        [Route("SaveConvictions")]
        public string SaveConvictions([FromBody]Conviction obj)
        {
            Conviction objConv = obj;
            try
            {
                int id = objConv.SaveConviction();
                if (id == 0)
                {
                    return "0";
                }
                return "1";
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.Message);
                ErrorLogger.Log(ex.StackTrace);
                throw ex;
            }
        }

        [HttpGet]
        [Route("GetConvictions/{ApplicantId}")]
        public List<Conviction> GetConvictions(int ApplicantId)
        {
            Conviction objCOnv = new Conviction() { ApplicantId = ApplicantId };
            List<Conviction> lst = objCOnv.GetConvictionByID();
            return lst;
        }

        [HttpGet]
        [Route("DeleleConviction/{ApplicantId}")]
        public string DeleleConviction(int ApplicantId)
        {
            Conviction objCOnv = new Conviction() { ApplicantId = ApplicantId };
            try
            {
                int id = objCOnv.DeleteConviction();
                if (id == 0)
                {
                    return "0";
                }
                return "1";
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.Message);
                ErrorLogger.Log(ex.StackTrace);
                throw ex;
            }
        }

        [HttpGet]
        [Route("GetApplicantInterviewVenue/{ApplicantId}")]
        public List<Applicant> GetApplicantInterviewVenue(int ApplicantId)
        {
            Applicant objApp = new Applicant();
            List<Applicant> objList = objApp.GetApplicantInterviewVenue(ApplicantId);
            return objList;
        }

        [HttpGet]
        [Route("SaveApplicantDocument/{ApplicantId}/{DocumentPath}/{ApplicantTaskId}/{CreatedBy}")]
        public int SaveApplicantDocument(int ApplicantId, string DocumentPath, int ApplicantTaskId, int CreatedBy)
        {
            ApplicantDAL _ApplicationDAL = new ApplicantDAL();
            return _ApplicationDAL.SaveApplicantDocument(ApplicantId, DocumentPath, ApplicantTaskId, CreatedBy);
        }

        [HttpGet]
        [Route("DeleteBgChecksAddressById/{BGCheckAddressId}")]
        public string DeleteBgChecksAddressById(int BGCheckAddressId)
        {
            BGCheck obj = new BGCheck();
            return obj.DeleteBgChecksAddressById(BGCheckAddressId).ToString();
        }

        [HttpGet]
        [Route("GetBgCheck/{ApplicantId}")]
        public List<BGCheck> GetBgCheck(int ApplicantId)
        {
            BGCheck obj = new BGCheck();
            List<BGCheck> lstBGCheck = obj.GetApplicantBGCheckById(ApplicantId);
            return lstBGCheck;
        }

        [HttpPost]
        [Route("SaveBgCheck")]
        public string SaveBgCheck([FromBody]BGCheck BGCheck)
        {
            BGCheck objBGCheck = BGCheck;
            try
            {
                int id = objBGCheck.SaveApplicantBGCheck();
                if (id == 0)
                {
                    return "0";
                }
                return "1";
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.Message);
                ErrorLogger.Log(ex.StackTrace);
                throw ex;
            }
        }
        [HttpPost]
        [Route("UploadCanvasImage")]
        public ShowMessage UploadCanvasImage([FromBody]CanvasImage imageData)
        {
            ShowMessage ShowMessage = new ShowMessage();
            try
            {

                string webRootPath = @"P:";//_hostingEnvironment.WebRootPath;

                string filename = imageData.ApplicantId.ToString() + '_' + DateTime.Now.ToString("yyyyMMddHHmmss") + ".png";
                bool folderExists = System.IO.Directory.Exists(webRootPath + "\\WSRApplicantForm\\" + imageData.ApplicantId + "\\");
                if (!folderExists)
                    Directory.CreateDirectory(webRootPath + "\\WSRApplicantForm\\" + imageData.ApplicantId + "\\");
                string fileNameWitPath = webRootPath + "\\WSRApplicantForm\\" + imageData.ApplicantId + "\\" + filename;

                imageData.ImageName = filename;
                imageData.imageData = imageData.imageData.Replace("data:image/png;base64,", "");
                using (FileStream fs = new FileStream(fileNameWitPath, FileMode.Create))
                {

                    using (BinaryWriter bw = new BinaryWriter(fs))
                    {

                        byte[] data = Convert.FromBase64String(imageData.imageData);

                        bw.Write(data);

                        bw.Close();
                    }

                }
                Applicant obj = new Applicant();
                obj.UpdateSignatureImage(imageData);
                return ShowMessage.Show("Electronic Signature saved successfully.", ShowMessage.messageType.Success);
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.Message);
                ErrorLogger.Log(ex.StackTrace);
                return ShowMessage.Show("Invalid", ShowMessage.messageType.Error);
            }
        }


        [HttpGet]
        [Route("GetW4Form/{ApplicantId}")]
        public List<Applicant> GetW4Form(int ApplicantId)
        {
            List<Applicant> lstLogin = new List<Applicant>();
            Applicant LoginInfo = new Applicant();
            LoginInfo.ApplicantId = ApplicantId;
            try
            {
                DataSet ds = LoginInfo.GetW4Form(LoginInfo);
                if (ds.Tables.Count > 0)
                {
                    lstLogin = SQLHelper.ContructList<Applicant>(ds);
                    foreach (Applicant obj in lstLogin)
                    {
                        obj.DOB = (obj.DOB != null && obj.DOB != "") ? Convert.ToDateTime(obj.DOB).ToString("MM/dd/yyyy") : null;
                    }
                    return lstLogin;
                }
                else
                {
                    throw new Exception("Either username or password incorrect");
                }
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.Message);
                ErrorLogger.Log(ex.StackTrace);
                return lstLogin;
            }

        }

        [HttpPost]
        [Route("UpdateFOrmW14")]
        public List<Applicant> UpdateFOrmW14([FromBody]Applicant obj)
        {
            try
            {
                Applicant LoginInfo = obj;
                LoginInfo.UpdateFOrmW14();
                List<Applicant> lstLogin = new List<Applicant>();
                DataSet ds = LoginInfo.GetApplicantByID(LoginInfo.ApplicantId);
                if (ds.Tables.Count == 0)
                {
                    return lstLogin;
                }
                lstLogin = SQLHelper.ContructList<Applicant>(ds);
                return lstLogin;
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.Message);
                ErrorLogger.Log(ex.StackTrace);
                throw ex;
            }

        }

        [HttpPost]
        [Route("UpdateFromWT4")]
        public DataTable UpdateFromWT4([FromBody]FormWT4 obj)
        {
            try
            {
                if (obj == null)
                {
                    return null;
                }
                Applicant LoginInfo = new Applicant();
                LoginInfo.UpdateFromWT4(obj);
                List<Applicant> lstLogin = new List<Applicant>();
                DataSet ds = LoginInfo.GetApplicantForWT4ByID(LoginInfo.ApplicantId);
                return ds.Tables[0];
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.Message);
                ErrorLogger.Log(ex.StackTrace);
                throw ex;
            }

        }
		
		//**********************************************************************
        // Added on 18 dec 2019 for new W4 Form
        //**********************************************************************

        [HttpGet]
        [Route("GetW4FormNew/{ApplicantId}")]
        public List<ApplicantW4> GetW4FormNew(int ApplicantId)
        {
            List<ApplicantW4> lstLogin = new List<ApplicantW4>();
            ApplicantW4 LoginInfo = new ApplicantW4();
            LoginInfo.ApplicantId = ApplicantId;
            try
            {
                DataSet ds = LoginInfo.GetW4Form(LoginInfo);
                lstLogin = SQLHelper.ContructList<ApplicantW4>(ds);
                return lstLogin;

                //if (ds.Tables.Count > 0)
                //{
                //    lstLogin = SQLHelper.ContructList<ApplicantW4>(ds);
                //    foreach (Applicant obj in lstLogin)
                //    {
                //        obj.DOB = (obj.DOB != null && obj.DOB != "") ? Convert.ToDateTime(obj.DOB).ToString("MM/dd/yyyy") : null;
                //    }
                //    return lstLogin;
                //}
                //else
                //{
                //    throw new Exception("Either username or password incorrect");
                //}
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.Message);
                ErrorLogger.Log(ex.StackTrace);
                return lstLogin;
            }

        }

        //**********************************************************************
        // Added on 18 dec 2019 for new W4 Form
        //**********************************************************************


        [HttpPost]
        [Route("UpdateFormW4")]
        public void UpdateFormW4([FromBody]ApplicantW4 obj)
        {
            try
            {
                ///Applicant applicant = new Applicant();

                ApplicantW4 LoginInfo = obj;
                LoginInfo.UpdateFormW4();

                PdfGenerator _pdfobj = new PdfGenerator();
                _pdfobj.FillW4Form(obj.ApplicantId, obj.AppTaskId, obj.UserId, obj.IsGenerate);

                //List<ApplicantW4> lstLogin = new List<ApplicantW4>();

                //DataSet ds = applicant.GetApplicantByID(LoginInfo.ApplicantId);
                //if (ds.Tables.Count == 0)
                //{
                //    return lstLogin;
                //}
                //lstLogin = SQLHelper.ContructList<Applicant>(ds);
                //return lstLogin;
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.Message);
                ErrorLogger.Log(ex.StackTrace);
                throw ex;
            }

        }

        [HttpGet]
        [Route("RegenerateW4Form/{ApplicantId}/{AppTaskId}/{UserId}/{IsGenerate}")]
        public void RegenerateW4Form(int ApplicantId, int AppTaskId, int UserId, string IsGenerate)
        {
            PdfGenerator _pdfobj = new PdfGenerator();
            _pdfobj.FillW4Form(ApplicantId, AppTaskId, UserId, IsGenerate);
        }

        //**********************************************************************
        // Added on 28 Jan 2021 for new W4 Form 2021
        //**********************************************************************


        [HttpPost]
        [Route("UpdateFormW42021")]
        public void UpdateFormW42021([FromBody]ApplicantW4 obj)
        {
            try
            {
                ///Applicant applicant = new Applicant();

                ApplicantW4 LoginInfo = obj;
                LoginInfo.UpdateFormW4();

                PdfGenerator _pdfobj = new PdfGenerator();
                _pdfobj.FillW4Form2021(obj.ApplicantId, obj.AppTaskId, obj.UserId, obj.IsGenerate);

            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.Message);
                ErrorLogger.Log(ex.StackTrace);
                throw ex;
            }

        }

        [HttpGet]
        [Route("RegenerateW4Form2021/{ApplicantId}/{AppTaskId}/{UserId}/{IsGenerate}")]
        public void RegenerateW4Form2021(int ApplicantId, int AppTaskId, int UserId, string IsGenerate)
        {
            PdfGenerator _pdfobj = new PdfGenerator();
            _pdfobj.FillW4Form2021(ApplicantId, AppTaskId, UserId, IsGenerate);
        }

        [HttpGet]
        [Route("GetApplicantFormI9/{ApplicantId}")]
        public List<ApplicantFormI9> GetApplicantFormI9(int ApplicantId)
        {
            ApplicantFormI9 obj = new ApplicantFormI9();
            obj.ApplicantId = ApplicantId;
            List<ApplicantFormI9> lstVerity = obj.GetApplicantFormI9(obj);
            return lstVerity;

        }

        [HttpPost]
        [Route("SaveApplicantFormI9")]
        public string SaveApplicantFormI9([FromBody]ApplicantFormI9 ApplicantFormI9)
        {
            ApplicantFormI9 objApplicantFormI9 = ApplicantFormI9;
            try
            {
                int id = objApplicantFormI9.SaveApplicantFormI9(objApplicantFormI9);
                if (id == 0)
                {
                    return "0";
                }
                return "1";
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.Message);
                ErrorLogger.Log(ex.StackTrace);
                throw ex;
            }

        }

        [HttpPost]
        [Route("InsApplicantPreScreening")]
        public int InsApplicantPreScreening([FromBody]ApplicantPreScreening Obj)
        {
            ApplicantPreScreening objApp = Obj;
            int id = objApp.InsApplicantPreScreening(objApp);
            if (id == 0)
            {
                return 0;
            }
            return id;
        }

        [HttpGet]
        [Route("GetApplicantPreScreening/{ApplicantId}")]
        public List<ApplicantPreScreening> GetApplicantPreScreening(int ApplicantId)
        {
            ApplicantPreScreening obj = new ApplicantPreScreening();
            DataSet ds = obj.GetApplicantPreScreening(ApplicantId);
            SQLHelper objHelper = new SQLHelper();
            List<ApplicantPreScreening> lstSources = SQLHelper.ContructList<ApplicantPreScreening>(ds);
            return lstSources;
        }


        [HttpGet]
        [Route("InsApplicantValidate/{ApplicantId}/{FieldName}/{FieldValue}")]
        public int InsApplicantValidate(int ApplicantId, string FieldName, string FieldValue)
        {
            Validate obj = new Validate();
            obj.ApplicantId = ApplicantId;
            switch (FieldName)
            {
                case "FirstName":
                    {
                        obj.FirstName = FieldValue;
                        obj.Type = 1;
                        break;
                    }
                case "MiddleName":
                    {
                        obj.MiddleName = FieldValue;
                        obj.Type = 2;
                        break;
                    }
                case "LastName":
                    {
                        obj.LastName = FieldValue;
                        obj.Type = 3;
                        break;
                    }
                case "SSN":
                    {
                        obj.SSN = FieldValue;
                        obj.Type = 4;
                        break;
                    }
                case "DOB":
                    {
                        FieldValue = FieldValue.Substring(0, 2) + "/" + FieldValue.Substring(2, 2) + "/" + FieldValue.Substring(4, 4);
                        obj.DateOfBirth = Convert.ToDateTime(FieldValue);
                        obj.Type = 5;
                        break;
                    }
                case "FINISH":
                    {
                        obj.Finish = 1;
                        obj.Type = 6;
                        break;
                    }
            }
            int id = obj.InsApplicantValidate(obj);
            if (id == 0)
            {
                return 0;
            }
            return id;
        }

        [HttpPost]
        [Route("InsApplicantWage")]
        public int InsApplicantWage([FromBody]ApplicantWage ApplicantWage)
        {
            ApplicantWage objApplicantFormI9 = ApplicantWage;
            int id = objApplicantFormI9.intApplicantWage(objApplicantFormI9);
            if (id == 0)
            {
                return 0;
            }
            return id;
        }

        [HttpGet]
        [Route("GetApplicantZurichDetails/{ApplicantId}")]
        public List<Applicant> GetApplicantZurichDetails(int ApplicantId)
        {
            Applicant obj = new Applicant();
            List<Applicant> lstBGCheck = obj.GetApplicantZurichDetails(ApplicantId);
            return lstBGCheck;
        }

        [HttpPost]
        [Route("SaveApplicantZurich")]
        public int SaveApplicantZurich([FromBody]Applicant obj)
        {
            Applicant objApplicant = new Applicant();
            int id = objApplicant.SaveApplicantZurich(obj);
            if (id == 0)
            {
                return 0;
            }
            return id;
        }

        [HttpGet]
        [Route("GetApplicantAllFAQ/{TaskId}/{ApplicantId}")]
        public List<FAQuestion> GetApplicantAllFAQ(int TaskId, int ApplicantId)
        {
            List<FAQuestion> lstFAQuestion = new List<FAQuestion>();
            try
            {
                Question _Ques = new Question();
                lstFAQuestion = _Ques.fnGetApplicantAllFAQ(TaskId, ApplicantId);
                return lstFAQuestion;
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.Message);
                ErrorLogger.Log(ex.StackTrace);
                return lstFAQuestion;
            }
        }

        [HttpGet]
        [Route("GetAllApplicantFAQ/{BranchId}/{TaskId}/{ApplicantId}/{Phase}")]
        public List<FAQuestion> GetAllApplicantFAQ(int BranchId, int TaskId, int ApplicantID, int Phase)
        {
            List<FAQuestion> lstFAQuestion = new List<FAQuestion>();
            try
            {
                Question _Ques = new Question();

                lstFAQuestion = SQLHelper.ContructList<FAQuestion>(_Ques.fnGetAllApplicantFAQ(BranchId, TaskId, ApplicantID, Phase).Tables[0]);
                return lstFAQuestion;
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.Message);
                ErrorLogger.Log(ex.StackTrace);
                return lstFAQuestion;
            }
        }

        [HttpGet]
        [Route("GetApplicantFAQ/{TaskId}/{ApplicantId}/{QuestionNo}")]
        public List<FAQuestion> GetApplicantFAQ(int TaskId, int ApplicantId, int QuestionNo)
        {
            List<FAQuestion> lstFAQuestion = new List<FAQuestion>();
            try
            {
                Question _Ques = new Question();
                lstFAQuestion = _Ques.fnGetApplicantFAQ(TaskId, ApplicantId, QuestionNo);
                return lstFAQuestion;
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.Message);
                ErrorLogger.Log(ex.StackTrace);
                return lstFAQuestion;
            }
        }

        [HttpPost]
        [Route("InsApplicantFAQ")]
        public int InsApplicantFAQ([FromBody]FAQuestion objFAQuestion)
        {
            try
            {
                Question _Ques = new Question();
                FAQuestion Obj = new FAQuestion();
                Obj.ApplicantId = objFAQuestion.ApplicantId;
                Obj.TaskId = objFAQuestion.TaskId;
                Obj.Question = objFAQuestion.Question;
                Obj.Answer = objFAQuestion.Answer;
                Obj.AnswerGivenBy = objFAQuestion.AnswerGivenBy;
                Obj.IAgreed = objFAQuestion.IAgreed;
                Obj.QuestionNo = objFAQuestion.QuestionNo;
                switch (objFAQuestion.TaskId)
                {
                    case 19:
                        if (objFAQuestion.QuestionNo == 1)
                        {
                            Obj.QuestionTittle = "Equal Employment Opportunity (EEO) Policy";
                        }
                        else if (objFAQuestion.QuestionNo == 2)
                        {
                            Obj.QuestionTittle = "Affirmative Action Policy";
                        }
                        else if (objFAQuestion.QuestionNo == 3)
                        {
                            Obj.QuestionTittle = "Workplace Violence Policy";
                        }
                        else if (objFAQuestion.QuestionNo == 4)
                        {
                            Obj.QuestionTittle = "Non-Harassment / Sexual Harassment Policy";
                        }
                        else if (objFAQuestion.QuestionNo == 5)
                        {
                            Obj.QuestionTittle = "Anti-Retaliation Policy";
                        }
                        else if (objFAQuestion.QuestionNo == 6)
                        {
                            Obj.QuestionTittle = "Drug and Alcohol Free Workplace Policy";
                        }
                        else if (objFAQuestion.QuestionNo == 7)
                        {
                            Obj.QuestionTittle = "Conflict of Business Interest Policy";
                        }
                        else if (objFAQuestion.QuestionNo == 8)
                        {
                            Obj.QuestionTittle = "At-Will Employment Policy";
                        }
                        break;
                };
                return _Ques.InsApplicantFAQ(Obj);
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.Message);
                ErrorLogger.Log(ex.StackTrace);
                return 0;
            }
        }
        [HttpGet]
        [Route("GetQuestionSetByTaskId/{BranchId}/{TaskId}/{ApplicantId}")]
        public List<FAQuestion> GetQuestionSetByTaskId(int BranchId, int TaskId, int ApplicantId)
        {
            List<FAQuestion> lstFAQuestion = new List<FAQuestion>();
            try
            {
                Question _Ques = new Question();
                lstFAQuestion = _Ques.fnGetFAQuestion(BranchId, TaskId, ApplicantId);
                return lstFAQuestion;
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.Message);
                ErrorLogger.Log(ex.StackTrace);
                return lstFAQuestion;
            }
        }

        [HttpPost]
        [Route("fnUpApplicantFAQ")]
        public int fnUpApplicantFAQ([FromBody]FAQuestion objFAQuestion)
        {
            try
            {
                Question _Ques = new Question();
                FAQuestion Obj = new FAQuestion();
                Obj.ApplicantId = objFAQuestion.ApplicantId;
                Obj.TaskId = objFAQuestion.TaskId;
                Obj.Answer = objFAQuestion.Answer;
                Obj.AnswerGivenBy = objFAQuestion.AnswerGivenBy;
                Obj.QuestionNo = objFAQuestion.QuestionNo;
                int temp = _Ques.fnUpApplicantFAQ(Obj);
                if (temp > 0)
                {
                    SendEmails sendEmails = new SendEmails();
                    sendEmails.setMailContent(objFAQuestion.ApplicantId, EStatus.Manager_Answer.ToString(), null, null);
                }
                return temp;
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.Message);
                ErrorLogger.Log(ex.StackTrace);
                return 0;
            }
        }

        [HttpPost]
        [Route("UpdateCompleteProfile")]
        public int UpdateCompleteProfile([FromBody]Applicant objApplicant)
        {
            return objApplicant.UpdateCompleteProfile();
        }

        [HttpGet]
        [Route("ShowLog/{ApplicantId}")]
        public List<ViewLog> ShowLog(int ApplicantId)
        {
            List<ViewLog> lstViewLog = new List<ViewLog>();
            try
            {
                Applicant obj = new Applicant();
                lstViewLog = SQLHelper.ContructList<ViewLog>(obj.ViewLog(ApplicantId));
                return lstViewLog;
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.Message);
                ErrorLogger.Log(ex.StackTrace);
                return lstViewLog;
            }
        }

        [HttpGet]
        [Route("GetEVerifyHistory/{ApplicantId}")]
        public DataSet GetEVerifyHistory(int ApplicantId)
        {
            DataSet ds = new DataSet();
            try
            {
                ApplicantTask obj = new ApplicantTask();
                obj.ApplicantId = ApplicantId;
                return obj.GetEVerifyHistory();
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.Message);
                ErrorLogger.Log(ex.StackTrace);
                return ds;
            }
        }

        [HttpGet]
        [Route("fnUpApplicantResultId/{ApplicantId}/{ResultId}")]
        public int fnUpApplicantResultId(int ApplicantId, int ResultId)
        {
            try
            {
                ApplicantTask obj = new ApplicantTask();
                obj.ApplicantId = ApplicantId;
                obj.ResultId = ResultId;
                return obj.fnUpApplicantResultId();
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.Message);
                ErrorLogger.Log(ex.StackTrace);
                return 0;
            }
        }

        [HttpGet]
        [Route("EVerifyDashboard/{BranchId}")]
        public string EVerifyDashboard(int BranchId)
        {
            string url = "";
            try
            {
                Applicant obj = new Applicant();
                DataTable dt = obj.fnGetBranch(BranchId).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    url = "https://www.formi9.com/formi9verify/formI9loginservice.aspx?" +
                                    "AlliancePartnerID=" + Startup.AlliancePartnerID +
                                    "&AlliancePartnerLogin=" + Startup.AlliancePartnerLogin +
                                    "&AlliancePartnerPassword=" + Startup.AlliancePartnerPassword +
                                    "&AlliancePartnerCompanyID=" + dt.Rows[0]["AlliancePartnerCompanyID"] + "&B2BUserName=&Target=Dashboard";
                }
                return url;
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.Message);
                ErrorLogger.Log(ex.StackTrace);
                return url;
            }
        }

        [HttpGet]
        [Route("EVerifyDashboardArchieve/{BranchId}")]
        public string EVerifyDashboardArchieve(int BranchId)
        {
            string url = "";
            try
            {
                Applicant obj = new Applicant();
                DataTable dt = obj.fnGetBranch(BranchId).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    url = "https://www.formi9.com/formi9verify/formI9loginservice.aspx?" +
                            "AlliancePartnerID=" + Startup.AlliancePartnerID +
                            "&AlliancePartnerLogin=" + Startup.AlliancePartnerLogin +
                            "&AlliancePartnerPassword=" + Startup.AlliancePartnerPassword +
                            "&AlliancePartnerCompanyID=" + dt.Rows[0]["AlliancePartnerCompanyID"] + "&B2BUserName=&Target=EVPArchived";
                }
                return url;
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.Message);
                ErrorLogger.Log(ex.StackTrace);
                return url;
            }
        }

        [HttpPost]
        [Route("AddCall")]
        public void AddCall([FromBody]CallLog objCall)
        {
            CallLog obj = new CallLog();
            obj.CallAction = objCall.CallAction;
            obj.Remarks = objCall.Remarks;
            obj.ApplicantId = objCall.ApplicantId;
            obj.UpdatedBy = objCall.UpdatedBy;
            obj.SaveCallLog();
        }

        [HttpGet]
        [Route("ResetPwd/{ApplicantId}")]
        public string ResetPwd(int ApplicantId)
        {
            Applicant obj = new Applicant();
            obj.ApplicantId = ApplicantId;
            obj.ResetPassword();
            SendEmails sendEmails = new SendEmails();
            sendEmails.setMailContent(ApplicantId, EStatus.PasswordReset.ToString());
            SendEmailUserInfo objuserInfo = sendEmails.GetUserInfo(ApplicantId, SendEmails.EStatus.PasswordReset);
            string msg = objuserInfo.Password;
            return msg;
        }

        [HttpGet]
        [Route("GetApplicant_StatusAll/{statusId}/{BranchId}/{PageIndex}/{PageCount}/{IsAgree}/{includeAllStatuses}/{FirstName}/{LastName}/{EmailID}/{PrimaryPhone}/{ZipCode}/{AppliedOnStart}/{AppliedOnEnd}/{SortColumn}/{Dir}/{Duplicate}/{showDuplicate}/{year}/{SearchByHomeBranch}")]
        public DataTable GetApplicant_StatusAll(int statusId, int BranchId, int PageIndex, int PageCount, string IsAgree, bool includeAllStatuses, string FirstName = null, string LastName = null, string EmailID = null, string PrimaryPhone = null, string ZipCode = null, string AppliedOnStart = null, string AppliedOnEnd = null, string SortColumn = "", string Dir = "", string Duplicate = null, Boolean showDuplicate = false, string year = null, bool SearchByHomeBranch = false)
        {
            ApplicantDAL _ApplicantDAL = new ApplicantDAL();
            return _ApplicantDAL.GetApplicantByStatusAll(statusId, BranchId, PageIndex, PageCount, includeAllStatuses, IsAgree == "null" ? "" : IsAgree,
                FirstName == "null" ? "" : FirstName, LastName == "null" ? "" : LastName, EmailID == "null" ? "" : EmailID,
                PrimaryPhone == "null" ? "" : PrimaryPhone, ZipCode == "null" ? "" : ZipCode, AppliedOnStart == "null" ? "" : AppliedOnStart,
                AppliedOnEnd == "null" ? "" : AppliedOnEnd, SortColumn, Dir, Duplicate, showDuplicate, year, SearchByHomeBranch);
        }

        string GetValue(DataTable tbl, string key)
        {
            tbl.DefaultView.RowFilter = "Key='" + key + "'";
            DataView vw = tbl.DefaultView;
            if (vw != null && vw.Count > 0)
            {
                return Convert.ToString(vw[0]["Value"]);
            }
            else
                return null;
        }

        [HttpPost]
        [Route("fnSendEmail")]
        public int fnSendEmail([FromBody]DashBoardSendEmail obj)
        {
            try
            {
                FileStream fs = null;
                FileInfo fInfo = null;
                SendEmails sendEmails = new SendEmails();
                User objUser = new User(obj.LoggedInUserId);
                DataTable tblSettings = objUser.GetSettings(SettingSections.SendEmail);
                //_JobApplication.JobApplicationId = Convert.ToInt32(Dict.Key);
                SendEmailUserInfo objuserInfo = sendEmails.GetUserInfo(obj.ApplicantId, SendEmails.EStatus.All);
                if (objuserInfo != null)
                {
                    string emailBody = obj.EmailBody;
                    string domainURL = Startup.WebSiteURL;

                    if (obj.SelectedLink == "0" || obj.SelectedLink == "1")
                    {
                        domainURL = domainURL + "/reschdule/" + Convert.ToString(obj.JobApplicationId) + "/" + obj.SelectedLink + "/" + obj.BranchId + "/" + obj.ApplicantId;
                    }
                    if (obj.SelectedLink == "2")
                    {
                        domainURL = domainURL + "/Login/" + Convert.ToString(obj.BranchId);
                    }
                    if (obj.SelectedLink == "4")
                    {
                        domainURL = domainURL + "/OfferLetter/" + obj.BranchId + "/" + obj.ApplicantId;
                    }
                    emailBody += "<br/>" + domainURL;
                    //MailContent objMailCOntent = new MailContent() { toEmailaddress = objuserInfo.Email, displayName = "Woodside  Recruiting Staff", subject = obj.SubjectName, emailBody = emailBody };
                    MailContent objMailCOntent = new MailContent() { From = "HR@woodsideranch.com", toEmailaddress = objuserInfo.Email, displayName = "Woodside Recruiting Staff", subject = obj.SubjectName, emailBody = emailBody };
                    if (obj.AttachFile != null || obj.AttachFile != "")
                    {

                        string filePath = domainURL + "/Temp/" + obj.AttachFile;

                        if (System.IO.File.Exists(filePath) && fs == null)
                        {
                            fInfo = new FileInfo(filePath);
                            fs = fInfo.Open(FileMode.Open, FileAccess.Read, FileShare.Read);
                        }
                        if (fs != null)
                        {
                            List<System.Net.Mail.Attachment> attachments = new List<System.Net.Mail.Attachment>();
                            attachments.Add(new System.Net.Mail.Attachment(fs, fInfo.Name));
                            // attachments.Add(new System.Net.Mail.Attachment(browseFile.PostedFile.InputStream, fileName));
                            objMailCOntent.strAttachment = attachments;
                            //fs.Close();
                            // File.Delete(filePath);
                        }

                    }
                    if (tblSettings.Rows.Count > 0)
                    {
                        if (obj.chkReplyMe)
                        {
                            objMailCOntent.From = GetValue(tblSettings, "ReplyEmail");
                        }
                        if (obj.chkCopyMe)
                        {
                            objMailCOntent.CopyTo.Add(GetValue(tblSettings, "CopyEmail"));
                        }
                    }

                    //SendEmails objSendEmail = new SendEmails();

                    //  ErrorLogger.Log(objMailCOntent.toEmailaddress);
                    //objSendEmail.SendBulEmail(objMailCOntent, null, obj.BranchId);
                    SendEmails.SendAttachment(objMailCOntent, null);

                    EmailLog objEmailLog = new EmailLog();
                    objEmailLog.ApplicantId = obj.ApplicantId;

                    objEmailLog.UserId = obj.LoggedInUserId;
                    objEmailLog.Subject = obj.SubjectName;
                    objEmailLog.Body = emailBody;
                    objEmailLog.SaveLog();
                }

            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.Message);
                ErrorLogger.Log(ex.StackTrace);
            }
            return 1;
        }

        [HttpGet]
        [Route("GetApplicantProgress/{ApplicantId}")]
        public DataSet GetApplicantProgress(int ApplicantId)
        {
            Applicant _Application = new Applicant();
            return _Application.GetApplicantProgress(ApplicantId);
        }

        [HttpGet]
        [Route("GetLog/{ApplicantId}")]
        public DataSet GetLog(int ApplicantId)
        {
            CallLog _CallLog = new CallLog();
            return _CallLog.GetLog(ApplicantId);
        }
        [AllowAnonymous]
        [HttpGet]
        [Route("ForgotPwd/{EmailID}")]
        public string ForgotPwd(string EmailID)
        {
            string webRootPath = _hostingEnvironment.WebRootPath;
            Applicant objApplicant = new Applicant();
            try
            {
                string pwd = objApplicant.ResetPassword(EmailID);
                if (pwd == "0")
                {
                    return "0";
                }
                else
                {
                    SendEmails objSendEmail = new SendEmails();
                    EmailParameters emailParameters = new EmailParameters()
                    {
                        UserName = "Applicant",
                        Email = EmailID,
                        Password = pwd,
                        XMLFilePath = "PasswordReset.xsl"
                    };
                    string xmlData = emailParameters.GetXML();
                    string strBody = MailerUtility.GetMailBody(webRootPath + "\\xslt\\" + emailParameters.XMLFilePath, xmlData);
                    MailContent objMailContent = new MailContent() { From = "HR@woodsideranch.com", toEmailaddress = EmailID, displayName = "Woodside Recruiting Staff", subject = "Password Reset", emailBody = strBody };
                    objSendEmail.SendEmailInBackgroundThread(objMailContent);
                    return "1";
                }
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.Message);
                ErrorLogger.Log(ex.StackTrace);
                return ex.Message;
            }

        }

        [HttpPost]
        [Route("TransferLocation")]
        public bool TransferLocation([FromBody]Applicant objApp)
        {
            Applicant obj = new Applicant();
            return obj.TransferLocation(objApp.ApplicantId, objApp.BranchId, objApp.UserId);
        }

        [HttpGet]
        [Route("GetApplicantSigninAddress/{ApplicantId}")]
        public DataSet GetApplicantSigninAddress(int ApplicantId)
        {
            ApplicantTask obj = new ApplicantTask();
            obj.ApplicantId = ApplicantId;
            DataSet ds = obj.getApplicantSigninAddress();
            Random rand = new Random();
            DataTable dtAddress = ds.Tables[0].AsEnumerable().OrderBy(r => rand.Next()).Take(4).CopyToDataTable();
            DataRow dr = dtAddress.NewRow();
            dr["ApplicantId"] = -1;
            dr["address"] = "None";
            dtAddress.Rows.Add(dr);
            dtAddress.AcceptChanges();
            ds.Tables.Add(dtAddress);
            return ds;
        }
        [AllowAnonymous]
        [HttpPost]
        [Route("ReHiredEmployee")]
        public string ReHiredEmployee([FromBody]Applicant obj)
        {
            Applicant objApplicant = new Applicant();
            string webRootPath = _hostingEnvironment.WebRootPath;
            try
            {
                string pwd = objApplicant.fnReHiredEmployee(obj.EmailID);
                SendEmails objSendEmail = new SendEmails();
                EmailParameters emailParameters = new EmailParameters()
                {
                    UserName = "Applicant",
                    Email = obj.EmailID,
                    Password = pwd,
                    XMLFilePath = "PasswordReset.xsl"
                    //  BranchEmail = objBranch != null ? objBranch.EmailId : "",
                    //  Branch = objBranch != null ? objBranch.BranchName : "",
                    //  VerificationLink = code
                };
                string xmlData = emailParameters.GetXML();
                string strBody = MailerUtility.GetMailBody(webRootPath + "\\xslt\\" + emailParameters.XMLFilePath, xmlData);
                MailContent objMailContent = new MailContent() { From = "HR@woodsideranch.com", toEmailaddress = obj.EmailID, displayName = "Woodside Recruiting Staff", subject = "Password Reset", emailBody = strBody };
                objSendEmail.SendEmailInBackgroundThread(objMailContent);
                return "1";
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.Message);
                ErrorLogger.Log(ex.StackTrace);
                return ex.Message;
            }

        }

        [HttpPost]
        [Route("GetDataforVerificationURL")]
        public string GetDataforVerificationURL([FromBody]Applicant obj)
        {
            string url = "";
            try
            {
                Applicant objApplicant = new Applicant();
                DataTable dt = objApplicant.GetDataforVerification(obj.ApplicantId).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    url = "https://www.formi9.com/formi9verify/formI9loginservice.aspx?" +
                                "AlliancePartnerID=" + Startup.AlliancePartnerID +
                                "&AlliancePartnerLogin=" + Startup.AlliancePartnerLogin +
                                "&AlliancePartnerPassword=" + Startup.AlliancePartnerPassword +
                                "&AlliancePartnerCompanyID=" + dt.Rows[0]["AlliancePartnerCompanyID"] + "&B2BUserName=&Target=directlink&ResultId=" + obj.ResultId + "";
                }
                return url;
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.Message);
                ErrorLogger.Log(ex.StackTrace);
                return url;
            }
        }

        [HttpGet]
        [Route("GetIsApplicantProfileCompleted/{ApplicantId}")]
        public int GetIsApplicantProfileCompleted(int ApplicantId)
        {
            SQL objSQL = new SQL();
            objSQL.AddParameter("@ApplicantId", DbType.Int32, ParameterDirection.Input, 0, ApplicantId);
            DataTable dt = objSQL.ExecuteDataSet("p_GetIsApplicantProfileCompleted").Tables[0];
            if (dt.Rows.Count > 0)
            {
                return Convert.ToInt32(dt.Rows[0]["Completed"]);
            }
            return 0;
        }
        [HttpPost]
        [Route("GetJobFairApplicantById")]
        public DataTable GetJobFairApplicantById([FromBody]JobFair objApplicant)
        {
            JobFair _obj = new JobFair();
            DataSet ds = _obj.GetJobFairApplicantById(objApplicant);
            return ds.Tables[0];
        }

        [HttpGet]
        public string Get()
        {
            return "Default Method";
            //return new string[] { "value1", "value2" };
        }

        [HttpPost]
        [Route("PostRegisterformstack")]
        public int PostRegisterformstack([FromBody]object objApplicant)
        {
            try
            {

                ErrorLogger.Log(objApplicant.ToString());

                string FName = "";
                string LName = "";
                string email = "";
                string phone = "";
                int AllowText = 0;

                int ChurchillDowns = 0;
                int Levy = 0;
                int CSC = 0;
                int PC = 0;
                int SP = 0;
                int Kentucky = 0;
                string[] arr = objApplicant.ToString().Split(',');
                for (int i = 0; i < arr.Length; i++)
                {
                    if (arr[i].Contains("Woodside Properties"))
                    {
                        CSC = 1;
                    }
                    if (arr[i].Contains("Churchill Downs"))
                    {
                        ChurchillDowns = 2;
                    }
                    if (arr[i].Contains("Levy Restaurants"))
                    {
                        Levy = 3;
                    }
                    if (arr[i].Contains("PC Staffing"))
                    {
                        PC = 4;
                    }
                    if (arr[i].Contains("SP+ Parking"))
                    {
                        SP = 5;
                    }
                    if (arr[i].Contains("Kentucky Derby Museum"))
                    {
                        Kentucky = 6;
                    }
                    string[] Id = arr[i].Split(':');
                    if (Id[0].Contains("74250243"))
                    {
                        FName = Id[2].Trim();

                    }

                    else if (Id[0].Contains("last"))
                    {
                        LName = Id[1].Trim('}').Trim().Trim('\"');
                    }
                    else if (Id[0].Contains("74250244"))
                    {
                        email = Id[1].Trim();
                    }
                    else if (Id[0].Contains("74250245"))
                    {
                        phone = Id[1].Trim();
                    }
                    else if (Id[0].Contains("74250247"))
                    {
                        AllowText = Id[1].Contains("Yes") ? 1 : 0;
                    }
                }


                SQL objSql = new SQL();
                object objContentId;
                int albumcontentId = 0;
                objSql.AddParameter("@FName", DbType.String, ParameterDirection.Input, 0, FName.Trim().Trim('\"'));
                objSql.AddParameter("@LName", DbType.String, ParameterDirection.Input, 0, LName.Trim('\"'));
                objSql.AddParameter("@Email", DbType.String, ParameterDirection.Input, 0, email.Trim('\"'));
                objSql.AddParameter("@Phone", DbType.String, ParameterDirection.Input, 0, phone.Trim('\"'));
                objSql.AddParameter("@AllowText", DbType.String, ParameterDirection.Input, 0, AllowText);
                objContentId = objSql.ExecuteScalar("jobfairapplicant_ins");
                int JFApplicantID = Convert.ToInt32(objContentId);

                if (CSC > 0)
                {
                    InsJobFairParticiapantApplicants(1, JFApplicantID, CSC);
                }
                if (ChurchillDowns > 0)
                {
                    InsJobFairParticiapantApplicants(1, JFApplicantID, ChurchillDowns);
                }
                if (Levy > 0)
                {
                    InsJobFairParticiapantApplicants(1, JFApplicantID, Levy);
                }
                if (PC > 0)
                {
                    InsJobFairParticiapantApplicants(1, JFApplicantID, PC);
                }
                if (SP > 0)
                {
                    InsJobFairParticiapantApplicants(1, JFApplicantID, SP);
                }
                if (Kentucky > 0)
                {
                    InsJobFairParticiapantApplicants(1, JFApplicantID, Kentucky);
                }


                return 1;
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.Message);
                ErrorLogger.Log(ex.StackTrace);
                return 0;
            }

        }
        public void InsJobFairParticiapantApplicants(int JobFairID, int JFApplicantID, int JFParticipantID)
        {
            SQL objSql = new SQL();
            object objContentId;
            int albumcontentId = 0;
            objSql.AddParameter("@JobFairID", DbType.String, ParameterDirection.Input, 0, JobFairID);
            objSql.AddParameter("@JFApplicantID", DbType.String, ParameterDirection.Input, 0, JFApplicantID);
            objSql.AddParameter("@JFParticipantID", DbType.String, ParameterDirection.Input, 0, JFParticipantID);
            objContentId = objSql.ExecuteNonQuery("p_JobFairParticiapantApplicants_ins");
        }

        [HttpPost]
        [Route("PostRegisterJobFair")]
        public int PostRegisterJobFair([FromBody]JobFair objApplicant)
        {
            try
            {
                JobFair _Application = new JobFair();
                objApplicant.SignInTime = DateTime.Now;
                objApplicant.SignOutTime = null;
                int id = _Application.Register(objApplicant);
                //SendEmails sendEmails = new SendEmails();
                //sendEmails.setMailContent(id, SendEmails.EStatus.Registration.ToString());
                //return id;
                return id;
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.Message);
                ErrorLogger.Log(ex.StackTrace);
                return 0;
            }
        }

        //[HttpPost]
        //[Route("PostJFPApplicants")]
        //public int PostJFPApplicants([FromBody]JobFair[] obj)
        //{
        //    if (obj != null)
        //    {
        //        JobFair _obj = new JobFair();
        //        foreach (JobFair item in obj)
        //        {
        //            int id = _obj.InsJFPApplicants(item);
        //        }             
        //        return 1;
        //    }
        //    return 0;
        //}
        [HttpPost]
        [Route("PostSignOutSave")]
        public int PostSignOutSave([FromBody]JobFair obj)
        {
            try
            {
                JobFair _Application = new JobFair();
                int id = _Application.SignOutSave(obj);
                return id;
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.Message);
                ErrorLogger.Log(ex.StackTrace);
                return 0;
            }
        }
        [HttpPost]
        [Route("PostSignInSave")]
        public int PostSignInSave([FromBody]JobFair obj)
        {
            try
            {
                JobFair _Application = new JobFair();
                int id = _Application.SignInSave(obj);
                return id;
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.Message);
                ErrorLogger.Log(ex.StackTrace);
                return 0;
            }
        }

        [HttpPost]
        [Route("GetJobFairApplicant")]
        public DataSet GetJobFairApplicant([FromBody]JobFair objApplicant)
        {
            try
            {
                JobFair _obj = new JobFair();
                DataSet ds = new DataSet();
                if (objApplicant != null)
                {
                    if (!string.IsNullOrEmpty(objApplicant.sortCol))
                    {
                        objApplicant.sortCol = objApplicant.sortCol + objApplicant.SortOrder;
                    }
                    else
                    {
                        objApplicant.sortCol = "FirstNameASC";
                    }
                    ds = _obj.GetJobFairApplicant(objApplicant);
                }
                return ds;
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.Message);
                ErrorLogger.Log(ex.StackTrace);
                return null;
            }
        }

        [HttpPost]
        [Route("PostSendMail")]
        public int PostSendMail([FromBody]JobFair objApplicant)
        {
            try
            {
                JobFairDAL sendEmails = new JobFairDAL();
                JobFair obj = new JobFair();
                string emailBody = null;
                SendEmailUserInfo objuserInfo = sendEmails.GetUserInfo(objApplicant.JFApplicantID);
                EmailParameters emailParameters = new EmailParameters()
                {
                    UserName = objuserInfo.FullName,
                    EmailBody = emailBody,
                    BranchEmail = objuserInfo.BranchEmail,
                    Branch = "Houston",
                    BranchId = objuserInfo.BranchId,
                    //OnlineInterview = "http://wish.protatechindia.com/Hportal4/JobFair/" + objApplicant.JFApplicantID,
                    OnlineInterview = Startup.WebSiteURL + "/jfhome/" + objApplicant.JFApplicantID,
                    Subject = "Job Fair",
                    XMLFilePath = "27"
                };
                obj.SendEmail(emailParameters, objuserInfo.Email);
                //JobFair _obj = new JobFair();
                //DataTable dt = _obj.GetJobFairApplicant(objApplicant);
                return 1;
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.Message);
                ErrorLogger.Log(ex.StackTrace);
                return 0;
            }
        }

        [HttpPost]
        [Route("GetJobFairParticipant")]
        public List<JobFair> GetJobFairParticipant([FromBody]JobFair objApplicant)
        {
            try
            {
                JobFair _obj = new JobFair();
                List<JobFair> lst = new List<JobFair>();
                DataSet ds = _obj.GetJobFairParticipant(objApplicant);
                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {


                    //IDArr = ds.Tables[1].Rows[0]["JFParticipant"].ToString().Split(',');  
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        string ID = dr.Field<string>("JFParticipantID").ToString();
                        bool IdFlag = false;
                        if (ds.Tables.Count > 1 && ds.Tables[1].Rows.Count > 0)
                        {
                            DataRow[] rows = ds.Tables[1].Select("[JFParticipantID] = " + ID);
                            if (rows.Length > 0)
                            {
                                IdFlag = true;
                            }
                        }
                        lst.Add(new JobFair()
                        {
                            JFParticipantID = dr.Field<string>("JFParticipantID"),
                            Name = dr.Field<string>("NAME"),
                            JFParticipantFlag = IdFlag,
                        });
                    }
                }
                return lst;

            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.Message);
                ErrorLogger.Log(ex.StackTrace);
                return null;
            }
        }
        [HttpGet]
        [Route("GetRelationShip/{ApplicantId}")]
        public List<PrivacyAct.LookUpRelationship> GetRelationShip(int ApplicantId)
        {
            try
            {
                PrivacyAct objApp = new PrivacyAct();
                List<PrivacyAct.LookUpRelationship> objList = objApp.fnGetRelationShip(ApplicantId);
                return objList;
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.Message);
                ErrorLogger.Log(ex.StackTrace);
                return null;
            }
        }

        [HttpGet]
        [Route("GetApplicantPrivacyAct/{ApplicantId}")]
        public List<PrivacyAct> GetApplicantPrivacyAct(int ApplicantId)
        {
            try
            {
                PrivacyAct objApp = new PrivacyAct();
                List<PrivacyAct> objList = objApp.fnApplicantPrivacyAct(ApplicantId);
                return objList;
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.Message);
                ErrorLogger.Log(ex.StackTrace);
                return null;
            }
        }

        [HttpPost]
        [Route("UpdateJobPosition")]
        public int UpdateJobPosition([FromBody]Applicant objApplicant)
        {
            try
            {
                Branch obj = new Branch();
                int temp = obj.UpdateJobPosition(objApplicant.ApplicantId, objApplicant.JobApplicationId.ToString());
                return temp;
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.Message);
                ErrorLogger.Log(ex.StackTrace);
                return 0;
            }
        }

        [HttpPost]
        [Route("fnInsApplicantTask")]
        public bool fnInsApplicantTask([FromBody]ApplicantTask ObjApplicantTask)
        {
            ApplicantTask obj = new ApplicantTask();
            int temp = obj.InsApplicantTask(ObjApplicantTask);
            if (temp > 0)
            {
                SendEmails sendEmails = new SendEmails();
                sendEmails.setMailContent(Convert.ToInt32(ObjApplicantTask.ApplicantId), EStatus.Manager_Offered.ToString(), null, null);
                return true;
            }
            return false;
        }


        [HttpPost]
        [Route("postPwdByApplicantId")]
        public string postPwdByApplicantId([FromBody]Applicant obj)
        {
            if (obj == null)
            {
                return "";
            }
            Applicant A_Obj = new Applicant();
            DataSet ds = A_Obj.GetPwdByApplicantId(obj.ApplicantId);
            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                return ds.Tables[0].Rows[0]["Password"].ToString();
            }
            return "";
        }
        [HttpPost]
        [Route("GetApplicantDocumentById")]
        public string GetApplicantDocumentById([FromBody]ApplicantTask obj)
        {
            Applicant A_Obj = new Applicant();
            DataSet ds = A_Obj.GetApplicantDocumentById(obj.ApplicantId, obj.ApplicantTaskId);
            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                return ds.Tables[0].Rows[0]["DocumentPath"].ToString();
            }
            return "";
        }

        [HttpPost]
        [Route("FillAerosolTransmissibleWaiverForm")]
        public int FillAerosolTransmissibleWaiverForm([FromBody] ApplicantTask obj)
        {
            try
            {
                PdfGenerator _pdfobj = new PdfGenerator();
                _pdfobj.FillAerosolTransmissibleWaiverForm(obj.ApplicantId, obj.ApplicantTaskId, obj.UserId);
                //var res = await _task.EmployeeTaskStatusUpdate(obj);
                return 1;
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.Message);
                ErrorLogger.Log(ex.StackTrace);
                throw ex;
            }

        }

    }


    public class VenueAvail
    {
        public int InterviewVenueId { get; set; }
        public int InterviewSession { get; set; }
        public String VenueName { get; set; }
        public int VenueId { get; set; }
        public int BranchId { get; set; }
        public string Date { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public int Slot { get; set; }
        public int Available { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public int IsOrientation { get; set; }
    }

    public class ViewLog
    {
        public int ApplicantId { get; set; }
        public int Num { get; set; }
        public string ApplicantName { get; set; }
        public string ModifiedDate { get; set; }
        public string STATUS { get; set; }
        public string ModifiedBy { get; set; }
        public string Remarks { get; set; }
    }

    public class DashBoardSendEmail
    {
        public int ApplicantId { get; set; }
        public int JobApplicationId { get; set; }
        public int BranchId { get; set; }
        public string SubjectName { get; set; }
        public string EmailBody { get; set; }
        public int LoggedInUserId { get; set; }
        public string AttachFile { get; set; }
        public bool chkReplyMe { get; set; }
        public bool chkCopyMe { get; set; }
        public string SelectedLink { get; set; }
    }

    public class summary
    {
        public String Source { get; set; }
        public String Status { get; set; }
        public decimal Count { get; set; }
    }
    public class ApplicantToken
    {
        public int ApplicantId { get; set; }
        public String Token { get; set; }
        public String Checkflag { get; set; }
        public int JobApplicationId { get; set; }
    }
}