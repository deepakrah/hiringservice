﻿using HPortalService.DAL;
using HPortalService.Model;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using HPortalService.Service;
using Microsoft.Extensions.Options;
using HPortalService.Helpers;
using Microsoft.AspNetCore.Authorization;

namespace HPortalService.Controllers
{
    [Produces("application/json")]
    [Authorize]
    [Route("api/Admin")]
    public class AdminController : Controller
    {
        string ReturnBatchPrintPath = Startup.ReturnBatchPrintPath;
        //string BatchPrintPath = @"F:\SVNProjects\Projects\Dev\HPortalV4.0\HPortalService\HPortalService\wwwroot\imagedoc\ProfileImages\";
        string BatchPrintPath = Startup.BatchPrintPath;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly AppSettings _appSettings;
        private IAuthorizationService _authorizationService;
        public AdminController(IHostingEnvironment hostingEnvironment, IOptions<AppSettings> appSettings, IAuthorizationService authorizationService)
        {
            _hostingEnvironment = hostingEnvironment;
            _appSettings = appSettings.Value;
            _authorizationService = authorizationService;
        }

        [HttpPost]
        [Route("AdminValidateUser")]
        [AllowAnonymous]
        public List<Applicant> AdminValidateUser([FromBody]Applicant objApplicant)
        {
            Applicant LoginInfo = objApplicant;
            List<Applicant> lstLogin = new List<Applicant>();
            try
            {
                DataSet ds = LoginInfo.AdminValidateUser(LoginInfo);
                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    lstLogin = SQLHelper.ContructList<Applicant>(ds);
                    AuthorizeService auth = new AuthorizeService();
                    string _token = auth.Authenticate(objApplicant.EmailID, _appSettings);
                    lstLogin[0].Token = _token;
                    return lstLogin;
                }
                else if (ds.Tables.Count > 1 && ds.Tables[1].Rows.Count > 0)
                {
                    return lstLogin;
                }
                else
                {
                    throw new Exception("Either username or password incorrect");
                }
            }
            catch (Exception ex)
            {
                return lstLogin;
            }
        }
        [HttpGet]
        [Route("GetCountry")]
        public List<Lookup> GetCountry()
        {
            Lookup country = new Lookup();
            List<Lookup> lstCountry = SQLHelper.ContructList<Lookup>(country.GetCountry());
            return lstCountry;
        }

        [HttpGet]
        [Route("GetBranchByUser/{id}")]
        public List<Branch> GetBranchByUser(int UserId = 2416)
        {

            Branch objBranch = new Branch();
            DataSet ds = objBranch.GetBranchByUserId(UserId);
            return SQLHelper.ContructList<Branch>(ds);
        }


        [HttpGet]
        [Route("GetState/{CountryId}")]
        public List<Lookup> GetState(string CountryId)
        {

            Lookup state = new Lookup() { CountryId = Convert.ToInt32(CountryId) };
            List<Lookup> lstState = SQLHelper.ContructList<Lookup>(state.GetStateByCountryId(state));
            return lstState;

        }
        [HttpGet]
        [Route("GetVenueByBranchId/{BranchID}")]
        public List<Venue> GetVenueByBranchId(int BranchID)
        {
            DataSet ds;
            VenueDAL _VenueInfo = new VenueDAL();
            ds = _VenueInfo.GetVenueByBranchId(BranchID);
            List<Venue> VenueList = SQLHelper.ContructList<Venue>(ds);
            return VenueList;
        }

        [HttpPost]
        [Route("PostEventScheduler")]
        public ShowMessage PostEventScheduler([FromBody]InterviewVenues vObj)
        {
            ShowMessage Message = new ShowMessage();
            if (vObj == null)
            {
                return Message.Show("Invalid.", ShowMessage.messageType.Error);
            }
            InterviewVenues IntVenueObj = new InterviewVenues();
            //if (!string.IsNullOrEmpty(vObj.Date))
            //{
            //    vObj.Date = Convert.ToDateTime(vObj.Date).ToShortDateString();
            //}
            //if (!string.IsNullOrEmpty(vObj.StartTime))
            //{
            //    vObj.StartTime = Convert.ToDateTime(vObj.StartTime).ToShortTimeString();
            //}
            //if (!string.IsNullOrEmpty(vObj.Duration))
            //{
            //    vObj.Duration = Convert.ToDateTime(vObj.Duration).ToString("HH:mm");
            //}
            int i = 0;
            i = IntVenueObj.InterviewVenue_Ins(vObj);
            return Message.Show("Event has been saved successfully.", ShowMessage.messageType.Success);

            //if (vObj.InterviewVenueId != 0)
            //{
            //    i = IntVenueObj.Mark_Publish(vObj);
            //    return Message.Show("Event has been published successfully.", ShowMessage.messageType.Success);
            //}
            //else
            //{
            //    i = IntVenueObj.InterviewVenue_Ins(vObj);
            //    return Message.Show("Event has been saved successfully.", ShowMessage.messageType.Success);
            //}


        }
        [HttpPost]
        [Route("UpEventSchedulePublish")]
        public ShowMessage UpEventSchedulePublish([FromBody]InterviewVenues vObj)
        {
            ShowMessage Message = new ShowMessage();
            InterviewVenues IntVenueObj = new InterviewVenues();
            if (vObj.InterviewVenueId != 0)
            {
                int i = IntVenueObj.Mark_Publish(vObj);
                if (vObj.isPublished)
                {
                    return Message.Show("Event has been published successfully.", ShowMessage.messageType.Success);
                }
                else
                {
                    return Message.Show("Event has been un-published successfully.", ShowMessage.messageType.Success);
                }
            }
            return Message.Show("Invalid.", ShowMessage.messageType.Error);
        }
        [HttpPost]
        [Route("PostEventSchedulerEdit")]
        public ShowMessage PostEventSchedulerEdit([FromBody]InterviewVenues vObj)
        {
            ShowMessage Message = new ShowMessage();
            InterviewVenues IntVenueObj = new InterviewVenues();
            //if (vObj.Date != null)
            //{
            //    vObj.Date = Convert.ToDateTime(vObj.Date);
            //}
            //if (!string.IsNullOrEmpty(vObj.StartTime))
            //{
            //    vObj.StartTime = Convert.ToDateTime(vObj.StartTime).ToShortTimeString();
            //}
            //if (!string.IsNullOrEmpty(vObj.Duration))
            //{
            //    vObj.Duration = Convert.ToDateTime(vObj.Duration).ToShortTimeString();
            //}

            int i = 0;
            if (vObj.InterviewVenueId != 0 && vObj.isPublished)
            {
                i = IntVenueObj.Mark_Publish(vObj);
                return Message.Show("Event has been published successfully.", ShowMessage.messageType.Success);
            }
            else
            {
                DataSet ds = IntVenueObj.EditInterviewVenue(vObj);
                if (ds != null)
                {
                    return Message.Show("Event has been updated successfully.", ShowMessage.messageType.Success);
                }
            }
            return Message.Show("Invalid", ShowMessage.messageType.Error);

            //SendEmails objsendemail = new SendEmails();
            //webOnlineTestService.BAL.EStatus enumstatus = emplList.InterviewType == "Orientation" ? webOnlineTestService.BAL.EStatus.Schedule_for_Orientation : webOnlineTestService.BAL.EStatus.Schedule_for_Interview;
            //foreach (System.Data.DataRow dr in ds.Tables[0].Rows)
            //{
            //    objsendemail.setMailContent(Convert.ToInt32(dr["ApplicantId"]), enumstatus);
            //}
            //if (vObj.InterviewVenueId != 0)
            //{
            //    i = IntVenueObj.Mark_Publish();
            //    return Message.Show("Event has been published successfully.", ShowMessage.messageType.Success);
            //}
            //else
            //{
            //    i = IntVenueObj.InterviewVenue_Ins(vObj);
            //    return Message.Show("Event has been saved successfully.", ShowMessage.messageType.Success);
            //}


        }
        [HttpPost]
        [Route("PostEventSchedulerDel")]
        public ShowMessage PostEventSchedulerDel([FromBody]InterviewVenues vObj)
        {
            ShowMessage Message = new ShowMessage();
            InterviewVenues IntVenueObj = new InterviewVenues();
            int i = IntVenueObj.DeleteInterviewVenueByInterviewVenueId(vObj.InterviewVenueId);
            if (i > 0)
            {
                return Message.Show("Event has been deleted successfully.", ShowMessage.messageType.Success);
            }
            //return Message.Show("Invalid", ShowMessage.messageType.Error);
            return Message.Show("Event date cannot be deleted as the applicants are scheduled.", ShowMessage.messageType.Error);
        }


        [HttpPost]
        [Route("PostBindEvent")]
        public List<EventsList> PostBindEvent([FromBody]InterviewVenues vObj)
        {
            if (vObj != null)
            {
                int BranchID = 0;
                string Type = "";
                string ChildBranchId = "0";
                bool SearchByHomeBranch = false;
                string color = "#84AC6E";
                List<EventsList> tasksList = new List<EventsList>();
                EventsList eLst = new EventsList();
                InterviewVenues objIV = new InterviewVenues();
                //
                if (vObj.ChildBranchId > 0)
                {
                    if (vObj.BranchId == vObj.ChildBranchId)
                    {
                        SearchByHomeBranch = true;
                    }
                    else
                    {
                        BranchID = Convert.ToInt32(ChildBranchId);
                    }
                }
                List<InterviewVenues> dt = objIV.GetInterviewVenueByBranchId(vObj.BranchId, Type != "All" ? Type : null, SearchByHomeBranch);

                foreach (InterviewVenues obj in dt)
                {
                    string strtemptime = Convert.ToDateTime(obj.Date).ToShortTimeString();

                    switch (obj.InterviewType)
                    {
                        case "Block":
                            color = "#1c2ee7";
                            break;
                        case "Group":
                            color = "#e7901c";
                            break;
                        case "Individual":
                            color = "#64ac6e";
                            break;
                        case "Orientation":
                            color = "#996633";
                            break;
                    }
                    if (!obj.isPublished)
                    {
                        color = "#a8a8a8";
                    }
                    tasksList.Add(new EventsList
                    {
                        Type = obj.InterviewType,
                        id = obj.InterviewVenueId,
                        title = obj.VenueName + " " + eLst.ToUnixDate(Convert.ToDateTime(obj.Date)) + " [" + obj.StartTime + "-" + obj.EndTime + "]",
                        VenueId = obj.VenueId,
                        start = eLst.ToUnixTimespan(Convert.ToDateTime(obj.Date), obj.StartTime),
                        end = eLst.ToUnixTimespan(Convert.ToDateTime(obj.Date), obj.EndTime),
                        bgColor = color,
                        url = "",//"EventDetails.aspx?e_id=" + obj.InterviewVenueId +  //+ Security.Encrypt(dt.Rows[i]["EventID"].ToString())",
                        Slot = obj.Slot,
                        StartTime = obj.StartTime,
                        EndTime = obj.EndTime,
                        Room = obj.RoomNo != null ? obj.RoomNo.ToString() : "",
                        Sessions = obj.Sessions != null ? obj.Sessions.ToString() : "",
                        Interviewer = obj.Interviewers != null ? obj.Interviewers.ToString() : "",
                        Duration = obj.Duration,
                        Parking = obj.ParkingInstructions,
                        ApplicantInterviewExist = obj.ApplicantInterviewExist,
                        allDay = false,
                        isPublished = obj.isPublished,
                        BranchName = obj.BranchName,
                        BranchId = obj.BranchId,
                        ShiftRoleId = obj.ShiftRoleId,
                        JobId = obj.JobId,
                        ShiftName = obj.ShiftName,
                        JobName = obj.JobName
                    });
                }
                return tasksList;
            }
            return null;
        }
        [HttpPost]
        [Route("PostCheckApplicant")]
        public List<CheckIn> PostCheckApplicant([FromBody]CheckIn vObj)
        {
            DataSet ds;
            JobApplicationDAL _AppInfo = new JobApplicationDAL();
            bool SearchByChildBranch = false;
            if (vObj == null)
            {
                return null;
            }
            int BranchID = Convert.ToInt32(vObj.BranchId);
            int ChildBranchId = Convert.ToInt32(vObj.ChildBranchId);
            if (vObj.ChildBranchId > 0)
            {
                if (BranchID == ChildBranchId)
                {
                    SearchByChildBranch = true;
                }
                else
                {
                    BranchID = ChildBranchId;
                }

            }
            bool IncludeAttended = vObj.CheckedIn == "chkAll" ? true : false;
            bool CheckedIn = vObj.CheckedIn == "chkCheckedIn" ? true : false;
            if (vObj.JobApplicationId != null)
            {
                vObj.JobApplicationId = vObj.JobApplicationId.Trim();
            }
            int JobNo = string.IsNullOrEmpty(vObj.JobApplicationId) ? 0 : Convert.ToInt32(vObj.JobApplicationId);

            string _checkInDate = string.IsNullOrEmpty(vObj.CheckInDate) ? null : Convert.ToDateTime(vObj.CheckInDate).ToShortDateString();

            ds = _AppInfo.GetApplicantInterview(vObj.BranchId, vObj.VenueId, _checkInDate, JobNo, vObj.Type, IncludeAttended, CheckedIn, SearchByChildBranch);

            SQLHelper objHelper = new SQLHelper();
            List<CheckIn> lstSources = SQLHelper.ContructList<CheckIn>(ds);
            if (lstSources.Count > 0)
            {
                lstSources[0].total = Convert.ToString(ds.Tables[0].Rows.Count);
                lstSources[0].checkedInCount = Convert.ToString(ds.Tables[0].Select("IsNULL(isAttend,0)=1").Count());
                lstSources[0].notcheckedInCount = Convert.ToString(ds.Tables[0].Select("IsNULL(isAttend,0)=0").Count());
            }
            return lstSources;
        }

        [HttpPost]
        [Route("PostCheckApplicant1")]
        public DataSet PostCheckApplicant1([FromBody]CheckIn vObj)
        {
            DataSet ds;
            JobApplicationDAL _AppInfo = new JobApplicationDAL();
            bool SearchByHomeBranch = false;
            if (vObj == null)
            {
                return null;
            }
            int BranchID = Convert.ToInt32(vObj.BranchId);
            int ChildBranchId = Convert.ToInt32(vObj.ChildBranchId);
            if (vObj.ChildBranchId > 0)
            {
                if (BranchID == ChildBranchId)
                {
                    SearchByHomeBranch = true;
                }
                else
                {
                    BranchID = ChildBranchId;
                }

            }
            bool IncludeAttended = vObj.CheckedIn == "chkAll" ? true : false;
            bool CheckedIn = vObj.CheckedIn == "chkCheckedIn" ? true : false;
            if (vObj.JobApplicationId != null)
            {
                vObj.JobApplicationId = vObj.JobApplicationId.Trim();
            }
            int JobNo = string.IsNullOrEmpty(vObj.JobApplicationId) ? 0 : Convert.ToInt32(vObj.JobApplicationId);

            string _checkInDate = string.IsNullOrEmpty(vObj.CheckInDate) ? null : Convert.ToDateTime(vObj.CheckInDate).ToShortDateString();

            ds = _AppInfo.GetApplicantInterview1(vObj.BranchId, vObj.VenueId, _checkInDate, JobNo, vObj.Type, IncludeAttended, CheckedIn, SearchByHomeBranch);

            return ds;
        }

        [HttpPost]
        [Route("PostInsAssignTask")]
        public int InsAssignTask([FromBody]ApplicantTask obj)
        {
            int ApplicantID = Convert.ToInt32(obj.ApplicantId);
            int isAttend = obj.GetApplicantCheckInStatus(obj);
            if (isAttend == 0)
            {
                return 2;
            }
            int temp = obj.InsApplicantTask(obj);
            if (temp > 0)
            {
                SendEmails sendEmails = new SendEmails();
                sendEmails.setMailContent(Convert.ToInt32(ApplicantID), EStatus.Manager_Offered.ToString(), null, null);
                return 1;
            }
            return 0;
        }
        [HttpPost]
        [Route("PostInsCheckInApplicant")]
        public Boolean PostInsCheckInApplicant([FromBody]List<CheckIn> obj)
        {
            ShowMessage Message = new ShowMessage();

            CheckIn objchk = new CheckIn();
            JobApplication objJobApplication = new JobApplication();
            objJobApplication.JobApplicationId = Convert.ToInt32(obj[0].JobApplicationId);
            objJobApplication.ModifiedBy = obj[0].UserId;
            objJobApplication.StatusID = obj[0].Type == 0 ? 15 : 16;
            int flag = objJobApplication.JobApplictionlog_InsUpd(objJobApplication);
            //code to insert Applicant Default task. done by vijay on 3 Aug 2017

            //comment on 26 feb 2019 by deepak
            //if (obj[0].ApplicantId > 0)
            //{
            //    objchk.fnInsApplicantTask(obj[0].ApplicantId, objJobApplication.StatusID);
            //}
            //end
            //if (print)
            //{
            //    if (hdnAppId != null & Convert.ToInt32(hdnAppId.Value) > 0)
            //    {
            //        objApps.Add(objJobApplication.JobApplicationId, hfID.Value + "|" + hdnAppId.Value);
            //        appIds = appIds + "," + hdnAppId.Value;
            //    }
            //}           
            if (flag > 0)
            {
                return true;
            }
            return false;
        }

        [HttpPost]
        [Route("PostInsCheckInApplicantAll")]
        public Boolean PostInsCheckInApplicantAll([FromBody]List<CheckIn> obj)
        {
            int flag = 0;
            if (obj.Count > 0)
            {
                foreach (CheckIn row in obj)
                {
                    JobApplication objJobApplication = new JobApplication();
                    objJobApplication.JobApplicationId = Convert.ToInt32(row.JobApplicationId);
                    objJobApplication.ModifiedBy = obj[0].UserId;
                    objJobApplication.StatusID = obj[0].Type == 0 ? 15 : 16;
                    flag = objJobApplication.JobApplictionlog_InsUpd(objJobApplication);
                }
            }
            if (flag > 0)
            {
                return true;
            }
            return false;
        }
        [HttpGet]
        [Route("GeteVerifyLookUp/{BranchId}")]
        public List<EverifyDoc> GeteVerifyLookUp(string BranchId)
        {
            EverifyDoc obj = new EverifyDoc();
            List<EverifyDoc> lstVerity = obj.GeteVerifyLookUp();
            return lstVerity;
        }
        [HttpGet]
        [Route("GetApplicantEverifyDoc/{ApplicantId}")]
        public List<EverifyDoc> GetApplicantEverifyDoc(string ApplicantId)
        {
            Applicant obj = new Applicant();
            DataSet ds = obj.GetDocument(Convert.ToInt32(ApplicantId));
            SQLHelper objHelper = new SQLHelper();
            List<EverifyDoc> lstVerity = SQLHelper.ContructList<EverifyDoc>(ds);
            return lstVerity;
        }
        [HttpGet]
        [Route("GetApplicantEVerifyList/{AID}")]
        public List<ApplicantFormI9> GetApplicantEVerifyList(int AID)
        {
            ApplicantFormI9 obj = new ApplicantFormI9();
            obj.ApplicantId = AID;
            return obj.fnGetApplicantEVerifyList(AID);
        }
        [HttpPost]
        [Route("PostInsEverifyDoc")]
        public List<EverifyDoc> InsEverifyDoc([FromBody]EverifyDoc obj)
        {

            return null;
        }
        [HttpGet]
        [Route("DelImg/{ApplicantEverifyDocId}/{ImageName}")]
        public int DelImg(int ApplicantEverifyDocId, string ImageName)
        {
            SQL objSQL = new SQL();
            objSQL.AddParameter("@ApplicantEverifyDoc", DbType.Int32, ParameterDirection.Input, 0, ApplicantEverifyDocId);
            int result = objSQL.ExecuteNonQuery("p_DelApplicantEverifyDocById");
            if (result > 0)
            {
                string DomainName = Startup.DomainName;
                return result;
            }
            return 0;
        }
        [HttpPost]
        [Route("PostUploadFile")]
        public async Task<IActionResult> UploadFile(List<IFormFile> files)
        {
            return RedirectToAction("Files");
        }
        [HttpGet]
        [Route("GetApplicantFormI9/{ApplicantId}")]
        public List<ApplicantFormI9> GetApplicantFormI9(int ApplicantId)
        {
            ApplicantFormI9 obj = new ApplicantFormI9();
            obj.ApplicantId = ApplicantId;
            return obj.GetApplicantFormI9(obj);
        }
        [HttpGet]
        [Route("GetDataforVerification/{ApplicantId}/{firstDayEmployment}")]
        public List<ApplicantFormI9> GetDataforVerification(int ApplicantId, string firstDayEmployment)
        {
            ShowMessage _message = new ShowMessage();
            try

            {
                if (ApplicantId > 0)
                {
                    Applicant objApplicant = new Applicant();
                    ApplicantFormI9 obj = new ApplicantFormI9();
                    DataSet ds = objApplicant.GetDataforVerification(ApplicantId);
                    SQLHelper objHelper = new SQLHelper();
                    List<ApplicantFormI9> lstVerity = new List<ApplicantFormI9>();
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        string url = obj.fnGeneratUrl(ds.Tables[0], firstDayEmployment);
                        lstVerity = SQLHelper.ContructList<ApplicantFormI9>(ds);
                        lstVerity[0].url = url;
                    }
                    return lstVerity;
                }
                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        [HttpGet]
        [Route("GetESignature/{UserId}")]
        public CanvasImage GetESignature(int UserId)
        {
            User obj = new User(UserId);
            List<CanvasImage> img = SQLHelper.ContructList<CanvasImage>(obj.fnGetESignature());
            string webRootPath = _hostingEnvironment.WebRootPath;
            if (img.Count > 0)
            {
                string url = webRootPath + "\\UserSignature\\" + UserId + "\\" + img[0].SignatureImage;
                bool folderExists = System.IO.File.Exists(url);
                img[0].FileExistsFlag = folderExists;
                return img[0];
            }
            else
            {
                CanvasImage objCanvasImage = new CanvasImage();
                return objCanvasImage;
            }
        }

        [HttpPost]
        [Route("UploadCanvasImageAdmin")]
        public ShowMessage UploadCanvasImageAdmin([FromBody]CanvasImage imageData)
        {
            ShowMessage ShowMessage = new ShowMessage();
            try
            {

                string webRootPath = _hostingEnvironment.WebRootPath;

                string filename = imageData.UserId.ToString() + '_' + DateTime.Now.ToString("yyyyMMddHHmmss") + ".png";
                bool folderExists = System.IO.Directory.Exists(webRootPath + "\\UserSignature\\" + imageData.UserId + "\\");
                if (!folderExists)
                    Directory.CreateDirectory(webRootPath + "\\UserSignature\\" + imageData.UserId + "\\");
                string fileNameWitPath = webRootPath + "\\UserSignature\\" + imageData.UserId + "\\" + filename;

                imageData.ImageName = filename;
                imageData.imageData = imageData.imageData.Replace("data:image/png;base64,", "");
                using (FileStream fs = new FileStream(fileNameWitPath, FileMode.Create))
                {

                    using (BinaryWriter bw = new BinaryWriter(fs))
                    {

                        byte[] data = Convert.FromBase64String(imageData.imageData);

                        bw.Write(data);

                        bw.Close();
                    }

                }

                string webRoot = @"D:\inetpub\wwwHportalWSR";
                bool folderExistsHPortal = System.IO.Directory.Exists(webRoot + "\\UserSignature\\" + imageData.UserId + "\\");
                if (!folderExistsHPortal)
                    Directory.CreateDirectory(webRoot + "\\UserSignature\\" + imageData.UserId + "\\");
                string fileNameWitPathHportal = webRoot + "\\UserSignature\\" + imageData.UserId + "\\" + filename;
                //Save in Hportal4 folder
                using (FileStream fs = new FileStream(fileNameWitPathHportal, FileMode.Create))
                {

                    using (BinaryWriter bw = new BinaryWriter(fs))
                    {

                        byte[] data = Convert.FromBase64String(imageData.imageData);

                        bw.Write(data);

                        bw.Close();
                    }

                }

                Applicant obj = new Applicant();
                obj.UpdateSignatureImageAdmin(imageData);
                return ShowMessage.Show("Electronic Signature saved successfully.", ShowMessage.messageType.Success);
            }
            catch
            {
                return ShowMessage.Show("Invalid", ShowMessage.messageType.Error);
            }
        }

        [HttpPost]
        [Route("PostUpdateSetting")]
        public BranchSetting PostUpdateSetting([FromBody]BranchSetting obj)
        {
            if (obj.Key == "MaxQuestion")
            {

                obj.BranchId = obj.BranchId;
                obj.Key = "MaxQuestion";
                obj.Value = obj.MaxInterview;
                obj.Section = Convert.ToString(Convert.ToInt16(SettingSections.Interview));
                obj.UpdateBranchSetting();
            }
            else if (obj.Key == "AllowSchedWithoutPublish")
            {

                obj.BranchId = obj.BranchId;
                obj.Key = "AllowSchedWithoutPublish";
                obj.Value = obj.ShowVenuesToApplicant ? "1" : "0";
                obj.Section = Convert.ToString(Convert.ToInt16(SettingSections.Venue));
                obj.UpdateBranchSetting();
            }
            else if (obj.Key == "NotHiring")
            {
                obj.BranchId = obj.BranchId;
                obj.Key = "NotHiring";
                obj.Value = obj.OpenHiring.ToString();
                obj.Section = Convert.ToString(Convert.ToInt16(SettingSections.Hiring));
                obj.UpdateBranchSetting();
                obj.Key = "HiringMonth";
                obj.Value = obj.HiringMonth;
                obj.Section = Convert.ToString(Convert.ToInt16(SettingSections.Hiring));
                obj.UpdateBranchSetting();
            }
            return obj.BindSettings(obj.BranchId);
        }
        [HttpGet]
        [Route("GetBranchSettings/{BranchId}")]
        public BranchSetting GetBranchSettings(int BranchId)
        {
            BranchSetting obj = new BranchSetting();
            return obj.BindSettings(BranchId);
        }

        [HttpPost]
        [Route("PostUpdateBranch")]
        public BranchSetting UpdateBranch([FromBody]BranchSetting obj)
        {
            try
            {
                Branch objBrch = new Branch(obj.BranchId);
                int temp = objBrch.UpdateBranch(obj.Email, obj.Address1, obj.City, obj.State, obj.Country, obj.Zip, obj.primaryPhone, obj.HiringManager, obj.Designation, obj.PayRate);
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.Message);
            }
            return obj.BindSettings(obj.BranchId);
        }

        [HttpGet]
        [Route("GetTemplateType/{BranchId}/{TemplateTypeId}")]
        public EmailTemplate GetTemplateType(int BranchId, string TemplateTypeId)
        {
            EmailTemplate objET = new EmailTemplate(TemplateTypeId, BranchId);
            return objET;
        }

        [HttpPost]
        [Route("PostSaveTemplate")]
        public ShowMessage SaveTemplate([FromBody]EmailTemplate obj)
        {
            ShowMessage Message = new ShowMessage();
            EmailTemplate objET = new EmailTemplate();
            int temp = objET.Save(obj);
            if (temp > 0)
            {
                return Message.Show(" Email template has been saved successfully.", ShowMessage.messageType.Success);
            }
            return Message.Show("Invalid.", ShowMessage.messageType.Error);
        }

        [HttpGet]
        [Route("GetSourceCategory/{BranchId}")]
        public List<BranchSource> GetSourceCategory(int BranchId)
        {
            BranchDAL _BranchDAL = new BranchDAL();
            DataSet ds = _BranchDAL.GetSourceCategory(BranchId);
            List<BranchSource> lst = SQLHelper.ContructList<BranchSource>(ds);
            return lst;
        }


        [HttpPost]
        [Route("BindExpense")]
        public DataSet BindExpense([FromBody] Source obj)
        {
            Source objSource = new Source();
            string from = null;
            string to = null;
            if (obj != null)
            {
                if (obj.DateFrom != "")
                {
                    string[] str = obj.DateFrom.Split('/');
                    from = str[0] + "/01/" + str[2];
                }
                if (obj.DateTo != "")
                {
                    string[] str = obj.DateTo.Split('/');
                    to = str[0] + "/01/" + str[2];
                }
                DataSet ds = objSource.GetExpense(obj.BranchId, from, to);
                return ds;
            }
            return null;
        }

        [HttpPost]
        [Route("PostInsertExpense")]
        public ShowMessage InsertExpense([FromBody] Source obj)
        {
            ShowMessage Message = new ShowMessage();
            try
            {
                Source objSource = new Source();

                objSource.SourceId = Convert.ToInt32(obj.SourceId);
                objSource.BranchId = Convert.ToInt32(obj.BranchId);
                objSource.Amount = Convert.ToDecimal(obj.Amount);
                objSource.CreatedBy = obj.CreatedBy;
                objSource.Method = Convert.ToInt32(obj.Method);
                if (obj.Method == 0)
                {
                    string[] month = obj.DateFrom.Split('/');
                    for (int i = 0; i < Convert.ToInt32(obj.NoOfMonths); i++)
                    {
                        if (Convert.ToInt32(month[0]) + i <= 12)
                        {
                            objSource.Month = Convert.ToInt32(month[0]) + i;
                            objSource.Year = Convert.ToInt32(month[2]);
                        }
                        else
                        {

                            objSource.Month = (Convert.ToInt32(month[0]) + i) - 12;
                            objSource.Year = Convert.ToInt32(month[2]) + 1;
                        }
                        objSource.InsertExpense();
                    }
                }
                else
                {
                    objSource.Month = Convert.ToInt32(obj.DateFrom.Split('/')[0]);
                    objSource.Year = Convert.ToInt32(obj.DateTo.Split('/')[2]);
                    objSource.DateFrom = obj.DateFrom;
                    objSource.DateTo = obj.DateTo;
                    objSource.InsertExpense();
                }
                return Message.Show("Event has been saved successfully.", ShowMessage.messageType.Success);
            }
            catch
            {
                return Message.Show("Invalid.", ShowMessage.messageType.Error);
            }

        }

        [HttpGet]
        [Route("GetAllSouceCodes/{BranchId}")]
        public DataSet GetAllSouceCodes(int BranchId)
        {
            BranchDAL _BranchDAL = new BranchDAL();
            DataSet ds = _BranchDAL.GetAllSouceCodes(BranchId);
            return ds;
        }

        [HttpPost]
        [Route("PostInsBranchSource")]
        public ShowMessage InsBranchSource([FromBody] BranchSource obj)
        {
            ShowMessage message = new ShowMessage();
            Branch objBranch = new Branch { BranchId = Convert.ToInt32(obj.BranchId) };
            int outPut = objBranch.AddCode(Convert.ToInt16(obj.SourceCategoryId), obj.Source, obj.UserId);
            if (outPut > 0)
            {
                return message.Show("Code added successfully", ShowMessage.messageType.Success);
            }
            return message.Show("Invalid", ShowMessage.messageType.Error);
        }
        [HttpGet]
        [Route("GetAllSourceCategory")]
        public DataSet GetAllSourceCategory()
        {
            BranchDAL _BranchDAL = new BranchDAL();
            return _BranchDAL.GetAllSourceCategory();

        }

        [HttpPost]
        [Route("PostInsSouceCategory")]
        public ShowMessage InsSouceCategory([FromBody] BranchSource obj)
        {
            ShowMessage message = new ShowMessage();
            try
            {
                Branch.AddSouceCategory(Convert.ToInt32(obj.BranchId), Convert.ToInt32(obj.SourceCategoryId), obj.ShowOnWeb);
                return message.Show("Category added successfully", ShowMessage.messageType.Success);
            }
            catch (Exception ex)
            {
                return message.Show(ex.Message, ShowMessage.messageType.Error);
            }

        }

        [HttpGet]
        [Route("GetSourceCategoryGroup")]
        public DataSet GetSourceCategoryGroup()
        {
            BranchDAL _BranchDAL = new BranchDAL();
            DataSet ds = _BranchDAL.GetSourceCategoryGroup();
            return ds;
        }

        [HttpPost]
        [Route("PostInsLookupSouceCategory")]
        public ShowMessage InsLookupSouceCategory([FromBody] BranchSource obj)
        {
            ShowMessage message = new ShowMessage();
            try
            {
                Branch.AddLookupSouceCategory(obj.Name, obj.GroupName);
                return message.Show("Source added to the list", ShowMessage.messageType.Success);
            }
            catch (Exception ex)
            {
                return message.Show(ex.Message, ShowMessage.messageType.Error);
            }
        }
        [HttpGet]
        [Route("GetRejectApplicant/{branchId}/{status}/{StartDate}/{enddate}/{SearchByHomeBranch}")]
        public List<Applicant> GetRejectApplicant(int branchId, Nullable<int> status = null, string StartDate = null, string enddate = null, bool SearchByHomeBranch = false)
        {
            List<Applicant> objList = new List<Applicant>();
            Applicant objAp = new Applicant();
            objList = objAp.GetRejectApplicant(branchId, status, StartDate, enddate, SearchByHomeBranch);
            return objList;
        }

        [HttpGet]
        [Route("GetHiredApplicant/{branchId}/{StartDate}/{enddate}/{SearchByHomeBranch}")]
        public DataSet GetHiredApplicant(int branchId, string StartDate = null, string enddate = null, bool SearchByHomeBranch = false)
        {
            JobApplicationDAL _JobApplicationDAL = new JobApplicationDAL();
            return _JobApplicationDAL.GetHiredApplicant(branchId, StartDate, enddate, SearchByHomeBranch);
        }

        [HttpGet]
        [Route("GetHiredApplicantSummary/{branchId}/{StartDate}/{enddate}/{source}/{GroupCheck}/{SearchByHomeBranch}")]
        public List<summary> GetHiredApplicantSummary(int branchId, string StartDate = null, string enddate = null, string source = null, bool GroupCheck = false, bool SearchByHomeBranch = false)
        {
            List<summary> objList = new List<summary>();
            JobApplication objJA = new JobApplication();
            DataTable dt = objJA.GetHiredApplicantSummary(StartDate, enddate, source == "0" ? null : source, branchId, SearchByHomeBranch).Tables[0];
            List<summary> result = new List<summary>();
            result = (from DataRow row in dt.Rows

                      select new summary
                      {
                          Source = row["HowDidYouHear"].ToString(),
                          Status = row["Status"].ToString(),
                          Count = Convert.ToDecimal(row["Count"])

                      }).ToList();

            if (!GroupCheck)
            {
                result = (from z in result
                          group z by z.Status into g

                          select new summary
                          {
                              Status = g.Key,
                              Count = g.Sum(o => o.Count)
                          }).ToList();
                //grdApplicants.Columns[0].Visible = false;
            }
            return result;
        }
        [HttpPost]
        [Route("PostApplicantForTaskDownload")]
        public List<Applicant> PostApplicantForTaskDownload([FromBody]Applicant obj)
        {
            if (obj == null)
            {
                return null;
            }
            Applicant _Applicant = new Applicant();
            try
            {

                DataTable dt = new DataTable();
                int totalApplications = 0;
                obj.SearchByHomeBranch = false;
                if (obj.ChildBranchId > 0)
                {
                    obj.SearchByHomeBranch = true;
                    if (obj.BranchId != obj.ChildBranchId)
                    {

                        obj.BranchId = obj.ChildBranchId;
                    }

                }

                if (!string.IsNullOrEmpty(obj.StartDate))
                {
                    try
                    {
                        obj.StartDate = Convert.ToDateTime(obj.StartDate).ToShortDateString();
                    }
                    catch
                    {
                        obj.StartDate = null;
                    }
                }
                if (!string.IsNullOrEmpty(obj.EndDate))
                {
                    try
                    {
                        obj.EndDate = Convert.ToDateTime(obj.EndDate).ToShortDateString();
                    }
                    catch
                    {
                        obj.EndDate = null;
                    }
                }
                //Search all years
                if (obj.chkShowAllYears)
                {
                    obj.Year = null;
                    dt = _Applicant.fnGetApplicantForTaskDownload(obj);
                    totalApplications = _Applicant.CountApplicationsForTaskDownload(obj);

                }
                else
                {

                    dt = _Applicant.fnGetApplicantForTaskDownload(obj);
                    totalApplications = _Applicant.CountApplicationsForTaskDownload(obj);

                }
                dt.Columns.Add("TotalCount", typeof(System.Int32));
                dt.Columns["TotalCount"].Expression = totalApplications.ToString();
                List<Applicant> lst = SQLHelper.ContructList<Applicant>(dt);
                return lst;
            }
            catch (Exception ex)
            {
                List<Applicant> lst = new List<Applicant>();
                lst[0].Address = ex.Message;
                return lst;
            }
        }

        [HttpGet]
        [Route("GetApplicantAllTaskDocs/{ApplicantId}")]
        public List<ApplicantTask> GetApplicantTask(int ApplicantId)
        {
            try
            {
                ApplicantTask objApplicantTask = new ApplicantTask
                {
                    ApplicantId = ApplicantId
                };
                DataSet dsApplicantTask = new DataSet();
                dsApplicantTask = objApplicantTask.GetApplicantAllTaskDocs();
                List<ApplicantTask> lst = new List<ApplicantTask>();
                lst = SQLHelper.ContructList<ApplicantTask>(dsApplicantTask);
                return lst;

            }
            catch (Exception ex)
            {
                List<ApplicantTask> lst = new List<ApplicantTask>();
                lst[0].Question = ex.Message;
                return lst;
            }
        }

        [HttpGet]
        [Route("GetBatchInterview/{branchId}/{VenueId}/{StartDate}/{JobApplicationId}")]
        public DataSet GetBatchInterview(int branchId, int VenueId, string StartDate, int JobApplicationId)
        {
            JobApplicationDAL _JobApplicationDAL = new JobApplicationDAL();
            return _JobApplicationDAL.GetApplicantForBatchInterview(branchId, VenueId, StartDate, JobApplicationId);
        }

        [HttpGet]
        [Route("GetBindJob/{BranchId}/{VenueDate}")]
        public DataSet GetBindJob(int BranchId, string VenueDate)
        {
            InterviewVenues objVenues = new InterviewVenues();
            DateTime vDate = Convert.ToDateTime(VenueDate);
            DataSet ds = objVenues.fnGetJobByDate(vDate, BranchId);
            // List<JobByVenu> lst = SQLHelper.ContructList<JobByVenu>(dsVenue);
            return ds;
        }
        [HttpGet]
        [Route("GetBindShiftJob/{BranchId}/{JobId}")]
        public DataSet GetBindShiftJob(int BranchId, int JobId)
        {
            InterviewVenues objVenues = new InterviewVenues();
            DataSet ds = objVenues.fnGetShiftRole(JobId, BranchId);
            //List<JobByVenu> lst = SQLHelper.ContructList<JobByVenu>(ds);
            return ds;
        }

        [HttpPost]
        [Route("PostBatchPrint")]
        public string PostBatchPrint([FromBody]BatchPrint[] obj)
        {
            try
            {
                Dictionary<int, string> DictgrdEmp = new Dictionary<int, string>();
                for (int i = 0; i < obj.Length; i++)
                {
                    DictgrdEmp.Add(obj[i].JobApplicationId, obj[i].Data);
                }
                PDFDocs objPdf = new PDFDocs();
                string fileName = obj[0].LoggedInUserId.ToString() + "_" + DateTime.Now.Day.ToString() + "_" + DateTime.Now.Month.ToString() + "_" + DateTime.Now.Year.ToString();
                objPdf.GenerateSelectedApplicantReport(BatchPrintPath + "Batch-" + fileName + ".pdf", Startup.WebSiteURL + "/assets/", DictgrdEmp);
                return ReturnBatchPrintPath + "Batch-" + fileName + ".pdf";
            }
            catch (Exception ex)
            {
                return "";
            }
        }
        [HttpGet]
        [Route("GetApplicantValidate/{ApplicantId}")]
        public List<Validate> GetApplicantValidate(int ApplicantId)
        {
            Validate obj = new Validate();
            List<Validate> lstBGCheck = obj.GetApplicantValidate(ApplicantId);
            return lstBGCheck;
        }

        [HttpGet]
        [Route("GetApplicantRehireDetails/{ApplicantId}")]
        public DataTable GetApplicantRehireDetails(int ApplicantId)
        {
            Applicant obj = new Applicant();
            DataTable dt = obj.GetApplicantRehireDetails(ApplicantId);
            return dt;
        }

        [HttpGet]
        [Route("GetInterviewVenuesById/{InterviewVenueId}")]
        public DataTable GetInterviewVenuesById(int InterviewVenueId)
        {
            Venue objApp = new Venue();
            DataSet ds = objApp.GetInterviewVenuesById(InterviewVenueId);
            return ds.Tables[0];
        }

        [HttpPost]
        [Route("UploadEVerifyDoc")]
        [AllowAnonymous]
        public ShowMessage UploadEVerifyDoc([FromBody]EverifyDoc obj)
        {
            ShowMessage Message = new ShowMessage();
            EverifyDoc eVerify = new EverifyDoc();
            if (obj == null)
            {
                return Message.Show("Invalid.", ShowMessage.messageType.Error);
            }
            string webRootPath = "P://";// _hostingEnvironment.WebRootPath;           
            bool folderExists = System.IO.Directory.Exists(webRootPath + "\\WSRApplicantForm\\" + obj.ApplicantId + "\\");
            if (!folderExists)
                Directory.CreateDirectory(webRootPath + "\\WSRApplicantForm\\" + obj.ApplicantId + "\\");


            if (obj.DocumentPathFront != null && obj.ImageFront != null)
            {
                int temp = 0;
                string AppendFileName = "Front";
                string filename = AppendFileName + '_' + obj.DocumentPathFront.Replace(" ", "_");
                string fileNameWitPath = webRootPath + "\\WSRApplicantForm\\" + obj.ApplicantId + "\\" + filename;
                obj.DocumentPath = filename;
                obj.DocType = AppendFileName;
                obj.ApplicantEverifyDocId = obj.ApplicantEverifyDocIdFront == null ? 0 : Convert.ToInt32(obj.ApplicantEverifyDocIdFront);
                temp = eVerify.InsEVerifyDoc(obj);
                if (temp > 0)
                {
                    using (FileStream fs = new FileStream(fileNameWitPath, FileMode.Create))
                    {

                        using (BinaryWriter bw = new BinaryWriter(fs))
                        {

                            byte[] data = Convert.FromBase64String(obj.ImageFront);
                            bw.Write(data);
                            bw.Close();
                        }

                    }
                }

            }
            if (obj.DocumentPathBack != null && obj.ImageBack != null)
            {
                int temp = 0;
                string AppendFileName = "Back";
                string filename = AppendFileName + '_' + obj.DocumentPathBack.Replace(" ", "_");
                string fileNameWitPath = webRootPath + "\\WSRApplicantForm\\" + obj.ApplicantId + "\\" + filename;
                obj.DocumentPath = filename;
                obj.DocType = AppendFileName;
                obj.ApplicantEverifyDocId = obj.ApplicantEverifyDocIdBack == null ? 0 : Convert.ToInt32(obj.ApplicantEverifyDocIdBack);
                temp = eVerify.InsEVerifyDoc(obj);
                if (temp > 0)
                {
                    using (FileStream fs = new FileStream(fileNameWitPath, FileMode.Create))
                    {

                        using (BinaryWriter bw = new BinaryWriter(fs))
                        {

                            byte[] data = Convert.FromBase64String(obj.ImageBack);
                            bw.Write(data);
                            bw.Close();
                        }

                    }
                }

            }
            if (obj.ImageFront == null && obj.ImageBack == null)
            {
                int temp = eVerify.fnUpEVerifyDoc(obj);
            }
            return Message.Show("File Uploaded Successfully.", ShowMessage.messageType.Success);
        }
        [HttpPost]
        [Route("PostSaveJobPosition")]
        public int SaveJobPosition([FromBody]JobPosition obj)
        {
            int res = 0;
            try
            {
                BranchSetting obj1 = new BranchSetting();
                res = obj1.SaveJobPosition(obj.JobPositionName, obj.PayRate, obj.BranchId, Convert.ToInt32(obj.JobPositionId), obj.JobType, obj.Description, obj.PayrateDescription);
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.Message);
            }
            return res;
        }

        [HttpGet]
        [Route("GetAllPositionByBranchId/{BranchId}")]
        public DataTable GetAllPositionByBranchId(int BranchId)
        {
            BranchSetting obj1 = new BranchSetting();
            DataSet ds = obj1.GetAllPositionByBranchId(BranchId);
            return ds.Tables[0];
        }

        [HttpPost]
        [Route("PostUpdateJobPosition")]
        public int UpdateJobPosition([FromBody]JobPosition obj)
        {
            int res = 0;
            try
            {
                BranchSetting obj1 = new BranchSetting();
                res = obj1.UpdateJobPosition(Convert.ToInt32(obj.JobPositionId), obj.Status);
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.Message);
            }
            return res;
        }
    }
}

public class BatchPrint
{
    public int JobApplicationId { get; set; }
    public string Data { get; set; }
    public int LoggedInUserId { get; set; }
}
