﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Net.Http;
using System.Xml;
using System.Text;
using System.IO;
using HPortalService.DAL;
using System.Data;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using System.Xml.Linq;
using System.Threading;
using System.Xml.Serialization;

namespace HPortalService.Controllers
{

    [Route("api/products")]
    public class ProductsController : Controller
    {
        //string ReturnBatchPrintPath = Startup.ReturnBatchPrintPath;

        //string HPortalResponseValue = @"E:\SVNProjects\Production\HPortalWoodSideService\HPortalService\wwwroot\JDPResponses\";
        //string LogPath = @"E:\SVNProjects\Production\HPortalWoodSideService\HPortalService\wwwroot\ErrorLog\" + DateTime.Now.ToString("MM-dd-yyyy hh-mm") + "-logfile.txt";

        string HPortalResponseValue = @"D:\inetpub\wwwHportalWSRService\wwwroot\JDPResponses\";
        string LogPath = @"D:\Inetpub\wwwHportalWSRService\wwwroot\ErrorLog\" + DateTime.Now.ToString("MM-dd-yyyy hh-mm") + "-logfile.txt";

        [AcceptVerbs("POST")]
        [HttpPost]

        public Task<IActionResult> Post([FromBody] object xmlMessage)
        {
            string fileName = DateTime.Now.ToFileTime().ToString() + ".xml";

            try
            {
                if (xmlMessage == null) return null;

                XDocument doc = (XDocument)xmlMessage;

                //StringWriter stringwriter = new System.IO.StringWriter();


                //    var serializer = new XmlSerializer(xmlMessage.GetType());
                //serializer.Serialize(stringwriter, xmlMessage);




                //var Form = request.Content.ReadAsFormDataAsync().Result;
                //string xml = Form["Status"].ToString();


                string locationPath = HPortalResponseValue + fileName;
                doc.Save(locationPath);// Save Received Response

                // Save Received Response
                //System.IO.File.WriteAllText(HPortalResponseValue + fileName, doc, Encoding.ASCII);
            }

            catch (Exception exx)

            {
                string str = exx.Message.ToString();
                StreamWriter log;
                if (!System.IO.File.Exists(LogPath))

                {
                    log = new StreamWriter(LogPath);
                }
                else
                {
                    log = System.IO.File.AppendText(LogPath);
                }
                log.Close();
                using (StreamWriter writer = new StreamWriter(LogPath, true))
                {
                    writer.WriteLine("xml realted:  " + exx.Message.ToString());
                    writer.Close();
                }

                return null;
            }

            return null;
        }

        public class XDocumentInputFormatter : InputFormatter, IInputFormatter, IApiRequestFormatMetadataProvider
        {
            public XDocumentInputFormatter()
            {
                SupportedMediaTypes.Add("application/xml");
            }

            protected override bool CanReadType(Type type)
            {
                if (type.IsAssignableFrom(typeof(XDocument))) return true;
                return base.CanReadType(type);
            }

            public override async Task<InputFormatterResult> ReadRequestBodyAsync(InputFormatterContext context)
            {
                var xmlDoc = await XDocument.LoadAsync(context.HttpContext.Request.Body, LoadOptions.None, CancellationToken.None);
                return InputFormatterResult.Success(xmlDoc);
            }
        }
    }
}