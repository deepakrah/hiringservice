﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace HPortalService.Controllers
{
    [Produces("application/json")]
    [Route("api/SendEmails")]
    public class SendEmailsController : Controller
    {
        [HttpGet]
        [Route("GetQuestionByBranchId/{BranchId}/{ApplicantId}/{ZipCode}")]
        public void setMailContent(int ApplicantId, string sendOnType, string subject = null, string emailBody = null)
        {
            SendEmails sendEmails = new SendEmails();
            try
            {
                sendEmails.setMailContent(Convert.ToInt32(ApplicantId), sendOnType, subject, emailBody);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}