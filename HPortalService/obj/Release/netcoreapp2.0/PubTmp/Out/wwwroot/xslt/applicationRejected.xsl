<?xml version="1.0" encoding="utf-8"?>
<xsl:transform version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

        <style type="text/css">
          body,td,th {
          font-family: Verdana;
          font-size: 12px;
          }
        </style>
      </head>

      <body>
        <p>
          <center>
            <img src="https://hportalwsr.schedulingsite.com/assets/images/email-header.png" width="300px"/>
          </center>
          <br/>
          <br/>
        </p>
        <p>
          Dear  <xsl:value-of select="UserInfo/UserName" />,
        </p>
        <p>
          Thank you for taking the time to pursue career opportunities with us.
        </p>
        <p>

        </p>
        <p>
          Woodside Properties has received many applications for Event Staff position. After reviewing your application we have decided not to hire you for this position. We will keep your application on file should a position meeting your qualifications become available.
        </p>


        <p>Thanks again for your interest in working for Woodside.</p>

        <p></p>
        <p>Sincerely,</p>
        <p>
          Woodside Human Resources<br/>
          Woodside Properties


        </p>
        <br/>
        Location:<xsl:value-of select="UserInfo/Branch" />

        <br/>
        <p>
            <b>*Please do not respond to this email. Contact the branch you are applying for.</b>
     

        </p>
      </body>
    </html>
  </xsl:template>
</xsl:transform>
