﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:transform version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

        <style type="text/css">
          body,td,th {
          font-family: Verdana;
          font-size: 12px;
          }
        </style>
      </head>

      <body>
        <p>
          <center>
            <img src="https://hportalwsr.schedulingsite.com/assets/images/email-header.png" width="300px"/>
          </center>
          <br/>
          <br/>
        </p>
        <p>
          Dear <xsl:value-of select="UserInfo/UserName" />,
        </p>
        <p>
          Thank you for registering with Woodside Properties. You will find a password below to log back in to our hiring website to complete your application.
        </p>
        <p>
          Your auto generated password is:    <span style="font-size:14px;font-weight:bold">
          <xsl:value-of select="UserInfo/Password" />
        </span>
          <p>
            Please click the link below to proceed with your application:<br/>
            <a href="https://hportalwsr.schedulingsite.com/login">Login to Woodside hiring portal</a>
          </p>
      </p>
        <p>
          Thank you for your interest in Woodside Properties.
        </p>

        <p>Sincerely,</p>

        <p>
          Woodside Human Resources      <br/>
          Woodside Properties
          <br/>
          Location:<xsl:value-of select="UserInfo/Branch" />
        </p>
      </body>
    </html>
  </xsl:template>
</xsl:transform>
