﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:transform version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

        <style type="text/css">
          body,td,th {
          font-family: Verdana;
          font-size: 12px;
          }
        </style>
      </head>

      <body>
        <p>
          <center>
            <img src="https://hportalwsr.schedulingsite.com/assets/images/email-header.png" width="300px"/>
          </center>
          <br/>
          <br/>
        </p>



        <p>
          Dear  <xsl:value-of select="UserInfo/UserName" />,
        </p>
        <p>
          We have received your request for an interview and are pleased to inform you that your request has been accepted.

        </p>
        <p style="margin-bottom:30px;">
          The type of interview we will be conducting is a block interview session, which is easiest explained as a first come first served interview format.  When scheduled for this type of interview session applicants can come any time after the start time and sign in. You will be checked in on a first come first served basis until the interviews have all been completed.

        </p>
        <p>

          <b> Your Interview details are below:</b>  
        </p>
        
       
          Venue: <xsl:value-of select="UserInfo/VenueName" />

     <br/>
          Room #:<xsl:value-of select="UserInfo/RoomNo" />
       
     <br/>
          Interview Date: <xsl:value-of select="UserInfo/InterviewDate" />

        
     <br/> Block Start Time: <xsl:value-of select="UserInfo/StartTime" />
      
     <br/>  Block End Time: <xsl:value-of select="UserInfo/EndTime" />
     
     <br/> Additional Information:  <xsl:value-of select="UserInfo/ParkingInstructions" />
    
        <br/>
        <p>
          This document will serve as your invitation to your interview session. Please print a copy and bring it with you. The bar code below will be used for check in at the interview.
        </p>
       
          <img  width="100" height="100" src="{UserInfo/ApplicationNumber}" alt="bar" />

       

        <p> We look forward to meeting you and are excited to present our employment opportunities.</p>

        
          Thank you<br/>
          Human Resources<br/>
        Woodside Properties
        <br/>
        Location:<xsl:value-of select="UserInfo/Branch" />

        <br/>
        <p>
       
            <b>*Please do not respond to this email. Contact the branch you are applying for.</b>
    

        </p>
      </body>
    </html>
  </xsl:template>
</xsl:transform>
