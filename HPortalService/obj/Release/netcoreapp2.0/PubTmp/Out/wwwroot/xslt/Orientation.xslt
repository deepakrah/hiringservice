﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:transform version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

        <style type="text/css">
          body,td,th {
          font-family: Verdana;
          font-size: 12px;
          }
        </style>
      </head>

      <body>
        <p>
          <center>
            <img src="https://hportalwsr.schedulingsite.com/assets/images/email-header.png" width="300px"/>
          </center>
          <br/>
          <div style="font-size:44px">CONGRATULATIONS!</div> 
          <br/>
        </p>



        <p>
          Dear  <xsl:value-of select="UserInfo/UserName" />,
        </p>
        <p>
          Thank you for your interest in working with Woodside Properties. We would like to invite you to our New Employee Orientation Session (NEO) based on your successful interview. This is required for you to begin working with Woodside. The NEO lasts approximately 3 hours. In the session, we will go over the employee handbook, security license requirements, as well as fill out any additional forms necessary for employment.
        </p>
       
      

        <table>
          <tr>
            <td>
              <p>

                <b>  Your Orientation details are below </b>
              </p>
            </td>
            <td colspan="2">

              <div style="font-size:22">Dress Code is Business Professional</div>
            </td>
            
          </tr>
          <tr>
            <td>
              Venue: <xsl:value-of select="UserInfo/VenueName" />


              <br/>
              Room #:<xsl:value-of select="UserInfo/RoomNo" />

              <br/>
              Orientation Date: <xsl:value-of select="UserInfo/InterviewDate" />


              <br/>
              Start Time: <xsl:value-of select="UserInfo/StartTime" />

              <br/>
              Additional Information:  <xsl:value-of select="UserInfo/ParkingInstructions" />
            </td>
            <td>
              <b>For Men:</b><br/>
              <b>Shirt: </b>Collared Polo, or Dress Shirt<br/>
              <b>Pants: </b> Dress Slacks or Black<br/>
              <b> Shoes: </b>Dress shoes
            </td>
            <td>
              <b>For Women:</b><br/>
              <b>Shirt: </b>Dress Shirt or Blouse<br/>
              <b>Pants: </b> Dress Pants/Skirt<br/>
              <b> Shoes: </b>Dress Shoes

            </td>
          </tr>
          <tr>
            <td>
              
            </td>
            <td colspan="2">
              <div style="font-size:24;color:red"> *NO SNEAKERS, SANDALS, SHORTS, T-SHIRTS OR JEANS*</div>
            </td>
          </tr>
        </table>
       

        <p>
          This document will serve as your invitation to your orientation session. Please print a copy and bring it with you. The bar code below will be used for check in at the interview.
        </p>

        <img  width="100" height="100" src="{UserInfo/ApplicationNumber}" alt="bar" />




        <p> We look forward to meeting you and are excited to present our employment opportunities.</p>
        <br/>
        <p>  Sincerely,</p>
        <br/>
        Human Resources      <br/>
        Woodside Properties
        <br/>
        Location:<xsl:value-of select="UserInfo/Branch" />

        <br/>
        <p>
       
            <b>*Please do not respond to this email. Contact the branch you are applying for.</b>
      

        </p>

      </body>
    </html>
  </xsl:template>
</xsl:transform>
