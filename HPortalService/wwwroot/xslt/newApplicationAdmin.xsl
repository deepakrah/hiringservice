<?xml version="1.0" encoding="utf-8"?>
<xsl:transform version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

        <style type="text/css">
          body,td,th {
          font-family: Verdana;
          font-size: 12px;
          }
        </style>
      </head>

      <body>
        <p>
          <center>
            <img src="https://hportalwsr.schedulingsite.com/assets/images/email-header.png" width="300px"/>
          </center>
          <br/>
          <br/>
        </p>
        <p>
          Dear Recruiter
        </p>
        <p>
          This is to notify that you have received a job application for the position of event staff.
        </p>
        <p>
          Applicant detail is give below.
        </p>
        <div>
          <table>
            <tr>
              <td>Application No</td>
              <td colspan="3">
                <xsl:value-of select="UserInfo/ApplicationNumber" />
              </td>

            </tr>
            <tr>
              <td>First Name</td>
              <td>
                <xsl:value-of select="UserInfo/FirstName" />
              </td>
              <td>Email </td>
              <td>
                <xsl:value-of select="UserInfo/Email" />
              </td>
            </tr>
            <tr>
              <td>Last Name</td>
              <td>
                <xsl:value-of select="UserInfo/LastName" />
              </td>
              <td>Address</td>
              <td>
                <xsl:value-of select="UserInfo/Address" />
              </td>
            </tr>

          </table>
        </div>


        <p>Thank you</p>

        <p>
          Woodside Recruiting Staff
        </p>
      </body>
    </html>
  </xsl:template>
</xsl:transform>
