﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:transform version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

        <style type="text/css">
          body,td,th {
          font-family: Verdana;
          font-size: 12px;
          }
        </style>
      </head>

      <body>
        <p>
          <center>
            <img src="https://hportalwsr.schedulingsite.com/assets/images/email-header.png" width="300px"/>
          </center>
          <br/>
          <br/>
        </p>
        <p>
          Dear <xsl:value-of select="UserInfo/UserName" />,
        </p>
        <p>
          Your Password has been reset.<br/>Your new password is shown below.
        </p>
        <p style="font-size:14px;font-weight:bold">
          Password:  <xsl:value-of select="UserInfo/Password" />
        </p>

        <p>Thank you</p>

        <p>
          Woodside Recruiting Staff
        </p>
        <br/>
        Location:<xsl:value-of select="UserInfo/Branch" />

        <br/>
        <p>
            <b>*Please do not respond to this email. Contact the branch you are applying for.</b>
        

        </p>
      </body>
    </html>
  </xsl:template>
</xsl:transform>
